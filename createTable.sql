-- Client table

CREATE TABLE Client(
    clientID INT NOT NULL,
    phoneNo VARCHAR(255) NOT NULL,
    emailAdd VARCHAR(255) NOT NULL,
    firstnm VARCHAR(255) NOT NULL,
    lastnm VARCHAR(255) NOT NULL,
    dob DATE NOT NULL,
    zipcode INT NOT NULL,
    gender VARCHAR(255) NOT NULL,
    CONSTRAINT chk_gender CHECK(gender = 'male' OR gender = 'female'),
    primary key (clientID),
    unique(emailAdd),
    unique(phoneNo);
 
    
alter table Client add CONSTRAINT
Client_EMAILFORMAT_CHK check 
   ( REGEXP_LIKE(emailAdd, '[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}'));
alter table Client
    add constraint client_zip_ck
    check (regexp_like(zipcode,'^[[:digit:]]{5}-[[:digit:]]{4}$'));

--Agent table

CREATE TABLE Agent(
    agentID INT NOT NULL,
    phoneNo VARCHAR(255) NOT NULL,
    emailAdd VARCHAR(255) NOT NULL,
    firstnm VARCHAR(255) NOT NULL,
    lastnm VARCHAR(255) NOT NULL,
    dob DATE NOT NULL,
    zipcode INT NOT NULL,
    gender VARCHAR(255) NOT NULL,
    CONSTRAINT chk_gender CHECK(gender = 'male' OR gender = 'female'),
    primary key (agentID),
    unique(emailAdd),
    unique(phoneNo));
    
alter table Agent add CONSTRAINT
Agent_EMAILFORMAT_CHK check 
   ( REGEXP_LIKE(emailAdd, '[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+\.[a-zA-Z]{2,4}'));
alter table Agent
    add constraint Agent_zip_ck
    check (regexp_like(zipcode,'^[[:digit:]]{5}-[[:digit:]]{4}$'));

--	Agent_type

CREATE TABLE Agent_type(
Agent_type_id int not null,
Agent_type_name varchar(255) not null,
primary key (Agent_type_id),
unique(Agent_type_name));

--Room_type
CREATE TABLE Room_type(
Room_type_id int not null,
Room_type_name varchar(255) not null,
primary key (Room_type_id),
unique(Room_type_name));

--Amenities
CREATE TABLE Amenities(
Amenity_id int not null,
Amenity_description varchar(255) not null,
primary key (Amenity_id),
unique(Amenity_description));

--	Resort

CREATE TABLE Resort(
Resort_id int not null,
Resort_name varchar(255) not null,
City varchar(255) not null,
Country varchar(255) not null,
Address varchar(255) not null,
phoneNo VARCHAR(255) NOT NULL,
price  VARCHAR(255) NOT NULL,
Roomtype_id REFERENCES Room_type ( Roomtype_id ),
Amenity_id REFERENCES Amenities (Amenity_id ),
primary key(Resort_id),
unique(Resort_name,City,Country,Address),
CONSTRAINT chk_price CHECK(price>0));

-- Resort_Rating
CREATE TABLE Resort_Rating (
Resort_id int not null,
Rate_id int not null,
primary key (Rate_id),
Resort_id references Resort(Resort_id));

--Booking(agentID,clientID,Date_Booking, Resort_id,Arrival_date,Dept_date,Room_type_id)

CREATE TABLE Booking(
agentID references agentID(Agent),
clientID references clientID(Client),
Date_Booking varchar(255) not null,
Resort_id references Resort(Resort_id),
Arrival_date varchar(255) not null,
Dept_date varchar(255) not null,
Roomtype_id REFERENCES Room_type ( Roomtype_id ));




    

