-- Client table

CREATE TABLE Client(
    clientID INT NOT NULL,
    phoneNo VARCHAR(255) NOT NULL,
    emailAdd VARCHAR(255) NOT NULL,
    firstnm VARCHAR(255) NOT NULL,
    lastnm VARCHAR(255) NOT NULL,
    dob DATE NOT NULL,
    zipcode INT NOT NULL,
    gender VARCHAR(255) NOT NULL,
    CONSTRAINT chk_gender CHECK(gender = 'male' OR gender = 'female'),
    primary key (clientID),
    unique(firstnm),
    unique(lastnm));

--Agent table

CREATE TABLE Agent(
    agentID INT NOT NULL,
    phoneNo VARCHAR(255) NOT NULL,
    emailAdd VARCHAR(255) NOT NULL,
    firstnm VARCHAR(255) NOT NULL,
    lastnm VARCHAR(255) NOT NULL,
    dob DATE NOT NULL,
    zipcode INT NOT NULL,
    gender VARCHAR(255) NOT NULL,
    CONSTRAINT chk_gender CHECK(gender = 'male' OR gender = 'female'),
    primary key (agentID),
    unique(firstnm),
    unique(lastnm));

--
    

