/*
 * Author: Subash Luitel
 * Copying this code is strictly prohibited and illigal.
 * Be careful Ni**as.
 */
package CS425Pacakge;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;


/**
 *
 * @author Subash
 */
public class ClientInfo {
    public static void main (String argv[]){
        ClientInfo test = new ClientInfo();
        test.ReadPrint();
    }
    
    
void ReadPrint(){
    try {

          DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse (new File("Client.xml"));
            // normalize text representation
            doc.getDocumentElement ().normalize ();
            System.out.println ("Root element of the doc is " +  doc.getDocumentElement().getNodeName());

            System.out.println("");

            NodeList clientInfoList = doc.getElementsByTagName("Client");
          System.out.println("----------------------------");
 
	for (int temp = 0; temp < clientInfoList.getLength(); temp++) {
 
		Node nNode = clientInfoList.item(temp);
 
		//System.out.println("\nCurrent Tour :" + nNode.getNodeName());
 
		if (nNode.getNodeType() == Node.ELEMENT_NODE) {
 
			Element eElement = (Element) nNode;
 
			System.out.println("First Name : " + eElement.getElementsByTagName("fName").item(0).getTextContent());
			System.out.println("Last Name : " + eElement.getElementsByTagName("lName").item(0).getTextContent());
			System.out.println("Phone Number : " + eElement.getElementsByTagName("Phone").item(0).getTextContent());
			System.out.println("Email Address : " + eElement.getElementsByTagName("email").item(0).getTextContent());
                        System.out.println("Date of Birth : " + eElement.getElementsByTagName("dob").item(0).getTextContent());
                        System.out.println("Gender : " + eElement.getElementsByTagName("gender").item(0).getTextContent());
                        System.out.println("Zip Code : " + eElement.getElementsByTagName("zip").item(0).getTextContent());
                        System.out.println("Agent Identifier : " + eElement.getAttribute("id"));
                        System.out.println("----------------------------");
                }
	}
    }

      
     catch (SAXException e) {
        Exception x = e.getException ();
        ((x == null) ? e : x).printStackTrace ();

        }catch (Throwable t) {
        t.printStackTrace ();
        }
    

    }


}
