INSERT INTO Agent VALUES(1, '7735980474', 'johndoe@gmail.com', 'John', 'Doe', DATE '1990-07-14','60616','male');
INSERT INTO Agent VALUES(2, '7736783232', 'sjobs@gmail.com', 'Steve', 'Jobs', DATE '1955-02-24','60616','male');
INSERT INTO Agent VALUES(3, '7735525521', 'bgates@gmail.com', 'Bill', 'Gates', DATE '1955-10-28','60616','male');
INSERT INTO Agent VALUES(4, '8005422425', 'lpage@gmail.com', 'Larry', 'Page', DATE '1973-03-26','60616','male');
INSERT INTO Agent VALUES(5, '2034452235', 'rbranson@gmail.com', 'Richard', 'Branson', DATE '1950-07-18','60616','male');
INSERT INTO Agent VALUES(6, '7734014144', 'mzuckerherg@gmail.com', 'Mark', 'Zuckerberg', DATE '1984-05-14','60616','male');
INSERT INTO Agent VALUES(7, '7736032452', 'tcook@gmail.com', 'Tim', 'Cook', DATE '1960-11-01','60616','male');
INSERT INTO Agent VALUES(8, '2423623334', 'lellison@gmail.com', 'Larry', 'Ellison', DATE '1944-08-17','60616','male');
INSERT INTO Agent VALUES(9, '7735223005', 'sballmer@gmail.com', 'Steve', 'Balmer', DATE '1956-03-24','60616','male');
INSERT INTO Agent VALUES(10, '7752235234', 'mmayer@gmail.com', 'Marissa', 'Mayer', DATE '1975-05-30','60616','female');


INSERT INTO Client VALUES (11, '1234567890',jsmith@gmail.com','John' ,'Smith', DATE '1973-05-05','60616', 'male');
INSERT INTO Client VALUES (12, 2345624590,'prosen@gmail.com','Peter' ,'Rosen', DATE '1978-12-08','90210', 'male');
INSERT INTO Client VALUES (13, '7734567811', 'acole@gmail.com', 'Anna', 'Cole', DATE '1982-10-01','60684', 'female');

INSERT INTO Client VALUES (14, '7737891012','gcita@gmail.com', 'Gladys', 'Citta', DATE '1943-02-04', '90861', 'female');

INSERT INTO Client VALUES (15, '8323480496', 'ealbiston@gmail.com', 'Edith','Albiston', DATE '1985-05-12', '31082', 'female');

INSERT INTO Client VALUES (16, '5128750921', 'jtang@gmail.com',  'Joe','Tang',DATE '1993-10-19', '60616', 'male');

INSERT INTO Client VALUES (17, '5128006921', 'slee@gmail.com',  'Sam','Lee',DATE '1989-11-23', '57616', 'male');

INSERT INTO Client VALUES (18, '23144671199', 'dcrawford@gmail.com',   'Daria','Crawford', DATE '1992-04-03', '83456', 'female');

INSERT INTO Client VALUES (19, '7731231092', 'flawson@gmail.com',   'Frank','Lawson', DATE '1982-05-04', '80714', 'male');

INSERT INTO Client VALUES (20, '2046691178', 'mellison@gmail.com',   'Miley','Ellison', DATE '1985-09-09', '10456', 'female');