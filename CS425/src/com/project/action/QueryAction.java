package com.project.action;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class QueryAction {
	private final String SUCCESS = "success";
	private final String ERROR = "error";
	
	private List<List<String>> result;

	private Statement stmt;
	
//all variables needed for query
//all query methods
	
	private Connection connect() throws Exception {
		    // Load the JDBC driver
		    Class.forName("oracle.jdbc.driver.OracleDriver");

		    // Create a connection to the database
		    String serverName = "fourier.cs.iit.edu";
		    String portNumber = "1521";
		    String sid = "orcl";
		    String url = "jdbc:oracle:thin:@" + serverName + ":" + portNumber + ":" + sid;
		    String username = "ouzondu";
		    String password = "Anti7Virus";
		    Connection connection = DriverManager.getConnection(url, username, password);
			connection.setAutoCommit(false);
			return connection;
	}
	
	private String query13(){
		result = null;
		try{
			Connection db = connect();
            Statement statement = db.createStatement() ;
            ResultSet rset = statement.executeQuery("SELECT agentID,firstnm,lastnm from Agent where agentID IN (Select agentID From  Booking  b  GROUP BY b.agentID HAVING COUNT(b.Book_id) >= ALL ( Select COUNT(b2.Book_id) FROM Booking b2 GROUP BY b2.agentID)) ") ;
            while(rset.next()){
            	List<String> list = new ArrayList<String>();
            	list.add(rset.getString(1));
            	list.add(rset.getString(2));
            	list.add(rset.getString(3));
            	result.add(list);
            }
		}catch (Exception e){
			result = null;
			return ERROR;
		}
		return SUCCESS;
	}
	
	public List<List<String>> getResult() {
		return result;
	}

	public void setResult(List<List<String>> result) {
		this.result = result;
	}

}
