<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Agent Page</title>
</head>
<body>
	<h2 align="center">Agent Manager</h2>
	<div align="center"></div>

	<div id="maindiv">
		<table width="1012" height="387" border="1">
			<tr class="center">
				<td width="1002" height="73">
					<table width="1003" height="281" border="1">



						<tr>
							<td width="499" height="272">&nbsp;
								<div>
									<strong>Start here from Tables to insert or update or
										remove predefined list: </strong>
								</div>

								<form>
									<label>Table Lookup:</label>
									<ul>
										<li><a href="Agent.jsp"> Agent Table</a></li>
										<li><a href="Client.jsp"> Client Table</a></li>
										<li><a href="Resort.jsp"> Resort Table</a></li>
										<li><a href="Rating.jsp"> Rating Table</a></li>
										<li><a href="Amenity.jsp"> Amenity Table</a></li>
										<li><a href="Booking.jsp"> Booking Table </a></li>
									</ul>

								</form> <%--     <p  id ="q06"><a href="JavaScript:void()">Total revenue from Bookings</a></p> --%>
							<td width="487"><p align="center" id="q13">
									<a href="<s:url action="query13"/>"> Agent who booked the most trip</a>
								</p>


								<p align="center" id="q11">
									<a href="Query11.jsp"> Users who stayed all inclusive last
										3 months</a>
								</p>

								<p align="center" id="q15">
									<a href="Query15.jsp">List of agents and number of clients
										who booked with them</a>
								</p>
						</tr>
						<tr>
						</tr>
					</table>
					<div id="a11">
						<p>
							<strong>Total Hotel Revenue</strong>
						</p>

						<label></label>
						<form id="form1" name="form1" method="post" action="Query6.jsp">
							<label>Agent Name/City <input type="text" name="v1"
								id="value" />
							</label> &nbsp;&nbsp; <label> <input type="submit" name="button2"
								id="button2" value="Search" />
							</label>
							<p>
								<label> <input type="radio" name="radio" id="radio1"
									value="radio1" /> Across All agent
								</label> <label> <input type="radio" name="radio" id="radio2"
									value="radio2" /> City
								</label> <label> <input type="radio" name="radio" id="radio3"
									value="radio3" /> Agent
								</label> <label></label>
							</p>
							<label></label>
						</form>
					</div>



					<div id="a12">
						<p>
							<strong>Find Trips booked by a specific client</strong>
						</p>
						<form id="form4" name="form1" method="post" action="Query4.jsp">
							<label>Client Name <input type="text" name="v2"
								id="value2" />
							</label> &nbsp;&nbsp; <label> <input type="submit" name="button"
								id="button" value="Search" />
							</label>

							<p>
								<label></label>
							</p>
						</form>
					</div>


					<div id="a15">
						<p>
							<strong>Average number of days booked per trip</strong>
						</p>
						<form id="form6" name="form1" method="post"
							action="Query12.jsp?act=a15">
							<label>Client name <input type="text" name="v3"
								id="value2" />
							</label> &nbsp;&nbsp; <label> <input type="submit" name="button"
								id="button" value="Search" />
							</label>
						</form>
					</div>


					<div id="a22">
						<p>
							<strong> Client who booked the most trips in a given
								period</strong>
						</p>
						<form id="form6" name="form1" method="post" action="Query14a.jsp">
							<label>From: <input type="text" name="v4" id="value" />
							</label> <label>To: <input type="text" name="v5" id="value1" />
							</label> &nbsp;&nbsp; <label> <input type="submit" name="button"
								id="button" value="Search" />
							</label>
						</form>
					</div>

					<div id="a23">
						<p>
							<strong> Client who spent the most in a given period</strong>
						</p>
						<form id="form6" name="form1" method="post"
							action="Query14b.jsp?act=a23">
							<label>From: <input type="text" name="v6" id="value3" />
							</label> <label>To: <input type="text" name="v7" id="value1" />
							</label> &nbsp;&nbsp; <label> <input type="submit" name="button"
								id="button" value="Search" />
							</label>
						</form>
					</div>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>