<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Page</title>
</head>
<body>
	<table width="895" height="550" border="1">
		<tr>
			<td width="855" align="left"><div align="center">Client
					Information</div></td>
		</tr>
		<tr>
			<td height="536"><table width="890" height="320" border="1">
					<tr>
						<td width="403">Booking and Resort Information</td>

					</tr>
					<tr>
						<td><p id="q11">
								<a href="JavaScript:void()"> Find booking information</a>
							</p>

							<p id="q22">
								<a href="Query7.jsp?act=a22"> Find resorts which have all
									amenities</a>
							</p>
							<p id="q23">
								<a href="JavaScript:void()">Most popular resorts in various
									countries</a>
							</p>
							<p id="q24">
								<a href="JavaScript:void()">List number of resorts by
									country</a>
							</p>
							<p id="q25">
								<a href="query1.php?act=a25"> Find the Country which has
									highest average Sun Rate</a>
							</p></td>
					</tr>
				</table>

				<div id="a21">
					<p>
						<strong>Resort Sun Rating</strong>
					</p>
					<form id="form2" name="form1" method="post" action="Query5.jsp">
						<label> Resort Name <input type="text" name="v8"
							id="value" />
						</label> &nbsp;&nbsp; <label> <input type="submit" name="button"
							id="button" value="Search" />
						</label>
					</form>
				</div>
				<div id="a24">
					<p>
						<strong>Find Resort by city</strong>
					</p>
					<form id="form3" name="form1" method="post" action="Query8.jsp">
						<label>City <input type="text" name="v9" id="value" />
						</label> &nbsp;&nbsp; <label> <input type="submit" name="button"
							id="button" value="Search" />
						</label>
					</form>
				</div>

				<div id="a25">
					<p>
						<strong>Find Resort by country</strong>
					</p>
					<form id="form3" name="form1" method="post" action="Query8b.jsp">
						<label>Country <input type="text" name="v20" id="value" />
						</label> &nbsp;&nbsp; <label> <input type="submit" name="button"
							id="button" value="Search" />
						</label>
					</form>
				</div>
				<div id="a12">
					<p>
						<strong>Resort Room type price</strong>
					</p>
					<form id="form4" name="form1" method="post"
						action="query..?act=a12">
						<label></label>&nbsp; <label> <input type="submit"
							name="button" id="button" value="Search" />
						</label>
					</form>
				</div>

				<div id="a14">
					<p>
						<strong>Find amenities by string</strong>
					</p>
					<form id="form5" name="form1" method="post" action="Query10.jsp">
						<label>Amenity name <input type="text" name="v10"
							id="value" />
						</label> &nbsp;&nbsp; <label> <input type="submit" name="button"
							id="button" value="Search" />
						</label>
					</form>
				</div>

				<div id="a23">
					<p>
						<strong>Most popular resorts:</strong>
					</p>
					<form id="form6" name="form1" method="post"
						action="query...jsp?act=a23">

						&nbsp;&nbsp; <label></label>
						<p>
							<label> <input type="radio" name="radio" id="radio"
								value="radio1" /> One Sun Rating
							</label> <label> <input type="radio" name="radio" id="radio"
								value="radio2" /> Two Sun Rating
							</label> <label> <input type="radio" name="radio" id="radio"
								value="radio3" /> Three Sun Rating
							</label> <label> <input type="radio" name="radio" id="radio"
								value="radio4" /> All
							</label> &nbsp;&nbsp; <input type="submit" name="button2" id="button2"
								value="Search" />
						</p>
					</form>
				</div>

				<p>&nbsp;</p></td>
		</tr>
	</table>
</body>
</html>