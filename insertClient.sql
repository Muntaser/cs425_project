-- This inserts into the Client table

INSERT INTO Client VALUES(1, '8237450922', 'jscott@gmail.com', 'James', 'Scott', DATE '1994-11-11','60616','male');
INSERT INTO Client VALUES(2, '5522334421', 'apatal@gmail.com', 'Ankhit', 'Patal', DATE '1957-06-13','60616','male');
INSERT INTO Client VALUES(3, '7735525521', 'ljames@gmail.com', 'Lebron', 'James', DATE '1985-10-28','60616','male');
INSERT INTO Client VALUES(4, '8005422425', 'mgrant@gmail.com', 'Melissa', 'Grant', DATE '1976-03-25','60616','female');
INSERT INTO Client VALUES(5, '3452234524', 'xjang@gmail.com', 'Xiao', 'Jang', DATE '1987-06-12','60616','female');
INSERT INTO Client VALUES(6, '5522323441', 'bwilliams@gmail.com', 'Bruce', 'Williams', DATE '1994-03-14','60616','male');
INSERT INTO Client VALUES(7, '4232341234', 'dyoung@gmail.com', 'Darren', 'Young', DATE '1967-12-11','60616','male');
INSERT INTO Client VALUES(8, '2423623334', 'kellison@gmail.com', 'Kerry', 'Ellison', DATE '1955-02-28','60616','female');
INSERT INTO Client VALUES(9, '7732235513', 'aparker@gmail.com', 'Amanda', 'Parker', DATE '1975-09-07','60616','female');
INSERT INTO Client VALUES(10, '4342352352', 'thilder@gmail.com', 'Thomas', 'Hilder', DATE '1956-08-18','60616','male');


