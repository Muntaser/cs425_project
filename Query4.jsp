<%-- 
    Document   : Agent
    Created on : Apr 25, 2013, 4:17:09 AM
    Author     : mkhan12
--%>

<%@ page import="java.sql.*" %>
<% Class.forName("oracle.jdbc.OracleDriver") ; %>


<html>
    <HEAD>
        <TITLE>List of agents and number of clients who have booked with them in descending 
        order</TITLE>
    </HEAD>

    <BODY>
        <H1>List of agents and number of clients who have booked with them in descending 
        order</H1>

        <%
               //String a=session.getAttribute("theValue");
            Connection connection = DriverManager.getConnection(
                "jdbc:oracle:thin:@fourier.cs.iit.edu:1521:orcl", "mkhan12", "sourov345");

                Statement statement = connection.createStatement() ;
                ResultSet resultset = 
                    statement.executeQuery("select  distinct(agentID) ,count(clientID) as Number_of_clients from"
                        + " Booking group by(agentID) order by Number_of_clients "+session.getAttribute("theValue")
                        ) ; 
        %>

       <TABLE BORDER="1">
            <TR>
                <TH>Agent ID</TH>
                <TH>Client Number</TH>
               
            </TR>
            <% while(resultset.next()){ %>
            <TR>
                <TD> <%= resultset.getString(1) %></td>
                <TD> <%= resultset.getString(2) %></td>
               
            </TR>
            <% } %>
        </TABLE>
    </BODY>
</html>
