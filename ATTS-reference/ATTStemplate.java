/**
 * ATTSaddAirSales class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSaddAirSales extends HttpServlet
{
 private DBFill dbFill = null; // ATTSsales
 private DBFill dbFill2= null; // ATTSemployee
 private DBFill dbFill3= null; // ATTSairline
 private DBFill dbFill4= null; // ATTScompany
 private String dbName = "ATTSsales";
 private String dbName2= "ATTSemployee";
 private String dbName3= "ATTSairline";
 private String dbName4= "ATTScompany";
 private PrintWriter out = null;
 private Closure cls = null;
 private ATTSutility util = null;

/**
 * init's method is to initialize the servlet
 * @param none
 * @return none
 */
 public void init() throws ServletException
 {
  dbFill = new DBFill();
  util = new ATTSutility();
 }

/**
 * destroy's method is to end the life-cycle of the servlet
 * @param none
 * @return none
 */
 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
 }

/**
 * doGet's method is to process GET method from user
 * @param HttpServletRequest req
 *        HttpServletResponse res
 * @return none
 */
 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 }

/**
 * doPost's method is to process POST method from user
 * @param HttpServletRequest req contains user request
 *        HttpServletResponse res contains user respond
 * @return none
 */
 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS Add Air Sales</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<BR></BR><H1>ATTS Add Air Sales</H1><HR></HR><BR></BR>"); 
  String view = req.getParameter("view");
  try
  {
   if(!dbFill.makeInitialConnection(dbName3,out))
    throw new SQLException("<BR>Failure to make initial connection to "+dbName3+"</BR>");
   if(view.compareToIgnoreCase("DISPLAY")==0)
    createForm();
   else if(view.compareToIgnoreCase("PROCESS")==0)
   {
    if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException("<BR>Failure to make initial connection to "+dbName+"</BR>");
    if(!dbFill.makeInitialConnection(dbName2,out))
     throw new SQLException("<BR>Failure to make initial connection to "+dbName2+"</BR>");
    if(!dbFill.makeInitialConnection(dbName4,out))
     throw new SQLException("<BR>Failure to make initial connection to "+dbName4+"</BR>");
    process(req);
   }
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
   view = null;
  }  
 } // end of doPost 

/**
 *
 *
 *
 */
 public void createForm() throws SQLException
 {
  

 }

/**
 *
 *
 *
 */
 public void process(HttpServletRequest req) throws SQLException
 {


 }
 
} // end of ATTSaddAirSales

