create table airline
(
 airlineID TINYINT UNSIGNED AUTO_INCREMENT NOT NULL,
 airlineName VARCHAR(32) NOT NULL DEFAULT ' ',
 airlineAbbr CHAR(3) NOT NULL DEFAULT ' ',
 airlineAgentNumber VARCHAR(32) NOT NULL DEFAULT ' ',
 airlineAgentExpire DATE NOT NULL DEFAULT '0000-00-00',
 airlineBNote INT UNSIGNED NOT NULL DEFAULT '0',
 airlineValid ENUM('Y','N') NOT NULL DEFAULT 'Y',
 PRIMARY KEY(airlineID,airlineAbbr)
);

create table airlineInfo
(
 airlineInfoKey TINYINT UNSIGNED AUTO_INCREMENT NOT NULL, 
 airlineAdd VARCHAR(48) NOT NULL DEFAULT ' ', 
 airlineAddCity INT UNSIGNED NOT NULL DEFAULT '0',
 airlineOfID TINYINT UNSIGNED NOT NULL DEFAULT '0',
 PRIMARY KEY(airlineInfoKey,airlineOfID)
);

create table airlinePhone
(
 airlinePhoneKey SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL,
 airlinePhoneNumber VARCHAR(14) NOT NULL DEFAULT ' ',
 airlineFaxNumber VARCHAR(14) NOT NULL DEFAULT ' ',
 airlineOfID TINYINT UNSIGNED NOT NULL DEFAULT '0',
 airlineInfoAddKey TINYINT UNSIGNED NOT NULL DEFAULT '0',
 PRIMARY KEY(airlinePhoneKey,airlineInfoAddKey) 
);

create table airlineSmallNote
(
 airlineSmallNoteKey INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
 smallNote VARCHAR(32) NOT NULL DEFAULT ' '
);

create table airlineMediumNote
(
 airlineMediumNoteKey INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
 mediumNote VARCHAR(64) NOT NULL DEFAULT ' '
);

create table airlineBigNote
(
 airlineBigNoteKey INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
 bigNote VARCHAR(128) NOT NULL DEFAULT ' '
);

create table airlineCity
(
 cityKey INT UNSIGNED NOT NULL AUTO_INCREMENT,
 cityName VARCHAR(16) NOT NULL DEFAULT ' ',
 cityAbbr CHAR(3) NOT NULL DEFAULT ' ',
 cityCountry VARCHAR(24) NOT NULL DEFAULT 'INDONESIA',
 PRIMARY KEY(cityKey,cityName,cityAbbr)
);

create table airlineRoute
(
 routeKey INT UNSIGNED NOT NULL AUTO_INCREMENT, 
 routeOrig CHAR(3) NOT NULL DEFAULT ' ',
 routeDest CHAR(3) NOT NULL DEFAULT ' ',
 routeViaSN VARCHAR(16) NOT NULL DEFAULT ' ', 
 routeAirlineID TINYINT UNSIGNED NOT NULL DEFAULT '0',
 routeFlightNum VARCHAR(16) NOT NULL DEFAULT ' ',
 routePlaneType VARCHAR(16) NOT NULL DEFAULT ' ',
 routeDTKey INT UNSIGNED NOT NULL DEFAULT '0',
 routeMNote INT UNSIGNED NOT NULL DEFAULT '0',
 routeTerminal VARCHAR(3) NOT NULL DEFAULT ' ',
 routeValid ENUM('Y','N') NOT NULL DEFAULT 'Y',
 PRIMARY KEY(routeKey,routeAirlineID)  
);

create table airlineRouteClass
(
 classKey INT UNSIGNED NOT NULL AUTO_INCREMENT,
 classRouteKey INT UNSIGNED NOT NULL NOT NULL DEFAULT '0',
 classCode VARCHAR(2) NOT NULL DEFAULT 'Y',
 classCodePAX CHAR(1) NOT NULL DEFAULT 'A',
 classPriceCode VARCHAR(3) NOT NULL DEFAULT 'REG',
 classPriceBegin DATE NOT NULL DEFAULT '0000-00-00',
 classPriceEnd DATE NOT NULL DEFAULT '0000-00-00',
 classPriceEndUFN ENUM('Y','N') NOT NULL DEFAULT 'Y',
 classPublishFare DOUBLE(12,2) NOT NULL DEFAULT '0.00',
 classSellingFare DOUBLE(12,2) NOT NULL DEFAULT '0.00',
 classPPN DOUBLE(12,2) NOT NULL DEFAULT '0.00',
 classNet DOUBLE(12,2) NOT NULL DEFAULT '0.00',
 classPAXPaid DOUBLE(12,2) NOT NULL DEFAULT '0.00',
 classPAXPaidRT DOUBLE(12,2) NOT NULL DEFAULT '0.00', 
 classCurrencyKey TINYINT UNSIGNED NOT NULL DEFAULT '1',
 classInput INT UNSIGNED NOT NULL DEFAULT '0',
 classInputDate DATE NOT NULL DEFAULT '0000-00-00',
 classInputTime TIME NOT NULL DEFAULT '00:00:00', 
 classMNote INT UNSIGNED NOT NULL DEFAULT '0',
 PRIMARY KEY(classKey,classRouteKey,classCode,classCodePAX) 
);

create table airlineExtraInfo
(
 extraInfoKey INT UNSIGNED NOT NULL AUTO_INCREMENT,
 extraInfoRouteClassKey INT UNSIGNED NOT NULL DEFAULT '0',
 extraInfoTourCode VARCHAR(16) NOT NULL DEFAULT ' ',
 extraInfoTktDesignator VARCHAR(16) NOT NULL DEFAULT ' ',
 extraInfoFareType VARCHAR(16) NOT NULL DEFAULT ' ', 
 extraInfoSNote INT UNSIGNED NOT NULL DEFAULT '0',
 PRIMARY KEY(extraInfoKey,extraInfoRouteClassKey)
);

create table airlineRouteDT
(
 airRouteDTKey INT UNSIGNED NOT NULL AUTO_INCREMENT,
 airRouteDay VARCHAR(7) NOT NULL DEFAULT '1234567',
 airRouteETD VARCHAR(48) NOT NULL DEFAULT ' ',
 airRouteETA VARCHAR(48) NOT NULL DEFAULT ' ',
 airRouteOfAirline TINYINT UNSIGNED NOT NULL,
 PRIMARY KEY(airRouteDTKey,airRouteOfAirline)
);

create table airlineReportDT
(
 reportKey INT UNSIGNED NOT NULL AUTO_INCREMENT,
 reportMadeBy INT UNSIGNED NOT NULL DEFAULT '0',
 reportAuthBy INT UNSIGNED NOT NULL DEFAULT '0',
 reportDate DATE NOT NULL DEFAULT '0000-00-00',
 reportTime TIME NOT NULL DEFAULT '00:00:00',
 reportToAirline TINYINT UNSIGNED NOT NULL DEFAULT '0', 
 reportPerBegin DATE NOT NULL DEFAULT '0000-00-00',
 reportPerEnd DATE NOT NULL DEFAULT '0000-00-00',
 reportTotalAmount DOUBLE(12,2) NOT NULL DEFAULT '0.00',
 reportTotalTicket SMALLINT UNSIGNED NOT NULL DEFAULT '0',
 reportCurrency TINYINT UNSIGNED NOT NULL DEFAULT '1',
 reportBorderelSNote INT UNSIGNED NOT NULL DEFAULT '0',
 reportRemarkMNote INT UNSIGNED NOT NULL DEFAULT '0',
 PRIMARY KEY(reportKey,reportToAirline)
);

create table airlineCurrency
(
 currencyKey TINYINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
 currencyAbbr CHAR(3) NOT NULL DEFAULT ' ',
 currencyDescSNote INT UNSIGNED NOT NULL DEFAULT '0'
);

create table airlineExchangeRate
(
 exchangeKey INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
 exchangeFrom TINYINT UNSIGNED NOT NULL DEFAULT '0',
 exchangeTo TINYINT UNSIGNED NOT NULL DEFAULT '0',
 exchangeRate DOUBLE(8,2) NOT NULL DEFAULT '0.00',
 exchangeInputBy INT UNSIGNED NOT NULL DEFAULT '0',
 exchangeChangeDate DATE NOT NULL DEFAULT '0000-00-00',
 exchangeChangeTime TIME NOT NULL DEFAULT '00:00:00', 
 exchangeMNote INT UNSIGNED NOT NULL DEFAULT '0'
);

create table airlineStockTicket
(
 stockTktKey INT UNSIGNED NOT NULL AUTO_INCREMENT,
 stockOfAirline TINYINT UNSIGNED NOT NULL DEFAULT '0',
 stockAmount SMALLINT(3) UNSIGNED NOT NULL DEFAULT '0',
 stockOWBeginNumber VARCHAR(24) NOT NULL DEFAULT ' ',
 stockOWEndNumber VARCHAR(24) NOT NULL DEFAULT ' ',
 stockRTBeginNumber VARCHAR(24) NOT NULL DEFAULT ' ',
 stockRTEndNumber VARCHAR(24) NOT NULL DEFAULT ' ',
 stockTakeBy INT UNSIGNED NOT NULL DEFAULT '0',
 stockCheckBy INT UNSIGNED NOT NULL DEFAULT '0',
 stockTktDate DATE NOT NULL DEFAULT '0000-00-00', 
 stockTktTime TIME NOT NULL DEFAULT '00:00:00',
 stockTktBNote INT UNSIGNED NOT NULL DEFAULT '0',
 PRIMARY KEY(stockTktKey,stockOfAirline)
);

create table airlinePPN
(
 airlinePPNKey TINYINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
 airlinePPNPercentage DOUBLE(4,2) NOT NULL DEFAULT '0.00',
 airlinePPNInputBy INT UNSIGNED NOT NULL DEFAULT '0',
 airlinePPNDate DATE NOT NULL DEFAULT '0000-00-00'
);

create table airlineIWJR 
(
 airlineIWJRKey TINYINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
 airlineIWJRAmount DOUBLE(8,2) NOT NULL DEFAULT '0.00',
 airlineIWJRInputBy INT UNSIGNED NOT NULL DEFAULT '0',
 airlineIWJRDate DATE NOT NULL DEFAULT '0000-00-00' 
);

create table airlineRules
(
 airlineRulesKey INT UNSIGNED NOT NULL AUTO_INCREMENT, 
 airlineRulesOf TINYINT UNSIGNED NOT NULL DEFAULT '0',
 airlineCancelTime TINYINT NOT NULL DEFAULT '24',
 airlineCancelFee DOUBLE(7,2) NOT NULL DEFAULT '0.00',
 airlineCHDMaxAge TINYINT UNSIGNED NOT NULL DEFAULT '144',
 airlineINFMaxAge TINYINT UNSIGNED NOT NULL DEFAULT '12',
 airlineCHDCharge DOUBLE(5,2) NOT NULL DEFAULT '0.75',
 airlineINFCharge DOUBLE(5,2) NOT NULL DEFAULT '0.10',
 airlineRulesMNote INT UNSIGNED NOT NULL DEFAULT '0', 
 airlineRulesInputBy INT UNSIGNED NOT NULL DEFAULT '0',
 airlineRulesChangeDate DATE NOT NULL DEFAULT '0000-00-00',
 PRIMARY KEY(airlineRulesKey,airlineRulesOf)
);

create table airlineClassRules
(
 classRulesKey INT UNSIGNED NOT NULL AUTO_INCREMENT,
 classRulesRouteKey INT UNSIGNED NOT NULL DEFAULT '0',
 classRefundable ENUM('Y','N') NOT NULL DEFAULT 'Y',
 classReroutable ENUM('Y','N') NOT NULL DEFAULT 'N',
 classEndorsable ENUM('Y','N') NOT NULL DEFAULT 'N',
 classExtendable ENUM('Y','N') NOT NULL DEFAULT 'N',
 classValidity VARCHAR(10) NOT NULL DEFAULT '1 MONTH',
 classAllowedBag TINYINT UNSIGNED NOT NULL DEFAULT '35',
 classExtraBagFee DOUBLE(9,2) NOT NULL DEFAULT '0.00',
 classCancellationTime TINYINT UNSIGNED NOT NULL DEFAULT '24',
 classCancellationFee DOUBLE(9,2) NOT NULL DEFAULT '0.00',
 classRefundFeeRules VARCHAR(32) NOT NULL DEFAULT ' ',
 classRulesSNote INT UNSIGNED NOT NULL DEFAULT '0',
 PRIMARY KEY(classRulesKey,classRulesRouteKey)
);

create table airlineBlockSeats
(
 blockSeatsKey INT UNSIGNED NOT NULL AUTO_INCREMENT,
 blockSeatsOfAirline TINYINT UNSIGNED NOT NULL DEFAULT '0',
 blockSeatsBeginDate DATE NOT NULL DEFAULT '0000-00-00',
 blockSeatsEndDate DATE NOT NULL DEFAULT '0000-00-00',
 blockSeatsTotal TINYINT UNSIGNED NOT NULL DEFAULT '0',
 blockSeatsRouteKey INT UNSIGNED NOT NULL DEFAULT '0',
 blockSeatsRequestBy INT UNSIGNED NOT NULL DEFAULT '0',
 PRIMARY KEY(blockSeatsKey,blockSeatsOfAirline,blockSeatsRouteKey)  
);

create table unusedAirlineTable
(
 unusedID INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
 tableName VARCHAR(32) NOT NULL DEFAULT ' ',
 tableUnusedKey VARCHAR(200) NOT NULL DEFAULT ' ',
 tableUnusedTotal TINYINT NOT NULL DEFAULT '0'
);
