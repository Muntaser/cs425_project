drop table finance;
drop table financePayable;
drop table financeReceivable;
drop table financeBank;
drop table financeLocation;
drop table financeContact;
drop table financeInventoryAsset;
drop table financePurchaseOrder;
drop table financeTransaction;
drop table financeTransactionPayType;
drop table financeSubCategory;
drop table financeBankBalance;
drop table financeCashFlow;
drop table financeState;
