create table airlineRules
(
 airlineRulesKey INT UNSIGNED NOT NULL AUTO_INCREMENT,
 airlineRulesOf TINYINT UNSIGNED NOT NULL,
 airlineCancelTime VARCHAR(32) NOT NULL DEFAULT ' ',
 airlineCancelFee DOUBLE(7,2) NOT NULL DEFAULT '0.00',
 airlineVoidFee DOUBLE(7,2) NOT NULL DEFAULT '0.00',
 airlineCHDMaxAge VARCHAR(16) NOT NULL DEFAULT '2 TAHUN',
 airlineINFMaxAge VARCHAR(16) NOT NULL DEFAULT '16 BULAN',
 airlineCHDCharge DOUBLE(4,2) NOT NULL DEFAULT '0.75',
 airlineINFCharge DOUBLE(4,2) NOT NULL DEFAULT '0.10',
 airlineRulesMNote INT UNSIGNED NOT NULL DEFAULT '0',
 PRIMARY KEY(airlineRulesKey,airlineRulesOf) 
);

create table airlineExtraInfo
(
 extraInfoKey INT UNSIGNED NOT NULL AUTO_INCREMENT,
 extraInfoRouteClassKey INT UNSIGNED NOT NULL DEFAULT '0',
 extraInfoTourCode VARCHAR(16) NOT NULL DEFAULT ' ',
 extraInfoTktDesignator VARCHAR(16) NOT NULL DEFAULT ' ',
 extraInfoFareType VARCHAR(16) NOT NULL DEFAULT ' ',
 extraInfoSNote INT UNSIGNED NOT NULL DEFAULT '0',
 PRIMARY KEY(extraInfoKey,extraInfoRouteClassKey)
);
 
create table airlineRouteClass
(
 classKey INT UNSIGNED NOT NULL AUTO_INCREMENT,
 classRouteKey INT UNSIGNED NOT NULL DEFAULT '0',
 classCode VARCHAR(2) NOT NULL DEFAULT 'Y',
 classCodePAX CHAR(1) NOT NULL DEFAULT 'A',
 classPriceCode VARCHAR(3) NOT NULL DEFAULT 'REG',
 classPriceBegin DATE NOT NULL DEFAULT '0000-00-00',
 classPriceEnd DATE NOT NULL DEFAULT '0000-00-00',
 classPriceEndUFN ENUM('Y','N') NOT NULL DEFAULT 'Y',
 classPublishFare DOUBLE(12,2) NOT NULL DEFAULT '0.00',
 classSellingFare DOUBLE(12,2) NOT NULL DEFAULT '0.00',
 classPPN DOUBLE(12,2) NOT NULL DEFAULT '0.00',
 classNET DOUBLE(12,2) NOT NULL DEFAULT '0.00',
 classPAXPaid DOUBLE(12,2) NOT NULL DEFAULT '0.00', 
 classPAXPaidRT DOUBLE(12,2) NOT NULL DEFAULT '0.00',
 classCurrencyKey INT UNSIGNED NOT NULL DEFAULT '0',
 classInput INT UNSIGNED NOT NULL DEFAULT '0',
 classInputDate DATE NOT NULL DEFAULT 'CURRENT_DATE',
 classInputTime TIME NOT NULL DEFAULT 'CURRENT_TIME',
 classMNote INT UNSIGNED NOT NULL DEFAULT '0',
 PRIMARY KEY(classKey,classRouteKey,classCode,classCodePAX) 
);

create table airlineClassRules
(
 classRulesKey INT UNSIGNED NOT NULL AUTO_INCREMENT, 
 classRulesRouteKey INT UNSIGNED NOT NULL DEFAULT '0',
 classRefundable ENUM('Y','N') NOT NULL DEFAULT 'Y',
 classReroutable ENUM('Y','N') NOT NULL DEFAULT 'N',
 classEndorsable ENUM('Y','N') NOT NULL DEFAULT 'N',
 classExtendable ENUM('Y','N') NOT NULL DEFAULT 'N',
 classValidity VARCHAR(10) NOT NULL DEFAULT '1 MONTH',
 classAllowedBag TINYINT UNSIGNED NOT NULL DEFAULT '35',
 classExtraBagFee DOUBLE(9,2) NOT NULL DEFAULT '0.00',
 classCancellationTime TINYINT UNSIGNED NOT NULL DEFAULT '24',
 classCancellationFee DOUBLE(9,2) NOT NULL DEFAULT '0.00',  
 classRefundFeeRules VARCHAR(32) NOT NULL DEFAULT ' ',
 classRulesSNote INT UNSIGNED NOT NULL DEFAULT '0',
 PRIMARY KEY(classRulesKey,classRulesRouteKey)
);
