create table employee
(
 employeeID INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
 employeeName VARCHAR(32) NOT NULL DEFAULT ' ',
 employeePosition ENUM('EXECUTIVE','IT_REPORT','ACCOUNTANT','SUPERVISOR',
 'TICKETING','MESSENGER','CASHIER','DRIVER','OFFICE_BOY') NOT NULL DEFAULT 'TICKETING',
 employeeOfOffice SMALLINT UNSIGNED NOT NULL DEFAULT '0',
 employeePassKey CHAR(32) NOT NULL DEFAULT ' ',
 employeePassword CHAR(32) NOT NULL DEFAULT ' ',
 employeeValid ENUM('Y','N') NOT NULL DEFAULT 'N',
 employeeAccessLvl ENUM('1','2','3') NOT NULL DEFAULT '3'
);

create table employeeInfo
(
 empInfoKey INT UNSIGNED NOT NULL AUTO_INCREMENT,
 empInfoIDCard VARCHAR(32) NOT NULL DEFAULT ' ',
 empInfoIDExpire DATE NOT NULL DEFAULT '0000-00-00', 
 empInfoBeginDate DATE NOT NULL DEFAULT '0000-00-00',
 empInfoEndDate DATE NOT NULL DEFAULT '0000-00-00',
 empInfoBirthDate DATE NOT NULL DEFAULT '0000-00-00',
 empInfoHometown VARCHAR(16) NOT NULL DEFAULT ' ',
 empInfoVacationLeft TINYINT UNSIGNED NOT NULL DEFAULT '14',
 empInfoSickDayLeft TINYINT UNSIGNED NOT NULL DEFAULT '7',
 empInfoDayOff VARCHAR(32) NOT NULL DEFAULT ' ',
 empInfoTerminateNote VARCHAR(64) NOT NULL DEFAULT ' ',
 empInfoOfID INT UNSIGNED NOT NULL,
 PRIMARY KEY(empInfoKey,empInfoOfID)
);

create table employeeAddress
(
 empAddressKey INT UNSIGNED NOT NULL AUTO_INCREMENT,
 empHomeAdd VARCHAR(32) NOT NULL DEFAULT ' ',
 empHomeAddCity VARCHAR(16) NOT NULL DEFAULT ' ',
 empHomeAddZip CHAR(5) NOT NULL DEFAULT ' ',
 empAddOfID INT UNSIGNED NOT NULL,
 PRIMARY KEY(empAddressKey,empAddOfID) 
);

create table employeeContact
(
 empContactKey INT UNSIGNED NOT NULL AUTO_INCREMENT,
 empContactHomePhone CHAR(14) NOT NULL DEFAULT ' ',
 empContactHandPhone VARCHAR(14) NOT NULL DEFAULT ' ',
 empContactPersonName VARCHAR(16) NOT NULL DEFAULT ' ',
 empContactPersonNumber VARCHAR(14) NOT NULL DEFAULT ' ',
 empContactPersonNotes INT UNSIGNED NOT NULL DEFAULT '0',
 empContactOfID INT UNSIGNED NOT NULL,
 PRIMARY KEY(empContactKey,empContactOfID) 
);

create table employeeNotes
(
 employeeNotesKey INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
 empNotes VARCHAR(64) NOT NULL DEFAULT ' '
);

create table employeeLocation
(
 empLocationKey INT UNSIGNED NOT NULL AUTO_INCREMENT,
 empDeliverOrder INT UNSIGNED NOT NULL DEFAULT '0',
 empLocationAdd VARCHAR(32) NOT NULL DEFAULT ' ',
 empLocGo TIME NOT NULL DEFAULT '00:00:00',
 empLocBack TIME NOT NULL DEFAULT '00:00:00',
 empLocDate DATE NOT NULL DEFAULT '0000-00-00',
 empLocOfID INT UNSIGNED NOT NULL,
 empLocNote INT UNSIGNED NOT NULL DEFAULT '0',
 PRIMARY KEY(empLocationKey,empDeliverOrder,empLocOfID)  
);

create table employeeClock
(
 empClockKey INT UNSIGNED NOT NULL AUTO_INCREMENT,
 empClockIn TIME NOT NULL DEFAULT '00:00:00',
 empClockOut TIME NOT NULL DEFAULT '00:00:00',
 empClockDate DATE NOT NULL DEFAULT '0000-00-00',
 empClockOfID INT UNSIGNED NOT NULL,
 PRIMARY KEY(empClockKey,empClockOfID) 
);

create table employeeViolation
(
 empViolationKey INT UNSIGNED NOT NULL AUTO_INCREMENT,
 empViolationDesc VARCHAR(100) NOT NULL DEFAULT ' ',
 empViolationGiverID INT UNSIGNED NOT NULL,
 empViolationRecID INT UNSIGNED NOT NULL,
 empViolationDate DATE NOT NULL DEFAULT '0000-00-00',
 empViolationTime TIME NOT NULL DEFAULT '00:00:00',
 PRIMARY KEY(empViolationKey,empViolationGiverID,empViolationRecID) 
);

create table employeeSalary
(
 empSalaryKey INT UNSIGNED NOT NULL AUTO_INCREMENT,
 empSalaryDate DATE NOT NULL DEFAULT '0000-00-00',
 empSalary FLOAT(10,2) UNSIGNED NOT NULL,
 empSalaryValid ENUM('Y','N') NOT NULL DEFAULT 'Y',
 empSalaryAuthByID INT UNSIGNED NOT NULL,
 empSalaryOfID INT UNSIGNED NOT NULL,
 PRIMARY KEY(empSalaryKey,empSalaryOfID)
);

create table employeeAbsence
(
 empAbsenceKey INT UNSIGNED NOT NULL AUTO_INCREMENT,
 empAbsenceBegin DATE NOT NULL DEFAULT '0000-00-00',
 empAbsenceEnd   DATE NOT NULL DEFAULT '0000-00-00',
 empAbsenceReq   DATE NOT NULL DEFAULT '0000-00-00',
 empAbsenceType ENUM('SICK','DAYS_OFF','UNKNOWN') NOT NULL DEFAULT 'UNKNOWN',
 empAbsencePermitted ENUM('Y','N') NOT NULL DEFAULT 'N',
 empPermittedByID INT UNSIGNED NOT NULL DEFAULT '0',
 empAbsenceOfID INT UNSIGNED NOT NULL,
 empAbsenceReason VARCHAR(32) NOT NULL DEFAULT ' ', 
 PRIMARY KEY(empAbsenceKey,empPermittedByID,empAbsenceOfID)
);

create table employeeEvaluation
(
 empEvaluationKey INT UNSIGNED NOT NULL AUTO_INCREMENT,
 empScore TINYINT UNSIGNED NOT NULL DEFAULT '0',
 empEvalByID INT UNSIGNED NOT NULL,
 empEvalOfID INT UNSIGNED NOT NULL,
 empEvalNote INT UNSIGNED NOT NULL DEFAULT '0',
 empEvalDate DATE NOT NULL DEFAULT '0000-00-00',
 empEvalTime TIME NOT NULL DEFAULT '00:00:00',
 PRIMARY KEY(empEvaluationKey,empEvalOfID) 
);
