create table finance
(
 financeKey INT UNSIGNED NOT NULL AUTO_INCREMENT,
 financeOfCompany TINYINT UNSIGNED NOT NULL DEFAULT '0',
 financeOfOffice INT UNSIGNED NOT NULL DEFAULT '0',
 financePeriodBegin DATE NOT NULL DEFAULT '0000-00-00',
 financePeriodEnd DATE NOT NULL DEFAULT '0000-00-00',
 financeInputBy INT UNSIGNED NOT NULL DEFAULT '0',
 financeInputDt DATE NOT NULL DEFAULT '0000-00-00',
 financeInputTm TIME NOT NULL DEFAULT '00:00:00',
 PRIMARY KEY(financeKey,financeInputBy)
);

create table financePayable
(
 payableKey INT UNSIGNED NOT NULL AUTO_INCREMENT,
 payToCustID INT UNSIGNED NOT NULL DEFAULT '0', 
 payToCustName VARCHAR(32) NOT NULL DEFAULT ' ',
 payToCustLoc INT UNSIGNED NOT NULL DEFAULT '0',
 payToCustContact INT UNSIGNED NOT NULL DEFAULT '0',
 payTransactionKey INT UNSIGNED NOT NULL DEFAULT '0',
 payExpect DATE NOT NULL DEFAULT '0000-00-00',
 payFinish ENUM('Y','N') NOT NULL DEFAULT 'N',
 payHandleByID INT UNSIGNED NOT NULL DEFAULT '0',
 payUpdateDate DATE NOT NULL DEFAULT '0000-00-00',
 payUpdateTime TIME NOT NULL DEFAULT '00:00:00',
 PRIMARY KEY(payableKey,payHandleByID)
);

create table financeReceivable
(
 receivableKey INT UNSIGNED NOT NULL AUTO_INCREMENT,
 recFromCustID INT UNSIGNED NOT NULL DEFAULT '0',
 recFromCustName VARCHAR(32) NOT NULL DEFAULT ' ',
 recFromCustLoc INT UNSIGNED NOT NULL DEFAULT '0',
 recFromCustContact INT UNSIGNED NOT NULL DEFAULT '0',
 recTransactionKey INT UNSIGNED NOT NULL DEFAULT '0',
 recExpect DATE NOT NULL DEFAULT '0000-00-00',
 recFinish ENUM('Y','N') NOT NULL DEFAULT 'N',
 recHandleByID INT UNSIGNED NOT NULL DEFAULT '0',
 recUpdateDate DATE NOT NULL DEFAULT '0000-00-00',
 recUpdateTime TIME NOT NULL DEFAULT '00:00:00',
 PRIMARY KEY(receivableKey,recHandleByID)
);

create table financeBank 
(
 financeBankKey TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
 financeBankName VARCHAR(24) NOT NULL DEFAULT ' ',
 financeBankAcctName VARCHAR(24) NOT NULL DEFAULT ' ',
 financeBankLocKey INT UNSIGNED NOT NULL DEFAULT '0',
 financeBankContKey INT UNSIGNED NOT NULL DEFAULT '0',
 financeBankOpen DATE NOT NULL DEFAULT '0000-00-00',
 financeBankClose DATE NOT NULL DEFAULT '0000-00-00',
 financeBankValid ENUM('YES','NO') NOT NULL DEFAULT 'NO',
 financeBankInputBy INT UNSIGNED NOT NULL DEFAULT '0',
 financeBankInputDt DATE NOT NULL DEFAULT '0000-00-00',
 financeBankInputTm TIME NOT NULL DEFAULT '00:00:00',
 PRIMARY KEY(financeBankKey,financeBankInputBy)
);

create table financeLocation
(
 financeLocationKey INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
 financeLocationAddr VARCHAR(32) NOT NULL DEFAULT ' ',
 financeLocationCity VARCHAR(16) NOT NULL DEFAULT ' ',
 financeLocationCountry VARCHAR(24) NOT NULL DEFAULT ' '
);

create table financeContact
(
 financeContactKey INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
 financeContactPhone VARCHAR(14) NOT NULL DEFAULT ' ',
 financeContactFax VARCHAR(14) NOT NULL DEFAULT ' ',
 financeContactEmail VARCHAR(32) NOT NULL DEFAULT ' ',
 financeContactHP VARCHAR(14) NOT NULL DEFAULT ' '
);

create table financeInventoryAsset
(
 inventoryKey INT UNSIGNED NOT NULL AUTO_INCREMENT,
 inventoryType ENUM('FIXED','MOBILE') NOT NULL DEFAULT 'FIXED',
 inventoryCategory ENUM('SUPPLY','EQUIPMENT','ACCESSORIES') NOT NULL DEFAULT 'SUPPLY',
 inventorySubCategoryKey SMALLINT UNSIGNED NOT NULL DEFAULT '0',
 inventoryName VARCHAR(32) NOT NULL DEFAULT ' ',
 inventoryValue DOUBLE(12,2) NOT NULL DEFAULT '0.00',
 inventoryDesc VARCHAR(215) NOT NULL DEFAULT ' ',
 inventoryBought DATE NOT NULL DEFAULT '0000-00-00',
 inventoryExpired DATE NOT NULL DEFAULT '0000-00-00',
 inventoryQuantity TINYINT UNSIGNED NOT NULL DEFAULT '0',
 inventoryPO INT UNSIGNED NOT NULL DEFAULT '0',
 inventoryNote VARCHAR(64) NOT NULL DEFAULT ' ',
 inventoryInputBy INT UNSIGNED NOT NULL DEFAULT '0',
 inventoryInputDate DATE NOT NULL DEFAULT '0000-00-00',
 inventoryVerify INT UNSIGNED NOT NULL DEFAULT '0',
 inventoryVerifyDate DATE NOT NULL DEFAULT '0000-00-00',
 PRIMARY KEY(inventoryKey,inventoryPO)
);

create table financePurchaseOrder
(
 purchaseOrderKey INT UNSIGNED NOT NULL AUTO_INCREMENT,
 purchaseOrderReqDate DATE NOT NULL DEFAULT '0000-00-00',
 purchaseOrderAppDate DATE NOT NULL DEFAULT '0000-00-00',
 purchaseOrderValue DOUBLE(12,2) NOT NULL DEFAULT '0.00',
 purchaseOrderReqBy INT UNSIGNED NOT NULL DEFAULT '0',
 purchaseOrderAppBy INT UNSIGNED NOT NULL DEFAULT '0',
 purchaseOrderDesc VARCHAR(32) NOT NULL DEFAULT '0',
 purchaseOrderCategory ENUM('SUPPLY','EQUIPMENT','ACCESSORIES') NOT NULL DEFAULT 'SUPPLY',
 purchaseOrderSubCategory SMALLINT UNSIGNED NOT NULL DEFAULT '0',
 purchaseOrderTransactionKey INT UNSIGNED NOT NULL DEFAULT '0',
 PRIMARY KEY(purchaseOrderKey,purchaseOrderTransactionKey)
);

create table financeTransaction
(
 transactionKey INT UNSIGNED NOT NULL AUTO_INCREMENT,
 transactionDt DATE NOT NULL DEFAULT '0000-00-00',
 transactionTm TIME NOT NULL DEFAULT '00:00:00',
 transactionHandleBy INT UNSIGNED NOT NULL DEFAULT '0',
 transactionCategory ENUM('SPEND','RECEIVED','PAYABLE','RECEIVEABLE') DEFAULT 'SPEND',
 transactionSubCategory SMALLINT UNSIGNED NOT NULL DEFAULT '0',
 transactionPayTypeKey INT UNSIGNED NOT NULL DEFAULT '0',
 transactionProofName VARCHAR(24) NOT NULL DEFAULT ' ',
 transactionProofNum VARCHAR(32) NOT NULL DEFAULT ' ',
 transactionClose ENUM('Y','N') NOT NULL DEFAULT 'N',
 transactionNote VARCHAR(64) NOT NULL DEFAULT ' ',
 PRIMARY KEY(transactionKey,transactionHandleBy)
);

create table financeTransactionPayType
(
 transactionPayKey INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
 transactionPayType ENUM('CASH','CREDIT_CARD','DEBIT_CARD','GIRO','CHEQUE') NOT NULL 
 DEFAULT 'CASH',
 transactionPayName VARCHAR(32) NOT NULL DEFAULT ' ',
 transactionPayBank VARCHAR(32) NOT NULL DEFAULT ' ',
 transactionPayCardExp DATE NOT NULL DEFAULT '0000-00-00',
 transactionPayDocNum VARCHAR(32) NOT NULL DEFAULT ' ',
 transactionPayMature DATE NOT NULL DEFAULT '0000-00-00'
);

create table financeSubCategory
(
 subCategoryKey SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
 subCategoryType ENUM('FIXED COST','VAR COST','SALES','OTHER') NOT NULL DEFAULT 'OTHER',
 subCategoryName VARCHAR(24) NOT NULL DEFAULT ' ',
 subCategoryDesc VARCHAR(48) NOT NULL DEFAULT ' '
);

create table financeBankBalance
(
 bankBalanceKey INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
 bankBalanceOf TINYINT UNSIGNED NOT NULL DEFAULT '0',
 bankBalanceAmount DOUBLE(12,2) NOT NULL DEFAULT '0.00',
 bankBalanceCurr VARCHAR(3) NOT NULL DEFAULT 'IDR',
 bankBalanceDate DATE NOT NULL DEFAULT '0000-00-00',
 bankBalanceTime TIME NOT NULL DEFAULT '00:00:00'
);

create table financeCashFlow
(
 cashFlowKey INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, 
 cashFlowTotAmt DOUBLE(12,2) UNSIGNED NOT NULL DEFAULT '0.00',
 cashFlowInSafe DOUBLE(12,2) UNSIGNED NOT NULL DEFAULT '0.00',
 cashFlowInReg  DOUBLE(12,2) UNSIGNED NOT NULL DEFAULT '0.00',
 cashFlowCurr VARCHAR(3) NOT NULL DEFAULT 'IDR',
 cashFlowDate DATE NOT NULL DEFAULT '0000-00-00',
 cashFlowTime TIME NOT NULL DEFAULT '00:00:00'
);

create table financeState
(
 stateKey INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
 stateType ENUM('PROFIT','LOSS','EVEN') NOT NULL DEFAULT 'LOSS',
 stateAmount DOUBLE(12,2) NOT NULL DEFAULT '0.00',
 statePerBeg DATE NOT NULL DEFAULT '0000-00-00',
 statePerEnd DATE NOT NULL DEFAULT '0000-00-00',
 stateChangeByID INT UNSIGNED NOT NULL DEFAULT '0',
 stateChangeBy DATE NOT NULL DEFAULT '0000-00-00'
);

