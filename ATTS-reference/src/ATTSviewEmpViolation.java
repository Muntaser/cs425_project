/**
 * ATTSviewRoute class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSviewEmpViolation extends HttpServlet
{
 private DBFill dbFill = null;
 private String dbName = "ATTSemployee";
 private PrintWriter out = null;
 private Closure cls = null;
 private ATTSutility util = null;

 public void init() throws ServletException
 {
  dbFill = new DBFill();
  util = new ATTSutility();
 } // end of init

 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
  dbFill = null;
 } // end of destroy

 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 } // end of doGet

 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS View Employee Violation</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  String view = req.getParameter("view");
  if(view == null)
  {
   cls.error(1,"view");
   cls.closing();
   out.close();
   return;
  }
  
  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException("<BR>Fail to make initial connection</BR>");
   if(view.compareToIgnoreCase("ALL")==0)
     createView();
   else if(view.compareToIgnoreCase("SET")==0)
     createForm(req);
//   else if(view.compareToIgnoreCase("PROCESS")==0)
//     processAll(req); 
   else
    cls.error(6,null);
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out); 
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
   view = null;
   out = null;
   cls = null;
  }

 } // end of doPost 

 public void createView() throws SQLException
 {
  Vector emp = util.queryAllEmp(dbFill,"WHERE employeeValid=\"Y\";",out);
  if(emp == null)
  {
   cls.error(5,"Employee ");
   return;
  }
  DBEncapsulation r = null;
  StringBuffer msg = new StringBuffer();
  int size = emp.size();
  int p1 = 0;
  int p2 = 0;
  util.createBegin("ATTSsetEmpViolation",null,null,false,out);
  out.println("<TR><TD ALIGN=RIGHT>Violation of Employee</TD><TD>");
  out.println("<SELECT NAME=\"empID\">");
  out.println("<OPTION SELECTED VALUE=\"-1\">");
  for(int i=0;i<size;i++)
  {
   r=new DBEncapsulation((DBEncapsulation)emp.get(i));
   p1 = r.getColumnNamePosition("employeeID");
   p2 = r.getColumnNamePosition("employeeName");
   msg.append("<OPTION VALUE=\""+r.getColumnStringValues(p1)+"\">");
   msg.append(r.getColumnStringValues(p2)+"</OPTION>"); 
   r.destroyAll();
  }
  out.println(msg.toString());
  out.println("</SELECT></TD><TD></TD></TR>");
  out.println("<TR><TD ALIGN=RIGHT>Violation Description:</TD><TD>");
  out.println("<TEXTAREA NAME=\"description\" COLS=4 ROWS=40></TEXTAREA></TD>");
  out.println("<TD><FONT SIZE=-1 COLOR=RED>max 100 karakter</TD></TR>");
  out.println("<TR><TD ALIGN=RIGHT COLOR=RED>Authorized Employee:</TD><TD>");
  out.println("<SELECT NAME=\"authID\">");
  out.println("<OPTION SELECTED VALUE=\"-1\">-- Pilih --</OPTION>");
  out.println(msg.toString());
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD ALIGN=RIGHT COLOR=RED>Authorized Employee PassKey</TD><TD>");
  out.println("<INPUT TYPE=PASSWORD SIZE=20 MAXLENGTH=6 NAME=\"ePK\"></TD><TD></TD></TR>");
  out.println("<TR><TD ALIGN=RIGHT COLOR=RED>Authorized Employee Password</TD><TD>");
  out.println("<INPUT TYPE=PASSWORD SIZE=20 MAXLENGTH=16 NAME=\"ePW\"></TD><TD></TD></TR>");
  out.println("<TR><TD ALIGN=RIGHT>Violation on [date]</TD><TD>");
  out.println("<INPUT TYPE=TEXT NAME=\"date\" SIZE=20 MAXLENGTH=10></TD><TD></TD></TR>"); 
  out.println("<TR><TD ALIGN=RIGHT>Violation at [time]</TD><TD>");
  out.println("<INPUT TYPE=TEXT NAME=\"time\" SIZE=20 MAXLENGTH=8></TD><TD></TD></TR>");
  util.createEnd(out);
  msg.delete(0,msg.length());
 } // end of createView's method

 public void createForm(HttpServletRequest req) throws SQLException
 {
  String evid = req.getParameter("eVID");
  String egid = req.getParameter("eGID");
  String pbeg = req.getParameter("perBeg");
  String pend = req.getParameter("perEnd");
  if((evid==null)||(egid==null)||(pbeg==null)||(pend==null))
  {
   cls.error(1,null);
   return;
  }
  if((evid.length()<1)&&(egid.length()<1)&&(pbeg.length()<1)&&(pend.length()<1))
  {
   cls.error(2,null);
   return;
  }
  if((!util.checkDigit(evid,true,out))||(!util.checkDigit(egid,true,out)))
  {
   cls.error(3,"Employee ID Violator/Employee ID Giver of Violator");
   return;
  }
  if((pbeg.length()<1)||(pend.length()<1))
  {
   cls.error(0,"Kalau mengisi period mulai/berakhir harus mengisi kedua kolom tersebut");
   return;
  }
  DateUtil du = new DateUtil();
  if((pbeg.length()>1)&&(pend.length()>1))
  {
   if((!du.checkSQLDate(pbeg,out))||(!du.checkSQLDate(pend,out)))
   {
    cls.error(4,"Period Pelangaran Mulai/Berakhir pada tanggal");
    return;
   }
   else if((!du.checkSQLDateValidity(pbeg,4,out))||(!du.checkSQLDateValidity(pend,4,out)))
   {
    cls.error(4,"Period Pelangaran Mulai/Berakhir pada tanggal");
    return; 
   }
  }

  StringBuffer msg = new StringBuffer(); 
  msg.append("WHERE ");
  int orgLen = msg.length();
  if(pbeg.length()>1) 
    msg.append("empViolationDate>=\""+pbeg+"\" AND empViolationDate=\""+pend+"\"");
  if(evid.length()>1)
    if(msg.length()>orgLen)
      msg.append(" AND empViolationRecID=\""+evid+"\"");
    else
      msg.append("empViolationRecID=\""+evid+"\""); 
  if(egid.length()>1)
    if(msg.length()>orgLen)
      msg.append(" AND empViolationGiverID=\""+egid+"\"");
    else
      msg.append("empViolationGiverID=\""+egid+"\"");
  if(msg.length()>orgLen)
    msg.append(";");
  else
  {
   cls.error(0,"You donot choose anything, thus there is no action");
   msg.delete(0,msg.length());
   return;
  }
  Vector v = dbFill.QuerySpecific("employeeViolation",msg.toString(),out);
  if(v==null)
  {
   cls.error(0,"Data yang anda cari belum ada didatabase");
   return;
  }
  dbFill.viewContainer(v,out);
  v.removeAllElements();
 } // end of createForm's method
/**
 public void processAll(HttpServletRequest req) throws SQLException
 {

 } // end of processAll's method
*/
} // end of ATTSviewEmpViolation' class

