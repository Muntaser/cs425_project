/**
 * ATTSaddCity class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSsetCurrency extends HttpServlet
{
 private DBFill dbFill = null;
 private String dbName = "ATTSairline";
 private PrintWriter out = null;
 private Closure cls = null;
 private ATTSutility util = null;

 public void init() throws ServletException
 {
  dbFill = new DBFill();
  util = new ATTSutility();
 }
 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
 }
 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 }
 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS Set Currency</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<hr></hr>");

  String airAbbr = req.getParameter("currencyAbbr");
  String airDesc = req.getParameter("currencyDesc");

  if((airAbbr==null)||(airDesc==null))
  {
   cls.error(1,"Currency Abbr/Currency Description");
   cls.makeClosing();
   cls.closing();
   out.close();
   return; 
  }
  if((airAbbr.length()<1)||(airDesc.length()<1))
  {
   cls.error(2,null);
   cls.makeClosing();
   cls.closing();
   out.close();
   return; 
  }

  Vector dbVec = new Vector();  

  DBEncapsulation dbEncap = new DBEncapsulation("airlineSmallNote");
  dbEncap.setColumnNameVal("smallNote",airDesc);
  dbEncap.setVariable("SET @airlineSmallNoteKey = LAST_INSERT_ID();");
  dbVec.addElement((DBEncapsulation)dbEncap.clone());
  dbEncap.clearAll();

  dbEncap.setDBTableName("airlineCurrency");
  dbEncap.setColumnNameVal("currencyAbbr",airAbbr);
  dbEncap.setColumnNameVal("currencyDescSNote","@airlineSmallNoteKey");
  dbVec.addElement((DBEncapsulation)dbEncap.clone());
  
  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException();  
   dbFill.InsertDatabase(dbVec,out);
   out.println("Successfully insert the new currency into database");
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   dbEncap.destroyAll();
   dbVec.removeAllElements();
   out.close();
   dbEncap= null;
   dbVec = null;
  } 
 } 

}

