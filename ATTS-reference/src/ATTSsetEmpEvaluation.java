/**
 * ATTSviewRoute class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSsetEmpEvaluation extends HttpServlet
{
 private DBFill dbFill = null;
 private String dbName = "ATTSemployee";
 private PrintWriter out = null;
 private Closure cls = null;
 private ATTSutility util = null;

/**
 * init's method is to initialize the servlet
 * @param none
 * @return none
 */
 public void init() throws ServletException
 {
  dbFill = new DBFill();
  util = new ATTSutility();
 }

/**
 * destroy's method is to end the life-cycle of the servlet
 * @param none
 * @return none
 */
 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
  dbFill = null;
 }

/**
 * doGet's method is to process GET method from user
 * @param HttpServletRequest req
 *        HttpServletResponse res
 * @return none
 */
 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 }

/**
 * doPost's method is to process POST method from user
 * @param HttpServletRequest req contains user request
 *        HttpServletResponse res contains user respond
 * @return none
 */
 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS Set Employee Evaluation</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=WHITE TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<BR></BR><H1>ATTS Set Employee Evaluation</H1><HR></HR><BR></BR>");
  
  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException("<BR>Failure to make initial connection</BR>");
   process(req);  
   out.println("<BR>Thank you for filling out the Employee Evaluation</BR>");
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
  }  
 } // end of doPost 

 private void process(HttpServletRequest req) throws SQLException
 {
  int eRID = Integer.parseInt(req.getParameter("eRID"));
  int eID  = Integer.parseInt(req.getParameter("eID"));
  int onTime = Integer.parseInt(req.getParameter("onTime"));
  int capable = Integer.parseInt(req.getParameter("capable"));
  int team = Integer.parseInt(req.getParameter("team"));
  int work = Integer.parseInt(req.getParameter("work"));
  int resp = Integer.parseInt(req.getParameter("resp"));
  int viol = Integer.parseInt(req.getParameter("viol"));
  int quick= Integer.parseInt(req.getParameter("quic"));
  int dec  = Integer.parseInt(req.getParameter("dec"));
  int spd  = Integer.parseInt(req.getParameter("spd"));
  int attd = Integer.parseInt(req.getParameter("attd")); 
  String ePK = req.getParameter("empPK");
  String ePW = req.getParameter("empPW");
  String eNT = req.getParameter("note");
  if((ePK==null)||(ePW==null)||(eNT==null))
  {
   cls.error(1,null);
   return;
  } 
  if((ePK.length()<1)||(ePW.length()<1))
  {
   cls.error(0,"Employee PassKey/Employee Password harus di-isi");
   return;
  }
  if(!util.checkDigit(ePK,true,out))
  {
   cls.error(3,"Employee PassKey harus dalam bentuk digit");
   return;
  }
  if((eRID==-1)||(eID==-1))
  {
   cls.error(1,null);
   return;
  }
  if(eRID==eID)
  {
   cls.error(0,"Anda tidak boleh mengevaluasi diri sendiri");
   return;
  }
  int sum=onTime+capable+team+work+resp+viol+attd+quick+spd+dec;
  DBEncapsulation dbI = new DBEncapsulation("employeeEvaluation");
  dbI.setColumnNameVal("empScore",sum);
  dbI.setColumnNameVal("empEvalByID",eID);
  dbI.setColumnNameVal("empEvalOfID",eRID);
  dbI.setColumnNameVal("empEvalDate","CURRENT_DATE");
  dbI.setColumnNameVal("empEvalTime","CURRENT_TIME");
  if(eNT.length()>1)
    dbI.setColumnNameVal("empEvalNote",eNT);
  Vector v = new Vector();
  v.addElement(dbI.clone());
  dbI.destroyAll();
  dbFill.InsertDatabase((Vector)v.clone(),out);
  v.removeAllElements();
 } // end of process' method

} // end of ATTSsetEmpEvaluation's class

