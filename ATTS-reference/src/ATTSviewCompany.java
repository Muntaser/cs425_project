/**
 * ATTSviewRoute class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSviewCompany extends HttpServlet
{
 private DBFill dbFill = null;
 private DBFill dbFill2= null;
 private String dbName = "ATTScompany";
 private String dbName2= "ATTSemployee";
 private PrintWriter out = null;
 private Closure cls = null;
 private ATTSutility util = null;

/**
 * init's method is to initialize the servlet
 * @param none
 * @return none
 */
 public void init() throws ServletException
 {
  dbFill = new DBFill();
  dbFill2= new DBFill();
  util = new ATTSutility();
 }

/**
 * destroy's method is to end the life-cycle of the servlet
 * @param none
 * @return none
 */
 public void destroy() 
 {
  dbFill.closeDatabase();
  dbFill2.closeDatabase();
  util.close();
 }

/**
 * doGet's method is to process GET method from user
 * @param HttpServletRequest req
 *        HttpServletResponse res
 * @return none
 */
 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 }

/**
 * doPost's method is to process POST method from user
 * @param HttpServletRequest req contains user request
 *        HttpServletResponse res contains user respond
 * @return none
 */
 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS View Route</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<BR></BR><H1></H1><HR></HR><BR></BR>"); 

  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException("<BR>Failure to make initial connection to dbName</BR>");
   if(!dbFill2.makeInitialConnection(dbName2,out))
     throw new SQLException("<BR>Failure to make initial connection to dbName2</BR>");
   int choice = Integer.parseInt(req.getParameter("CHOICE"));
   switch(choice)
   {
    case 1 : processCompany(req); 
	     break;
    case 2 : processOffice(req);
	     break;
    default: cls.error(6,null);
	     break;
   }
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
  }  
 } // end of doPost 

/**
 * processCompany's method is to process user request to view company entire info
 * It will give a complete name of the executive if available and email/website.
 * User may fill all the parameter or only few selected parameters.
 * @param  HttpServletRequest req contains user's request
 * @return none
 */
 private void processCompany(HttpServletRequest req) throws SQLException
 {
  String cID = req.getParameter("cID");
  String cSign = req.getParameter("cFound");
  String cDate = req.getParameter("cDate");
  String cType = req.getParameter("cType");

  if((cID==null)||(cSign==null)||(cDate==null)||(cType==null))
  {
   cls.error(1,null);
   return;
  }
  if((cID.length()<1)&&(cSign.compareToIgnoreCase("NON")==0)&&(cDate.length()<1)&&
     (cType.compareToIgnoreCase("NON")==0))
  {
   cls.error(2,null);
   return;
  }
  boolean errorFlag = false; 
  DateUtil du = new DateUtil();
  if((cID.length()>0)&&(!util.checkDigit(cID,true,out)))
  {
   cls.error(0,"Company ID harus dalam bentuk digit/angka");
   errorFlag = true;
  }
  if((cSign.compareToIgnoreCase("NON")!=0)&&(cDate.length()<1))
  {
   cls.error(0,"Anda harus isi tanggal apabila anda memilih View Company by founded date");
   errorFlag = true;
  }
  else if((!du.checkSQLDate(cDate,out))&&(cDate.length()>1)&&
    (cSign.compareToIgnoreCase("NON")!=0))
  {
   cls.error(4,"Company Founded [date]");
   errorFlag = true;
  }
  else if((!du.checkSQLDateValidity(cDate,4,out))&&(cDate.length()>1)&&
    (cSign.compareToIgnoreCase("NON")!=0))
  {
   cls.error(4,"Company Founded [date]");
   errorFlag = true;
  }
  if(errorFlag)
   return;
  StringBuffer q = new StringBuffer("WHERE ");
  if(cID.length()>0)
   q.append("companyID=\""+cID+"\" ");
  if(cSign.compareToIgnoreCase("NON")!=0)
   q.append("companyFounded"+cSign+"\""+cDate+"\" ");
  if(cType.compareToIgnoreCase("NON")==0)
   q.append("companyOwnership=\""+cType+"\" ");
  q.append(";");
  Vector v = dbFill.QuerySpecific("company",q.toString(),out);
  if(v == null)
  {
   cls.error(0,"Query anda tidak menghasilkan apa-apa");
   return;
  }
  v = completeCompany(v);

  dbFill.viewContainer(v,out);

 } // processCompany

/**
 * completeCompany's method is to complete info of company given the vector
 * containing the company info. Using those info, the method complete the company
 * info as far as it can including to complete the employee info.
 * @param Vector v - contain company info
 * @return Vector v - contain completed info to display
 */
 private Vector completeCompany(Vector v) throws SQLException
 {
  if(v==null)
   return v;
  int p1 = 0;
  int p2 = 0;
  int id = 0;
  int size = v.size();
  DBEncapsulation t = null;
  DBEncapsulation t2= null;
  StringBuffer str = new StringBuffer();
  for(int i=0;i<size;i++)
  {
   t = (DBEncapsulation)((DBEncapsulation)v.get(i)).clone();
   p1 = t.getColumnNamePosition("companyID");
   id = t.getColumnIntegerValue(p1);
   if(p1<0)
    throw new SQLException("the container does not contain company information");
   p1 = t.getColumnNamePosition("companyEnd");  
   if(t.getColumnStringValues(p1).compareToIgnoreCase("0000-00-00")==0)
     t.replaceValue(p1,"belum tutup");
   t.replaceName(p1,"Perusahaan tutup tanggal");
   t.replaceName(t.getColumnNamePosition("companyName"),"Nama Perusahaan");
   t.replaceName(t.getColumnNamePosition("companyFounded"),"Perusahaan buka tanggal");
   t.replaceName(t.getColumnNamePosition("companyOwnership"),"Kepemilikan Perusahaan");

   p1 = t.getColumnNamePosition("companyCEO");
   t.replaceName(p1,"Chief Of Executive Officer");
   if(t.getColumnIntegerValue(p1)==0)
    t.replaceValue(p1,"Masih belum ditunjuk");
   else
   {
    str.append("WHERE employeeID=\""+t.getColumnStringValues(p1)+"\";");
    t2 = dbFill2.QuerySingleSpecific("employee",str.toString(),out);
    if(t2 == null)
     throw new SQLException("the container does not contain employee information");
    p2 = t2.getColumnNamePosition("employeeName");
    t.replaceValue(p1,t2.getColumnStringValue(p2));
   }
   p1 = t.getColumnNamePosition("companyCFO");
   t.replaceName(p1,"Chief Of Financial Officer");
   if(t.getColumnIntegerValue(p1)==0)
    t.replaceValue(p1,"Masih belum ditunjuk");
   else
   {
    t2.destroyAll();
    str.append("WHERE employeeID=\""+t.getColumnStringValues(p1)+"\";");
    t2 = dbFill2.QuerySingleSpecific("employee",str.toString(),out);
    if(t2 == null)
     throw new SQLException("the container does not contain employee info");
    p2 = t2.getColumnNamePosition("employeeName");
    t.replaceValue(p1,t2.getColumnStringValue(p2));
   }
   p1 = t.getColumnNamePosition("companyCIO");
   t.replaceName(p1,"Chief Of Information Officer");
   if(t.getColumnIntegerValue(p1)==0)
    t.replaceValue(p1,"Masih belum ditunjuk");
   else
   {
    t2.destroyAll();
    str.append("WHERE employeeID=\""+t.getColumnStringValues(p1)+"\";");
    t2 = dbFill2.QuerySingleSpecific("employee",str.toString(),out);
    if(t2 == null)
     throw new SQLException("the container does not contain employee info");
    p2 = t2.getColumnNamePosition("employeeName");
    t.replaceValue(p1,t2.getColumnStringValue(p2));
   }
   p1 = t.getColumnNamePosition("companyCOO");
   t.replaceName(p1,"Chief Of Operating Officer");
   if(t.getColumnIntegerValue(p1)==0)
    t.replaceValue(p1,"Masih belum ditunjuk");
   else
   {
    t2.destroyAll();
    str.append("WHERE employeeID=\""+t.getColumnStringValues(p1)+"\";");
    t2 = dbFill2.QuerySingleSpecific("employee",str.toString(),out);
    if(t2 == null)
     throw new SQLException("the container does not contain employee info");
    p2 = t2.getColumnNamePosition("employeeName");
    t.replaceValue(p1,t2.getColumnStringValue(p2));
   }
   p1 = t.getColumnNamePosition("companyEmail");
   if(t.getColumnStringValues(p1).length()>1)
    t.replaceName(p1,"Email Perusahaan");
   else
    t.removeNameVal(p1);
   p1 = t.getColumnNamePosition("companyWeb");
   if(t.getColumnStringValues(p1).length()>1)
    t.replaceName(p1,"Website Perusahaan");
   else
    t.removeNameVal(p1);
   t.replaceName(t.getColumnNamePosition("companyValid"),"Perusahaan masih active");
   t.replaceName(t.getColumnNamePosition("companyLastChange"),"Terakhir kali dirubah");
   p1 = t.getColumnNamePosition("companyLastChangeBy");
   str.append("WHERE employeeID=\""+t.getColumnStringValues(p1)+"\";");
   t2.destroyAll();
   t2 = dbFill2.QuerySingleSpecific("employee",str.toString(),out);
   if(t2 == null)
    throw new SQLException("the container does not contain employee info");
   p2 = t2.getColumnNamePosition("employeeName");
   t.replaceValue(p1,t2.getColumnStringValue(p2));
   t2.destroyAll();
   str.append("WHERE rulesOfCompanyID=\""+id+"\";");
   t2 = dbFill.QuerySingleSpecific("companyRules",str.toString(),out);
   if(t2 != null)
   {
    p1=t2.getColumnNamePosition("rulesWorkHours");    
    p2=t2.getColumnNamePosition("rulesSickDays");
    t.setColumnNameVal("Aturan jumlah jam kerja dalam satu hari",
      t2.getColumnIntegerValue(p1));
    t.setColumnNameVal("Aturan jumlah hari tidak masuk karena sakit dalam 1 tahun",
      t2.getColumnIntegerValue(p1));
    p1=t2.getColumnNamePosition("rulesVacationDays");
    t.setColumnNameVal("Aturan jumlah hari libur dalam 1 tahun",
      t2.getColumnIntegerValue(p1));
   }
   else
     cls.error(0,"cannot find company rules of id "+id);
   v.removeElementAt(i);
   v.insertElementAt(t.clone(),i);
   t.destroyAll();
   t2.destroyAll();
  }
  Vector v2 = dbFill.QuerySpecific("companyOffice",str.toString(),out);
  if(v2 == null)
   cls.error(0,"Perusahaan ini belum mempunyai kantor");
  else
  {
   v2 = completeOffice(v2);
   util.converge(v,v2,"companyID","officeID");
  }
  if(!v2.isEmpty())
   v2.removeAllElements();
  str.delete(0,str.length());
  return v;
 }

/**
 * processOffice's method is to process user request to view company offices
 * if available. It allows user to fill all or few of the options.
 * @param  HttpServletRequest req contains user request
 * @return none
 */ 
 private void processOffice(HttpServletRequest req) throws SQLException
 {
  String ofCID = req.getParameter("ofCID");
  String ofStt = req.getParameter("ofStatus");
  String ofAct = req.getParameter("ofActive");
  String ofCty = req.getParameter("ofCity");
  String ofCnt = req.getParameter("ofCountry");
  if((ofCID==null)||(ofStt==null)||(ofAct==null)||(ofCty==null)||(ofCnt==null))
  {
   cls.error(1,null);
   return;
  }
  if((ofCID.length()<1)&&(ofStt.compareToIgnoreCase("NON")==0)&&
     (ofAct.compareToIgnoreCase("NON")==0)&&(ofCty.length()<1)&&(ofCnt.length()<1))
  {
   cls.error(2,null);
   return;
  }
  boolean errorFlag = false;
  if((ofCID.length()>0)&&(!util.checkDigit(ofCID,true,out)))
  {
   cls.error(3,"company id harus dalam bentuk angka/digit");
   errorFlag = true;
  }
  if(errorFlag)
   return;
  StringBuffer str = new StringBuffer();
  str.append("WHERE ");
  if(ofCty.length()>0) 
   str.append("officeCity=\""+ofCty+"\" ");
  if(ofCnt.length()>0)
   str.append("officeCountry=\""+ofCnt+"\" ");
  if(ofAct.compareToIgnoreCase("NON")!=0)
   str.append("officeActive=\""+ofAct+"\" ");
  if(ofStt.compareToIgnoreCase("NON")!=0)
   str.append("officeState=\""+ofStt+"\" ");
  if(ofCID.length()>0)
   str.append("officeOfCompanyID=\""+ofCID+"\" ");
  str.append(";");
  Vector v = dbFill.QuerySpecific("companyOffice",str.toString(),out);
  if(v == null)
  {
   cls.error(0,"Query Company Office tidak menghasilkan apa-apa");
   return;
  }
  v = completeOffice(v);
  dbFill.viewContainer(v,out);
  if(!v.isEmpty())
    v.removeAllElements(); 
 } // end of processOffice's method

/**
 * completeOffice's method is to complete the office info given the result
 * of the office info query. It will also complete the office contact info
 * using the helper method of completeOfficeContact.
 * @param Vector v - contain the office info result of query
 * @return Vector v - contain completed office result.
 */
 private Vector completeOffice(Vector v) throws SQLException
 {
  if(v == null)
   return null;
  int p1 = 0;
  int id = 0;
  int size = v.size();
  DBEncapsulation t = null;
  DBEncapsulation t2= null;
  StringBuffer str = new StringBuffer("WHERE officeOfCompanyID=\"");
  for(int i=0;i<size;i++)
  {
   t = (DBEncapsulation)((DBEncapsulation)v.get(i)).clone();
   t = completeOfficeContact(t); 
   if(i==0)
   {
    id = t.getColumnIntegerValue(t.getColumnNamePosition("officeOfCompanyID"));
    str.append(id+"\";");
    t2 = dbFill.QuerySingleSpecific("company",str.toString(),out);
    if(t2==null)
     throw new SQLException("Not found company with the id :"+id);
    str.delete(0,str.length());
    str.append(t2.getColumnStringValue(t2.getColumnNamePosition("companyName")));
    t.setColumnNameVal("Nama Perusahaan",str.toString());
    t.replaceName(t.getColumnNamePosition("officeOfCompanyID"),"ID Perusahaan");
   }
   else
   {
    t.replaceName(t.getColumnNamePosition("officeOfCompanyID"),"ID Perusahaan");
    t.setColumnNameVal("Nama Perusahaan",str.toString());
   }
   t.replaceName(t.getColumnNamePosition("officeStatus"),"Office Status");
   t.replaceName(t.getColumnNamePosition("officeRegHourOpen"),"Office buka jam");
   t.replaceName(t.getColumnNamePosition("officeRegHourClose"),"Office tutup jam");
   t.replaceName(t.getColumnNamePosition("officeRegWorkDay"),"Office buka pada hari ke");
   t.replaceName(t.getColumnNamePosition("officeFounded"),"Office didirikan tanggal");
   t.replaceName(t.getColumnNamePosition("officeActive"),"Office Active");
   t.replaceName(t.getColumnNamePosition("officeAddr"),"Office beralamat di");
   t.replaceName(t.getColumnNamePosition("officeCity"),"Office berkedudukan di");
   t.replaceName(t.getColumnNamePosition("officeCountry"),"Office berada di negara");
   v.removeElementAt(i);
   v.insertElementAt(t.clone(),i);
   t.destroyAll();
  }
  t2.destroyAll();
  str.delete(0,str.length());
  return v;  
 } // end completeOffice's method

/**
 * completeOfficeContact's method is to complete the office contact numbers
 * that could be numerous for a single office. This method integrates the 
 * the office info with the contact info.
 * @param DBEncapsulation - contains office info
 * @return DBEncapsulation - contains office info and its contact info
 */
 private DBEncapsulation completeOfficeContact(DBEncapsulation dbE) 
  throws SQLException
 {
  int p1 = 0;
  int id = 0;
  int size = 0;
  Vector v = null;
  DBEncapsulation r = null;
  p1 = dbE.getColumnNamePosition("officeID");
  if(p1<0)
   throw new SQLException("the container does not contain office ID");

  id = dbE.getColumnIntegerValue(p1);
  StringBuffer str = new StringBuffer();
  str.append("WHERE officeContactOf=\""+id+"\";");
  v = dbFill.QuerySpecific("companyOfficeContact",str.toString(),out);
  if(v == null)
  {
   dbE.setColumnNameVal("Office Phone","belum tersedia sama sekali");
   dbE.setColumnNameVal("Office Fax","belum tersedia sama sekali");
   return dbE;
  }
  size = v.size();
  for(int i=0;i<size;i++)
  {
   r = (DBEncapsulation)((DBEncapsulation)v.get(i)).clone();
   p1 = r.getColumnNamePosition("officeContactPhone");
   if(r.getColumnStringValue(p1).length()>1)
    dbE.setColumnNameVal("Office Phone",r.getColumnStringValue(p1));   
   else
    dbE.setColumnNameVal("Office Phone","belum ada");
   p1 = r.getColumnNamePosition("officeContactFax");
   if(r.getColumnStringValue(p1).length()>1)
    dbE.setColumnNameVal("Office Fax",r.getColumnStringValue(p1));
   else
    dbE.setColumnNameVal("Office Fax","belum ada"); 
   r.destroyAll();
  }
  str.delete(0,str.length());
  v.removeAllElements();
  return dbE;
 }

} // end of ATTSviewCompany's class

