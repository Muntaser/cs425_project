/**
 * ATTSaddAirlineRules class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSaddAirlineRules extends HttpServlet
{
 private Closure cls = null;
 private DBFill dbFill = null;
 private PrintWriter out = null;
 private ATTSutility util = null;
 private String dbName = "ATTSairline";

 public void init() throws ServletException
 {
  dbFill = new DBFill();
  util = new ATTSutility();
 }
 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
  dbFill = null;
  util = null;
 }
 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 }
 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");

  try
  {
   process(req);
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
  }
 }

 
 private void process(HttpServletRequest req) throws SQLException
 {
  out.println("<HTML><HEAD><TITLE>ATTS View Route</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<BR></BR>ATTS Add Airline Rules<HR></HR><BR></BR>");

  int airID = Integer.parseInt(req.getParameter("airID"));
  String cancelTime = req.getParameter("cancelTime");
  String cancelFee  = req.getParameter("cancelFee");
  String maxCAge    = req.getParameter("cMax");
  String maxIAge    = req.getParameter("iMax");
  String maxCCharge = req.getParameter("cCharge"); 
  String maxICharge = req.getParameter("iCharge");
  String notes      = req.getParameter("RulesNote");
  String empID      = req.getParameter("empID");
  String empPassKey = req.getParameter("pass");

  if((cancelTime==null)||(cancelFee==null)||(maxCAge==null)||(maxIAge==null)||
     (maxCCharge==null)||(maxICharge==null)||(notes==null)||(empID==null)||
     (empPassKey==null))
  {
   cls.error(1,null);
   return;
  }
  if((cancelTime.length()<1)||(cancelFee.length()<1)||(maxCAge.length()<1)||
     (maxIAge.length()<1)||(maxCCharge.length()<1)||(maxICharge.length()<1)||
     (empID.length()<1)||(empPassKey.length()<1)||(airID<0))
  {
   cls.error(2,null);
   return;
  }
  boolean errorFlag = false;
  if((!util.checkDigit(empID,true,out))||(!util.checkDigit(empPassKey,true,out)))
  {
   cls.error(3,"Employee ID and Employee PassKey harus dalam bentuk angka.");
   errorFlag = true;
  }
  if((!util.checkDigit(cancelTime,true,out))||(!util.checkDigit(cancelFee,false,out)))
  {
   cls.error(3,"Cancel Time/Cancel Fee harus dalam bentuk angka/digit.");
   errorFlag = true;
  }
  if((!util.checkDigit(maxCCharge,false,out))||(!util.checkDigit(maxICharge,false,out)))
  {
   cls.error(3,"Child Charge/Infant Charge harus dalam bentuk angka/digit.");
   errorFlag = true;
  }
  if((!util.checkDigit(maxCAge,true,out))&&(!util.checkDigit(maxIAge,true,out)))
  {
   cls.error(3,"Max Child Age dan Max Infant Age harus dalam bentuk angka/digit.");
   errorFlag = true;
  }
  if(errorFlag)
    return; 
  int eID = Integer.parseInt(empID);
  int maxCAg  = Integer.parseInt(maxCAge);
  int maxIAg  = Integer.parseInt(maxIAge);
  int canTime = Integer.parseInt(cancelTime);
  int ePassKey  = Integer.parseInt(empPassKey);
  double maxCCh = Double.parseDouble(maxCCharge);
  double maxICh = Double.parseDouble(maxICharge);
  double canFee = Double.parseDouble(cancelFee);

  DBEncapsulation dbI = new DBEncapsulation("airlineRules");
  dbI.setColumnNameVal("airlineRulesOf",airID);
  dbI.setColumnNameVal("airlineCancelTime",canTime);
  dbI.setColumnNameVal("airlineCancelFee",canFee);
  dbI.setColumnNameVal("airlineCHDMaxAge",maxCAg);
  dbI.setColumnNameVal("airlineINFMaxAge",maxIAg);
  dbI.setColumnNameVal("airlineCHDCharge",maxCCh);
  dbI.setColumnNameVal("airlineINFCharge",maxICh);
  if(notes.length()>1)
    dbI.setColumnNameVal("airlineRulesMNote",notes);
  dbI.setColumnNameVal("airlineRulesInputBy",eID);
  dbI.setColumnNameVal("airlineRulesChangeDate","CURRENT_DATE");
  Vector v = new Vector();
  v.addElement(dbI);
  
  if(!dbFill.makeInitialConnection(dbName,out))
    throw new SQLException("<BR>Failure to make initial connection</BR>");
  dbFill.InsertDatabase(v,out);
  out.println("Successfully insert the new airline rules into the database");

  if(!v.isEmpty())
    v.removeAllElements();
  dbI.destroyAll(); 
 } 

}

