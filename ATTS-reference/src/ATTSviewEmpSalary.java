/**
 * ATTSviewRoute class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSviewEmpSalary extends HttpServlet
{
 private DBFill dbFill = null;
 private String dbName = "ATTSemployee";
 private PrintWriter out = null;
 private Closure cls = null;
 private ATTSutility util = null;

 public void init() throws ServletException
 {
  dbFill = new DBFill();
  util = new ATTSutility();
 } // end of init

 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
  dbFill = null;
 } // end of destroy

 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 } // end of doGet

 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS View Employee Salary</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<BR></BR><H1>ATTS View Employee Salary</H1><HR></HR><BR></BR>");
  String view = req.getParameter("view");
  if(view == null)
  {
   cls.error(1,"view");
   cls.closing();
   out.close();
   return;
  }
  
  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException("<BR>Fail to make initial connection</BR>");
   if(view.compareToIgnoreCase("ALL")==0)
     createView(req);
   else if(view.compareToIgnoreCase("SET")==0)
     createForm();
   else if(view.compareToIgnoreCase("PROCESS")==0)
     processAll(req); 
   else
    cls.error(6,null);
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out); 
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
   view = null;
   out = null;
   cls = null;
  }

 } // end of doPost 

/**
 * createView's method is process user request to view employee info on salary from html
 * @param  HttpServletRequest req
 * @return none
 */
 public void createView(HttpServletRequest req) throws SQLException
 {
  String authID = req.getParameter("authID");
  String authPK = req.getParameter("authPK");
  String authPW = req.getParameter("authPW");
  if((authID==null)||(authPK==null)||(authPW==null))
  {
   cls.error(1,null);
   return;
  }
  if((authID.length()<1)||(authPK.length()<1)||(authPW.length()<1))
  {
   cls.error(2,null);
   return;
  }
  if((!util.checkDigit(authID,true,out))||(!util.checkDigit(authPK,true,out)))
  {
   cls.error(3,"Authorized ID/Authorized PassKey");
   return;
  }
  Vector v = util.queryAllEmp(dbFill,null,out);
  if(v == null)
  {
   cls.error(5,"Employee Data");
   return;
  }
  if(!util.verifyAuthorization(dbFill,cls,out,Integer.parseInt(authID),authPK,authPW,1,null))
  {
   cls.error(7,null);
   return;
  }
  util.createBegin("ATTSviewEmpSalary","view","PROCESS",true,out);
  int p1 = 0;
  int p2 = 0;
  int size = v.size();
  DBEncapsulation j = null;
  StringBuffer str = new StringBuffer();
  out.println("<TR><TD><INPUT TYPE=RADIO NAME=CHOICE VALUE=1></TD>");
  out.println("<TD>By Employee Name:</TD><TD><SELECT NAME=\"empID\">");
  out.println("<OPTION VALUE=\"-1\" SELECTED>-- Pilih Employee --</OPTION>");
  for(int i=0;i<size;i++) 
  {
   j=new DBEncapsulation((DBEncapsulation)v.get(i));
   p1 = j.getColumnNamePosition("employeeID");
   p2 = j.getColumnNamePosition("employeeName");
   str.append("<OPTION VALUE=\""+j.getColumnIntegerValue(p1)+"\">");
   str.append(j.getColumnStringValues(p2)+"</OPTION>");
   j.destroyAll();
  } 
  out.println(str.toString());
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD><INPUT TYPE=RADIO NAME=CHOICE VALUE=2></TD>");
  out.println("<TD>By Employee Position</TD><TD><SELECT NAME=\"position\">");
  out.println("<OPTION VALUE=\"-1\" SELECTED>-- Pilih Position --</OPTION>");
  out.println("<OPTION VALUE=\"TICKETING\">TICKETING</OPTION>");
  out.println("<OPTION VALUE=\"MESSENGER\">MESSENGER</OPTION>");
  out.println("<OPTION VALUE=\"ACCOUNTANT\">ACCOUNTANT</OPTION>");
  out.println("<OPTION VALUE=\"CASHIER\">CASHIER</OPTION>");
  out.println("<OPTION VALUE=\"SUPERVISOR\">SUPERVISOR</OPTION>");
  out.println("<OPTION VALUE=\"DRIVER\">DRIVER</OPTION>");
  out.println("<OPTION VALUE=\"EXECUTIVE\">EXECUTIVE</OPTION>");
  out.println("<OPTION VALUE=\"IT_SUPPORT\">IT SUPPORT</OPTION>");
  out.println("<OPTION VALUE=\"OFFICE_BOY\">OFFICE BOY</OPTION>");
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD><INPUT TYPE=RADIO NAME=CHOICE VALUE=3></TD>");
  out.println("<TD>By Validity:</TD><TD><SELECT NAME=\"valid\">");
  out.println("<OPTION VALUE=\"-1\" SELECTED>-- Pilih Validity -- </OPTION>");
  out.println("<OPTION VALUE=\"Y\">YES</OPTION>");
  out.println("<OPTION VALUE=\"N\">NO</OPTION>");
  out.println("</SELECT></TD></TR>");
  util.createEnd(out);  
 } // end of createView's method

/**
 * createForm's method is to create form for user to set employee salary
 * @param none 
 * @return none
 */
 public void createForm() throws SQLException
 {
  Vector v = util.queryAllEmp(dbFill,null,out);
  if(v == null)
  {
   cls.error(5,"Employee Data");
   return;
  }
  util.createBegin("ATTSsetEmpSalary",null,null,false,out);
  int p1 = 0;
  int p2 = 0;
  int p3 = 0;
  out.println("<TR><TD>Employee Name:</TD><TD><SELECT NAME=\"empID\">");
  out.println("<OPTION SELECTED VALUE=\"-1\">-- Pilih Employee -- </OPTION>");
  int size = v.size();
  StringBuffer str=new StringBuffer();
  DBEncapsulation j = null; 
  for(int i=0;i<size;i++)
  {
   j=new DBEncapsulation((DBEncapsulation)v.get(i));
   p1=j.getColumnNamePosition("employeeID");
   p2=j.getColumnNamePosition("employeeName");
   p3=j.getColumnNamePosition("employeePosition");
   str.append("<OPTION VALUE=\""+j.getColumnStringValues(p1)+"\">");
   str.append(j.getColumnStringValues(p2)+" "+j.getColumnStringValues(p3));
   str.append("</OPTION>");
   j.destroyAll();
  } 
  out.println(str.toString());
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD>Employee Salary per month</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=empSalary SIZE=20 MAXLENGTH=10></TD></TR>");
  out.println("<TR><TD BGCOLOR=RED>Authorized Employee:</TD><TD><SELECT NAME=\"authID\">");
  out.println("<OPTION SELECTED VALUE=\"-1\">-- Pilih Authorized Employee --</OPTION>");
  out.println(str.toString());
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD BGCOLOR=RED>Authorized Employee PassKey</TD>");
  out.println("<TD><INPUT TYPE=PASSWORD NAME=ePK SIZE=20 MAXLENGTH=6></TD></TR>");
  out.println("<TR><TD BGCOLOR=RED>Authorized Employee Password</TD>");
  out.println("<TD><INPUT TYPE=PASSWORD NAME=ePW SIZE=20 MAXLENGTH=16></TD></TR>"); 
  util.createEnd(out); 
  v.removeAllElements();
 } // end of createForm's method

/**
 * processAll's method is to process request from createView()'s method above
 * @param HttpServletRequest req
 * @return none
 */
 public void processAll(HttpServletRequest req) throws SQLException
 {
  int ch  = Integer.parseInt(req.getParameter("CHOICE"));
  int eID = Integer.parseInt(req.getParameter("CHOICE"));
  String position = req.getParameter("position"); 
  String valid = req.getParameter("valid");
  StringBuffer cond = new StringBuffer();

  if((position==null)||(valid==null))
  {
   cls.error(1,null);
   return;
  }
  switch(ch)
  {
   case 1 : if(eID<0) 
            {
	     cls.error(0,"Anda tidak memilih kolom yg tepat: By Employee Name");
             return;
            }
            cond.append("WHERE empSalaryOfID=\""+eID+"\";"); break;
   case 2 : if(position.compareToIgnoreCase("-1")==0)
            {
	     cls.error(0,"Anda tidak memilih kolom yg tepat: By Employee Position");
             return;
            }
            cond.append("WHERE employeePosition=\""+position+"\";"); break;
   case 3 : if(valid.compareToIgnoreCase("-1")==0)
            {
	     cls.error(0,"Anda tidak memilih kolom yg tepat: By Validity");
             return;
            }
            cond.append("WHERE empSalaryValid=\""+valid+"\";"); break;
   default: throw new SQLException("Wrong Choice!");
  }
  if(ch==2)
    processPosition(cond);
  else 
    processNormal(cond);
 } // end of processAll's method

 private void processPosition(StringBuffer msg) throws SQLException
 {
  if(msg == null)
  {
   cls.error(0,"processPosition got passed a null value");
   return; 
  }
  Vector v = dbFill.QuerySpecific("employee",msg.toString(),out);
  if(v == null)
  {
   cls.error(0,"Tidak ditemukan Employee Salary dengan Position yg di-isi");
   return;
  } 
  DBEncapsulation r = null;
  DBEncapsulation j = null;
  int p1 = 0;
  int size = v.size();
  msg.delete(0,msg.length());
  for(int i=0;i<size;i++)
  {
   r = new DBEncapsulation((DBEncapsulation)v.get(i));
   p1 = r.getColumnNamePosition("employeeID");
   msg.append("WHERE empSalaryOfID=\""+r.getColumnStringValues(p1)+"\";"); 
   j = dbFill.QuerySingleSpecific("employeeSalary",msg.toString(),out);
   if(j == null)
    cls.error(0,"Tidak ditemukan employee salary dengan id:"+r.getColumnStringValues(p1));
   else
   {
    r.mergeEncap(j);
    v.removeElementAt(i);
    v.insertElementAt(r.clone(),i); 
   }  
   r.destroyAll();
   j.destroyAll();
  } 
  if(!v.isEmpty())
  {
   dbFill.viewContainer(v,out);
   v.removeAllElements();
  }
 } // end of processPosition

 private void processNormal(StringBuffer msg) throws SQLException
 {
  if(msg == null)
  {
   cls.error(0,"processNormal got passed a null value");
   return;
  }
  Vector v = dbFill.QuerySpecific("employeeSalary",msg.toString(),out);
  if(v==null)
  {
   cls.error(0,"Tidak ditemukan employee salary yg dicari");
   return;
  }
  dbFill.viewContainer(v,out);
  v.removeAllElements();
 } // end of processNormal

} // end of ATTSviewAirlineAccess' class

