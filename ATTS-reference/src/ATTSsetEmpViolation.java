/**
 * ATTSsetEmpViolation class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSsetEmpViolation extends HttpServlet
{
 private DBFill dbFill = null;
 private String dbName = "ATTSemployee";
 private PrintWriter out = null;
 private Closure cls = null;
 private ATTSutility util = null;

/**
 * init's method is to initialize the servlet
 * @param none
 * @return none
 */
 public void init() throws ServletException
 {
  dbFill = new DBFill();
  util = new ATTSutility();
 }

/**
 * destroy's method is to end the life-cycle of the servlet
 * @param none
 * @return none
 */
 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
  dbFill = null;
 }

/**
 * doGet's method is to process GET method from user
 * @param HttpServletRequest req
 *        HttpServletResponse res
 * @return none
 */
 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 }

/**
 * doPost's method is to process POST method from user
 * @param HttpServletRequest req contains user request
 *        HttpServletResponse res contains user respond
 * @return none
 */
 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS Set Employee Violation</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<BR></BR><H1>ATTS Set Employee Violation</H1><HR></HR><BR></BR>"); 

  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException("<BR>Failure to make initial connection</BR>");
   process(req);
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
  }  
 } // end of doPost 

 private void process(HttpServletRequest req) throws SQLException
 {
  int eID = Integer.parseInt(req.getParameter("empID"));
  int aID = Integer.parseInt(req.getParameter("authID"));
  String eNT = req.getParameter("description");
  String ePK = req.getParameter("ePK");
  String ePW = req.getParameter("ePW");
  String dt  = req.getParameter("date");
  String tm  = req.getParameter("time");
  if((eNT==null)||(ePK==null)||(ePW==null)||(dt==null)||(tm==null))
  {
   cls.error(1,null);
   return;
  }
  if((eNT.length()<1)||(ePK.length()<1)||(ePW.length()<1)||
     (dt.length()<1)||(tm.length()<1)||(eID<0)||(aID<0))
  {
   cls.error(2,null);
   return; 
  } 
  if(!util.checkDigit(ePK,true,out)) 
  {
   cls.error(3,"Employee PassKey");
   return;
  }
  if(!util.checkTime(tm,out))
  {
   cls.error(8,null);
   return;
  }

  DateUtil du = new DateUtil();
  if(!du.checkSQLDate(dt,out))
  {
   cls.error(4,"Violation Date");
   return;
  }
  else if(!du.checkSQLDateValidity(dt,4,out))
  {
   cls.error(4,"Violation Date");
   return;
  }
  if(!util.verifyAuthorization(dbFill,cls,out,aID,ePK,ePW,2,null))
  {
   cls.error(7,null);
   return;
  }

  DBEncapsulation dbI = new DBEncapsulation("employeeViolation");
  dbI.setColumnNameVal("empViolationDesc",eNT);  
  dbI.setColumnNameVal("empViolationGiverID",aID);
  dbI.setColumnNameVal("empViolationRecID",eID);
  dbI.setColumnNameVal("empViolationDate",dt);
  dbI.setColumnNameVal("empViolationTime",tm);
  Vector v = new Vector();
  v.addElement(dbI.clone());
  dbI.destroyAll();
  dbFill.InsertDatabase(v,out);
  v.removeAllElements();
  out.println("<BR>Successfully insert the new violation of employee to database</BR>");
 }

} // end of ATTSsetEmpViolation

