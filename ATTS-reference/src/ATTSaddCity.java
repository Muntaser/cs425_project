/**
 * ATTSaddCity class
 * @author Edison Chindrawaly
 */

package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSaddCity extends HttpServlet
{
 private Closure cls = null;
 private DBFill dbFill = null;
 private PrintWriter out = null;
 private ATTSutility util = null;
 private String dbName = "ATTSairline";
 
 public void init() throws ServletException
 {
  dbFill = new DBFill();
  util = new ATTSutility();
 }

 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
 }

 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 }

 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  res.setContentType("text/html");
  cls = new Closure(out);

  try
  {
   process(req);
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
  }
 }

/**
 * process' method is to process user request and input the user submitted data into db
 * @param HttpServletRequest req
 * @return null
 */
 private void process(HttpServletRequest req) throws SQLException
 {
  out.println("<HTML><HEAD><TITLE>ATTS add City</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<BR></BR>");
  out.println("<h1>ATTS add city</h1>");
  out.println("<HR></HR><BR></BR>");
  
  String airCity = req.getParameter("airCity");
  String airAbbr = req.getParameter("airAbbr");
  String airCountry = req.getParameter("airCountry");

  if((airCity==null)||(airAbbr==null)||(airCountry==null))
  {
   cls.error(1,null);
   return;
  }
  if((airCity.length()<1)||(airAbbr.length()<1)||(airCountry.length()<1))
  {
   cls.error(2,null);
   return;
  } 
  DBEncapsulation dbEncap = new DBEncapsulation("airlineCity");
  dbEncap.setColumnNameVal("cityName",airCity); 
  dbEncap.setColumnNameVal("cityAbbr",airAbbr);
  if(airCountry.compareToIgnoreCase("INDONESIA")!=0)
     dbEncap.setColumnNameVal("cityCountry",airCountry);

  Vector dbVec = new Vector();  
  dbVec.addElement(dbEncap);
  if(!dbFill.makeInitialConnection(dbName,out))
    throw new SQLException();  
  dbFill.InsertDatabase(dbVec,out);
  out.println("<BR>Successfully insert the new city to database</BR>");
  dbEncap.destroyAll();
  dbVec.removeAllElements();
  dbEncap= null;
  dbVec = null;
 } 

}

