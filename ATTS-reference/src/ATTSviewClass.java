/**
 * ATTSviewRoute class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSviewClass extends HttpServlet
{
 private DBFill dbFill;
 private String dbName = "ATTSairline";
 private PrintWriter out = null;
 private ATTSutility util = null;
 private Closure cls = null; 

 public void init() throws ServletException
 {
  dbFill = new DBFill();
  util = new ATTSutility();
 }

 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
 }

 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 }

 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS Class </TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<HR></HR>");
  String view = req.getParameter("view");
  if(view == null)
  {
   cls.error(1,"view");
   cls.makeClosing();
   cls.closing();
   out.close();
   return; 
  }

  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException("<BR><B>Initial Connection has failed</B></BR>");
   if(view.compareToIgnoreCase("SET")==0)
     createForm();
   else if(view.compareToIgnoreCase("ALL")==0)
     viewForm();
   else if(view.compareToIgnoreCase("PROCESS")==0)
     processForm(req);
   else
   {
    out.print("<BR><FONT COLOR=RED><H1>You have set a wrong view parameter: "+view);
    out.println("</h1></font></BR>");
   }
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
   view = null;
   out = null;
  }
 } 

/**
 * searchValue's method is to search the value of the particular element determine
 * by parameters (colName and colCond). Once it found those particular element it
 * goes to find the value of parameter (q) and return the value
 * @param vec contains the main container to be search,
 *        String colName contains column name that contain the element q
 *        String colCond contains the condition of the colName
 *        String q contains the element in question
 * @return value of element q
 */
 private String searchValue(Vector vec,String colName,String q,String colCond) 
	throws SQLException 
 {
  if((vec==null)||(colName==null)||(colCond==null)||(q==null))
    throw new SQLException("<BR><B>Either vector or string is null</B></BR>");
  int pos1 = 0;
  int size = vec.size();
  DBEncapsulation dbR = null;
  for(int i=0;i<size;i++)
  {
   dbR=(DBEncapsulation)((DBEncapsulation)vec.get(i)).clone(); 
   pos1 = dbR.getColumnNamePosition(colName);
   if(pos1<0)
   {
    cls.error(1,null);  
    return null; 
   }
   if(dbR.getColumnStringValues(pos1).compareToIgnoreCase(colCond)==0) 
   {
    pos1=dbR.getColumnNamePosition(q);
    if(pos1<0)
    {
     cls.error(1,null);
     return null;
    }
    return dbR.getColumnStringValues(pos1);
   }// end of if-statement
   dbR.destroyAll();
  } // end of for-loop
  return null;
 } // end of searchValue's method

/**
 * viewForm method is to provide form to view classes in database provided by the
 * airline Route  
 * @param none
 * @return none
 */
 public void viewForm() throws SQLException
 {
  out.println("<HR></HR>");
  Vector vecRoute = dbFill.QuerySpecific("airlineRoute","ORDER BY routeAirlineID;",out);
  if(vecRoute == null)
  {
   cls.error(0,"Tidak ada route didalam database");
   return;
  }
  Vector vecAirline=dbFill.QuerySpecific("airline",null,out);
  if(vecAirline == null)
  {
   cls.error(0,"Tidak ada airline didalam database");
   return;
  }
  Vector vecCity=dbFill.QuerySpecific("airlineCity",null,out);
  if(vecCity == null)
  {
   cls.error(0,"Tidak ada city didalam database");
   return;
  }
  Vector vecCurr=dbFill.QuerySpecific("airlineCurrency",null,out);
  if(vecCurr == null)
  {
   cls.error(0,"Tidak ada currency didalam database");
   return;
  }

  DBEncapsulation dbR = dbFill.QuerySingleSpecific("airlineRouteClass",
  			"WHERE classKey=1;",out);
  if(dbR == null)
  {
   cls.error(0,"Belum ada class rute didalam database");
   return;
  } 
  dbR.destroyAll();
  StringBuffer str = new StringBuffer();
  int pos1 = 0;
  int pos2 = 0;
  int pos3 = 0;
  int pos4 = 0;
  int pos5 = 0; 
  out.println("<BR><B>Pilih salah satu method atau semua method untuk mencari</B></BR>");
  util.createBegin("ATTSviewClass","view","PROCESS",true,out);
  out.println("<CAPTION><BR><H1>View Class</H1></BR></CAPTION>");
  out.println("<TR><TD>Periksa dengan rute</TD>");
  out.println("<TD><SELECT NAME=\"classRoute\">");
  out.println("<OPTION SELECTED VALUE=\"-1\">-- Pilih --</OPTION>");
  while(vecRoute.size()>0)
  {
   dbR=(DBEncapsulation)vecRoute.remove(0);
   pos1=dbR.getColumnNamePosition("routeKey");
   pos2=dbR.getColumnNamePosition("routeOrig");
   pos3=dbR.getColumnNamePosition("routeDest");
   pos4=dbR.getColumnNamePosition("routeViaSN");
   pos5=dbR.getColumnNamePosition("routeAirlineID");
   out.println("<OPTION VALUE=\""+dbR.getColumnStringValues(pos1)+"\">");
   out.println(dbR.getColumnStringValues(pos2)+"-"+dbR.getColumnStringValues(pos3));
   if(dbR.getColumnStringValues(pos4).compareToIgnoreCase("0")!=0)
     out.println(dbR.getColumnStringValues(pos4));
   out.println(" | "+dbR.getColumnStringValues(pos5)+"</OPTION>");
   dbR.destroyAll();
  } 
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD>Periksa dengan batas harga ke-bawah [PAX PAID]:</TD>");
  out.println("<TD><SELECT NAME=\"classCurrency\">");
  out.println("<OPTION SELECTED VALUE=\"-1\">-- Pilih Currency --</OPTION>");
  while(vecCurr.size()>0)
  {
   dbR=(DBEncapsulation)vecCurr.remove(0);
   pos1 = dbR.getColumnNamePosition("currencyKey");
   pos2 = dbR.getColumnNamePosition("currencyAbbr");
   out.println("<OPTION VALUE=\""+dbR.getColumnStringValues(pos1)+"\">");
   out.println(dbR.getColumnStringValues(pos2)+"</OPTION>");
   dbR.destroyAll();
  }
  out.println("</SELECT>&nbsp;<SELECT NAME=\"typeOfTravel\">");
  out.println("<OPTION SELECTED VALUE=\"OW\">One-Way</OPTION>");
  out.println("<OPTION VALUE=\"RT\">Round-Trip</OPTION>");
  out.println("</SELECT>&nbsp;<INPUT TYPE=TEXT NAME=\"PAXPaid\" MAXLENGTH=8 SIZE=16>"); 
  out.println("</TD></TR>");
  out.println("<TR><TD>Periksa dengan siapa yang meng-input:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"classInput\" SIZE=16 MAXLENGTH=10></TD></TR>");
  out.println("<TR><TD>Periksa dengan tanggal input:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"classInputDate\" SIZE=16 MAXLENGTH=10>");
  out.println("</TD></TR>");
  out.println("<TR><TD>Periksa dengan tanggal mulai:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"classPriceBegin\" SIZE=16 MAXLENGTH=10>");
  out.println("</TD></TR>");
  out.println("<TR><TD>Periksa dengan tanggal akhir:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"classPriceEnd\" SIZE=16 MAXLENGTH=10>");
  out.println("</TD></TR>");
  out.println("</TABLE>");
  out.println("<INPUT TYPE=SUBMIT VALUE=\"SUBMIT\">&nbsp;");
  out.println("<INPUT TYPE=RESET VALUE=\"CLEAR\">");
  out.println("</FORM>");
 }

/**
 * processForm's method is to process request by view form method. 
 * @param HttpServletRequest req 
 * @return none
 */
 public void processForm(HttpServletRequest req) throws ServletException,SQLException
 {
  String cRoute    = req.getParameter("classRoute");
  String cCurrency = req.getParameter("classCurrency");
  String cRTravel  = req.getParameter("typeOfTravel"); // OW or RT
  String cPAXPaid  = req.getParameter("PAXPaid");
  String cInputBy  = req.getParameter("classInput");
  String cInputDt  = req.getParameter("classInputDate");  //1990-12-31  Length = 10
  String cPriBeg   = req.getParameter("classPriceBegin"); //1990-12-31  Length = 10
  String cPriEnd   = req.getParameter("classPriceEnd");   //1990-12-31  Length = 10
 
  if((cRoute==null)||(cCurrency==null)||(cRTravel==null)||(cPAXPaid==null)||
     (cInputBy==null)||(cInputDt==null)||(cPriBeg==null)||(cPriEnd==null))
  {
   cls.error(1,null);
   return;
  } 

  int cR = Integer.parseInt(cRoute);
  int cInLen = cInputBy.length();
  int cInDt = cInputDt.length();
  int cPBeg = cPriBeg.length();
  int cPEnd = cPriEnd.length();

  boolean cPax = checkTwo(cCurrency,cPAXPaid);

  if((cR<0)&&(cPax!=true)&&(cInLen<1)&&(cInDt!=10)&&(cPBeg!=10)&&(cPEnd!=10))      //000000
  {
   cls.error(2,null);
   return;
  }

  StringBuffer str = new StringBuffer();
  DateUtil du = new DateUtil();

  if((cR>0)&&(cPax==true)&&(cInLen>=1)&&(cInDt==10)&&(cPBeg==10)&&(cPEnd==10))//111111
  {
   str.append("WHERE classRouteKey="+cR);
   if(du.checkSQLDate(cInputDt,out))
     if(du.checkSQLDateValidity(cInputDt,4,out)) 
       str.append(" AND classInputDate="+cInputDt);
     else cls.error(4,null);
   else cls.error(4,null);
   if(du.checkSQLDate(cPriBeg,out))
     if(du.checkSQLDateValidity(cPriBeg,4,out)) 
       str.append(" AND classPriceBegin="+cPriBeg); 
     else cls.error(4,null); 
   else cls.error(4,null);
   if(du.checkSQLDate(cPriEnd,out))
     if(du.checkSQLDateValidity(cPriEnd,4,out)) 
       str.append(" AND classPriceEnd="+cPriEnd);
     else cls.error(4,null);
   else cls.error(4,null);
   str.append(" AND classCurrencyKey="+cCurrency);
   if(cRTravel.compareToIgnoreCase("OW")==0)
     str.append(" AND classPAXPaid="+cPAXPaid);
   else
     str.append(" AND classPAXPaidRT="+cPAXPaid);
   if(du.checkDigit(cInputBy,out))
     str.append(" AND classInput="+cInputBy);
   else
     cls.error(0,"Class Input");
  }
  else if((cR<0)&&(cPax!=true)&&(cInLen<1)&&(cInDt!=10)&&(cPBeg!=10)&&(cPEnd==10)) //000001
  {
   if(du.checkSQLDate(cPriEnd,out))
     if(du.checkSQLDateValidity(cPriEnd,4,out))
       str.append("WHERE classPriceEnd="+cPriEnd);
     else cls.error(4,null);
   else cls.error(4,null);
  }
  else if((cR<0)&&(cPax!=true)&&(cInLen<1)&&(cInDt!=10)&&(cPBeg==10)&&(cPEnd!=10)) //000010
  {
    if(du.checkSQLDate(cPriBeg,out))
      if(du.checkSQLDateValidity(cPriBeg,4,out))
        str.append("WHERE classPriceBegin="+cPriBeg);
      else cls.error(4,null);
    else cls.error(4,null);
  }
  else if((cR<0)&&(cPax!=true)&&(cInLen<1)&&(cInDt==10)&&(cPBeg!=10)&&(cPEnd!=10)) //000100
  {
    if(du.checkSQLDate(cInputDt,out))
      if(du.checkSQLDateValidity(cInputDt,4,out))
        str.append("WHERE classInputDate="+cInDt);
      else cls.error(4,null);
    else cls.error(4,null);
  }
  else if((cR<0)&&(cPax!=true)&&(cInLen>=1)&&(cInDt!=10)&&(cPBeg!=10)&&(cPEnd!=10))//001000
  {
    if(du.checkDigit(cInputBy,out))
      str.append("WHERE classInput="+cInputBy);
    else cls.error(0,"Class Input");
  }
  else if((cR<0)&&(cPax==true)&&(cInLen<1)&&(cInDt!=10)&&(cPBeg!=10)&&(cPEnd!=10)) //010000
  {
    str.append("WHERE classCurrencyKey="+cCurrency);
    if(cRTravel.compareToIgnoreCase("OW")==0)
    {
     if(du.checkDigit(cPAXPaid,out)) str.append(" AND classPAXPaid="+cPAXPaid);
     else cls.error(3,null);
    }
    else
    {
      if(du.checkDigit(cPAXPaid,out)) str.append(" AND classPAXPaidRT="+cPAXPaid);
      else cls.error(3,null);
    }
  }
  else if((cR>0)&&(cPax!=true)&&(cInLen<1)&&(cInDt!=10)&&(cPBeg!=10)&&(cPEnd!=10)) //100000
  {
    str.append("WHERE classRouteKey="+cR);
  }
  else if((cR<0)&&(cPax!=true)&&(cInLen<1)&&(cInDt!=10)&&(cPBeg==10)&&(cPEnd==10)) //000011
  {
    if(du.checkSQLDate(cPriEnd,out))
      if(du.checkSQLDateValidity(cPriEnd,4,out))
        str.append("WHERE classPriceEnd="+cPriEnd);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cPriBeg,out))
      if(du.checkSQLDateValidity(cPriBeg,4,out))
        if(str.length()<1)
          str.append("WHERE classPriceBegin="+cPriBeg);
        else
          str.append(" AND classPriceBegin="+cPriBeg);
      else cls.error(4,null);
    else cls.error(4,null);
  }
  else if((cR<0)&&(cPax!=true)&&(cInLen<1)&&(cInDt==10)&&(cPBeg!=10)&&(cPEnd==10)) //000101
  {
    if(du.checkSQLDate(cPriEnd,out))
      if(du.checkSQLDateValidity(cPriEnd,4,out))
        str.append("WHERE classPriceEnd="+cPriEnd);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cInputDt,out))
      if(du.checkSQLDateValidity(cInputDt,4,out)) 
        if(str.length()<1)
          str.append("WHERE classInputDate="+cInputDt);
        else
          str.append(" AND classInputDate="+cInputDt);
      else cls.error(4,null);
    else cls.error(4,null); 
  }
  else if((cR<0)&&(cPax!=true)&&(cInLen>=1)&&(cInDt!=10)&&(cPBeg!=10)&&(cPEnd==10))//001001
  {
    if(du.checkSQLDate(cPriEnd,out))
      if(du.checkSQLDateValidity(cPriEnd,4,out)) 
        str.append("WHERE classPriceEnd="+cPriEnd);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkDigit(cInputBy,out))
      if(str.length()<1)
        str.append("WHERE classInput="+cInputBy);
      else 
        str.append(" AND classInput="+cInputBy);
    else
      cls.error(4,null); 
  }
  else if((cR<0)&&(cPax==true)&&(cInLen<1)&&(cInDt!=10)&&(cPBeg!=10)&&(cPEnd==10)) //010001
  {
    if(du.checkSQLDate(cPriEnd,out))
      if(du.checkSQLDateValidity(cPriEnd,4,out)) 
        str.append("WHERE classPriceEnd="+cPriEnd);
      else cls.error(4,null);
    else cls.error(4,null);

    if(str.length()<1)
     str.append("WHERE classCurrency="+cCurrency);
    else
     str.append(" AND classCurrency="+cCurrency);

    if(cRTravel.compareToIgnoreCase("OW")==0)
      str.append(" AND classPAXPaid="+cPAXPaid);
    else
      str.append(" AND classPAXPaidRT="+cPAXPaid);

  }
  else if((cR>0)&&(cPax!=true)&&(cInLen<1)&&(cInDt!=10)&&(cPBeg!=10)&&(cPEnd==10)) //100001
  {
    if(du.checkSQLDate(cPriEnd,out))
      if(du.checkSQLDateValidity(cPriEnd,4,out)) 
        str.append("WHERE classPriceEnd="+cPriEnd);
      else cls.error(4,null);
    else cls.error(4,null);
    if(str.length()<1)
      str.append("WHERE classRouteKey="+cRoute);
    else
      str.append(" AND classRouteKey="+cRoute);
  }
  else if((cR<0)&&(cPax!=true)&&(cInLen<1)&&(cInDt==10)&&(cPBeg==10)&&(cPEnd==10)) //000111
  {
    if(du.checkSQLDate(cPriEnd,out))
      if(du.checkSQLDateValidity(cPriEnd,4,out)) 
        str.append("WHERE classPriceEnd="+cPriEnd);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cPriBeg,out))
      if(du.checkSQLDateValidity(cPriBeg,4,out))
        if(str.length()<1)
          str.append("WHERE classPriceBegin="+cPriBeg);
        else
          str.append(" AND classPriceBegin="+cPriBeg);
      else cls.error(4,null);
    else  cls.error(4,null);
    if(du.checkSQLDate(cInputDt,out))
      if(du.checkSQLDateValidity(cInputDt,4,out))
        if(str.length()<1)
          str.append("WHERE classInputDate="+cInputDt);
        else
          str.append(" AND classInputDate="+cInputDt);
      else cls.error(4,null);
    else cls.error(4,null);
  }
  else if((cR<0)&&(cPax!=true)&&(cInLen>=1)&&(cInDt!=10)&&(cPBeg==10)&&(cPEnd==10))//001011
  {
    if(du.checkSQLDate(cPriEnd,out))
      if(du.checkSQLDateValidity(cPriEnd,4,out)) 
        str.append("WHERE classPriceEnd="+cPriEnd);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cPriBeg,out))
      if(du.checkSQLDateValidity(cPriBeg,4,out))
        if(str.length()<1)
          str.append("WHERE classPriceBegin="+cPriBeg);
        else
          str.append(" AND classPriceBegin="+cPriBeg);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkDigit(cInputBy,out))
      if(str.length()<1)
        str.append("WHERE classInput="+cInputBy);
      else
        str.append(" AND classInput="+cInputBy);
    else cls.error(4,null);
  }
  else if((cR<0)&&(cPax==true)&&(cInLen<1)&&(cInDt!=10)&&(cPBeg==10)&&(cPEnd==10)) //010011
  {
    if(du.checkSQLDate(cPriEnd,out))
      if(du.checkSQLDateValidity(cPriEnd,4,out)) 
        str.append("WHERE classPriceEnd="+cPriEnd);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cPriBeg,out))
      if(du.checkSQLDateValidity(cPriBeg,4,out)) 
        if(str.length()<1)
          str.append("WHERE classPriceBegin="+cPriBeg); 
        else
          str.append(" AND classPriceBegin="+cPriBeg);
      else cls.error(4,null);
    else cls.error(4,null);
    if(str.length()<1)
      str.append("WHERE classCurrencyKey="+cCurrency);
    else
      str.append(" AND classCurrencyKey="+cCurrency);
    if(cRTravel.compareToIgnoreCase("OW")==0)
      str.append(" AND classPAXPaid="+cPAXPaid);
    else
      str.append(" AND classPAXPaidRT="+cPAXPaid);
  }
  else if((cR>0)&&(cPax!=true)&&(cInLen<1)&&(cInDt!=10)&&(cPBeg==10)&&(cPEnd==10)) //100011
  {
    if(du.checkSQLDate(cPriEnd,out))
      if(du.checkSQLDateValidity(cPriEnd,4,out)) 
        str.append("WHERE classPriceEnd="+cPriEnd);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cPriBeg,out))
      if(du.checkSQLDateValidity(cPriBeg,4,out)) 
        if(str.length()<1)
          str.append("WHERE classPriceBegin="+cPriBeg); 
        else
          str.append(" AND classPriceBegin="+cPriBeg);
      else cls.error(4,null);
    else cls.error(4,null);
    if(str.length()<1)
      str.append("WHERE classRouteKey="+cRoute);
    else
      str.append(" AND classRouteKey="+cRoute);
  }
  else if((cR<0)&&(cPax!=true)&&(cInLen>=1)&&(cInDt==10)&&(cPBeg==10)&&(cPEnd==10))//001111
  {
    if(du.checkSQLDate(cPriEnd,out))
      if(du.checkSQLDateValidity(cPriEnd,4,out)) 
        str.append("WHERE classPriceEnd="+cPriEnd);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cPriBeg,out))
      if(du.checkSQLDateValidity(cPriBeg,4,out)) 
        if(str.length()<1)
          str.append("WHERE classPriceBegin="+cPriBeg);
        else
          str.append(" AND classPriceBegin="+cPriBeg);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cInputDt,out))
      if(du.checkSQLDateValidity(cInputDt,4,out)) 
        if(str.length()<1)
          str.append("WHERE classInputDate="+cInputDt);
        else
          str.append(" AND classInputDate="+cInputDt); 
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkDigit(cInputBy,out))
      if(str.length()<1)
        str.append("WHERE classInput="+cInputBy);
      else
        str.append(" AND classInput="+cInputBy); 
    else
      cls.error(4,null);
  }
  else if((cR<0)&&(cPax==true)&&(cInLen<1)&&(cInDt==10)&&(cPBeg==10)&&(cPEnd==10)) //010111
  {
    if(du.checkSQLDate(cPriEnd,out))
      if(du.checkSQLDateValidity(cPriEnd,4,out)) 
        str.append("WHERE classPriceEnd="+cPriEnd);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cPriBeg,out))
      if(du.checkSQLDateValidity(cPriBeg,4,out)) 
        if(str.length()<1)
          str.append("WHERE classPriceBegin="+cPriBeg);
        else
          str.append(" AND classPriceBegin="+cPriBeg);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cInputDt,out))
      if(du.checkSQLDateValidity(cInputDt,4,out)) 
        if(str.length()<1)
          str.append("WHERE classInputDate="+cInputDt);
        else
          str.append(" AND classInputDate="+cInputDt); 
      else cls.error(4,null);
    else cls.error(4,null);
    if(str.length()<1)
      str.append("WHERE classCurrencyKey="+cCurrency);
    else
      str.append(" AND classCurrencyKey="+cCurrency);
    if(cRTravel.compareToIgnoreCase("OW")==0)
      str.append(" AND classPAXPaid="+cPAXPaid);
    else
      str.append(" AND classPAXPaidRT="+cPAXPaid);
  }
  else if((cR>0)&&(cPax!=true)&&(cInLen<1)&&(cInDt==10)&&(cPBeg==10)&&(cPEnd==10)) //100111
  {
    if(du.checkSQLDate(cPriEnd,out))
      if(du.checkSQLDateValidity(cPriEnd,4,out)) 
        str.append("WHERE classPriceEnd="+cPriEnd);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cPriBeg,out))
      if(du.checkSQLDateValidity(cPriBeg,4,out)) 
        if(str.length()<1)
          str.append("WHERE classPriceBegin="+cPriBeg);
        else
          str.append(" AND classPriceBegin="+cPriBeg);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cInputDt,out))
      if(du.checkSQLDateValidity(cInputDt,4,out)) 
        if(str.length()<1)
          str.append("WHERE classInputDate="+cInputDt);
        else
          str.append(" AND classInputDate="+cInputDt);
      else cls.error(4,null);
    else cls.error(4,null);
    if(str.length()<1)
      str.append("WHERE classRouteKey="+cRoute);
    else 
      str.append(" AND classRouteKey="+cRoute);
  }
  else if((cR<0)&&(cPax==true)&&(cInLen>=1)&&(cInDt==10)&&(cPBeg==10)&&(cPEnd==10))//011111
  {
    if(du.checkSQLDate(cPriEnd,out))
      if(du.checkSQLDateValidity(cPriEnd,4,out)) 
        str.append("WHERE classPriceEnd="+cPriEnd);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cPriBeg,out))
      if(du.checkSQLDateValidity(cPriBeg,4,out)) 
        if(str.length()<1)
          str.append("WHERE classPriceBegin="+cPriBeg);
        else
          str.append(" AND classPriceBegin="+cPriBeg);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cInputDt,out))
      if(du.checkSQLDateValidity(cInputDt,4,out)) 
        if(str.length()<1)
          str.append("WHERE classInputDate="+cInputDt);
        else
          str.append(" AND classInputDate="+cInputDt);
      else cls.error(4,null);
    else cls.error(4,null);

    if(str.length()<1)
      str.append("WHERE classCurrencyKey="+cCurrency);
    else
      str.append(" AND classCurrencyKey="+cCurrency);
    if(cRTravel.compareToIgnoreCase("OW")==0)
      str.append(" AND classPAXPaid="+cPAXPaid);
    else
      str.append(" AND classPAXPaidRT="+cPAXPaid);
  }
  else if((cR>0)&&(cPax!=true)&&(cInLen>=1)&&(cInDt==10)&&(cPBeg==10)&&(cPEnd==10))//101111
  {
    if(du.checkSQLDate(cPriEnd,out))
      if(du.checkSQLDateValidity(cPriEnd,4,out)) 
        str.append("WHERE classPriceEnd="+cPriEnd);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cPriBeg,out))
      if(du.checkSQLDateValidity(cPriBeg,4,out)) 
        if(str.length()<1)
          str.append("WHERE classPriceBegin="+cPriBeg);
        else
          str.append(" AND classPriceBegin="+cPriBeg);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cInputDt,out))
      if(du.checkSQLDateValidity(cInputDt,4,out)) 
        if(str.length()<1)
          str.append("WHERE classInputDate="+cInputDt);
        else
          str.append(" AND classInputDate="+cInputDt);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkDigit(cInputBy,out))
      if(str.length()<1)
        str.append("WHERE classInput="+cInputBy);
      else
        str.append(" AND classInput="+cInputBy);
    else
      cls.error(4,null);
    if(str.length()<1)
      str.append("WHERE classRouteKey="+cRoute);
    else
      str.append(" AND classRouteKey="+cRoute);
  }
  else if((cR>0)&&(cPax==true)&&(cInLen<1)&&(cInDt!=10)&&(cPBeg!=10)&&(cPEnd!=10)) //110000
  {
    str.append("WHERE classCurrencyKey="+cCurrency);
    if(cRTravel.compareToIgnoreCase("OW")==0)
      str.append(" AND classPAXPaid="+cPAXPaid);
    else
      str.append(" AND classPAXPaidRT="+cPAXPaid);
    str.append(" AND classRouteKey="+cRoute);
  }
  else if((cR>0)&&(cPax!=true)&&(cInLen>=1)&&(cInDt!=10)&&(cPBeg!=10)&&(cPEnd!=10))//101000
  {
    if(du.checkDigit(cInputBy,out))
      str.append("WHERE classInput="+cInputBy);
    else
      cls.error(0,"Class Input");
    if(str.length()<1)
      str.append("WHERE classRouteKey="+cRoute);
    else
      str.append(" AND classRouteKey="+cRoute);
  }
  else if((cR>0)&&(cPax!=true)&&(cInLen<1)&&(cInDt==10)&&(cPBeg!=10)&&(cPEnd!=10)) //100100
  {
    if(du.checkSQLDate(cInputDt,out))
      if(du.checkSQLDateValidity(cInputDt,4,out)) 
        str.append("WHERE classInputDate="+cInputDt);
      else cls.error(4,null);
    else cls.error(4,null);
    if(str.length()<1)
      str.append("WHERE classRouteKey="+cRoute);
    else
      str.append(" AND classRouteKey="+cRoute);
  }
  else if((cR>0)&&(cPax!=true)&&(cInLen<1)&&(cInDt!=10)&&(cPBeg==10)&&(cPEnd!=10)) //100010
  {
    if(du.checkSQLDate(cPriBeg,out))
      if(du.checkSQLDateValidity(cPriBeg,4,out)) 
        str.append("WHERE classPriceBegin="+cPriBeg);
      else cls.error(4,null);
    else cls.error(4,null);
    if(str.length()<1)
      str.append("WHERE classRouteKey="+cRoute);
    else
      str.append(" AND classRouteKey="+cRoute);
  }
  else if((cR<0)&&(cPax==true)&&(cInLen>=1)&&(cInDt!=10)&&(cPBeg!=10)&&(cPEnd!=10))//011000
  {
    str.append("WHERE classCurrencyKey="+cCurrency);
    if(cRTravel.compareToIgnoreCase("OW")==0)
      str.append(" AND classPAXPaid="+cPAXPaid);
    else
      str.append(" AND classPAXPaidRT="+cPAXPaid);
    if(du.checkDigit(cInputBy,out))
      if(str.length()<1)
        str.append("WHERE classInput="+cInputBy);
      else
        str.append(" AND classInput="+cInputBy);
  }
  else if((cR>0)&&(cPax==true)&&(cInLen<1)&&(cInDt==10)&&(cPBeg!=10)&&(cPEnd!=10)) //010100
  {
    if(du.checkSQLDate(cInputDt,out))
      if(du.checkSQLDateValidity(cInputDt,4,out)) 
        str.append("WHERE classInputDate="+cInputDt);
      else cls.error(4,null);
    else cls.error(4,null);
    if(str.length()<1)
      str.append("WHERE classCurrencyKey="+cCurrency);
    else
      str.append(" AND classCurrencyKey="+cCurrency);
    if(cRTravel.compareToIgnoreCase("OW")==0)
      str.append(" AND classPAXPaid="+cPAXPaid);
    else
      str.append(" AND classPAXPaidRT="+cPAXPaid);
    str.append(" AND classRouteKey="+cRoute);
  }
  else if((cR>0)&&(cPax==true)&&(cInLen<1)&&(cInDt!=10)&&(cPBeg==10)&&(cPEnd!=10)) //010010
  {
    if(du.checkSQLDate(cPriBeg,out))
      if(du.checkSQLDateValidity(cPriBeg,4,out)) 
        str.append("WHERE classPriceBegin="+cPriBeg);
      else cls.error(4,null);
    else cls.error(4,null);
    if(str.length()<1)
      str.append("WHERE classCurrencyKey="+cCurrency);
    else
      str.append(" AND classCurrencyKey="+cCurrency);
    if(cRTravel.compareToIgnoreCase("OW")==0)
      str.append(" AND classPAXPaid="+cPAXPaid);
    else
      str.append(" AND classPAXPaidRT="+cPAXPaid);
    str.append(" AND classRouteKey="+cRoute);
  }
  else if((cR<0)&&(cPax!=true)&&(cInLen>=1)&&(cInDt==10)&&(cPBeg!=10)&&(cPEnd!=10))//001100
  {
    if(du.checkSQLDate(cInputDt,out))
      if(du.checkSQLDateValidity(cInputDt,4,out)) 
        str.append("WHERE classInputDate="+cInputDt);
      else cls.error(4,null);
    else cls.error(4,null);
    if(str.length()<1)
      str.append("WHERE classInput="+cInputBy);
    else
      str.append(" AND classInput="+cInputBy);
  }
  else if((cR<0)&&(cPax!=true)&&(cInLen>=1)&&(cInDt!=10)&&(cPBeg==10)&&(cPEnd!=10))//001010
  {
    if(du.checkSQLDate(cPriBeg,out))
      if(du.checkSQLDateValidity(cPriBeg,4,out)) 
        str.append("WHERE classPriceBegin="+cPriBeg);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkDigit(cInputBy,out))
      if(str.length()<1)
        str.append("WHERE classInput="+cInputBy);
      else
        str.append(" AND classInput="+cInputBy);
  }
  else if((cR<0)&&(cPax!=true)&&(cInLen<1)&&(cInDt==10)&&(cPBeg==10)&&(cPEnd!=10)) //000110
  {
    if(du.checkSQLDate(cPriBeg,out))
      if(du.checkSQLDateValidity(cPriBeg,4,out)) 
        str.append("WHERE classPriceBegin="+cPriBeg);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cInputDt,out))
      if(du.checkSQLDateValidity(cInputDt,4,out)) 
        if(str.length()<1)
          str.append("WHERE classInputDate="+cInputDt);
        else
         str.append(" AND classInputDate="+cInputDt);
      else cls.error(4,null);
    else cls.error(4,null);
  }
  else if((cR>0)&&(cPax==true)&&(cInLen>=1)&&(cInDt!=10)&&(cPBeg!=10)&&(cPEnd!=10))//111000
  {
    str.append("WHERE classCurrencyKey="+cCurrency);
    if(cRTravel.compareToIgnoreCase("OW")==0)
      str.append(" AND classPAXPaid="+cPAXPaid);
    else 
      str.append(" AND classPAXPaidRT="+cPAXPaid);
    str.append(" AND classRouteKey="+cRoute);
    if(du.checkDigit(cInputBy,out))
      str.append(" AND classInput="+cInputBy);
    else
      cls.error(4,null);
  }
  else if((cR>0)&&(cPax==true)&&(cInLen<1)&&(cInDt==10)&&(cPBeg!=10)&&(cPEnd!=10)) //110100
  {
    if(du.checkSQLDate(cInputDt,out))
      if(du.checkSQLDateValidity(cInputDt,4,out)) 
        str.append("WHERE classInputDate="+cInputDt);
      else cls.error(4,null);
    else cls.error(4,null);
    if(str.length()<1)
      str.append("WHERE classCurrencyKey="+cCurrency);
    else
      str.append(" AND classCurrencyKey="+cCurrency);
    if(cRTravel.compareToIgnoreCase("OW")==0)
      str.append(" AND classPAXPaid="+cPAXPaid);
    else
      str.append(" AND classPAXPaidRT="+cPAXPaid);
    str.append(" AND classRouteKey="+cRoute);
  }
  else if((cR>0)&&(cPax==true)&&(cInLen<1)&&(cInDt!=10)&&(cPBeg==10)&&(cPEnd!=10)) //110010
  {
    if(du.checkSQLDate(cPriBeg,out))
      if(du.checkSQLDateValidity(cPriBeg,4,out)) 
        str.append("WHERE classPriceBegin="+cPriBeg);
      else cls.error(4,null);
    else cls.error(4,null);
    if(str.length()<1)
      str.append("WHERE classCurrencyKey="+cCurrency);
    else
      str.append(" AND classCurrencyKey="+cCurrency);
    if(cRTravel.compareToIgnoreCase("OW")==0)
      str.append(" AND classPAXPaid="+cPAXPaid);
    else
      str.append(" AND classPAXPaidRT="+cPAXPaid);
    str.append(" AND classRouteKey="+cRoute);
  }
  else if((cR>0)&&(cPax==true)&&(cInLen<1)&&(cInDt!=10)&&(cPBeg!=10)&&(cPEnd==10)) //110001
  {
    if(du.checkSQLDate(cPriEnd,out))
      if(du.checkSQLDateValidity(cPriEnd,4,out)) 
        str.append("WHERE classPriceEnd="+cPriEnd);
      else cls.error(4,null);
    else cls.error(4,null);
    if(str.length()<1)
      str.append("WHERE classCurrencyKey="+cCurrency);
    else
      str.append(" AND classCurrencyKey="+cCurrency);
    if(cRTravel.compareToIgnoreCase("OW")==0)
      str.append(" AND classPAXPaid="+cPAXPaid);
    else
      str.append(" AND classPAXPaidRT="+cPAXPaid);
    str.append(" AND classRouteKey="+cRoute);
  }
  else if((cR>0)&&(cPax==true)&&(cInLen>=1)&&(cInDt==10)&&(cPBeg!=10)&&(cPEnd!=10))//011100
  {
    if(du.checkSQLDate(cInputDt,out))
      if(du.checkSQLDateValidity(cInputDt,4,out)) 
        str.append("WHERE classInputDate="+cInputDt);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkDigit(cInputBy,out))
      if(str.length()<1)
        str.append("WHERE classInput="+cInputBy);
      else
        str.append(" AND classInput="+cInputBy);

    if(str.length()<1)
      str.append("WHERE classCurrencyKey="+cCurrency);
    else 
      str.append(" AND classCurrencyKey="+cCurrency); 

    if(cRTravel.compareToIgnoreCase("OW")==0)
      str.append(" AND classPAXPaid="+cPAXPaid);
    else
      str.append(" AND classPAXPaidRT="+cPAXPaid);
    str.append(" AND classRouteKey="+cRoute);
  }
  else if((cR<0)&&(cPax==true)&&(cInLen>=1)&&(cInDt!=10)&&(cPBeg==10)&&(cPEnd!=10))//011010
  {
    if(du.checkSQLDate(cPriBeg,out))
      if(du.checkSQLDateValidity(cPriBeg,4,out)) 
        str.append("WHERE classPriceBegin="+cPriBeg);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkDigit(cInputBy,out))
      if(str.length()<1)
        str.append("WHERE classInput="+cInputBy);
      else
        str.append(" AND classInput="+cInputBy); 
    if(str.length()<1)
      str.append("WHERE classCurrencyKey="+cCurrency);
    else
      str.append(" AND classCurrencyKey="+cCurrency);
    if(cRTravel.compareToIgnoreCase("OW")==0)
      str.append(" AND classPAXPaid="+cPAXPaid);
    else
      str.append(" AND classPAXPaidRT="+cPAXPaid);
  }
  else if((cR<0)&&(cPax==true)&&(cInLen>=1)&&(cInDt!=10)&&(cPBeg!=10)&&(cPEnd==10))//011001
  {
    if(du.checkSQLDate(cPriEnd,out))
      if(du.checkSQLDateValidity(cPriEnd,4,out)) 
        str.append("WHERE classPriceEnd="+cPriEnd);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkDigit(cInputBy,out))
      if(str.length()<1)
        str.append("WHERE classInput="+cInputBy);
      else
        str.append(" AND classInput="+cInputBy); 
    if(str.length()<1)
      str.append("WHERE classCurrencyKey="+cCurrency);
    else
      str.append(" AND classCurrencyKey="+cCurrency);
    if(cRTravel.compareToIgnoreCase("OW")==0)
      str.append(" AND classPAXPaid="+cPAXPaid);
    else
      str.append(" AND classPAXPaidRT="+cPAXPaid);
  }
  else if((cR<0)&&(cPax!=true)&&(cInLen>=1)&&(cInDt==10)&&(cPBeg==10)&&(cPEnd!=10))//001110
  {
    if(du.checkSQLDate(cPriBeg,out))
      if(du.checkSQLDateValidity(cPriBeg,4,out))
        str.append("WHERE classPriceBegin="+cPriBeg);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cInputDt,out))
      if(du.checkSQLDateValidity(cInputDt,4,out)) 
        if(str.length()<1)
          str.append("WHERE classInputDate="+cInputDt);
        else
          str.append(" AND classInputDate="+cInputDt);
      else cls.error(4,null);
    else cls.error(4,null);

    if(du.checkDigit(cInputBy,out))
      if(str.length()<1)
        str.append("WHERE classInput="+cInputBy);
      else
        str.append(" AND classInput="+cInputBy);
  }
  else if((cR<0)&&(cPax==true)&&(cInLen<1)&&(cInDt==10)&&(cPBeg==10)&&(cPEnd!=10)) //010110
  {
    if(du.checkSQLDate(cPriBeg,out))
      if(du.checkSQLDateValidity(cPriBeg,4,out)) 
        str.append("WHERE classPriceBegin="+cPriBeg);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cInputDt,out))
      if(du.checkSQLDateValidity(cInputDt,4,out)) 
        if(str.length()<1)
          str.append("WHERE classInputDate="+cInputDt);
        else
          str.append(" AND classInputDate="+cInputDt);
      else cls.error(4,null);
    else cls.error(4,null);
    if(str.length()<1)
      str.append("WHERE classCurrencyKey="+cCurrency);
    else
      str.append(" AND classCurrencyKey="+cCurrency);
    if(cRTravel.compareToIgnoreCase("OW")==0)
      str.append(" AND classPAXPaid="+cPAXPaid);
    else
      str.append(" AND classPAXPaidRT="+cPAXPaid);
  }
  else if((cR>0)&&(cPax!=true)&&(cInLen<1)&&(cInDt==10)&&(cPBeg==10)&&(cPEnd!=10)) //100110
  {
    if(du.checkSQLDate(cPriBeg,out))
      if(du.checkSQLDateValidity(cPriBeg,4,out)) 
        str.append("WHERE classPriceBegin="+cPriBeg);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cInputDt,out))
      if(du.checkSQLDateValidity(cInputDt,4,out)) 
        if(str.length()<1)
          str.append("WHERE classInputDate="+cInputDt);
        else
          str.append(" AND classInputDate="+cInputDt);
      else cls.error(4,null);
    else cls.error(4,null);

    if(str.length()<1)
      str.append("WHERE classRouteKey="+cRoute);
    else
      str.append(" AND classRouteKey="+cRoute);
  }
  else if((cR>0)&&(cPax==true)&&(cInLen>=1)&&(cInDt==10)&&(cPBeg!=10)&&(cPEnd!=10))//111100
  {
    if(du.checkSQLDate(cInputDt,out))
      if(du.checkSQLDateValidity(cInputDt,4,out)) 
        str.append("WHERE classInputDate="+cInputDt);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkDigit(cInputBy,out))
      if(str.length()<1)
        str.append("WHERE classInput="+cInputBy);
      else
        str.append(" AND classInput="+cInputBy);

    if(str.length()<1)
      str.append("WHERE classCurrencyKey="+cCurrency);
    else
      str.append(" AND classCurrencyKey="+cCurrency);
    if(cRTravel.compareToIgnoreCase("OW")==0)
      str.append(" AND classPAXPaid="+cPAXPaid);
    else
      str.append(" AND classPAXPaidRT="+cPAXPaid);
    str.append(" AND classRouteKey="+cRoute);
  }
  else if((cR>0)&&(cPax==true)&&(cInLen>=1)&&(cInDt!=10)&&(cPBeg==10)&&(cPEnd!=10))//111010
  {
    if(du.checkSQLDate(cPriBeg,out))
      if(du.checkSQLDateValidity(cPriBeg,4,out)) 
        str.append("WHERE classPriceBegin="+cPriBeg);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkDigit(cInputBy,out))
      if(str.length()<1)
        str.append("WHERE classInput="+cInputBy);
      else
        str.append(" AND classInput="+cInputBy);

    if(str.length()<1)
      str.append("WHERE classCurrencyKey="+cCurrency);
    else
      str.append(" AND classCurrencyKey="+cCurrency);
    if(cRTravel.compareToIgnoreCase("OW")==0)
      str.append(" AND classPAXPaid="+cPAXPaid);
    else
      str.append(" AND classPAXPaidRT="+cPAXPaid);
    str.append(" AND classRouteKey="+cRoute);
  }
  else if((cR>0)&&(cPax==true)&&(cInLen>=1)&&(cInDt!=10)&&(cPBeg!=10)&&(cPEnd==10))//111001
  {
    if(du.checkSQLDate(cPriEnd,out))
      if(du.checkSQLDateValidity(cPriEnd,4,out)) 
        str.append("WHERE classPriceEnd="+cPriEnd);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkDigit(cInputBy,out))
      if(str.length()<1)
        str.append("WHERE classInput="+cInputBy);
      else
        str.append(" AND classInput="+cInputBy);
    if(str.length()<1)
      str.append("WHERE classCurrencyKey="+cCurrency);
    else
      str.append(" AND classCurrencyKey="+cCurrency);
    if(cRTravel.compareToIgnoreCase("OW")==0)
      str.append(" AND classPAXPaid="+cPAXPaid);
    else
      str.append(" AND classPAXPaidRT="+cPAXPaid);
    str.append(" AND classRouteKey="+cRoute);
  }
  else if((cR>0)&&(cPax==true)&&(cInLen>=1)&&(cInDt==10)&&(cPBeg==10)&&(cPEnd!=10))//111110
  {
    if(du.checkSQLDate(cPriBeg,out))
      if(du.checkSQLDateValidity(cPriBeg,4,out)) 
        str.append("WHERE classPriceBegin="+cPriBeg);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cInputDt,out))
      if(du.checkSQLDateValidity(cInputDt,4,out)) 
        if(str.length()<1)
          str.append("WHERE classInputDate="+cInputDt);
        else
          str.append(" AND classInputDate="+cInputDt);
      else cls.error(4,null);
    else cls.error(4,null);

    if(du.checkDigit(cInputBy,out))
      if(str.length()<1)
        str.append("WHERE classInput="+cInputBy);
      else
        str.append(" AND classInput="+cInputBy);
    if(str.length()<1)
      str.append("WHERE classCurrencyKey="+cCurrency);
    else
      str.append(" AND classCurrencyKey="+cCurrency);
    if(cRTravel.compareToIgnoreCase("OW")==0)
      str.append(" AND classPAXPaid="+cPAXPaid);
    else
      str.append(" AND classPAXPaidRT="+cPAXPaid);
    str.append(" AND classRouteKey="+cRoute);
  }
  else if((cR>0)&&(cPax==true)&&(cInLen>=1)&&(cInDt==10)&&(cPBeg!=10)&&(cPEnd==10))//111101
  {
    if(du.checkSQLDate(cPriEnd,out))
      if(du.checkSQLDateValidity(cPriEnd,4,out)) 
        str.append("WHERE classPriceEnd="+cPriEnd);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cInputDt,out))
      if(du.checkSQLDateValidity(cInputDt,4,out)) 
        if(str.length()<1)
          str.append("WHERE classInputDate="+cInputDt);
        else
          str.append(" AND classInputDate="+cInputDt);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkDigit(cInputBy,out))
      if(str.length()<1)
        str.append("WHERE classInput="+cInputBy);
      else
        str.append(" AND classInput="+cInputBy);
    if(str.length()<1)
      str.append("WHERE classCurrencyKey="+cCurrency);
    else
      str.append(" AND classCurrencyKey="+cCurrency);
    if(cRTravel.compareToIgnoreCase("OW")==0)
      str.append(" AND classPAXPaid="+cPAXPaid);
    else
      str.append(" AND classPAXPaidRT="+cPAXPaid);
    str.append(" AND classRouteKey="+cRoute);
  }
  else if((cR<0)&&(cPax==true)&&(cInLen>=1)&&(cInDt==10)&&(cPBeg==10)&&(cPEnd!=10))//011110
  {
    if(du.checkSQLDate(cPriBeg,out))
      if(du.checkSQLDateValidity(cPriBeg,4,out)) 
        str.append("WHERE classPriceBegin="+cPriBeg);
      else cls.error(4,null);
    else cls.error(4,null);

    if(du.checkSQLDate(cInputDt,out))
      if(du.checkSQLDateValidity(cInputDt,4,out)) 
        if(str.length()<1)
          str.append("WHERE classInputDate="+cInputDt);
        else
          str.append(" AND classInputDate="+cInputDt);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkDigit(cInputBy,out))
      if(str.length()<1)
        str.append("WHERE classInput="+cInputBy);
      else
        str.append(" AND classInput="+cInputBy); 
    if(str.length()<1)
      str.append("WHERE classCurrencyKey="+cCurrency);
     else
      str.append(" AND classCurrencyKey="+cCurrency);
  
    if(cRTravel.compareToIgnoreCase("OW")==0)
      str.append(" AND classPAXPaid="+cPAXPaid);
    else
      str.append(" AND classPAXPaidRT="+cPAXPaid);
  }
  else if((cR>0)&&(cPax!=true)&&(cInLen>=1)&&(cInDt==10)&&(cPBeg!=10)&&(cPEnd==10))//101101
  {
    if(du.checkSQLDate(cPriEnd,out))
      if(du.checkSQLDateValidity(cPriEnd,4,out)) 
        str.append("WHERE classPriceEnd="+cPriEnd);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cInputDt,out))
      if(du.checkSQLDateValidity(cInputDt,4,out)) 
        if(str.length()<1)
          str.append("WHERE classInputDate="+cInputDt);
        else
          str.append(" AND classInputDate="+cInputDt);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkDigit(cInputBy,out))
      if(str.length()<1)
        str.append("WHERE classInput="+cInputBy);
      else
        str.append(" AND classInput="+cInputBy);
 
    if(str.length()<1)
      str.append("WHERE classRouteKey="+cRoute);
    else
      str.append(" AND classRouteKey="+cRoute);
  }
  else if((cR<0)&&(cPax==true)&&(cInLen>=1)&&(cInDt!=10)&&(cPBeg==10)&&(cPEnd==10))//011011
  {
    if(du.checkSQLDate(cPriEnd,out))
      if(du.checkSQLDateValidity(cPriEnd,4,out)) 
        str.append("WHERE classPriceEnd="+cPriEnd);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cPriBeg,out))
      if(du.checkSQLDateValidity(cPriBeg,4,out)) 
        if(str.length()<1)
          str.append("WHERE classPriceBegin="+cPriBeg);
        else
          str.append(" AND classPriceBegin="+cPriBeg);
      else cls.error(4,null);
    else cls.error(4,null);

    if(du.checkDigit(cInputBy,out))
      if(str.length()<1)
        str.append("WHERE classInput="+cInputBy);
      else
        str.append(" AND classInput="+cInputBy);

    if(str.length()<1)
      str.append("WHERE classCurrencyKey="+cCurrency);
    else
      str.append(" AND classCurrencyKey="+cCurrency);
    if(cRTravel.compareToIgnoreCase("OW")==0)
      str.append(" AND classPAXPaid="+cPAXPaid);
    else
      str.append(" AND classPAXPaidRT="+cPAXPaid);
  }
  else if((cR>0)&&(cPax==true)&&(cInLen<1)&&(cInDt==10)&&(cPBeg==10)&&(cPEnd!=10)) //110110
  {
    if(du.checkSQLDate(cPriBeg,out))
      if(du.checkSQLDateValidity(cPriBeg,4,out)) 
        str.append("WHERE classPriceBegin="+cPriBeg);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cInputDt,out))
      if(du.checkSQLDateValidity(cInputDt,4,out)) 
        if(str.length()<1)
          str.append("WHERE classInputBy="+cInputDt);
        else
          str.append(" AND classInputBy="+cInputDt);
      else cls.error(4,null);
    else cls.error(4,null);

    if(str.length()<1)
      str.append("WHERE classCurrencyKey="+cCurrency);
    else
      str.append(" AND classCurrencyKey="+cCurrency);
    if(cRTravel.compareToIgnoreCase("OW")==0)
      str.append(" AND classPAXPaid="+cPAXPaid);
    else
      str.append(" AND classPAXPaidRT="+cPAXPaid);
    str.append(" AND classRouteKey="+cRoute);
  }
  else if((cR>0)&&(cPax==true)&&(cInLen<1)&&(cInDt!=10)&&(cPBeg==10)&&(cPEnd==10)) //110011
  {
    if(du.checkSQLDate(cPriEnd,out))
      if(du.checkSQLDateValidity(cPriEnd,4,out)) 
        str.append("WHERE classPriceEnd="+cPriEnd);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cPriBeg,out))
      if(du.checkSQLDateValidity(cPriBeg,4,out)) 
        if(str.length()<1)
          str.append("WHERE classPriceBegin="+cPriBeg);
        else
          str.append(" AND classPriceBegin="+cPriBeg);
      else cls.error(4,null);
    else cls.error(4,null);

    if(str.length()<1)
      str.append("WHERE classCurrencyKey="+cCurrency);
    else
      str.append(" AND classCurrencyKey="+cCurrency);
    if(cRTravel.compareToIgnoreCase("OW")==0)
      str.append(" AND classPAXPaid="+cPAXPaid);
    else
      str.append(" AND classPAXPaidRT="+cPAXPaid);
    str.append(" AND classRouteKey="+cRoute);
  }
  else if((cR<0)&&(cPax==true)&&(cInLen>=1)&&(cInDt==10)&&(cPBeg!=10)&&(cPEnd==10))//011101
  {
    if(du.checkSQLDate(cPriEnd,out))
      if(du.checkSQLDateValidity(cPriEnd,4,out)) 
        str.append("WHERE classPriceEnd="+cPriEnd);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cInputDt,out))
      if(du.checkSQLDateValidity(cInputDt,4,out)) 
        if(str.length()<1)
          str.append("WHERE classInputDate="+cInputDt);
        else
          str.append(" AND classInputDate="+cInputDt);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkDigit(cInputBy,out))
      if(str.length()<1)
        str.append("WHERE classInput="+cInputBy);
      else
        str.append(" AND classInput="+cInputBy);

    if(str.length()<1)
      str.append("WHERE classCurrencyKey="+cCurrency);
    else
      str.append(" AND classCurrencyKey="+cCurrency);
    if(cRTravel.compareToIgnoreCase("OW")==0)
      str.append(" AND classPAXPaid="+cPAXPaid);
    else
      str.append(" AND classPAXPaidRT="+cPAXPaid);
  }
  else if((cR>0)&&(cPax!=true)&&(cInLen>=1)&&(cInDt==10)&&(cPBeg==10)&&(cPEnd!=10))//101110
  {
    if(du.checkSQLDate(cPriBeg,out))
      if(du.checkSQLDateValidity(cPriBeg,4,out)) 
        str.append("WHERE classPriceBegin="+cPriBeg);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkSQLDate(cInputDt,out))
      if(du.checkSQLDateValidity(cInputDt,4,out)) 
        if(str.length()<1)
          str.append("WHERE classInputDate="+cInputDt);
        else
          str.append(" AND classInputDate="+cInputDt);
      else cls.error(4,null);
    else cls.error(4,null);
    if(du.checkDigit(cInputBy,out))
      if(str.length()<1)
        str.append("WHERE classInput="+cInputBy);
      else
        str.append(" AND classInput="+cInputBy);
    if(str.length()<1)
      str.append("WHERE classRouteKey="+cRoute);
    else
      str.append(" AND classRouteKey="+cRoute);
  }
  else 
  {
   out.println("<BR><FONT COLOR=RED><B>This should not happen!</B></FONT></BR>");
   return;
  }

  out.println("<HR></HR>");
  if(str.length()<1)
  {
   out.println("<BR><FONT COLOR=RED><B>");
   out.println("Anda melakukan hal yang tidak benar dalam memilih pilihan.</B></FONT>");
   return;
  }
  str.append(";"); 
  Vector result = dbFill.QuerySpecific("airlineRouteClass",str.toString(),out);
  if(result == null)
  {
   out.println("<BR><h2>Query tidak menghasilkan apa-apa</h2></BR>");
   return;
  }
  dbFill.viewContainer(result,out);
 }

 private boolean checkTwo(String a, String b)
 {
  if((a==null)||(b==null))
    return false; 

  int a1 = Integer.parseInt(a);
  int blen = b.length();

  if((a1<0)&&(blen<1))
  {
   return false;
  }
  else if((a1>0)&&(blen>=1))
  {
   for(int x=0;x<blen;x++)
   {
    if(!Character.isDigit(b.charAt(x)))
    {
     out.println("<BR><FONT COLOR=RED><B>Character diposisi: "+x+" bukan digit</B></FONT>");
     return false;
    }
   }
  }
  else if((a1>0)&&(blen<1))
  {
   out.println("<BR><FONT COLOR=RED><B>Anda belum memasukan harga yg anda cari</B></FONT>");
   return false;
  }
  else if((a1<0)&&(blen>=1))
  {
   out.println("<BR><FONT COLOR=RED><B>Anda belum memilih currency</B></FONT>");
   return false;
  }
  return true;
 } // end of checkTwo's method

/**
 * createForm's method is to create form to fill by user. It will fill up
 * the database for the class price. One route can have many classes.
 * @param none
 * @return none
 */
 public void createForm() throws SQLException
 {
  Vector vecRoute = dbFill.QuerySpecific("airlineRoute",null,out);
  if(vecRoute == null)
  {
   cls.error(1,"Tidak ada rute penerbangan di dalam database");
   return;
  } 
  Vector vecAirline = util.queryAirline(dbFill,out);
  if(vecAirline == null)
  {
   cls.error(1,"Tidak ada airline di dalam database");
   return;
  }
  Vector vecCity = util.queryCity(dbFill,out);
  if(vecCity == null)
  {
   cls.error(1,"Tidak ada kota di dalam database");
   return;
  }
  Vector vecCurr = util.queryCurrency(dbFill,out);
  if(vecCurr == null)
  {
   cls.error(1,"Tidak ada currency di dalam database");
   return;
  }

  int pos1 = 0;
  int pos2 = 0;
  int pos3 = 0;
  int pos4 = 0;
  int pos5 = 0;
  DBEncapsulation dbR = null;
  out.println("<HR></HR>");
  out.println("<FORM METHOD=POST ACTION=\"ATTSaddClass\">");
  out.println("<TABLE BORDER=0 WIDTH=75%>");
  out.println("<CAPTION><B><BR>Set Class for Route</BR></B></CAPTION>");
  out.println("<TR><TD>Class untuk rute:</TD>");
  out.println("<TD><SELECT NAME=\"classRouteKey\">");
  out.println("<OPTION VALUE=\"-1\">-- Pilih --</OPTION>");
  while(vecRoute.size()>0)
  {
   dbR = (DBEncapsulation)vecRoute.remove(0);
   pos1 = dbR.getColumnNamePosition("routeKey");
   pos2 = dbR.getColumnNamePosition("routeOrig");
   pos3 = dbR.getColumnNamePosition("routeDest");
   pos4 = dbR.getColumnNamePosition("routeViaSN");
   pos5 = dbR.getColumnNamePosition("routeAirlineID");
   out.println("<OPTION VALUE=\""+dbR.getColumnStringValues(pos1)+"\">");
   out.println(dbR.getColumnStringValues(pos2)+"-"+dbR.getColumnStringValues(pos3));
   if(dbR.getColumnStringValues(pos4).length()>2)
     out.println(" (via "+dbR.getColumnStringValues(pos4)+" )");
   out.println(" | " + dbR.getColumnStringValues(pos5)+"</OPTION>");
   dbR.destroyAll();
  }
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD>Class Code:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"classCode\" MAXLENGTH=3 SIZE=8 VALUE=\"Y\">");
  out.println("</TD></TR><TR><TD>Class Code PAX:</TD>");
  out.println("<TD><SELECT NAME=\"classCodePAX\">"); 
  out.println("<OPTION SELECTED VALUE=\"A\">Adult</OPTION>");
  out.println("<OPTION VALUE=\"C\">Child</OPTION>");
  out.println("<OPTION VALUE=\"I\">Infant</OPTION>");
  out.println("</SELECT></TD></TR><TR><TD>Class Price Code:</TD><TD>");
  out.println("<SELECT NAME=\"classPriceCode\">");
  out.println("<OPTION SELECTED VALUE=\"REG\">Regular</OPTION>");
  out.println("<OPTION VALUE=\"SPE\">Special</OPTION>");
  out.println("<OPTION VALUE=\"PRO\">Promotion</OPTION></SELECT>");
  out.println("</TD></TR><TR><TD>Class Price effective begin date;</TD><TD>");
  out.println("<INPUT TYPE=TEXT NAME=\"classBegin\" MAXLENGTH=10 SIZE=12></TD></TR>");
  out.println("<TR><TD>Class Price effective end date:</TD><TD>");
  out.println("<INPUT TYPE=TEXT NAME=\"classEnd\" MAXLENGTH=10 SIZE=12></TD></TR>");
  out.println("<TR><TD>Class Price effective end date is Until Further Notice (UFN)</TD>");
  out.println("<TD><SELECT NAME=\"classEndUFN\">"); 
  out.println("<OPTION SELECTED VALUE=\"Y\">YES</OPTION>");
  out.println("<OPTION VALUE=\"N\">NO</OPTION>");
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD>Class Publish Fare</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"classPublishFare\" MAXLENGTH=12 SIZE=16>");
  out.println("</TD></TR>");
  out.println("<TR><TD>Class Selling Fare/Base Fare</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"classSellingFare\" MAXLENGTH=12 SIZE=16>");
  out.println("</TD></TR>");
  out.println("<TR><TD>Class PPN</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"classPPN\" MAXLENGTH=12 SIZE=16></TD></TR>");
  out.println("<TR><TD>Class Net to Airline:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"classNet\" MAXLENGTH=12 SIZE=16></TD></TR>");
  out.println("<TR><TD>Class PAX Paid:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"classPAXPaid\" MAXLENGTH=12 SIZE=16></TD></TR>");
  out.println("<TR><TD>Class PAX Paid Route Trip [Pulang-Pergi]</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"classPAXPaidRT\" MAXLENGTH=12 SIZE=16>");
  out.println("</TD></TR>");
  out.println("<TR><TD>Class Price in Currency:</TD>");
  out.println("<TD><SELECT NAME=\"classCurrency\">"); 
  while(vecCurr.size()>0) 
  {
   dbR=(DBEncapsulation)vecCurr.remove(0);
   pos1=dbR.getColumnNamePosition("currencyKey");
   pos2=dbR.getColumnNamePosition("currencyAbbr");
   out.println("<OPTION VALUE=\""+dbR.getColumnStringValues(pos1)+"\">");
   out.println(dbR.getColumnStringValues(pos2)+"</OPTION>"); 
   dbR.destroyAll();
  }
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD>Di-input oleh</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"classInput\" MAXLENGTH=10 SIZE=16></TD></TR>");
  out.println("<TR><TD>Class Note:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"classMNote\" MAXLENGTH=64 SIZE=16></TD></TR>");
  out.println("</TABLE>");
  out.println("<HR></HR>");
  out.println("<TABLE BORDER=0 WIDTH=75%>");
  out.println("<CAPTION><BR><H1>Class Extra Info</H1></BR></caption>");
  out.println("<TR><TD>Tour Code [kalau tersedia]:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"extraInfoTourCode\" MAXLENGTH=16 SIZE=16>");
  out.println("</TD></TR>");
  out.println("<TR><TD>Class Info Ticket Designator</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"extraInfoTktDesignator\" MAXLENGTH=16 SIZE=16>");
  out.println("</TD></TR>");
  out.println("<TR><TD>Class Info Fare Type</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"extraInfoFareType\" MAXLENGTH=16 SIZE=16>");
  out.println("</TD></TR>");
  out.println("<TR><TD>Class Info Type</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"extraInfoSNote\" MAXLENGTH=32 SIZE=16>");
  out.println("</TD></TR></TABLE><HR></HR>");
  out.println("<TABLE BORDER=0 WIDTH=75%>");
  out.println("<CAPTION><BR><H1>Class Rules</H1></BR></CAPTION>");
  out.println("<TR><TD>Refundable</TD>");
  out.println("<TD><SELECT NAME=\"classRefundable\">");
  out.println("<OPTION SELECTED VALUE=\"Y\">YES</OPTION>");
  out.println("<OPTION VALUE=\"N\">NO</OPTION>");
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD>Rerouteable:</TD>");
  out.println("<TD><SELECT NAME=\"classReroutable\">"); 
  out.println("<OPTION SELECTED VALUE=\"Y\">YES</OPTION>");
  out.println("<OPTION VALUE=\"N\">NO</OPTION>");
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD>Endorsable</TD>");
  out.println("<TD><SELECT NAME=\"classEndorsable\">");
  out.println("<OPTION SELECTED VALUE=\"Y\">YES</OPTION>");
  out.println("<OPTION VALUE=\"N\">NO</OPTION>");
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD>Extendable</TD>");
  out.println("<TD><SELECT NAME=\"classExtendable\">");
  out.println("<OPTION SELECTED VALUE=\"Y\">YES</OPTION>");
  out.println("<OPTION VALUE=\"N\">NO</OPTION>");
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD>Ticket Validity</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"classValidity\" MAXLENGTH=10 SIZE=16>");
  out.println("</TD></TR>");
  out.println("<TR><TD>Class Allowed Baggage [in KG]</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"classAllowedBag\" MAXLENGTH=3 SIZE=8>");
  out.println("</TD></TR>");
  out.println("<TR><TD>Extra baggage Fee [in Rupiah/KG]</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"classExtraBagFee\" MAXLENGTH=11 SIZE=16>");
  out.println("</TD></TR>"); 
  out.println("<TR><TD>Time limit for cancellation [Berapa Jam sebelum CHECK-IN]:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"cancelTimeLimit\" MAXLENGTH=3 SIZE=8>");
  out.println("</TD></TR>");
  out.println("<TR><TD>Cancellation Fee:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"cancelFee\" MAXLENGTH=11 SIZE=16></TD></TR>");
  out.println("<TR><TD>Aturan untuk refund [Jika diperbolehkan]:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"refundRulesFee\" MAXLENGTH=32 SIZE=16>");
  out.println("</TD></TR>"); 
  out.println("<TR><TD>Class Rules Note</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"classRulesSNote\" MAXLENGTH=32 SIZE=16>");
  out.println("</TD></TR>");
  out.println("</TABLE>");
  out.println("<INPUT TYPE=SUBMIT VALUE=\"SUBMIT\">&nbsp;");
  out.println("<INPUT TYPE=RESET VALUE=\"CLEAR\">");
  out.println("</FORM>");
 }
 
}

