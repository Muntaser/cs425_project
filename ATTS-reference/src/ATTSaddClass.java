/**
 * ATTSviewRoute class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSaddClass extends HttpServlet
{
 private DBFill dbFill = null;
 private String dbName = "ATTSairline";
 private PrintWriter out = null;
 private Closure cls = null;
 private ATTSutility util = null;

 public void init() throws ServletException
 {
  String driver = "com.mysql.jdbc.Driver";
  String url = "jdbc:mysql://starcloud:3306/";
  String user= "edisonch";
  String pwd = "gblj21mysql03";
  dbFill = new DBFill(driver,url,user,pwd);
  util = new ATTSutility();
 }
 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
  dbFill = null;
 }
 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 }
 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  try
  {
   process(req);
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
  }
 }

 private void process(HttpServletRequest req) throws SQLException
 {
  out.println("<HTML><HEAD><TITLE>ATTS Add Class </TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<HR></HR>");
  boolean allFill = true;
 
  String cRKey = req.getParameter("classRouteKey");
  String cCode = req.getParameter("classCode");
  String cCPAX = req.getParameter("classCodePAX");
  String cCPri = req.getParameter("classPriceCode");
  String cBeg  = req.getParameter("classBegin");
  String cEnd  = req.getParameter("classEnd");  // boleh tidak di-isi
  String cUFN  = req.getParameter("classEndUFN");
  String cPF   = req.getParameter("classPublishFare");
  String cSF   = req.getParameter("classSellingFare");
  String cPPN  = req.getParameter("classPPN");
  String cNet  = req.getParameter("classNet");
  String cPPaid= req.getParameter("classPAXPaid");
  String cPdRT = req.getParameter("classPAXPaidRT");
  String cCurr = req.getParameter("classCurrency");
  String cIn   = req.getParameter("classInput");
  String cMNote= req.getParameter("classMNote"); // boleh tidak di-isi
    
  int cRK = Integer.parseInt(cRKey);
  if((cBeg==null)||(cPF==null)||(cSF==null)||(cPPN==null)||(cNet==null)||
     (cPPaid==null)||(cPdRT==null)||(cCurr==null)||(cIn==null))
  {
   cls.error(1,null);
   return;
  }

  if((cRK<0)||(cBeg.length()!=10)||(cPF.length()<1)||(cSF.length()<1)||
     (cPPN.length()<1)||(cNet.length()<1)||(cPPaid.length()<1)||(cPdRT.length()<1)||
     (cCurr.length()<1)||(cIn.length()<1))
  {
   out.println("<FONT COLOR=RED><BR><B>");
   out.println("Anda harus mengisi semua pilihan dalam <i>");
   out.println("Set Class for Route</i> dengan benar.");
   out.println("Anda hanya diperbolehkan untuk tidak mengisi <i>Class Note</i></B></FONT>");
   allFill = false;
  }
  if((cEnd.length()>1)&&(cUFN.compareToIgnoreCase("Y")==0))
  {
   out.println("<FONT COLOR=RED><BR><B>");
   out.println("Anda hanya boleh mengisi salah satu dari <i>Class Price effective");
   out.println(" end date</i> atau <i>Class Price effective end date is Until Further");
   out.println(" Notice(UFN)</i>. <BR>Anda tidak boleh mengisi dua-dua nya.");
   out.println("<BR>Contoh yg diperbolehkan:");
   out.println("<BR>effective end date: 2003-02-15 [harus 10 character]");
   out.println("<BR>UFN: NO</B></FONT>"); 
   allFill = false;
  }

  DateUtil du = new DateUtil();
  if(cBeg.length()!=10)
  {
   out.println("<FONT COLOR=RED><B><BR>");
   out.println("Class Price effective begin date harus di-isi dengan format:YYYY-MM-DD");
   out.println("<BR>Format tanggal harus 10 character panjangnya.");
   out.println("<BR>Contoh: 2003-01-31");
   allFill = false; 
  }
  else
  {
   if(du.checkSQLDate(cBeg,out))
   {
     if(!du.checkSQLDateValidity(cBeg,3,out))
     {
      out.println("<BR><B><FONT COLOR=RED>");
      out.println("Class Price effective begin date salah.");
      out.println("Tanggal tidak boleh lebih kurang dari tanggal sekarang</FONT></B>");
      cls.error(3,null);
      allFill = false; 
     }
     else // otherwise the input is all correct
     {  }
   }
   else
   {
     cls.error(3,null);
     allFill = false;
   }
  }

  if(cUFN.compareToIgnoreCase("N")==0)
  {
   if(cEnd.length()!=10)
   {
    cls.error(3,null);
    allFill = false;
   }
   else //verify the validation
   {
    if(du.checkSQLDate(cEnd,out))
    {
      if(!du.checkSQLDateValidity(cEnd,3,out))
      {
	out.println("<BR><FONT COLOR=RED><B>");
        out.println("Class Price effective end date salah.");
        out.println("Tanggal tidak boleh lebih kurang dari tanggal sekarang</b></font>");
        cls.error(3,null);
        allFill = false;	
      }
      else // otherwise the input is correct
      {  }
    }
    else 
    {
      cls.error(3,null);
      allFill = false;
    }
   } // end of else - the validation
  }

  if((!du.checkDigit(cPF,out))||(!du.checkDigit(cSF,out))||(!du.checkDigit(cNet,out))||
    (!du.checkDigit(cPPN,out))||(!du.checkDigit(cPPaid,out))||(!du.checkDigit(cPdRT,out))||
    (!du.checkDigit(cIn,out)))
  {
   cls.error(2,"Input musti berupa digit");
   allFill = false;
  }

  String eTC   = req.getParameter("extraInfoTourCode");  // boleh tidak di-isi
  String eITD  = req.getParameter("extraInfoTktDesignator"); // boleh tidak di-isi
  String eIFT  = req.getParameter("extraInfoFareType"); // boleh tidak di-isi
  String eISN  = req.getParameter("extraInfoSNote");  // boleh tidak di-isi
  
  String cRF   = req.getParameter("classRefundable");
  String cRR   = req.getParameter("classReroutable");
  String cEd   = req.getParameter("classEndorsable");
  String cEx   = req.getParameter("classExtendable");
  String cVal  = req.getParameter("classValidity");
  String cAB   = req.getParameter("classAllowedBag");
  String cEBF  = req.getParameter("classExtraBagFee");
  String cTL   = req.getParameter("cancelTimeLimit");
  String cCF   = req.getParameter("cancelFee");
  String cRRF  = req.getParameter("refundRulesFee");
  String cRSN  = req.getParameter("classRulesSNote"); // boleh tidak di-isi
   
  if((eTC==null)||(eITD==null)||(eIFT==null)||(eISN==null)||(cRRF==null)||
     (cVal==null)||(cAB==null)||(cEBF==null)||(cTL==null)||(cCF==null))
  {
   cls.error(1,null);
   allFill = false;
  }

  if((cVal.length()<1)||(cAB.length()<1)||(cEBF.length()<1)||(cEBF.length()<1)||
     (cTL.length()<1)||(cCF.length()<1)||(cRRF.length()<1))
  {
   out.println("<FONT COLOR=RED><BR><B>");
   out.println("Anda harus mengisi semua kolom di <i>Class Rules</i> secara benar.");
   out.println("<BR>Anda hanya diperbolehkan untuk tidak mengisi <i>");
   out.println("Class Rules Note</i>.</B></FONT>");
   allFill = false; 
  }

  if((!du.checkDigit(cAB,out))||(!du.checkDigit(cEBF,out))||(!du.checkDigit(cTL,out))||
    (!du.checkDigit(cCF,out)))
  {
   cls.error(2,"Input musti berupa digit");
   allFill = false;
  }

  if(!allFill)
  {
   out.println("<FONT COLOR=RED><B><BR>");
   out.println("Harus di-isi ulang dengan lengkap.</BR></B></FONT>");
   cls.makeClosing();
   cls.closing();
   out.close(); 
   return;
  }

  DBEncapsulation dbI = new DBEncapsulation();
  Vector vec = new Vector();
  if(cMNote.length()>1)
  {
   dbI.setDBTableName("airlineMediumNote");
   dbI.setColumnNameVal("mediumNote",cMNote);
   dbI.setVariable("SET @classMNoteID = LAST_INSERT_ID();");
   vec.addElement((DBEncapsulation)dbI.clone());
   dbI.destroyAll();
  }   
  if(eISN.length()>1)
  {
   dbI.setDBTableName("airlineSmallNote");
   dbI.setColumnNameVal("smallNote",eISN);
   dbI.setVariable("SET @extraInfoSNoteID = LAST_INSERT_ID();");
   vec.addElement((DBEncapsulation)dbI.clone());
   dbI.destroyAll();
  }
  if(cRSN.length()>1)
  {
   dbI.setDBTableName("airlineSmallNote");
   dbI.setColumnNameVal("smallNote",cRSN);
   dbI.setVariable("SET @classRulesSNoteID = LAST_INSERT_ID();");
   vec.addElement((DBEncapsulation)dbI.clone());
   dbI.destroyAll();
  }

  dbI.setDBTableName("airlineRouteClass");
  dbI.setColumnNameVal("classRouteKey",cRKey);
  dbI.setColumnNameVal("classCode",cCode);
  dbI.setColumnNameVal("classCodePAX",cCPAX);
  dbI.setColumnNameVal("classPriceCode",cCPri);
  dbI.setColumnNameVal("classPriceBegin",cBeg);
  dbI.setColumnNameVal("classPriceEnd",cEnd);
  dbI.setColumnNameVal("classPriceEndUFN",cUFN);
  dbI.setColumnNameVal("classPublishFare",cPF);
  dbI.setColumnNameVal("classSeelingFare",cSF);
  dbI.setColumnNameVal("classPPN",cPPN);
  dbI.setColumnNameVal("classNet",cNet);
  dbI.setColumnNameVal("classPAXPaid",cPPaid);
  dbI.setColumnNameVal("classPAXPaidRT",cPdRT);
  dbI.setColumnNameVal("classCurrencyKey",cCurr);
  dbI.setColumnNameVal("classInput",cIn);
  if(cMNote.length()>1)
    dbI.setColumnNameVal("classMNote","@classMNoteID");
  dbI.setVariable("SET @airlineRouteClassID = LAST_INSERT_ID();");
  vec.addElement((DBEncapsulation)dbI.clone());
  dbI.destroyAll(); 
  
  if((eTC.length()>1)||(eITD.length()>1)||(eIFT.length()>1)||(eISN.length()>1))
  {
   dbI.setDBTableName("airlineExtraInfo");
   dbI.setColumnNameVal("extraInfoRouteClassKey","@airlineRouteClassID");
   if(eTC.length()>1)
     dbI.setColumnNameVal("extraInfoTourCode",eTC);
   if(eITD.length()>1)
     dbI.setColumnNameVal("extraInfoTktDesignator",eIFT);
   if(eIFT.length()>1)
     dbI.setColumnNameVal("extraInfoFareType",eIFT);
   if(eISN.length()>1)
     dbI.setColumnNameVal("extraInfoSNote","@extraInfoSNoteID");
   vec.addElement((DBEncapsulation)dbI.clone());
   dbI.destroyAll();
  }

  dbI.setDBTableName("airlineClassRules");
  dbI.setColumnNameVal("classRulesRouteKey","@airlineRouteClassID");
  dbI.setColumnNameVal("classRefundable",cRF);
  dbI.setColumnNameVal("classReroutable",cRR);
  dbI.setColumnNameVal("classEndorsable",cEd);
  dbI.setColumnNameVal("classExtendable",cEx);
  dbI.setColumnNameVal("classValidity",cVal);
  dbI.setColumnNameVal("classAllowedBag",cAB);
  dbI.setColumnNameVal("classExtraBagFee",cEBF);
  dbI.setColumnNameVal("classCancellationTime",cTL);
  dbI.setColumnNameVal("classCancellationFee",cCF);
  if(cRSN.length()>1)
   dbI.setColumnNameVal("classRulesSNote","@classRulesSNoteID");
  vec.addElement((DBEncapsulation)dbI.clone());
  dbI.destroyAll();

  if(!dbFill.makeInitialConnection(dbName,out))
   throw new SQLException("Unable to make initial connection");
  dbFill.InsertDatabase(vec,out);
  out.println("<BR><B>Successfully Insert the data</B></BR>");
  if(vec.isEmpty())
  {
   vec.removeAllElements();
   vec = null;
  }
  dbI = null;
  du.clearTime();
  du = null;
 } 

}

