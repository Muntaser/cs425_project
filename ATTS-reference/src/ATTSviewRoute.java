/**
 * ATTSviewRoute class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSviewRoute extends HttpServlet
{
 private DBFill dbFill = null;
 private PrintWriter out = null;
 private String dbName = "ATTSairline";
 private Closure cls = null;
 private ATTSutility util = null;

 public void init() throws ServletException
 {
  dbFill = new DBFill();
  util = new ATTSutility();
 }
 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
 }

 public void doPost(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
 {
  doGet(req,res);
 }

 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS Set & View Route</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  String view = req.getParameter("view");

  if(view == null)
  {
   out.println("<BR>You have set the wrong parameter or view is null: "+view);
   cls.makeClosing();
   cls.closing();
   out.close();
   return;
  }
  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException("<BR>Initial Connection has failed</BR>");
   if(view.compareToIgnoreCase("SET")==0)
     createForm();
   else if(view.compareToIgnoreCase("ALL")==0)
     viewForm();
   else if(view.compareToIgnoreCase("PROCESS")==0)
     processForm(req);
   else
    cls.error(6,null); 
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   //closing all used variables
   cls.closing();
   out.close();
   view = null;
   out = null;
  }
  return;
 } 

/**
 * createForm's method is to create web pages where the user can add the 
 * new route into the database. It passes the user input to ATTSaddRoute 
 * @param none
 * @return none
 */
 public void createForm() throws SQLException
 {
  boolean success = true;
  Vector airTmp = dbFill.QuerySpecific("airline",null,out);
  Vector ctyTmp = dbFill.QuerySpecific("airlineCity",null,out);
  if(airTmp == null)
  {
   cls.error(0,"Belum ada airline didalam database.");
   success = false;
  }
  if(ctyTmp == null)
  {
   cls.error(0,"Belum ada data kota-kota didalam database");
   success = false;
  }
  if(!success)
  {
   cls.error(0,"Harap dimasukan ke database data-data yang diperlukan");
   return;
  }  

  int pos1 = 0;
  int pos2 = 0;
  DBEncapsulation dbR = null;
  StringBuffer strBuf = new StringBuffer();

  out.println("<br><h1>ATTS Set Route</h1></br>");
  out.println("<hr></hr>");
  util.createBegin("ATTSaddRoute",null,null,false,out);
  out.println("<TR><TD>Airline:</TD>");
  out.println("<TD><SELECT NAME=\"airlineID\">");
  out.println("<OPTION SELECTED VALUE=\"-1\">-- Pilih --</OPTION>");
  while(airTmp.size()>0) 
  { 
   dbR=(DBEncapsulation)airTmp.remove(0);
   pos1=dbR.getColumnNamePosition("airlineID");
   pos2=dbR.getColumnNamePosition("airlineName");
   out.println("<OPTION VALUE=\""+dbR.getColumnStringValues(pos1)+"\">");
   out.println(dbR.getColumnStringValues(pos2)+"</OPTION>");
   dbR.destroyAll();
  }
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD>Dari Kota:</TD>");
  out.println("<TD><SELECT NAME=\"orgCity\">");
  out.println("<OPTION SELECTED VALUE=\"NON\">-- Pilih --</OPTION>");
  while(ctyTmp.size()>0) 
  {
   dbR=(DBEncapsulation)ctyTmp.remove(0);
   pos1=dbR.getColumnNamePosition("cityAbbr");
   pos2=dbR.getColumnNamePosition("cityName");
   strBuf.append("<OPTION VALUE=\""+dbR.getColumnStringValues(pos1)+"\">");
   strBuf.append(dbR.getColumnStringValues(pos2)+"</OPTION>"); 
   dbR.destroyAll();
  } 
  out.println(strBuf.toString());
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD>Ke Kota:</TD>");
  out.println("<TD><SELECT NAME=\"destCity\">");
  out.println("<OPTION SELECTED VALUE=\"NON\">-- Pilih --</OPTION>");
  out.println(strBuf.toString());
  out.println("</SELECT></TD></TR>");
  for(int i=1;i<4;i++)
  {
   out.println("<TR><TD>Kota Transit "+i+"</TD>");
   out.println("<TD><SELECT NAME=\"viaCity"+i+"\">");
   out.println("<OPTION SELECTED VALUE=\"NON\">-- Pilih --</OPTION>");
   out.println("<OPTION VALUE=\"NON\">NON-STOP</OPTION>");
   out.println(strBuf.toString());
   out.println("</SELECT></TD></TR>");
  }
  out.println("<TR><TD>Flight Number:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"flightNumber\" MAXLENGTH=16 SIZE=20></TD></TR>");
  out.println("<TR><TD>Plane Type:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"planeType\" MAXLENGTH=16 SIZE=20></TD></TR>");
  out.println("<TR><TD>Terbang Hari:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"flyDay\" MAXLENGTH=7 SIZE=10></TD></TR>");
  out.println("<TR><TD>Jam Terbang:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"ETD\" MAXLENGTH=48 SIZE=40></TD></TR>");
  out.println("<TR><TD>Jam Tiba:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"ETA\" MAXLENGTH=48 SIZE=40></TD></TR>");
  out.println("<TR><TD>Dari Terminal:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"terminal\" MAXLENGTH=3 SIZE=10></TD></TR>");
  out.println("<TR><TD>Route Note:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"routeNote\" MAXLENGTH=64 SIZE=40></TD></TR>");
  util.createEnd(out);
  strBuf.delete(0,strBuf.length());
 }

/**
 * viewForm's method is to create view form where the user can view the content of
 * database airline route. It will feed the user request into the same servlet.
 * The processForm's method is the one that process these viewForm
 * @param none
 * @return none
 */
 public void viewForm() throws SQLException
 {
  boolean success = true;
  Vector rotTmp = null;
  Vector airTmp = null;
  Vector ctyTmp = null;

  airTmp = dbFill.QuerySpecific("airline",null,out);
  if(airTmp == null)
  {
   cls.error(0,"Belum ada airline di dalam database."); 
   success = false;
  }
  ctyTmp = dbFill.QuerySpecific("airlineCity",null,out);
  if(ctyTmp == null)
  {
   cls.error(0,"Belum ada kota-kota di dalam database.");
   success = false;
  }
  rotTmp = dbFill.QuerySpecific("airlineRoute",null,out);
  if(rotTmp == null)
  {
   cls.error(0,"Harap di-isi rute-rute penerbangan.");
   success = false; 
  }
  if(!success)
  {
   if(rotTmp!=null) 
     rotTmp.removeAllElements();
   if(airTmp!=null) 
     airTmp.removeAllElements();
   if(ctyTmp!=null)
     ctyTmp.removeAllElements();
   return;
  }
  DBEncapsulation dbR = null;
  StringBuffer strBuf = new StringBuffer();
  int pos1 = 0;
  int pos2 = 0;
  out.println("<BR></BR>");
  util.createBegin("ATTSviewRoute","view","PROCESS",true,out);
  out.println("<CAPTION><H1>PERIKSA RUTE SECARA</H1></CAPTION>");
  out.println("<TR><TD>Dengan airline:</TD>");   
  out.println("<TD><SELECT NAME=\"airlineID\">");
  out.println("<OPTION SELECTED VALUE=\"-1\">-- Pilih --</OPTION>");
  out.println("<OPTION VALUE=\"0\">Semua airline</OPTION>"); 
  while(airTmp.size()>0)
  {
   dbR = (DBEncapsulation)airTmp.remove(0);
   pos1 = dbR.getColumnNamePosition("airlineID");
   pos2 = dbR.getColumnNamePosition("airlineName");
   out.println("<OPTION VALUE=\""+dbR.getColumnStringValues(pos1)+"\">");
   out.println(dbR.getColumnStringValues(pos2)+"</OPTION>");
   dbR.destroyAll();
  }
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD>Dengan Kota Asal:</TD>");
  out.println("<TD><SELECT NAME=\"orgCity\">");  
  out.println("<OPTION SELECTED VALUE=\"NON\">-- Pilih --</OPTION>");
  out.println("<OPTION VALUE=\"ALL\">Semua Kota</OPTION>");
  while(ctyTmp.size()>0)
  {
   dbR = (DBEncapsulation)ctyTmp.remove(0);
   pos1= dbR.getColumnNamePosition("cityAbbr");
   pos2= dbR.getColumnNamePosition("cityName");
   strBuf.append("<OPTION VALUE=\""+dbR.getColumnStringValues(pos1)+"\">");
   strBuf.append(dbR.getColumnStringValues(pos2)+"</OPTION>");
   dbR.destroyAll();
  }
  out.println(strBuf.toString());
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD>Dengan Kota Tujuan:</TD>");
  out.println("<TD><SELECT NAME=\"destCity\">");
  out.println("<OPTION SELECTED VALUE=\"NON\">-- Pilih --</OPTION>");
  out.println("<OPTION VALUE=\"ALL\">Semua Kota</OPTION>");
  out.println(strBuf.toString());
  out.println("</SELECT></TD></TR>");
  util.createEnd(out); 
 } // end of viewForm()

/**
 * processForm's is to process user submitted data from viewForm()'s method above.
 * It does not do the data verification with the existed airlineCity's data in db
 * because it assumed the data given has been verify before being submitted.
 * @param HttpServletRequest req
 * @return none
 */
 public void processForm(HttpServletRequest req) 
	throws ServletException,SQLException
 {
  int airKey = Integer.parseInt(req.getParameter("airlineID"));
  String orgCity = req.getParameter("orgCity");
  String destCity= req.getParameter("destCity");
  if((orgCity==null)||(destCity==null))
  {
   cls.error(1,"airlineID/original city/destination city");
   return;
  }

  if((airKey<0)&&(orgCity.compareToIgnoreCase("NON")==0)&&
     (destCity.compareToIgnoreCase("NON")==0))  
  {
   cls.error(2,null);
   return;
  }

  //3 variables 3 types of operations = 27 possible combinations
  if((airKey<0)&&(orgCity.length()!=3)&&(destCity.length()!=3))
    throw new SQLException("processForm should not have this value");
     
  StringBuffer str = new StringBuffer();
  int oCity = 0; // -1 = NON, 1 = FILL, 0 = ALL
  int dCity = 0; // -1 = NON, 1 = FILL, 0 = ALL
  if(orgCity.compareToIgnoreCase("NON")==0)
   oCity = -1;
  else if(orgCity.compareToIgnoreCase("ALL")!=0)
   oCity = 1;
  if(destCity.compareToIgnoreCase("NON")==0)
   dCity = -1;
  else if(destCity.compareToIgnoreCase("ALL")!=0)
   dCity = 1;

  //search all possible result from airlineRoute
  if((airKey==0)&&(oCity==0)&&(dCity==0)) { }
  else if((airKey>0)&&(oCity>0)&&(dCity>0)) //search specific of all 3
  {
   str.append("WHERE routeAirlineID=\""+airKey+"\" AND routeOrig=\""+orgCity);
   str.append("\" AND routeDest=\""+destCity+"\";");
  }
  else if((airKey>0)&&(oCity>0)&&(dCity==0))
   str.append("WHERE routeAirlineID=\""+airKey+"\" AND routeOrig=\""+orgCity+"\";");
  else if((airKey>0)&&(oCity>0)&&(dCity<0))
   str.append("WHERE routeAirlineID=\""+airKey+"\" AND routeOrig=\""+orgCity+"\";");
  else if((airKey>0)&&(oCity==0)&&(dCity>0))
   str.append("WHERE routeAirlineID=\""+airKey+"\" AND routeDest=\""+destCity+"\";");
  else if((airKey>0)&&(oCity==0)&&(dCity==0))
   str.append("WHERE routeAirlineID=\""+airKey+"\";");
  else if((airKey>0)&&(oCity==0)&&(dCity<0))
   str.append("WHERE routeAirlineID=\""+airKey+"\";");
  else if((airKey>0)&&(oCity<0)&&(dCity>0))
   str.append("WHERE routeAirlineID=\""+airKey+"\" AND routeDest=\""+destCity+"\";");
  else if((airKey>0)&&(oCity<0)&&(dCity==0))
   str.append("WHERE routeAirlineID=\""+airKey+"\";");
  else if((airKey>0)&&(oCity<0)&&(dCity<0))
   str.append("WHERE routeAirlineID=\""+airKey+"\";");
  else if((airKey==0)&&(oCity>0)&&(dCity>0))
   str.append("WHERE routeOrig=\""+orgCity+"\" AND routeDest=\""+destCity+"\";");
  else if((airKey==0)&&(oCity>0)&&(dCity==0))
   str.append("WHERE routeOrig=\""+orgCity+"\";");
  else if((airKey==0)&&(oCity>0)&&(dCity<0))
   str.append("WHERE routeOrig=\""+orgCity+"\";");
  else if((airKey==0)&&(oCity==0)&&(dCity>0))
   str.append("WHERE routeDest=\""+destCity+"\";");
  else if((airKey==0)&&(oCity==0)&&(dCity<0)) { }
  else if((airKey==0)&&(oCity<0)&&(dCity>0))
   str.append("WHERE routeDest=\""+destCity+"\";");
  else if((airKey==0)&&(oCity<0)&&(dCity==0)) { }
  else if((airKey==0)&&(oCity<0)&&(dCity<0)) { }
  else if((airKey<0)&&(oCity>0)&&(dCity>0))
   str.append("WHERE routeOrig=\""+orgCity+"\" AND routeDest=\""+destCity+"\";");
  else if((airKey<0)&&(oCity>0)&&(dCity==0))
   str.append("WHERE routeOrig=\""+orgCity+"\";");
  else if((airKey<0)&&(oCity>0)&&(dCity<0))
   str.append("WHERE routeOrig=\""+orgCity+"\";");
  else if((airKey<0)&&(oCity==0)&&(dCity>0))
   str.append("WHERE routeDest=\""+destCity+"\";");
  else if((airKey<0)&&(oCity==0)&&(dCity==0)) { }
  else if((airKey<0)&&(oCity==0)&&(dCity<0)) { }
  else if((airKey<0)&&(oCity<0)&&(dCity>0))
   str.append("WHERE routeDest=\""+destCity+"\";");
  else if((airKey<0)&&(oCity<0)&&(dCity==0)) { }

  Vector tmpVec = null;
  if(str.length()>1)
   tmpVec = dbFill.QuerySpecific("airlineRoute",str.toString(),out);
  else
   tmpVec = dbFill.QuerySpecific("airlineRoute",null,out);
  if(tmpVec==null)
   cls.error(0,"Belum ada rute yang anda cari di-database");
  else
   dbFill.viewContainer(tmpVec,out);
  str.delete(0,str.length()); 
  return; 
 } // end of processForm 

} // end of ATTSviewRoute.java

