/**
 * ATTSsetEmpClock class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSsetEmpClock extends HttpServlet
{
 private DBFill dbFill = null;
 private String dbName = "ATTSemployee";
 private PrintWriter out = null;
 private Closure cls = null;
 private ATTSutility util = null;

/**
 * init's method is to initialize the servlet
 * @param none
 * @return none
 */
 public void init() throws ServletException
 {
  String driver = "com.mysql.jdbc.Driver";
  String url = "jdbc:mysql://starcloud:3306/";
  String user= "edisonch";
  String pwd = "gblj21mysql03";
  dbFill = new DBFill(driver,url,user,pwd);
  util = new ATTSutility();
 }

/**
 * destroy's method is to end the life-cycle of the servlet
 * @param none
 * @return none
 */
 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
  dbFill = null;
 }

/**
 * doGet's method is to process GET method from user
 * @param HttpServletRequest req
 *        HttpServletResponse res
 * @return none
 */
 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 }

/**
 * doPost's method is to process POST method from user
 * @param HttpServletRequest req contains user request
 *        HttpServletResponse res contains user respond
 * @return none
 */
 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS Set Employee Clock</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
 
  String empID = req.getParameter("empID");
  String empPK = req.getParameter("empPK");
  int eAction  = Integer.parseInt(req.getParameter("clockAction"));

  if((empID==null)||(empPK==null))
  {
   cls.error(1,"Employee ID atau/dan Employee PassKey");
   return; 
  }
 
  if((empID.length()<1)&&(empPK.length()<1)&&(eAction<0))
  {
   cls.error(2,null);
   return;
  }
  DateUtil du = new DateUtil();
  if((!du.checkDigit(empID,out))||(!du.checkDigit(empPK,out))) 
  {
   cls.error(0,"Employee ID dan/atau Employee PassKey anda tidak berupa angka/digit");
   return;
  }

  int eID = Integer.parseInt(empID);
  int ePK = Integer.parseInt(empPK);
  DBEncapsulation r = null;
  
  Vector v = new Vector();
  StringBuffer msg = new StringBuffer();

  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException("<BR>Failure to make initial connection</BR>");
   if(!util.verifyAuthorization(dbFill,cls,out,eID,empPK,null,3,null))
   {
    cls.error(0,"Employee ID dan Employee PassKey tidak cocok.");
    cls.error(0,"Atau employee ID anda tidak valid lagi. Cobalah lagi");
    cls.error(0,"Kalau masih gagal, harap hubungi System Admin anda");
    return;
   }
   if(eAction==1)
   {
//    msg.append("WHERE empClockOfID="+eID+" AND empClockOut=00:00:00;");
//    r = dbFill.QuerySingleSpecific("employeeViolatedClock",msg.toString(),out); 
    r = new DBEncapsulation("employeeViolatedClock");
    r.setColumnNameVal("empClockIn","CURRENT_TIME");
    r.setColumnNameVal("empClockDate","CURRENT_DATE");
    r.setColumnNameVal("empClockOfID",eID);
    v.addElement((DBEncapsulation)r.clone());
    dbFill.InsertDatabase(v,out); 
    out.println("<BR><B>Successfully Clock in</B></BR>");
   }
   else
   {
    r = new DBEncapsulation("employeeViolatedClock");
    r.setColumnNameVal("empClockOut","CURRENT_TIME");
    r.setCondition("WHERE empClockOfID="+eID);
    v.addElement((DBEncapsulation)r.clone());
    dbFill.UpdateDatabase(v,out);
    out.println("<BR><B>Successfully Clock out</B></BR>");
   }
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out);
  }
  finally
  {
   out.close();
   cls.closing();
   r.destroyAll();
   if(!v.isEmpty())
     v.removeAllElements();
  }  
 } // end of doPost 

}

