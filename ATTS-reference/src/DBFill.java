package ATTS;

import java.io.*;
import java.util.*;
import java.sql.*;

public class DBFill implements DatabaseFill 
{
 private boolean startFlag = false;
 private int orgLength = 0;
 private boolean successFlag;
 private StringBuffer url = null;
 private StringBuffer driver = null;
 private StringBuffer user = null;
 private StringBuffer password = null;
 private final static int SP_VAR = 99;
 private String defaultDriver = "com.mysql.jdbc.Driver";
 private String defaultUser   = "edisonch";
 private String defaultUrl    = "jdbc:mysql://starcloud:3306/";
 private String defaultPwd    = "gblj21mysql03"; 
 
 public DBFill(String newDriver, String newURL, 
               String newUser, String newPassword)
 {
  driver   = new StringBuffer(newDriver);
  url      = new StringBuffer(newURL);
  user     = new StringBuffer(newUser);
  password = new StringBuffer(newPassword);
  orgLength = url.length();
 }

 public DBFill()
 {
  driver   = new StringBuffer(defaultDriver);
  url      = new StringBuffer(defaultUrl);
  user     = new StringBuffer(defaultUser);
  password = new StringBuffer(defaultPwd);
  orgLength = url.length();
 }
 
 private StringBuffer findAndReplace(int orgLen,StringBuffer val, String newVal)
 {
  if(val.length() == orgLen)
  {
    val.append(newVal);
    return val;
  }
  if(val.indexOf(newVal,orgLen) == -1)
  {
   val.delete(orgLen,val.length());
   val.append(newVal);
  }
  return val;
 }

 public boolean makeInitialConnection(String DBName,PrintWriter out)
 { 
  successFlag = true;
  url = findAndReplace(orgLength,url,DBName); 
 
  try
  {
   new ATTS.JDCConnectionDriver(driver.toString(),url.toString(),user.toString(),
				password.toString());
   startFlag = true;
  }
  catch(Exception ex)
  { 
   out.println("<BR><FONT COLOR=RED><B>ERROR (makeInitialConnection)</B></FONT></BR>");
   out.println("<BR>");
   ex.printStackTrace(out);
   out.println("</BR>");
   successFlag = false;
  }
  return successFlag;
 } 

 public void closeDatabase()
 {
  if(!startFlag)
   return;
  startFlag = false;
  url.delete(0,url.length());
  driver.delete(0,driver.length());
  user.delete(0,user.length());
  password.delete(0,password.length());
  orgLength = 0;
 }

 public Connection getConnection() throws SQLException
 {
  if(!startFlag)
    throw new SQLException("You have not start DBFill");
  return DriverManager.getConnection("jdbc:jdc:jdcpool");
 }

 /**
  * UpdateDatabase's method is to update certain data in
  * the Database table. It uses Vector that contains
  * DBEncapsulation object to do its work. It forms the SQL
  * syntax in this method to update. Upon successful operation
  * it returns true else false
  * MySQL syntax = UPjava.sql.Types.DATE table_name SET colName1=colVal1, colName2=colVal2
  *                WHERE condition 
  * @param  Vector vec, PrintWriter out
  * @return boolean true if update operation is successful 
  */
 public boolean UpdateDatabase(Vector vec,PrintWriter out) throws SQLException
 {
  int vecSize = vec.size();
  int counter = 0;
  int outerCounter = 0;
  int colSize = 0;
  StringBuffer data = new StringBuffer();
  Statement stmt = null;
  DBEncapsulation dbEncap = null;
  Connection dbConn = null;
  Enumeration dbColName = null;
  Enumeration dbColValue= null;
  boolean successFlag = true;
 
  try
  {
    if(!startFlag)
      throw new SQLException("You have not make the initial connection to database");
    dbConn = getConnection();
    dbConn.setAutoCommit(false); 
    stmt = dbConn.createStatement();
    for(;outerCounter<vecSize;outerCounter++)
    {
      dbEncap = (DBEncapsulation)vec.get(counter);
      colSize = dbEncap.getSizeColumnName();
      dbColName = dbEncap.getAllColumnNames();
      dbColValue= dbEncap.getAllColumnValues();
      data.append("UPDATE " + dbEncap.getDBTableName() + " SET "); 
      while(dbColName.hasMoreElements())
      {
        data.append((String)dbColName.nextElement()+"=");
        data.append((String)dbColValue.nextElement());
        counter++;
        if(counter != colSize)
          data.append(","); 
        else
          data.append(" ");
      } // end of while-loop
      if(dbEncap.getConditionFlag() != 0)
        data.append(dbEncap.getCondition());
      else
        data.append(";");
      stmt.executeUpdate(data.toString());
      data.delete(0,data.length());
      stmt.clearBatch();
    } // end of for-loop
    dbConn.commit();
  }
  catch(SQLException sqle)
  {
    dbConn.rollback();
    throw sqle;
  }
  catch(Exception ex)
  {
   dbConn.rollback();
   successFlag = false;
  }
  finally
  {
    stmt.close();
    dbConn.close();
    data.delete(0,data.length());
    dbColName = null;
    dbColValue= null;
    data = null; 
    stmt = null;
  }
  return successFlag;
 }

 /**
  * InsertDatabase's method is to insert data into database.
  * It uses Vector that contains DBEncapsulation class.
  * @param  Vector vec, PrintWriter out
  * @return true if successful else false
  */
 public void InsertDatabase(Vector vec, PrintWriter out) throws SQLException
 {
  int vecSize = vec.size();
  int running = 0;
  int repeat  = 0;
  int sum = 0;
  Connection dbConn = null;
  StringBuffer data = new StringBuffer();
  Statement stmt = null;
  DBEncapsulation dbEncap = null;

  try
  {
    if(!startFlag)
      throw new SQLException("You have not make initial connection to database");
    dbConn = getConnection();
    dbConn.setAutoCommit(false);
    stmt = dbConn.createStatement();

    for(int outerCounter=0;outerCounter<vecSize;outerCounter++)
    {
      dbEncap = (DBEncapsulation)((DBEncapsulation)vec.get(outerCounter)).clone();
      data.append("INSERT INTO " + dbEncap.getDBTableName() + " (");
      repeat = dbEncap.getRepeatFlag();
      if(repeat != 0)
        running = repeat;
      else
        running = dbEncap.getSizeColumnName();

      for(int inCounter=0;inCounter<running;inCounter++)
      {
        data.append(dbEncap.getColumnNames(inCounter));
        if(inCounter+1 == running)
          data.append(") ");
        else
          data.append(",");
      }
      data.append("VALUES (");
      running = dbEncap.getSizeColumnValue();
      for(int counter=0;counter<running;counter++)
      {
       switch(dbEncap.getColumnType(counter))
       {
         case java.sql.Types.VARCHAR : 
           data.append("\"" + dbEncap.getColumnStringValue(counter) + "\""); 
           break;
         case java.sql.Types.INTEGER : 
           data.append(dbEncap.getColumnIntegerValue(counter)); 
       	   break;
     	 case java.sql.Types.FLOAT  : 
           data.append(dbEncap.getColumnFloatValue(counter));
	   break;
     	 case java.sql.Types.DOUBLE : 
           data.append(dbEncap.getColumnDoubleValue(counter));
	   break;
	 case java.sql.Types.TIME   :
         case java.sql.Types.DATE   :
         case SP_VAR : 
           data.append(dbEncap.getColumnStringValue(counter));
           break;
         default     : throw new SQLException("default value");
       }
       if(repeat != 0)
         sum = (counter+1)%repeat;
       if((repeat != 0) && (sum == 0))
         data.append(")");
       if((counter+1 == running) && (repeat != 0))
         data.append(";");
       else if((counter+1 == running) && (repeat == 0))
         data.append(");");
       else if((sum==0) && (repeat != 0))
         data.append(",(");
       else
         data.append(",");
      }//inner for-loop 
//      out.println("<BR>Data : "+data.toString());
      stmt.executeUpdate(data.toString());
      data.delete(0,data.length()); 
      if(dbEncap.getVariableFlag() != 0)
      {
        data.append(dbEncap.getVariable());
//	out.println("<BR>Append Variable, DATA: "+data.toString());
        stmt.executeUpdate(data.toString());
        data.delete(0,data.length());
      }
      dbEncap.destroyAll();
  }
  dbConn.commit();
 }
 catch(SQLException sqle)
 {
   dbConn.rollback(); 
   throw sqle;
 }
 finally
 {
   stmt.close();
   dbConn.close();
   dbConn = null;
   stmt = null;
 } 
}

 /**
  * DeleteDatabase's method is to delete Database certain
  * data from the table. It takes Vector that contains
  * DBEncapsulation object in it. It forms SQL syntax in here
  * and return boolean true if successful else false
  * MySQL syntax: DELETE FROM tableName WHERE condition
  * @param  Vector vec, PrintWriter out
  * @return true if successful else return false
  */
 public void DeleteDatabase(Vector vec,PrintWriter out) throws SQLException
 {
   int vecSize = vec.size();
   Statement stmt = null;
   Connection dbConn = null;
   StringBuffer data = new StringBuffer();
   DBEncapsulation dbEncap = null; 
   try
   {
    if(!startFlag)
      throw new SQLException("You have not make initial connection to database");
    dbConn = getConnection();
    dbConn.setAutoCommit(false);
    stmt = dbConn.createStatement();
    for(int counter=0;counter<vecSize;counter++)
    {
     dbEncap = (DBEncapsulation)vec.get(counter);
     data.append("DELETE FROM " + dbEncap.getDBTableName());
     if(dbEncap.getConditionFlag() != 0)
      data.append(" " + dbEncap.getCondition());
     stmt.executeUpdate(data.toString());
     data.delete(0,data.length()); 
     stmt.clearBatch();
    } // end of for-loop
    dbConn.commit();
   }
   catch(SQLException sqle)
   {
    dbConn.rollback();
    throw sqle;
   }
   finally
   {
    stmt.close(); 
    dbConn.close();
    dbConn = null;
    stmt = null;
   }
 }

/**
 * QueryDatabase's method is to do general query to Database.
 * It takes parameters Vector and PrintWriter. Vector has 
 * to contain DBEncapsulation class in it. DBEncapsulation
 * helps the operation of Query. The SQL syntax is form in here
 * and these method returns ResultSet
 * MySQL syntax: SELECT colName1,colName2 FROM tableName WHERE condition;
 * @param  Vector vec, PrintWriter out
 * @return Vector of ResultSet
 */

 public Vector QueryDatabase(DBEncapsulation dbEncap,PrintWriter out) 
	throws SQLException
 {
   if(dbEncap == null)
    throw new SQLException("dbEncap is null");
   int counter = 0; 
   int colNameSize = 0;
   int outerCounter = 0; 
   ResultSet rs = null;
   Statement stmt = null;
   Connection dbConn = null;
   StringBuffer data = new StringBuffer();
   DBEncapsulation dbResult = new DBEncapsulation(); 
   ResultSetMetaData rsmd = null;
   Vector resultVec = new Vector();
   Enumeration dbColName = null;

   try
   { 
     if(!startFlag)
       throw new SQLException("You have not make initial connection to database");
     dbConn = getConnection();
     dbConn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
     stmt = dbConn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                                   ResultSet.CONCUR_UPDATABLE);   
     stmt = dbConn.createStatement();
     dbColName = dbEncap.getAllColumnNames();
     colNameSize = dbEncap.getSizeColumnName();
     data.append("SELECT ");

     while(dbColName.hasMoreElements())
     {
       data.append((String)dbColName.nextElement());
       counter++;
       if(counter < colNameSize) 
          data.append(",");
       else
          data.append(" ");
     } // end of while-loop 
     data.append("FROM " + dbEncap.getDBTableName());
     if(dbEncap.getConditionFlag() != 0)
       data.append(" " + dbEncap.getCondition()); 
     else
       data.append(";"); 

     // execute Query
     rs = stmt.executeQuery(data.toString());
     if(rs == null)
       return null;
     int colSize = 0;
     while(rs.next())
     {
       rsmd = rs.getMetaData();
       colSize = rsmd.getColumnCount();
       dbResult.setDBTableName(rsmd.getTableName(1));
       for(int count=0;count<colSize;count++)
       {
         dbResult.setColumnName(rsmd.getColumnName(count+1));
         switch(rsmd.getColumnType(count+1))
         {
          case java.sql.Types.CHAR    :
	  case java.sql.Types.VARCHAR :
	   if((rs.getString(count+1)).length()>0) 
            dbResult.setColumnValue(rs.getString(count+1));
           else
            dbResult.setColumnValue(" ");
       	   break;
          case java.sql.Types.SMALLINT:
	  case java.sql.Types.TINYINT :
	  case java.sql.Types.INTEGER : 
	   dbResult.setColumnValue(rs.getInt(count+1));
           break;
          case java.sql.Types.FLOAT   : 
	   dbResult.setColumnValue(rs.getFloat(count+1));
	   break;
	  case java.sql.Types.DOUBLE  : 
	   dbResult.setColumnValue(rs.getDouble(count+1));
	   break;
          case java.sql.Types.TIME: 
	   dbResult.setColumnValue(rs.getTime(count+1).toString());
           break;
          case java.sql.Types.DATE: 
	   dbResult.setColumnValue(rs.getDate(count+1).toString());
           break;
          default: 
	   throw new SQLException("Received a default value error");
         }
       } // end of for-loop

       data.delete(0,data.length());
       resultVec.addElement(dbResult.clone());
       dbResult.clearAll();
     } // end of while(rs.next());
   }
   catch(SQLException sqle)
   {
    throw sqle;
   }
   finally
   {
    dbConn.close();
    stmt.close();
    data.delete(0,data.length());
    rs = null;
    rsmd = null;
    data = null;
    dbConn = null;
    stmt = null;
   }
   return resultVec;
 } 

 public DBEncapsulation QuerySingleResult(DBEncapsulation dbEncap,PrintWriter out)
	throws SQLException
 {
   if(dbEncap == null)
     return null;
   int colSize = 0;
   ResultSet rs = null;
   Statement stmt = null;
   boolean success = false;
   Connection dbConn = null;
   ResultSetMetaData rsmd = null;
   DBEncapsulation dbResult = null;
   StringBuffer data = new StringBuffer();

   try
   {
     if(!startFlag)
       throw new SQLException("You have not make initial connection to database");
     dbConn = getConnection();
     dbConn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
     stmt = dbConn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                                   ResultSet.CONCUR_UPDATABLE);
     stmt = dbConn.createStatement();
     colSize = dbEncap.getSizeColumnName();
     data.append("SELECT ");
     for(int an=0;an<colSize;an++)
     {
      data.append((String)dbEncap.getColumnNames(an));
      if(an+1<colSize)
        data.append(",");
      else
        data.append(" ");
     } 
     data.append("FROM " + dbEncap.getDBTableName());
     if(dbEncap.getConditionFlag() != 0)
       data.append(" " + dbEncap.getCondition());
     else
       data.append(";");
     rs = stmt.executeQuery(data.toString());
     if(rs == null)
       return null; 
     while(rs.next())
     {
       rsmd = rs.getMetaData();
       colSize = rsmd.getColumnCount();
       dbResult.setDBTableName(rsmd.getTableName(1));
       for(int cn=0;cn<colSize;cn++)
       {
         dbResult.setColumnName(rsmd.getColumnName(cn+1));
         switch(rsmd.getColumnType(cn+1))
         {
	   case java.sql.Types.CHAR   :
           case java.sql.Types.VARCHAR: 
	    if((rs.getString(cn+1)).length()>0) 
              dbResult.setColumnValue(rs.getString(cn+1));
            else
              dbResult.setColumnValue(" ");
            break;
           case java.sql.Types.SMALLINT:
           case java.sql.Types.TINYINT :
           case java.sql.Types.INTEGER : 
	    dbResult.setColumnValue(rs.getInt(cn+1));
    	    break;
           case java.sql.Types.FLOAT   : 
	    dbResult.setColumnValue(rs.getFloat(cn+1));
	    break;
           case java.sql.Types.DOUBLE  : 
	    dbResult.setColumnValue(rs.getDouble(cn+1));
            break;
           case java.sql.Types.DATE : 
	    dbResult.setColumnValue(rs.getDate(cn+1).toString());
            break;
           case java.sql.Types.TIME : 
	    dbResult.setColumnValue(rs.getTime(cn+1).toString());
	    break;
           default : throw new SQLException("rsmd.getColumnType default");
         }
       }
     } // end of while-loop
   }
   catch(SQLException sqle)
   {
    throw sqle;
   }
   finally
   {
     dbConn.close();
     stmt.close();
     data.delete(0,data.length());
     rsmd = null;
     rs = null;
     dbConn = null;
     data = null;
     stmt = null; 
   }
   return dbResult;
 }

 public void viewContainer(Vector vec, PrintWriter out) throws SQLException 
 {
  int vecSize = vec.size();
  int dbColNameLength = 0;
  DBEncapsulation dbEx = null;
  out.println("<TABLE BORDER=1 WIDTH=\"75%\">");
  for(int cn=0;cn<vecSize;cn++)
  {
   dbEx = (DBEncapsulation)((DBEncapsulation)vec.get(cn)).clone();
   if(dbEx == null)
   {
    out.println("<BR>dbEx = null</BR>");
    break;
   }
   viewEncap(dbEx,out);
   dbEx.destroyAll();
  }
  out.println("</TABLE>");
 }

 public void viewEncap(DBEncapsulation encap, PrintWriter out) throws SQLException
 {
  if(encap == null)
  {
   out.println("<BR><FONT COLOR=RED>Encap is null</FONT></BR>");
   return;
  }

  out.println("<TABLE BORDER=1 WIDTH=\"75%\">");
  out.println("<CAPTION><H1><TT>"+encap.getDBTableName()+"</TT></H1></CAPTION>");
  int colSize = encap.getSizeColumnName();
  for(int a=0;a<colSize;a++)
  {
   out.println("<TR>"); 
   out.println("<TD>"+a+"</TD>");
   out.println("<TD>"+encap.getColumnNames(a)+"</TD>");
   out.println("<TD>"+encap.getColumnStringValues(a)+"</TD>");
   if(encap.getVariableFlag()!=0)
     out.println("<TD>Variable:</TD><TD>"+encap.getVariable()+"</TD>");
   if(encap.getConditionFlag()!=0)
     out.println("<TD>Condition:</TD><TD>"+encap.getCondition()+"</TD>");
   out.println("</TR>");
  } 
  out.println("</TABLE>");
 }

 public Vector QuerySpecific(String tableName,String cond,PrintWriter out) 
	throws SQLException
 {
  if(!startFlag)
    throw new SQLException("<B><BR>You have to make initial connection to database</B>");
  if((tableName==null)||(tableName.length()<1))
    return null;
  DBEncapsulation dbQ = new DBEncapsulation(tableName);
  dbQ.setColumnName("*");
  if(cond!=null)
    dbQ.setCondition(cond);
  return QueryDatabase(dbQ,out);
 }

 public DBEncapsulation QuerySingleSpecific(String tableName,String cond,PrintWriter out)
	throws SQLException
 {
  if(!startFlag)
    throw new SQLException("<B><BR>You have to make initial connection to database</B>");
  if((tableName==null)||(tableName.length()<1))
    return null;
  DBEncapsulation dbQ = new DBEncapsulation(tableName);
  dbQ.setColumnName("*");
  if(cond!=null)
    dbQ.setCondition(cond);
  return QuerySingleResult(dbQ,out);
 }

 public Vector merge(Vector v,DBEncapsulation dbE,String name,String val) 
 	throws SQLException
 {
  if(v == null)
    return v;
  if((dbE == null)||(name==null)||(val==null))
    return v;
  DBEncapsulation r = null;
  int size = v.size();
  int pos1 = 0;
  int pos2 = 0;
  for(int i=0;i<size;i++)
  {
   r=(DBEncapsulation)((DBEncapsulation)v.get(i)).clone();
   pos1 = r.getColumnNamePosition(name);
   if((pos1!=r.NOT_FOUND)||(pos1!=r.SAME_SIZE)||(pos1!=r.OUT_RANGE))
   {
    if(r.getColumnStringValues(pos1).compareToIgnoreCase(val)==0)
    {
     r.mergeEncap(dbE);
     v.removeElementAt(i);
     v.insertElementAt(r.clone(),i);
     break;
    }
   }
   r.destroyAll();
  }
  return v;
 } // end of merge's method

} //end of DBFill's class
