package ATTS;

import java.sql.*;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.Vector;
import java.io.*;
import java.util.Enumeration;

public class ATTSaddRoute extends HttpServlet
{
 private Closure cls = null;
 private DBFill dbFill = null;
 private PrintWriter out = null;
 private ATTSutility util = null;
 private String dbName = "ATTSairline";

 public void init() throws ServletException
 {
  dbFill = new DBFill();
  util = new ATTSutility();
 } 

 public void destroy()
 {
  dbFill.closeDatabase(); 
  util.close();
 }

 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 }

 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  res.setContentType("text/html");  
  cls = new Closure(out);

  try
  {
   process(req);
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
  }
 }


/**
 * process' method is to process user request to add airline route.
 * @param HttpServletReqeust req contains user request
 * @return none
 */
 private void process(HttpServletRequest req) throws SQLException
 {
  out.println("<HTML><HEAD><TITLE>ATTS Register Route</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<BR></BR>");
  out.println("<H1>ATTS Add Route</H1>");
  out.println("<hr></hr><BR></BR>");

  int routeAirline = Integer.parseInt(req.getParameter("airlineID"));
  String routeOrig = req.getParameter("orgCity");
  String routeDest = req.getParameter("destCity");
  String routeFlightNum = req.getParameter("flightNumber");
  String routePlaneType = req.getParameter("planeType");
  String routeTerminal  = req.getParameter("terminal");
  String routeDay = req.getParameter("flyDay");
  String routeETD = req.getParameter("ETD");
  String routeETA = req.getParameter("ETA");
  String airNote = req.getParameter("routeNote");
  String routeViaSN1 = req.getParameter("viaCity1");  
  String routeViaSN2 = req.getParameter("viaCity2");  
  String routeViaSN3 = req.getParameter("viaCity3");  

  if((routeFlightNum==null)||(routePlaneType==null)||(routeDay==null)||
     (routeETD==null)||(routeETA==null)||(routeTerminal==null)||(routeViaSN1==null)||
     (routeViaSN2==null)||(routeViaSN3==null))
  {
   cls.error(1,null);
   return; 
  }
  if((routeOrig.compareToIgnoreCase("NON")==0)||(routeDest.compareToIgnoreCase("NON")==0)||
    (routeViaSN1.compareToIgnoreCase("NON")==0)||(routeAirline<0)||
    (routeViaSN2.compareToIgnoreCase("NON")==0)||(routeViaSN3.compareToIgnoreCase("NON")==0))
  {
   cls.error(2,null);
   return;
  }
  StringBuffer routeViaSN = new StringBuffer();
  if(routeViaSN1.compareToIgnoreCase("NON")!=0)
   routeViaSN.append(routeViaSN1);
  if(routeViaSN2.compareToIgnoreCase("NON")!=0)
   routeViaSN.append("-"+routeViaSN2);
  if(routeViaSN3.compareToIgnoreCase("NON")!=0)
   routeViaSN.append("-"+routeViaSN3);

  //check validity of the entered route
  boolean flag = checkValidity(routeOrig,routeDest,routeViaSN.toString());
  if(!flag)
  {
   cls.error(0,"Route Original/Destination/Via Tidak boleh semua sama");
   return;
  }

  Vector dbVec = new Vector();
  DBEncapsulation dbEncap = new DBEncapsulation("airlineRouteDT");
  dbEncap.setColumnNameVal("routeDay",Integer.parseInt(routeDay));
  dbEncap.setColumnNameVal("routeETD",routeETD);
  dbEncap.setColumnNameVal("routeETA",routeETA);
  dbEncap.setColumnNameVal("routeOfAirline",routeAirline);
  dbEncap.setVariable("SET @routeDTKey = LAST_INSERT_ID();");
  dbVec.addElement(dbEncap.clone());
  dbEncap.clearAll();

  if(airNote.length()>1)
  {
   dbEncap.setDBTableName("airlineMediumNote");
   dbEncap.setColumnNameVal("mediumNote",airNote);
   dbEncap.setVariable("SET @routeNote = LAST_INSERT_ID();");
   dbVec.addElement(dbEncap.clone());
   dbEncap.clearAll();
  }
  
  dbEncap.setDBTableName("airlineRoute");
  dbEncap.setColumnNameVal("routeOrig",routeOrig);
  dbEncap.setColumnNameVal("routeDest",routeDest);
  if(routeViaSN.length()>1)
    dbEncap.setColumnNameVal("routeViaSN",routeViaSN.toString());
  dbEncap.setColumnNameVal("routeAirlineID",routeAirline);
  dbEncap.setColumnNameVal("routeFlightNum",routeFlightNum);
  dbEncap.setColumnNameVal("routePlaneType",routePlaneType);
  dbEncap.setColumnNameVal("routeDTKey","@routeDTKey");
  if(airNote.length()>1)
    dbEncap.setColumnNameVal("routeMNote","@routeNote");
  dbEncap.setColumnNameVal("routeTerminal",routeTerminal);
  dbVec.addElement(dbEncap.clone());
  dbEncap.destroyAll();

  if(!dbFill.makeInitialConnection(dbName,out))
   throw new SQLException("Fail to make initial Connection to database");
  dbFill.InsertDatabase(dbVec,out);
  out.println("Finish inserting the new route");

  routeViaSN.delete(0,routeViaSN.length());
  if(!dbVec.isEmpty())
    dbVec.removeAllElements();
  routeFlightNum = null;
  routePlaneType = null;
  routeTerminal  = null;
  routeDay       = null;
  routeETD       = null;
  routeETA       = null;
  airNote        = null;
  routeViaSN1    = null;
  routeViaSN2    = null;
  routeViaSN3    = null;
  routeViaSN     = null;
 }

/**
 * checkValidity's method is to check validity of the route (dest,original,via).
 * Especially in via's section - in current DB, VIA is 16 characters long. It can
 * contain 4 different destinations. Each destination is 3 character long. Excluding
 * one separation character in each city name. Example JKT-MDN-NAD. It will check
 * for any similiar city name. It does not allow the same name. For Round-Trip
 * flight such as JKT-MDN-JKT, it will be written as JKT-MDN and flightType: RT.
 * @param String org - original city
 *        String dest- destination city
 *        String via - transit cities [up to 4 cities MAX]
 * @return false if found similiar city name
 */
 private boolean checkValidity(String orig,String dest,String via)
 {
  if(orig.compareToIgnoreCase(dest)==0)
    return false;
  if(via == null)
   return true;
  if(via.length()>1)
  {
   int index = 0;
   int separator = 1;
   int p1=via.indexOf('-'); 
   if(p1<0) return false;
   int divider = (via.length()/(p1+separator));
   int diff = (via.length()%(p1+separator)); 
   if((divider-diff)!=separator)
    divider-=separator;
   int count = 0;
   while(count<=divider)
   {
    count+=1;
    if(p1>=0)
    {
     if(!checking(orig,via.substring(index,p1)))
      return false;
     if(!checking(dest,via.substring(index,p1)))
      return false;
    }
    else // found the last one
    {
     if(!checking(orig,via.substring(index,p1)))
      return false;
     if(!checking(dest,via.substring(index,p1)))
      return false;
     break;
    }
    index+=p1;
    p1 = via.indexOf('-',index);
   }
  } 
  return true;
 }

 private boolean checking(String s1,String s2)
 {
  if(s1.compareToIgnoreCase(s2)==0)
    return false;
  return true;
 }
}
