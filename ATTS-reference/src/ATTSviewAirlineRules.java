/**
 * ATTSviewRoute class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSviewAirlineRules extends HttpServlet
{
 private DBFill dbFill = null;
 private String dbName = "ATTSairline";
 private PrintWriter out = null;
 private Closure cls = null;
 private ATTSutility util = null;

 public void init() throws ServletException
 {
  String driver = "com.mysql.jdbc.Driver";
  String url = "jdbc:mysql://starcloud:3306/";
  String user= "edisonch";
  String pwd = "gblj21mysql03";
  dbFill = new DBFill(driver,url,user,pwd);
 } // end of init

 public void destroy() 
 {
  dbFill.closeDatabase();
  dbFill = null;
 } // end of destroy

 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 } // end of doGet

 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  util = new ATTSutility();

  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS View Airline Rules</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<HR></HR>");
  String view = req.getParameter("view");
  if(view == null)
  {
   cls.error(1,"view");
   cls.closing();
   out.close();
   util.close();
   return;
  }
  
  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException("<BR><B>Failure to make db connection</B></BR>");
   if(view.compareToIgnoreCase("ALL")==0)
     createView();
   else if(view.compareToIgnoreCase("SET")==0)
     createForm();
   else if(view.compareToIgnoreCase("PROCESS")==0)
     processAll(req); 
   else
    cls.error(6,null);
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out); 
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
   util.close();
   view = null;
   out = null;
   cls = null;
  }

 } // end of doPost 

 public void createForm() throws SQLException
 {
  Vector v = util.queryAirline(dbFill,out);
  if(v == null)
  {
   cls.error(5,"airline");
   return;
  }
  out.println("<FORM METHOD=POST ACTION=\"ATTSaddAirlineRules\">");
  out.println("<TABLE BORDER=0 WIDTH=75%>");
  out.println("<CAPTION><BR><B>ATTS set airline rules</B></BR></CAPTION>");
  out.println("<TR><TD>Rules untuk airline:</TD>");
  writeAirline(0,v);
  out.println("</TR>");
  out.println("<TR><TD>Time limit untuk cancel tanpa dikenakan denda:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"cancelTime\" SIZE=40 MAXLENGTH=32></TD></TR>");
  out.println("<TR><TD>Jikalau melampai time limit, denda untuk cancel [rupiah]:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"cancelFee\" SIZE=10 MAXLENGTH=9></TD></TR>");
  out.println("<TR><TD>Child Maximum Age</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"cMax\" SIZE=10 MAXLENGTH=16></TD></TR>");
  out.println("<TR><TD>Infant Maximum Age</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"iMax\" SIZE=10 MAXLENGTH=16></TD></TR>");
  out.println("<TR><TD>Berapa persen harga ticket Child discount</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"cCharge\" SIZE=5 MAXLENGTH=4></TD></TR>");
  out.println("<TR><TD>Berapa persen harga ticket Infant</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"iCharge\" SIZE=5 MAXLENGTH=4></TD></TR>");
  out.println("<TR><TD>Catatan khusus: </TD>");
  out.println("<TD><INPUT TYPE=TEXTAREA NAME=\"RulesNote\" COLS=10 ROWS=3></TD></TR>");
  out.println("<TR><TD>Di-input oleh [employeeID]:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"empID\" SIZE=10 MAXLENGTH=10></TD></TR>");
  out.println("<TR><TD>PassKey [employeeID]:</TD>");
  out.println("<TD><INPUT TYPE=PASSWORD NAME=\"pass\" SIZE=10 MAXLENGTH=10></TD></TR>");
  out.println("</TABLE>");
  out.println("<INPUT TYPE=SUBMIT VALUE=SUBMIT>&nbps;<INPUT TYPE=RESET VALUE=CLEAR>");
  out.println("</FORM>");
  v.removeAllElements();
 } // end of createForm's method

 public void createView() throws SQLException
 {
  Vector v = util.queryAirline(dbFill,out);
  if(v == null)
  {
   cls.error(5,"airline");
   return;
  }
  out.println("<FORM METHOD=POST ACTION=\"ATTSviewAirlineRules\">");
  out.println("<INPUT TYPE=HIDDEN NAME=\"view\" VALUE=\"PROCESS\">");
  out.println("<TABLE BORDER=0 WIDTH=75%>");
  out.println("<CAPTION><BR><B>ATTS view airline rules</B></BR></CAPTION>");
  out.println("<TR><TD>Rules untuk airline:</TD>");
  writeAirline(1,v);
  out.println("</TR>");
  out.println("</TABLE>");
  out.println("<INPUT TYPE=SUBMIT VALUE=SUBMIT>&nbps;<INPUT TYPE=RESET VALUE=CLEAR>");
  out.println("</FORM>");
  v.removeAllElements();
 } // end of createView's method

 public void processAll(HttpServletRequest req) throws SQLException
 {
  int airID = Integer.parseInt(req.getParameter("airID"));
  if(airID == -1)
  {
   cls.error(2,null);
   return;
  }

  DBEncapsulation q = new DBEncapsulation("airlineRules"); 
  q.setColumnName("*");
  if(airID != 0)
    q.setCondition("WHERE airlineRulesOf="+airID);
  Vector r = dbFill.QueryDatabase(q,out);
  if(r == null)
  {
   cls.error(0,"Hasil query adalah nihil.");
   return;
  }
  dbFill.viewContainer((Vector)r.clone(),out);
  r.removeAllElements(); 
 } // end of processAll's method

 private void writeAirline(int n,Vector v) throws SQLException
 {
  if(v == null)
  {
   cls.error(5,"airline");
   throw new SQLException();
  }
  int pos1 = 0;
  int pos2 = 0;
  int size = v.size();
  DBEncapsulation r = null;
  StringBuffer str = new StringBuffer();
  out.println("<TD><SELECT NAME=\"airID\">");
  out.println("<OPTION SELECTED VALUE=\"-1\">-- Pilih airline --</OPTION>");
  if(n!=0)
    out.println("<OPTION VALUE=\"0\">Semua Airline</OPTION>");
  for(int i=0;i<size;i++)
  {
   r = (DBEncapsulation)((DBEncapsulation)v.get(i)).clone();
   pos1 = r.getColumnNamePosition("airlineID");
   pos2 = r.getColumnNamePosition("airlineName");
   str.append("<OPTION VALUE=\""+r.getColumnStringValues(pos1)+"\">");
   str.append(r.getColumnStringValues(pos2)+"</OPTION>");
   out.println(str.toString());
   r.destroyAll(); 
   str.delete(0,str.length());
  }  
  out.println("</SELECT></TD>");
 }

} // end of ATTSviewAirlineAccess' class

