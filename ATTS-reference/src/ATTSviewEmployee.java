/**
 * ATTSviewEmployee class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSviewEmployee extends HttpServlet
{
 private ATTSutility util = null;
 private DBFill dbFill = null;
 private String dbName = "ATTSemployee";
 private String dbName2 = "ATTSairline";
 private PrintWriter out = null;
 private Closure cls = null;

 public void init() throws ServletException
 {
  dbFill = new DBFill();
  util = new ATTSutility();
 } // end of init

 public void destroy() 
 {
  dbFill.closeDatabase();
  dbFill = null;
  util.close();
 } // end of destroy

 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 } // end of doGet

 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  util = new ATTSutility();
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS View Employee</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<BR></BR><H1>ATTS view employee</H1>"); 
  out.println("<HR></HR><BR></BR>");
  String view = req.getParameter("view");
  if(view == null)
  {
   cls.error(1,"view");
   cls.closing();
   out.close();
   return;
  }
  
  try
  {
   if(view.compareToIgnoreCase("ALL")==0)
     processView(req);
   else if(view.compareToIgnoreCase("UPDATE")==0)
     processUpdate(req); 
   else if(view.compareToIgnoreCase("SET_MANAGE")==0)
     createForm(req);
   else if(view.compareToIgnoreCase("PROCESS_MANAGE")==0)
     processManage(req);
   else
    cls.error(6,null);
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out); 
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
   view = null;
   out = null;
   cls = null;
  }

 } // end of doPost 

/**
 * processView's method is to process the user request to view.
 * User request to view employee info is html - ATTSviewEmployee.html 
 * @param HttpServletRequest req - contain user's request
 * @return void
 */
 public void processView(HttpServletRequest req) throws SQLException
 {
  int choice = Integer.parseInt(req.getParameter("CHOICE"));
  String authID = req.getParameter("autID");
  String authPK = req.getParameter("auPK");
  String authPW = req.getParameter("auPW");
  if((authID == null)||(authPK == null)||(authPW == null))
  {
   cls.error(1,null);
   return;
  }
  if((authID.length()<1)||(authPK.length()<1)||(authPW.length()<1))
  {
   cls.error(2,null);
   return;
  }
  if((!util.checkDigit(authID,true,out))||(!util.checkDigit(authPK,true,out)))
  {
   cls.error(3,null);
   return;
  }
  int id = Integer.parseInt(authID);

  if(!util.verifyAuthorization(dbFill,cls,out,id,authPK,authPW,2,null))
  {
   cls.error(7,null);
   return;
  }

  String ch = null; 
  DBEncapsulation dbI = null;
  DateUtil du = new DateUtil();
  switch(choice)
  {
   case 1 : ch = req.getParameter("empName");     
            dbI = new DBEncapsulation("employee");
            dbI.setColumnName("*");
	    dbI.setCondition("WHERE employeeName=\""+ch+"\";");
	    break;
   case 2 : ch = req.getParameter("empID");       
            if(!util.checkDigit(ch,true,out))
            {
             cls.error(3,"Employee ID harus dalam bentuk angka/digit");
             return;
            }
            dbI = new DBEncapsulation("employee");
            dbI.setColumnName("*");
            dbI.setCondition("WHERE employeeID=\""+ch+"\";");
            break;
   case 3 : ch = req.getParameter("empIDCard");   
            if(!util.checkDigit(ch,false,out))
            {
	     cls.error(3,"Employee ID Card [KTP] harus dalam bentuk angka/digit");
             return;
            }
            dbI = new DBEncapsulation("employeeInfo");
	    dbI.setColumnName("*");
	    dbI.setCondition("WHERE empInfoIDCard=\""+ch+"\";");
	    break;
   case 4 : ch = req.getParameter("empExIDCard"); 
            if(!du.checkSQLDate(ch,out))
            {
             cls.error(4,"Expiration Date of Employee ID Card");
             return;
            }
            dbI = new DBEncapsulation("employeeInfo");
            dbI.setColumnName("*");
            dbI.setCondition("WHERE empInfoIDExpire=\""+ch+"\";");
            break;
   case 5 : ch = req.getParameter("empHp");       
            if(!util.checkDigit(ch,false,out))
            {
             cls.error(3,"Nomor Handphone harus dalam bentuk angka/digit");
             return;
            }
            dbI = new DBEncapsulation("employeeContact");
            dbI.setColumnName("*");
            dbI.setCondition("WHERE empContactHandPhone=\""+ch+"\";");
            break;
   case 6 : ch = req.getParameter("empPh");	   
            if(!util.checkDigit(ch,false,out))
            {
             cls.error(3,"Nomor Telephone harus dalam bentuk angka/digit");
             return;
            }
            dbI = new DBEncapsulation("employeeContact");
            dbI.setColumnName("*");
            dbI.setCondition("WHERE empContactHomePhone=\""+ch+"\";");
            break;
   case 7 : ch = req.getParameter("empCity");     
            dbI = new DBEncapsulation("employeeAddress");
            dbI.setColumnName("*");
            dbI.setCondition("WHERE empHomeAddCity=\""+ch+"\";");
            break;
   case 8 : ch = req.getParameter("empHome");     
            dbI = new DBEncapsulation("employeeInfo");
            dbI.setColumnName("*");
            dbI.setCondition("WHERE empInfoHometown=\""+ch+"\";");
            break;
   case 9 : ch = req.getParameter("empContactPerson"); 
            dbI = new DBEncapsulation("employeeContact");
            dbI.setColumnName("*");
            dbI.setCondition("WHERE empContactPersonName=\""+ch+"\";");
            break;
   case 10: ch = req.getParameter("empSickDay");  
            dbI = new DBEncapsulation("employeeInfo");
            dbI.setColumnName("*");
            dbI.setCondition("WHERE empInfoDayOffLeft=\""+ch+"\";"); 
            break;
   case 11: ch = req.getParameter("empDayOff");   
            dbI = new DBEncapsulation("employeeInfo");
            dbI.setColumnName("*");
            dbI.setCondition("WHERE empInfoDayOffLeft=\""+ch+"\";");
            break;
   case 12: dbI = new DBEncapsulation("employee");
            dbI.setColumnName("*"); 
            break;
   default: cls.error(6,null);
            return;
  }
  if((choice==12)||(choice==1)||(choice==2))
    startFromEmployee((DBEncapsulation)dbI.clone());
  else if((choice==11)||(choice==10)||(choice==8)||(choice==4)||(choice==3))
    startFromEmployeeInfo((DBEncapsulation)dbI.clone());
  else 
    startFromEmployeeContact((DBEncapsulation)dbI.clone());
   dbI.destroyAll();
 } // end of processView's method

/**
 * startFromEmployee's method is to start inquery from employee's table
 * @param DBEncapsulation db - contain the inquery to employee's table
 * @return none
 */
 private void startFromEmployee(DBEncapsulation db) throws SQLException
 {
  Vector v = dbFill.QueryDatabase((DBEncapsulation)db.clone(),out);
  if(v==null)
  {
   cls.error(5,"Data Employee");
   return;
  }
  db.destroyAll();
  DBEncapsulation r = null;
  int pos1 = 0;
  int pos2 = 0;
  int empID = 0;
  int size = v.size();
  for(int i=0;i<size;i++)
  {
   db=new DBEncapsulation((DBEncapsulation)v.get(i));
   pos1 = db.getColumnNamePosition("employeeID");
   if((pos1==db.NOT_FOUND)||(pos1==db.OUT_RANGE)||(pos1==db.SAME_SIZE))
   {
    cls.error(0,"Tidak ditemukan employeeID");
    return;
   } 
   empID = db.getColumnIntegerValue(pos1);
   r=dbFill.QuerySingleSpecific("employeeInfo","WHERE empInfoOfID=\""+empID+"\";",out);
   if(r==null)
   {
    cls.error(0,"Tidak ditemukan employeeInfo");
    return;
   } 
   db.mergeEncap((DBEncapsulation)r.clone());
   r.destroyAll();
   r=dbFill.QuerySingleSpecific("employeeAddress","WHERE empAddOfID=\""+empID+"\";",out);
   if(r==null)
   {
    cls.error(0,"Tidak ditemukan employeeAddress");
    return;
   } 
   db.mergeEncap((DBEncapsulation)r.clone());
   r.destroyAll();
   r=dbFill.QuerySingleSpecific("employeeContact","WHERE empContractOfID=\""+empID+"\";",out);
   if(r==null)
   {
    cls.error(0,"Tidak ditemukan employeeContract");
    return;
   }
   db.mergeEncap((DBEncapsulation)r.clone());
   r.destroyAll();
   pos1 = db.getColumnNamePosition("empContactPersonNotes");
   pos2 = db.getColumnIntegerValue(pos1);
   if(pos2!=0)
   {
    r=dbFill.QuerySingleSpecific("employeeNotes","WHERE employeeNotesKey=\""+pos2+"\";",out);
    pos2=r.getColumnNamePosition("empNotes");
    db.replaceValue(pos1,r.getColumnStringValue(pos2));
    r.destroyAll(); 
   }
   v.removeElementAt(i);
   v.insertElementAt((DBEncapsulation)db.clone(),i); 
   db.destroyAll();
  }
  dbFill.viewContainer(v,out);
 } // end of startFromEmployee's method

/**
 * startFromEmployeeInfo's method is start inquery from employeeInfo's table 
 * @param DBEncapsulation db - contain the inquery
 * @return none
 */
 private void startFromEmployeeInfo(DBEncapsulation db) throws SQLException
 {
  Vector v = dbFill.QueryDatabase((DBEncapsulation)db.clone(),out);
  if(v==null)
  {
   cls.error(5,"Data Employee Info");
   return;
  }
  db.destroyAll();
  DBEncapsulation r = null;
  int size = v.size();
  int pos1 = 0;
  int pos2 = 0;
  int empID= 0;
  for(int i=0;i<size;i++)
  {
   db=(DBEncapsulation)((DBEncapsulation)v.get(i)).clone();
   pos1=db.getColumnNamePosition("empInfoOfID");
   if((pos1!=r.NOT_FOUND)||(pos1!=r.SAME_SIZE)||(pos1!=r.OUT_RANGE))
   {
    cls.error(0,"Tidak ditemukan empInfoOfID");
    return;
   }   
   empID=db.getColumnIntegerValue(pos1);
   r=dbFill.QuerySingleSpecific("employeeAddress","WHERE empAddOfID=\""+empID+"\";",out);
   if(r==null)
   {
    cls.error(0,"Tidak ditemukan employeeAddress");
    return;
   }  
   db.mergeEncap((DBEncapsulation)r.clone());
   r.destroyAll();
   r=dbFill.QuerySingleSpecific("employeeContract","WHERE empContractOfID=\""+empID+"\";",out);
   if(r==null)
   {
    cls.error(0,"Tidak ditemukan employeeContract");
    return;
   } 
   db.mergeEncap((DBEncapsulation)r.clone());
   r.destroyAll();
   pos1=db.getColumnNamePosition("empNotes");
   pos2=db.getColumnIntegerValue(pos1);
   if(pos2!=0)
   {
    r=dbFill.QuerySingleSpecific("employeeNotes","WHERE employeeNotesKey=\""+pos2+"\";",out);
    pos2=r.getColumnNamePosition("empNotes");
    db.replaceValue(pos1,r.getColumnStringValue(pos2));
    r.destroyAll();
   }
   r=dbFill.QuerySingleSpecific("employee","WHERE employeeID=\""+empID+"\";",out);
   if(r==null)
   {
    cls.error(0,"Tidak ditemukan employee");
    return;
   }
   db.mergeEncap((DBEncapsulation)r.clone());
   r.destroyAll();
   v.removeElementAt(i);
   v.insertElementAt((DBEncapsulation)db.clone(),i);
   db.destroyAll();
  }
  dbFill.viewContainer(v,out);
 } // end of startFromEmployeeInfo's method

/**
 * startFormEmployeeContact's method is to start inquery from employeeContact's table
 * @param DBEncapsulation db - contain enquery to employeeContact's table
 * @return none
 */
 private void startFromEmployeeContact(DBEncapsulation db) throws SQLException
 {
  Vector v = dbFill.QueryDatabase((DBEncapsulation)db.clone(),out);
  if(v==null)
  {
   cls.error(5,"Data Employee Contract");
   return;
  }
  db.destroyAll();
  int size = v.size();
  int pos1 = 0;
  int pos2 = 0;
  int empID= 0;
  DBEncapsulation r=null;
  for(int i=0;i<size;i++)
  {
   db=(DBEncapsulation)((DBEncapsulation)v.get(i)).clone();
   pos1=db.getColumnNamePosition("empContactOfID");
   empID=db.getColumnIntegerValue(pos1);
   pos1=db.getColumnNamePosition("empContactPersonNote");
   pos2=db.getColumnIntegerValue(pos1);
   if(pos2!=0)
   {
    r=dbFill.QuerySingleSpecific("employeeNotes","WHERE employeeNotesKey=\""+pos2+"\";",out);
    pos2=r.getColumnNamePosition("empNotes");
    db.replaceValue(pos1,r.getColumnStringValue(pos2));
    r.destroyAll();
   } 
   r=dbFill.QuerySingleSpecific("employeeAddress","WHERE empAddOfID=\""+empID+"\";",out);
   if(r==null)
   {
    cls.error(0,"Tidak ditemukan employeeAddress");
    return;
   }
   db.mergeEncap((DBEncapsulation)r.clone());
   r.destroyAll();
   r=dbFill.QuerySingleSpecific("employeeInfo","WHERE empInfoOfID=\""+empID+"\";",out);
   if(r==null)
   {
    cls.error(0,"Tidak ditemukan employeeInfo");
    return;
   } 
   db.mergeEncap((DBEncapsulation)r.clone());
   r.destroyAll();
   r=dbFill.QuerySingleSpecific("employee","WHERE employeeID=\""+empID+"\";",out);
   if(r==null)
   {
    cls.error(0,"Tidak ditemukan employee");
    return;
   }
   db.mergeEncap((DBEncapsulation)r.clone());
   r.destroyAll(); 
   v.removeElementAt(i);
   v.insertElementAt((DBEncapsulation)db.clone(),i);
   db.destroyAll(); 
  }
  dbFill.viewContainer(v,out);
 } // end of startFromEmployeeContract's method

/**
 * processUpdate's method is to serve user request from ATTSupdateEmployee.html
 * to update the employee profile.
 * @param  HttpServletRequest req
 * @return none
 */
 public void processUpdate(HttpServletRequest req) throws SQLException
 {
  String eID = req.getParameter("empUpID");           // digit
  String eName  = req.getParameter("empUpName");
  int eValid    = Integer.parseInt(req.getParameter("eValid"));
  int eAccess   = Integer.parseInt(req.getParameter("empAccess"));
  String eIDCard= req.getParameter("empIDCard");      // digit
  String eIDExp = req.getParameter("empIDExpire");    // digit - DATE
  String eSickD = req.getParameter("empSickDayLeft"); // digit
  String eDayOff= req.getParameter("empDaysOffLeft"); // digit
  String eEndDt = req.getParameter("empEndDate");     // digit - DATE
  String eHomeAd= req.getParameter("empHomeAdd");
  String eHomeAC= req.getParameter("empHomeAddCity");
  String eHomeAZ= req.getParameter("empHomeAddZip");  // digit
  String eHomePh= req.getParameter("empHomePh");      // digit
  String eHomeHp= req.getParameter("empHandPh");      // digit
  String eEmCPer= req.getParameter("empEmConPer");
  String eEmCPN = req.getParameter("empEmConPerNum"); // digit
  String aID    = req.getParameter("authID");         // digit
  String aPK    = req.getParameter("authPK");         // digit
  String aPW    = req.getParameter("authPW");
  String eNote  = req.getParameter("empNote");

  if((eID==null)||(eIDCard==null)||(eIDExp==null)||(eSickD==null)||(eDayOff==null)||
     (eHomeAd==null)||(eHomeAC==null)||(eHomeAZ==null)||(eHomePh==null)||(eHomeHp==null)||
     (eEmCPer==null)||(eEmCPN==null)||(eEndDt==null)||(aID==null)||(aPK==null)||
     (aPW==null)||(eName==null)||(eNote==null))
  {
   cls.error(1,null);
   return;
  }

  StringBuffer msg = new StringBuffer();
  DateUtil du = new DateUtil();
  boolean success = true;

  if((eID.length()<1)&&(eName.length()<1)&&(aID.length()<1)&&(aPK.length()<1)&&
     (aPW.length()<1))
  {
   msg.append("Employee ID/Employee Name/Authorized Personel Employee ID/");
   msg.append("Authrozied Employee PassKey/Authorized Employee Password ");
   msg.append(" musti di-isi selengkapnya");
   cls.error(0,msg.toString());
   success = false;   
   msg.delete(0,msg.length());
  }
  if((!du.checkDigit(eID,out))||(!du.checkDigit(aID,out))||(!du.checkDigit(aPK,out)))
  {
   msg.append("Employee ID/Authorized Personel ID/PassKey ");
   msg.append("musti dalam bentuk angka/digit");
   cls.error(0,msg.toString());
   success = false;
   msg.delete(0,msg.length());
  } 

  int empID = Integer.parseInt(eID);
  int autID = Integer.parseInt(aID);
  int pos1  = 0;
  msg.append("WHERE employeeID="+empID);
 
  DBEncapsulation dbI = dbFill.QuerySingleSpecific("employee",msg.toString(),out); 
  if(dbI == null)
  {
   cls.error(5,"Employee ID");
   return;
  } 
  pos1 = dbI.getColumnNamePosition("employeeName");
  if((pos1==dbI.NOT_FOUND)||(pos1==dbI.SAME_SIZE)||(pos1==dbI.OUT_RANGE))
  {
   cls.error(0,"Tidak ditemukan employeeName");
   return;
  }
  msg.delete(0,msg.length());
  msg.append("Employee ID ["+empID+"] dan Employee Name ["+eName+"] tidak sesuai");
  msg.append(" dengan Employee ID dan Employee Name yg ada dalam database"); 
  if(eName.compareToIgnoreCase(dbI.getColumnStringValue(pos1))!=0)
    if(eName.indexOf(dbI.getColumnStringValue(pos1))==-1)
    {
     cls.error(0,msg.toString());
     return;
    }   
  if(!util.verifyAuthorization(dbFill,cls,out,autID,aPK,aPW,2,null))
  {
   cls.error(7,null);
   return;
  }
  if(!success)
    return;
  else
    success = true;

  Vector v = new Vector();
  dbI.clearAll();
  if(eValid!=-1)
  {
   dbI.setDBTableName("employee");
   dbI.setColumnName("employeeValid");
   if(eValid==1)
     dbI.setColumnValue("Y");
   else
     dbI.setColumnValue("N");
  }
  if(eAccess!=-1)
  {
   if(dbI.getDBTableName()==null)
     dbI.setDBTableName("employee");
   dbI.setColumnNameVal("employeeAccessLvl",eAccess);
  }
  if(dbI.getSizeColumnName()>0)
  {
   dbI.setCondition("WHERE employeeID="+empID);
   v.addElement((DBEncapsulation)dbI.clone());
   dbI.clearAll(); 
  } 
  if(eIDCard.length()>1)
    if(!util.checkDigit(eIDCard,false,out))
    {
     cls.error(0,"Employee ID Card Number harus dalam bentuk angka/digit");
     success = false;
    }
    else 
    {
     dbI.setDBTableName("employeeInfo");
     dbI.setColumnNameVal("empInfoIDCard",eIDCard); 
    }
  if(eIDExp.length()>1)
    if(!du.checkSQLDate(eIDExp,out))
    {
     cls.error(4,"Employee ID expiration date");
     success = false;
    }
    else if(!du.checkSQLDateValidity(eIDExp,3,out))
    {
     cls.error(4,"Employee ID Card expiration date harus lebih dari tanggal sekarang"); 
     success = false;
    }
    else
    {
     if(dbI.getDBTableName()==null)
       dbI.setDBTableName("employeeInfo");
     dbI.setColumnNameVal("empInfoIDExpire",eIDExp);
    }
  if(eSickD.length()>0)
    if(!util.checkDigit(eSickD,true,out))  
    {
     cls.error(0,"Sick Day yg tersisa haruslah dalam bentuk angka/digit");
     success = false;
    }
    else
    {
     if(dbI.getDBTableName()==null)
       dbI.setDBTableName("employeeInfo");
     dbI.setColumnNameVal("empInfoSickDayLeft",eSickD);
    }
  if(eDayOff.length()>0)
    if(!du.checkDigit(eDayOff,out))
    {
     cls.error(0,"Day Off yg tersisa haruslah dalam bentuk angka/digit");
     success=false;
    }
    else
    {
     if(dbI.getDBTableName()==null)
       dbI.setDBTableName("employeeInfo");
     dbI.setColumnNameVal("employeeInfoDayOffLeft",eDayOff);
    }
  if(eEndDt.length()>1)
    if(!du.checkSQLDate(eEndDt,out))
    {
     cls.error(4,"Employee End Date");
     success = false;
    }
    else if(!du.checkSQLDateValidity(eEndDt,4,out))
    {
     cls.error(4,null);
     success = false;
    }
    else
    {
     if(dbI.getDBTableName()==null)
       dbI.setDBTableName("employeeInfo");
     dbI.setColumnNameVal("employeeInfoEndDate",eEndDt);
    } 
  if(!success)
  {
   cls.error(0,"Kesalahan dalam pengisian diatas akan diabaikan perintahnya"); 
   success = true;
  }

  msg.delete(0,msg.length()); 
  msg.append("WHERE empContactOfID=\""+empID+"\";");  
  if(dbI.getSizeColumnName()>0)  
  {
   dbI.setCondition(msg.toString());
   v.addElement(dbI.clone());
   dbI.clearAll();
  }
  int val1 = 0;
  if(eNote.length()>1)
  {
   DBEncapsulation dbQ = dbFill.QuerySingleSpecific("employeeContact",msg.toString(),out);
   if(dbQ == null)
   {
    cls.error(0,"Tidak ditemukan employeeContact dengan employee ID yg dipakai");
    success = false;
   }
   else
   {
    pos1 = dbQ.getColumnNamePosition("empContactPersonNotes");
    val1 = dbQ.getColumnIntegerValue(pos1);
    dbI.setDBTableName("employeeNotes");
    dbI.setColumnNameVal("empNotes",eNote);
    dbI.setCondition("WHERE employeeNotesKey=\""+val1+"\";");
    v.addElement(dbI.clone());
    dbI.clearAll();
    dbQ.destroyAll();
    dbQ = null;
   } 
  } // end of if-statement
  if(eHomeAd.length()>1)
  {
   dbI.setDBTableName("employeeAddress");
   dbI.setColumnNameVal("empHomeAdd",eHomeAd);
  }
  if(eHomeAC.length()>1)
  {
   if(dbI.getDBTableName()==null)
     dbI.setDBTableName("employeeAddress");
   dbI.setColumnNameVal("empHomeAddCity",eHomeAC);
  }
  if(eHomeAZ.length()>1)
  {
   if(!util.checkDigit(eHomeAZ,true,out))
   {
    cls.error(0,"Employee Home Address Post Code harus dalam bentuk angka/digit");
    success = false;
   }
   else
   {
    if(dbI.getDBTableName()==null)
      dbI.setDBTableName("employeeAddress");
    dbI.setColumnNameVal("empHomeAddZip",eHomeAZ);
   } 
  }
  if(!success)
  {
   cls.error(0,"Kesalahan dalam pengisian diatas akan diabaikan perintahnya");
   success = true;
  }
  if(dbI.getSizeColumnName()>0) 
  {
   dbI.setCondition(msg.toString());
   v.addElement(dbI.clone());
   dbI.clearAll(); 
  }
  if(eHomePh.length()>1)
  {
   if(!util.checkDigit(eHomePh,false,out))
   {
    cls.error(0,"Employee Home Phone harus dalam bentuk angka/digit");
    success = false;
   }
   else
   {
    dbI.setDBTableName("employeeContact");
    dbI.setColumnNameVal("empContactHomePhone",eHomePh);
   }
  }
  if(eHomeHp.length()>1)
  {
   if(!util.checkDigit(eHomeHp,false,out))
   {
    cls.error(0,"Employee Handphone harus dalam bentuk angka/digit");
    success = false;
   }
   else
   {
    if(dbI.getDBTableName()==null)
      dbI.setDBTableName("employeeContact");
    dbI.setColumnNameVal("empContactHandPhone",eHomeHp);
   }
  }
  if(eEmCPer.length()>1)
  {
   if(dbI.getDBTableName()==null)
     dbI.setDBTableName("employeeContact");
   dbI.setColumnNameVal("empContactPersonName",eEmCPer);
  }
  if(eEmCPN.length()>1)
  {
   if(!util.checkDigit(eEmCPN,false,out))
   {
    cls.error(0,"Contact Person Number harus dalam bentuk angka/digit");
    success = false; 
   }
   else
   {
    if(dbI.getDBTableName()==null)
      dbI.setDBTableName("employeeContact");
    dbI.setColumnNameVal("empContactPersonNumber",eEmCPN);
   }
  }
  if(eNote.length()>0)
  {
   if(dbI.getDBTableName()==null)
     dbI.setDBTableName("employeeContact");
   dbI.setColumnNameVal("empContactPersonNotes",val1); 
  }
  if(dbI.getSizeColumnName()>0)
  {
   dbI.setCondition(msg.toString());
   v.addElement(dbI.clone());
   dbI.clearAll();
  }
  if(v.isEmpty())
  {
   cls.error(0,"Karena semua kesalahan diatas maka semua perintah diabaikan");
   return;
  }

  dbFill.UpdateDatabase(v,out);
  out.println("<BR><B>Successful updating database</B></BR>");
  v.removeAllElements();
 } // end of processUpdate's method

/**
 * createForm's method is to authorize the employee to use the ATTS system.
 * It receives from ATTSmanageEmployee.html request and validate/activate employee
 * @param HttpServletRequest req - user request
 * @return none
 */
 public void createForm(HttpServletRequest req) throws SQLException
 {
  int choice = Integer.parseInt(req.getParameter("CHOICE"));
  String eName  = req.getParameter("chooseName");
  String eID    = req.getParameter("chooseID");
  String authID = req.getParameter("authID");
  String authPK = req.getParameter("authPK");
  String authPWD= req.getParameter("authPWD");

  if((authID==null)||(authPK==null)||(authPWD==null)||(eName==null)||(eID==null))
  {
   cls.error(1,null);
   return;
  }
  if((choice==1)&&(eName.length()<1))
  {
   cls.error(3,"Tidak mengisi <i>By Complete Name</i> setelah anda memilih itu"); 
   return;
  } 
  else if((choice==2)&&(eID.length()<1))
  {
   cls.error(3,"Tidak mengisi <i>By Employee ID</i> setelah anda memilih itu");
   return;
  }
  if((authID.length()<1)||(authPK.length()<1)||(authPWD.length()<1))
  {
   cls.error(2,null);
   return;
  }
  if((eID.length()>1)&&(!util.checkDigit(eID,true,out)))
  {
   cls.error(3,"Employee ID");
   return;
  }
  if((!util.checkDigit(authID,true,out))||(!util.checkDigit(authPK,true,out)))
  {
   cls.error(3,"Authorized ID/Authorized Employee PassKey");
   return;
  }
  int aID = Integer.parseInt(authID);
  if(!util.verifyAuthorization(dbFill,cls,out,aID,authPK,authPWD,2,null))
  {
   cls.error(7,null);
   return;
  }
  StringBuffer msg = new StringBuffer("WHERE ");
  if(choice == 1)
    msg.append("employeeName=\""+eName+"\";");
  else
    msg.append("employeeID=\""+eID+"\";");
  DBEncapsulation master = dbFill.QuerySingleSpecific("employee",msg.toString(),out);
  if(master == null)
  {
   cls.error(0,"Employee not found");
   return;
  }
  int empID  = 0;
  if(choice==1)
    empID=master.getColumnIntegerValue(master.getColumnNamePosition("employeeID"));
  else
    empID=Integer.parseInt(eID);
  msg.delete(0,msg.length());
  msg.append("WHERE empInfoOfID=\""+empID+"\";"); 
  master.mergeEncap(dbFill.QuerySingleSpecific("employeeInfo",msg.toString(),out));
  msg.delete(0,msg.length());
  msg.append("WHERE empAddOfID=\""+empID+"\";");
  master.mergeEncap(dbFill.QuerySingleSpecific("employeeAddress",msg.toString(),out));
  msg.delete(0,msg.length());
  msg.append("WHERE empContactOfID=\""+empID+"\";");
  master.mergeEncap(dbFill.QuerySingleSpecific("employeeContact",msg.toString(),out));
  form((DBEncapsulation)master.clone()); 
  master.destroyAll();
 } // end of createForm's method

/**
 * processManage's method is receiving user request from createForm above. It processes
 * user request to validate/activate new user in the ATTS system.
 * @param HttpServletRequest req = user request
 * @return none
 */
 public void processManage(HttpServletRequest req) throws SQLException
 {
  int eID = Integer.parseInt(req.getParameter("empID"));
  String ePos = req.getParameter("position");
  String ePK  = req.getParameter("empPassKey");  // digit
  String ePW  = req.getParameter("empPassword");
  String eVal = req.getParameter("empValid");
  String eAL  = req.getParameter("empAccLvl");   
  String eED  = req.getParameter("empEnd");     // date
  String eTN  = req.getParameter("empTerNote");
  String eVL  = req.getParameter("empVac");     // digit
  String eSL  = req.getParameter("empSick");    // digit
  String eDON = req.getParameter("eDayOffNote");
  if(eID<1)
  {
   cls.error(0,"Error in Input, employee ID has to be bigger or equal to 1");
   return;
  } 
  if((ePos.compareToIgnoreCase("NON")==0)&&(ePK.length()<1)&&(ePW.length()<1)&&
     (eVal.compareToIgnoreCase("NON")==0)&&(eAL.compareToIgnoreCase("NON")==0)&&
     (eED.length()<1)&&(eTN.length()<1)&&(eVL.length()<1)&&(eSL.length()<1)&&
     (eDON.length()<1))
  {
   cls.error(0,"Nothing change.");
   return;
  }
  boolean success = true;
  if(ePK.length()>0)
    if(!util.checkDigit(ePK,true,out))
    {
     cls.error(3,"Employee PassKey harus dalam bentuk digit/angka");
     success = false;
    }
    else if(ePK.length()!=6)
    {
     cls.error(3,"Employee PassKey harus 6 digit panjangnya");
     success = false;
    } 
  if(eVL.length()>0)
    if(!util.checkDigit(eVL,true,out))
    {
     cls.error(3,"Employee Vacation Left harus dalam bentuk digit");
     success = false;
    }
    else if(eVL.length()>2)
    {
     cls.error(3,"Employee Vacation Left max days is 14 days");
     success = false;
    }
  if(eSL.length()>0)
    if(!util.checkDigit(eSL,true,out))
    {
     cls.error(3,"Employee Sick Day Left harus dalam bentuk digit");
     success = false;
    }
    else if((eSL.length()>1)||(Integer.parseInt(eSL)>7))
    {
     cls.error(3,"Employee Sick Day Left max days is 7 days");
     success = false;
    } 
  DateUtil du = new DateUtil();
  if(eED.length()>0)
    if(!du.checkSQLDate(eED,out))
    {
     cls.error(4,"End Date");
     success = false;
    }
    else if(!du.checkSQLDateValidity(eED,4,out))
    {
     cls.error(4,"End Date");
     success = false;
    }
  if(!success)
    return; 
  DBEncapsulation dbI = new DBEncapsulation();
  Vector v = new Vector();
  if(ePos.compareToIgnoreCase("NON")!=0)
  {
   dbI.setDBTableName("employee");
   dbI.setColumnNameVal("employeePosition",ePos);
   dbI.setCondition("WHERE employeeID=\""+eID+"\";");
  }
  if(eVal.compareToIgnoreCase("NON")!=0)
  {
   if(dbI.getDBTableName()==null)
   {
    dbI.setDBTableName("employee");
    dbI.setCondition("WHERE employeeID=\""+eID+"\";");
   } 
   dbI.setColumnNameVal("employeeValid",eVal);
  }
  if(eAL.compareToIgnoreCase("NON")!=0)
  {
   if(dbI.getDBTableName()==null)
   {
    dbI.setDBTableName("employee");
    dbI.setCondition("WHERE employeeID=\""+eID+"\";");
   }
   dbI.setColumnNameVal("employeeAccessLvl",eAL);
  }
  if(ePK.length()>1)
  { 
   if(dbI.getDBTableName()==null)
   {
    dbI.setDBTableName("employee");
    dbI.setCondition("WHERE employeeID=\""+eID+"\";");
   }
   dbI.setColumnNameVal("employeePassKey",ePK); 
  }
  if(ePW.length()>1)
  {
   if(dbI.getDBTableName()==null)
   {
    dbI.setDBTableName("employee");
    dbI.setCondition("WHERE employeeID=\""+eID+"\";");
   }
   dbI.setColumnNameVal("employeePassword",ePW);
  }
  if(dbI.getDBTableName()!=null)
    v.addElement(dbI.clone());
  dbI.clearAll();
  if(eED.length()>1)
  {
   dbI.setDBTableName("employeeInfo");
   dbI.setCondition("empInfoOfID=\""+eID+"\";");
   dbI.setColumnNameVal("empInfoEndDate",eED);
  }  
  if(eVL.length()>0)
  {
   if(dbI.getDBTableName()==null)
   {
    dbI.setDBTableName("employeeInfo"); 
    dbI.setCondition("empInfoOfOD=\""+eID+"\";");
   }
   dbI.setColumnNameVal("empInfoVacationLeft",eVL);
  }
  if(eSL.length()>0)
  {
   if(dbI.getDBTableName()==null)
   {
    dbI.setDBTableName("employeeInfo"); 
    dbI.setCondition("empInfoOfOD=\""+eID+"\";");
   }
   dbI.setColumnNameVal("empInfoSickDayLeft",eSL);
  }
  if(eTN.length()>0)
  {
   if(dbI.getDBTableName()==null)
   {
    dbI.setDBTableName("employeeInfo"); 
    dbI.setCondition("empInfoOfOD=\""+eID+"\";");
   }
   dbI.setColumnNameVal("empInfoTerminateNote",eTN);
  }
  if(eDON.length()>0)
  {
   if(dbI.getDBTableName()==null)
   {
    dbI.setDBTableName("employeeInfo"); 
    dbI.setCondition("empInfoOfOD=\""+eID+"\";");
   }
   dbI.setColumnNameVal("empInfoDayOff",eDON);
  }
  if(dbI.getDBTableName()!=null)
    v.addElement(dbI.clone());
  if(!v.isEmpty())
  {
   dbFill.UpdateDatabase(v,out);
   v.removeAllElements();
   out.println("<BR>Successfully update the employee management profile</BR>");
  } 
  dbI.destroyAll(); 
 } // end of processManage's method

 public void form(DBEncapsulation e)
 {
  if(e == null)
    return;
  util.createBegin("ATTSviewEmployee","view","PROCESS_MANAGE",true,out);
  out.println("<INPUT TYPE=HIDDEN NAME=\"empID\" VALUE=\"");
  out.println(e.getColumnStringValues(e.getColumnNamePosition("empAddOfID"))+"\">");
  out.println("<TR><TD>Employee Name:</TD><TD>");
  out.println(e.getColumnStringValue(e.getColumnNamePosition("employeeName"))+"</TD>");
  out.println("<TD></TD></TR>");
  out.println("<TR><TD>Employee Position:</TD><TD>");
  out.println(e.getColumnStringValue(e.getColumnNamePosition("employeePosition"))+"</TD>");
  out.println("<TD><SELECT NAME=\"position\">");
  out.println("<OPTION SELECTED VALUE=\"NON\">-- Pilih kalau berubah --</OPTION>");
  out.println("<OPTION VALUE=\"TICKETING\">TICKETING</OPTION>");
  out.println("<OPTION VALUE=\"MESSENGER\">MESSENGER</OPTION>");
  out.println("<OPTION VALUE=\"ACCOUNTANT\">ACCOUNTANT</OPTION>");
  out.println("<OPTION VALUE=\"CASHIER\">CASHIER</OPTION>");
  out.println("<OPTION VALUE=\"SUPERVISOR\">SUPERVISOR</OPTION>");
  out.println("<OPTION VALUE=\"DRIVER\">DRIVER</OPTION>");
  out.println("<OPTION VALUE=\"EXECUTIVE\">EXECUTIVE</OPTION>"); 
  out.println("<OPTION VALUE=\"IT_SUPPORT\">IT SUPPORT</OPTION>");
  out.println("<OPTION VALUE=\"OFFICE_BOY\">OFFICE BOY</OPTION>");
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD>Reset Employee PassKey:</TD><TD>");
  out.println("<INPUT TYPE=PASSWORD NAME=empPassKey SIZE=20 MAXLENGTH=6></TD>");
  out.println("<TD>Kosongkan bila tidak berubah</TD></TR>");  
  out.println("<TR><TD>Reset Employee Password:</TD><TD>");
  out.println("<INPUT TYPE=PASSWORD NAME=empPassword SIZE=20 MAXLENGTH=16></TD>");
  out.println("<TD>Kosongkan bila tidak berubah</TD></TR>");
  out.println("<TR><TD>Employee Validity:</TD><TD>");
  out.println(e.getColumnStringValue(e.getColumnNamePosition("employeeValid"))+"</TD>");
  out.println("<TD><SELECT NAME=\"empValid\">");
  out.println("<OPTION SELECTED VALUE=\"NON\">-- Pilih kalau berubah --</OPTION>");
  out.println("<OPTION VALUE=\"Y\">YES</OPTION>");
  out.println("<OPTION VALUE=\"N\">NO</OPTION>");
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD>Employee Access Level</TD><TD>");
  out.println(e.getColumnStringValues(e.getColumnNamePosition("employeeAccessLvl"))+"</TD>");
  out.println("<TD><SELECT NAME=\"empAccLvl\">");
  out.println("<OPTION SELECTED VALUE=\"NON\">-- Pilih kalau berubah --</OPTION>");
  out.println("<OPTION VALUE=\"1\">1</OPTION>");
  out.println("<OPTION VALUE=\"2\">2</OPTION>");
  out.println("<OPTION VALUE=\"3\">3</OPTION>");
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD>Employee End Date</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=empEnd SIZE=20 MAXLENGTH=10></TD>");
  out.println("<TD></TD></TR>");
  out.println("<TR><TD>Employee Terminated Note:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=empTerNote SIZE=40 MAXLENGTH=64></TD>");
  out.println("<TD></TD></TR>");
  out.println("<TR><TD>Employee Vacation Days Left:</TD><TD>");
  out.println(e.getColumnStringValues(e.getColumnNamePosition("empInfoVacationLeft")));
  out.println("</TD><TD><INPUT TYPE=TEXT NAME=empVac SIZE=20 MAXLENGTH=2></TD></TR>");
  out.println("<TR><TD>Employee Sick Days Left:</TD><TD>");
  out.println(e.getColumnStringValues(e.getColumnNamePosition("empInfoSickDayLeft")));
  out.println("</TD><TD><INPUT TYPE=TEXT NAME=empSick SIZE=20 MAXLENGTH=2></TD></TR>");
  out.println("<TR><TD>Employee Schedule Day Off Note:</TD><TD>");
  if(e.getColumnStringValues(e.getColumnNamePosition("empInfoTerminateNote")).length()>0)
    out.println(e.getColumnStringValues(e.getColumnNamePosition("empInfoTerminateNote"))); 
  out.println("</TD><TD><INPUT TYPE=TEXT NAME=eDayOffNote SIZE=40 MAXLENGTH=32></TD></TR>");

  util.createEnd(out); 
 }
} // end of ATTSviewEmployee's class

