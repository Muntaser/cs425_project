package ATTS;

import java.util.Enumeration;
import java.util.Vector;
import java.sql.SQLException;

/**
 * One Encapsulation one table name
 * MySQL table:
 * create table victor
 * (
 *  name VARCHAR(8) NOT NULL,
 *  id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY
 * );
 *
 * DBEncapsulation example of usage:
 * int number = 100; 
 * DBEncapsulation db = new DBEncapsulation(victor);
 * db.setColumnName("name");
 * db.setColumnName("id"); 
 * db.setColumnValue("jack");
 * db.setColumnValue(null);
 */
public class DBEncapsulation implements Cloneable, DatabaseEncapsulation 
{
 final public static int SP_VAR = 99;
 final public static int SAME_SIZE = -17;
 final public static int NOT_FOUND = -19;
 final public static int OUT_RANGE = -23;

 private String DBTableName = null;
 private Vector DBColumnName, DBColumnValue, DBColumnType;
 private int conditionFlag = 0;
 private int variableFlag = 0; 
 private int repeatFlag = 0;
 private int DBColumnNameTableLength = 0;
 private int DBColumnValueTableLength= 0;
 private int DBColumnTypeTableLength = 0;
 private StringBuffer condition = null;
 private StringBuffer variable = null;
 private boolean clearFlag = false;

 public DBEncapsulation()
 {
  initialize(); 
 }

 public DBEncapsulation(DBEncapsulation dbCopy) throws SQLException
 {
  if(dbCopy == null)
    throw new SQLException("the Encapsulation is null");
  if(dbCopy.getDBTableName()!= null)
    this.DBTableName   = new String(dbCopy.getDBTableName());
  else
    throw new SQLException("there is no db table name");
  this.DBColumnName  = (Vector)dbCopy.DBColumnName.clone();
  this.DBColumnValue = (Vector)dbCopy.DBColumnValue.clone();
  this.DBColumnType  = (Vector)dbCopy.DBColumnType.clone();
  if(dbCopy.getConditionFlag()!=0)
  {
   this.condition = new StringBuffer(dbCopy.condition.toString());
   this.conditionFlag = dbCopy.conditionFlag;
  }
  else
  {
   this.conditionFlag = 0;
   this.condition = new StringBuffer();
  }
  if(dbCopy.getVariableFlag()!=0)
  {
   this.variable = new StringBuffer(dbCopy.variable.toString());
   this.variableFlag = dbCopy.variableFlag;
  }
  else
  {
   this.variableFlag  = 0;
   this.variable = new StringBuffer();
  }
  this.repeatFlag    = dbCopy.getRepeatFlag();
  this.DBColumnNameTableLength = dbCopy.getSizeColumnName();
  this.DBColumnValueTableLength= dbCopy.getSizeColumnValue();
  this.DBColumnTypeTableLength = dbCopy.getSizeColumnType();
  this.clearFlag     = false;
 }

 public DBEncapsulation(String tableName)
 { 
  setDBTableName(tableName);
  initialize();
 }

 public void initialize()
 {
  DBColumnName = new Vector();
  DBColumnValue = new Vector();
  DBColumnType = new Vector();
  variable = new StringBuffer();
  condition= new StringBuffer();
  resetClearFlag();
 }

 public boolean isDBTableNameNull()
 {
  if(DBTableName == null)
   return true;
  return false;
 }

 public void setDBTableName(String tableName)
 {
  DBTableName = new String(tableName);
  resetClearFlag(); 
 }
 
 public void setColumnNameVal(String colName, String colVal)
 {
  setColumnName(colName);
  setColumnValue(colVal);    
 }

 public void setColumnNameVal(String colName, int colVal)
 {
  setColumnName(colName);
  setColumnValue(colVal);
 }

 public void setColumnNameVal(String colName, float colVal)
 {
  setColumnName(colName);
  setColumnValue(colVal);
 }

 public void setColumnNameVal(String colName, double colVal)
 {
  setColumnName(colName);
  setColumnValue(colVal);
 }

 public void setColumnName(String colName)
 { 
  DBColumnName.addElement(new String(colName));
  DBColumnNameTableLength++;
 }
 
 public void setColumnValue(String colValue)
 {
  DBColumnValue.addElement(new String(colValue));
  if(colValue.charAt(0) == '@')
    DBColumnType.addElement(new Integer(SP_VAR));
  else if(colValue.compareToIgnoreCase("CURRENT_TIME")==0)
    DBColumnType.addElement(new Integer(java.sql.Types.TIME));
  else if(colValue.compareToIgnoreCase("CURRENT_DATE")==0)
    DBColumnType.addElement(new Integer(java.sql.Types.DATE));
  else
    DBColumnType.addElement(new Integer(java.sql.Types.VARCHAR));
  DBColumnValueTableLength++;
  DBColumnTypeTableLength++;
 } 

 public void setColumnValue(int colValue)
 {
  DBColumnValue.addElement(new Integer(colValue));
  DBColumnType.addElement(new Integer(java.sql.Types.INTEGER));
  DBColumnValueTableLength++;
  DBColumnTypeTableLength++;
 }

 public void setColumnValue(float colValue)
 {
  DBColumnValue.addElement(new Float(colValue));
  DBColumnType.addElement(new Integer(java.sql.Types.FLOAT));
  DBColumnValueTableLength++;
  DBColumnTypeTableLength++;
 }

 public void setColumnValue(double colValue)
 {
  DBColumnValue.addElement(new Double(colValue));
  DBColumnType.addElement(new Integer(java.sql.Types.DOUBLE));
  DBColumnValueTableLength++;
  DBColumnTypeTableLength++;
 }

 public void setSizeColumnValue()
 {
  DBColumnValueTableLength = DBColumnValue.size(); 
 }

 public void setCondition(String cond) throws SQLException
 {
  if(cond == null)
    throw new SQLException("setCond has null");
  condition.append(cond); 
  conditionFlag++;
 }

 public void setVariable(String var) throws SQLException
 {
  if(var == null)
    throw new SQLException("setVar has null");
  variable.append(var);
  variableFlag++;
 }

 public String getDBTableName()
 {
  return DBTableName;
 }

 public String getVariable()
 {
  return variable.toString();
 }

 public String getCondition()
 {
  return condition.toString();
 }

 public int getVariableFlag()
 {
  return variableFlag;
 }

 public int getConditionFlag()
 {
  return conditionFlag;
 }

 public int getRepeatFlag()
 {
  return repeatFlag;
 }

 public void setRepeatFlag(int number)
 {
  repeatFlag = number;
 }

 public int getColumnType(int pos)
 {
  if(pos>DBColumnTypeTableLength)
    return OUT_RANGE;
  if(pos == DBColumnTypeTableLength)
    return SAME_SIZE;
  
  return ((Integer)DBColumnType.get(pos)).intValue();
 }

 public String getColumnNames(int pos) throws SQLException 
 {
  if((pos>DBColumnNameTableLength)||(pos == DBColumnNameTableLength))
    return null;
  if((String)DBColumnName.get(pos) == null)
    throw new SQLException();
  return (String)DBColumnName.get(pos);
 }

 public String getColumnStringValue(int pos)
 {
  if((pos>DBColumnValueTableLength)||(pos==DBColumnValueTableLength))
    return null;
  return (String)DBColumnValue.get(pos);
 }

 public String getColumnStringValues(int pos) throws SQLException
 {
  if((pos>DBColumnValueTableLength)||(pos==DBColumnValueTableLength))
    return null;
  switch(getColumnType(pos))
  {
   case java.sql.Types.DATE    :
   case java.sql.Types.TIME    :
   case java.sql.Types.CHAR    :
   case SP_VAR                 :
   case java.sql.Types.VARCHAR : return getColumnStringValue(pos);
   case java.sql.Types.SMALLINT:
   case java.sql.Types.TINYINT :
   case java.sql.Types.INTEGER : return String.valueOf(getColumnIntegerValue(pos));
   case java.sql.Types.FLOAT   : return String.valueOf(getColumnFloatValue(pos));
   case java.sql.Types.DOUBLE  : return String.valueOf(getColumnDoubleValue(pos));
   case OUT_RANGE : 
   case NOT_FOUND :
   case SAME_SIZE : throw new SQLException("Error in getColumnStringValues");
   default: throw new SQLException("Error Default value="+getColumnType(pos)); 
  }
   
 }

 public int getColumnIntegerValue(int pos) throws SQLException
 {
  if(pos > DBColumnValueTableLength)
    throw new SQLException("OutOfRange getColumnIntegerValue (pos)="+pos);
  return ((Integer)DBColumnValue.get(pos)).intValue();
 }

 public float getColumnFloatValue(int pos) throws SQLException
 {
  if(pos > DBColumnValueTableLength)
    throw new SQLException("OutOfRange getColumnFloatValue (pos)="+pos);
  return ((Float)DBColumnValue.get(pos)).floatValue();
 }

 public double getColumnDoubleValue(int pos) throws SQLException
 {
  if(pos > DBColumnValueTableLength)
    throw new SQLException("OutOfRange getColumnDoubleValue (pos)="+pos);
  return ((Double)DBColumnValue.get(pos)).doubleValue();
 }

 public void destroyAll()
 {
  if(!clearFlag)  
    clearAll();
  else
    return;
  DBColumnName = null;
  DBColumnValue= null;
  DBColumnType = null;
  condition = null;
  variable = null;
 } 

 public void clearAll()
 {
  if(clearFlag)
    return;
  DBTableName = null;
  if(conditionFlag!=0)
    condition.delete(0,condition.length());
  if(variableFlag!=0)
    variable.delete(0,variable.length());
  conditionFlag = 0;
  variableFlag = 0;
  repeatFlag = 0;
  DBColumnNameTableLength = 0;
  DBColumnValueTableLength= 0;
  DBColumnTypeTableLength = 0;
  if(!DBColumnName.isEmpty())
    DBColumnName.removeAllElements();
  if(!DBColumnValue.isEmpty())
    DBColumnValue.removeAllElements();
  if(!DBColumnType.isEmpty())
    DBColumnType.removeAllElements(); 
  clearFlag = true;
 }

 public Enumeration getAllColumnNames()
 {
  return DBColumnName.elements();
 }

 public Enumeration getAllColumnValues()
 {
  return DBColumnValue.elements();
 }

 public void setSizeColumnName()
 {
  DBColumnNameTableLength = DBColumnName.size();
 }

 public void setSizeColumnSize()
 {
  DBColumnValueTableLength = DBColumnValue.size();
 }

 public void setSizeColumnType()
 {
  DBColumnTypeTableLength = DBColumnType.size();
 }

 public int getSizeColumnName()
 {
  return DBColumnNameTableLength;
 }

 public int getSizeColumnValue()
 {
  return DBColumnValueTableLength;
 }

 public int getSizeColumnType()
 {
  return DBColumnTypeTableLength;
 }

 public int getColumnNamePosition(String colName) throws SQLException
 {
   int colSize = DBColumnName.size();
   if(colName.length()<1)
     throw new SQLException("colName less than 1");
   for(int pos=0; pos<colSize;pos++)
    if(colName.compareToIgnoreCase((String)DBColumnName.get(pos))==0)
      return pos;
   return NOT_FOUND;
 }


 public void replaceValue(int pos, String val) throws SQLException
 {
  if((pos > DBColumnValue.size()) || (pos == DBColumnValue.size()))
    throw new SQLException("replaceValue pos="+pos+" size:"+DBColumnValue.size());
  DBColumnValue.removeElementAt(pos);
  DBColumnValue.insertElementAt(new String(val),pos); 
  if(getColumnType(pos) != java.sql.Types.VARCHAR)
  {
   DBColumnType.removeElementAt(pos);
   DBColumnType.insertElementAt(new Integer(java.sql.Types.VARCHAR),pos);
  }
 }

 public void replaceValue(int pos, int val) throws SQLException
 {
  if((pos > DBColumnValue.size()) || (pos == DBColumnValue.size()))
    throw new SQLException("replaceValue pos="+pos+" size:"+DBColumnValue.size());
  DBColumnValue.removeElementAt(pos);
  DBColumnValue.insertElementAt(new Integer(val),pos); 
  if(getColumnType(pos) != java.sql.Types.INTEGER)
  {
   DBColumnType.removeElementAt(pos);
   DBColumnType.insertElementAt(new Integer(java.sql.Types.INTEGER),pos);
  }   
 }

 public void replaceValue(int pos, float val) throws SQLException
 {
  if((pos > DBColumnValue.size()) || (pos == DBColumnValue.size()))
    throw new SQLException("replaceValue pos="+pos+" size:"+DBColumnValue.size());
  DBColumnValue.removeElementAt(pos);
  DBColumnValue.insertElementAt(new Float(val),pos); 
  if(getColumnType(pos) != java.sql.Types.FLOAT)
  {
   DBColumnType.removeElementAt(pos);
   DBColumnType.insertElementAt(new Integer(java.sql.Types.FLOAT),pos);
  }
 }

 public void replaceValue(int pos, double val) throws SQLException
 {
  if((pos > DBColumnValue.size()) || (pos == DBColumnValue.size()))
    throw new SQLException("replaceValue pos="+pos+" size:"+DBColumnValue.size());
  DBColumnValue.removeElementAt(pos);
  DBColumnValue.insertElementAt(new Double(val),pos); 
  if(getColumnType(pos) != java.sql.Types.DOUBLE)
  {
    DBColumnType.removeElementAt(pos);
    DBColumnType.insertElementAt(new Integer(java.sql.Types.DOUBLE),pos);
  }
 }

 public void replaceName(int pos, String name) throws SQLException
 {
  if((pos>DBColumnName.size())||(pos==DBColumnValue.size()))
   throw new SQLException("replaceName pos="+pos+" size:"+DBColumnName.size());
  DBColumnName.removeElementAt(pos);
  DBColumnName.insertElementAt(new String(name),pos);
 }

 protected Object clone() 
 {
  try
  {
   DBEncapsulation dbCopy = (DBEncapsulation)(super.clone());
   dbCopy.DBColumnName = (Vector)DBColumnName.clone(); 
   dbCopy.DBColumnValue= (Vector)DBColumnValue.clone(); 
   dbCopy.DBColumnType = (Vector)DBColumnType.clone(); 
   if(conditionFlag != 0)
   {
    if(dbCopy.condition == null)
    {
     dbCopy.condition = new StringBuffer(this.condition.toString());
     dbCopy.conditionFlag = this.conditionFlag;
    }
    else
    {
     dbCopy.condition.append(this.condition.toString());
     dbCopy.conditionFlag = this.conditionFlag;
    }
   }
   else
   {
    dbCopy.condition = new StringBuffer();
    dbCopy.conditionFlag = 0;
   }
   if(variableFlag != 0)
   {
    if(dbCopy.variable == null)
    {
     dbCopy.variable = new StringBuffer(this.variable.toString());
     dbCopy.variableFlag = this.variableFlag;
    }
    else
    {
     dbCopy.variable.append(this.variable.toString());
     dbCopy.variableFlag = this.variableFlag;
    }
   }
   else
   {
    dbCopy.variable = new StringBuffer();
    dbCopy.variableFlag = 0;
   }
   dbCopy.DBTableName = new String(DBTableName);
   dbCopy.repeatFlag    = this.repeatFlag;
   dbCopy.DBColumnNameTableLength = this.DBColumnNameTableLength;
   dbCopy.DBColumnValueTableLength= this.DBColumnValueTableLength;
   dbCopy.DBColumnTypeTableLength = this.DBColumnTypeTableLength;
   return dbCopy;
  }
  catch(CloneNotSupportedException e)
  {
   System.out.println(e);
   return this;
  }
 }

 private void resetClearFlag()
 {
  if(clearFlag)
    clearFlag = false;
 }

 public void mergeEncap(DBEncapsulation a) throws SQLException
 {
  if(a==null)
    return;
  int colNameSize = a.getSizeColumnName();
  for(int i=0;i<colNameSize;i++)
  {
   setColumnName(a.getColumnNames(i));
   switch(a.getColumnType(i))
   {
    case java.sql.Types.TIME : 
    case java.sql.Types.DATE :
    case java.sql.Types.CHAR :
    case java.sql.Types.VARCHAR :
    case SP_VAR : setColumnValue(a.getColumnStringValue(i));
                  break;
    case java.sql.Types.FLOAT   : setColumnValue(a.getColumnFloatValue(i));
				  break;
    case java.sql.Types.DOUBLE  : setColumnValue(a.getColumnDoubleValue(i));
				  break;
    case java.sql.Types.INTEGER :
    case java.sql.Types.SMALLINT:
    case java.sql.Types.TINYINT : setColumnValue(a.getColumnIntegerValue(i));
				  break;
    case OUT_RANGE : throw new SQLException("OutOfRange at mergeEncap");
    case SAME_SIZE : throw new SQLException("SAME_SIZE at mergeEncap");
    case NOT_FOUND : throw new SQLException("NOT_FOUND at mergeEncap");
    default : break;
   }   
  }
  return;
 } 

 public void removeNameVal(String name) throws SQLException
 {
  if(name == null)
    return;
  removeNameVal(getColumnNamePosition(name));
 }

 public void removeNameVal(int pos)
 {
  if(pos<0)
   return;
  DBColumnName.removeElementAt(pos);
  DBColumnValue.removeElementAt(pos);
  DBColumnType.removeElementAt(pos);
  setSizeColumnName();
  setSizeColumnType();
  setSizeColumnValue(); 
 }

} // end class DBEncapsulation
