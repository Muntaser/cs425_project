/**
 * ATTSaddCity class
 * @author Edison Chindrawaly
 */

package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSsetExtraFee extends HttpServlet
{
 private DBFill dbFill = null;
 private String dbName = "ATTSairline";
 private PrintWriter out = null;
 private Closure cls = null;
 private ATTSutility util = null;

 public void init() throws ServletException
 {
  String driver = "com.mysql.jdbc.Driver";
  String url = "jdbc:mysql://starcloud:3306/";
  String user= "edisonch";
  String pwd = "gblj21mysql03";
  dbFill = new DBFill(driver,url,user,pwd);
  util = new ATTSutility();
 }
 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
 }
 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 }
 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS set Extra Fee </TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<HR></HR>");
  String airPPN    = req.getParameter("airlinePPN");
  String airIWJR   = req.getParameter("airlineIWJR");
  String airChange = req.getParameter("changeBy");
  String airPass   = req.getParameter("passKey");

  if((airPPN==null)||(airIWJR==null)||(airChange==null)||(airPass==null))
  {
   cls.error(1,null);
   cls.makeClosing();
   cls.closing();
   out.close();
   return;
  }
  if((airPPN.length()<1)||(airIWJR.length()<1)||(airChange.length()<1)||
     (airPass.length()<1))
  {
   cls.error(2,null);
   cls.makeClosing();
   cls.closing();
   out.close();
   return;
  }

  Vector dbVec = new Vector();  
  DBEncapsulation dbEncap = new DBEncapsulation();

  dbEncap.setDBTableName("airlinePPN");
  dbEncap.setColumnNameVal("airlinePPNPercentage",airPPN);
  dbEncap.setColumnNameVal("airlinePPNInputBy",airChange);
  dbEncap.setColumnNameVal("airlinePPNDate","CURRENT_DATE");
  dbEncap.setColumnNameVal("airlinePPNTime","CURRENT_TIME");
  dbVec.addElement((DBEncapsulation)dbEncap.clone());
  dbEncap.clearAll();
 
  dbEncap.setDBTableName("airlineIWJR");
  dbEncap.setColumnNameVal("airlineIWJRAmount",airIWJR);
  dbEncap.setColumnNameVal("airlineIWJRInputBy",airChange);
  dbEncap.setColumnNameVal("airlineIWJRDate","CURRENT_DATE");
  dbEncap.setColumnNameVal("airlineIWJRTime","CURRENT_TIME");
  dbVec.addElement((DBEncapsulation)dbEncap.clone());
  dbEncap.clearAll();

  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException();  
   dbFill.InsertDatabase(dbVec,out);
   out.println("<BR></BR>");
   out.println("<h2>Successfully set extra fee</h2>");
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   dbEncap.destroyAll();
   dbVec.removeAllElements();
   out.close();
   dbEncap= null;
   dbVec = null;
  } 
 } 

}

