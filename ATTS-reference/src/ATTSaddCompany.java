/**
 * ATTSaddCompany class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSaddCompany extends HttpServlet
{
 private DBFill dbFill = null;
 private DBFill dbFill2= null;
 private String dbName2= "ATTSemployee";
 private String dbName = "ATTScompany";
 private PrintWriter out = null;
 private Closure cls = null;
 private ATTSutility util = null;

/**
 * init's method is to initialize the servlet
 * @param none
 * @return none
 */
 public void init() throws ServletException
 {
  dbFill = new DBFill(); // ATTSairline
  dbFill2= new DBFill(); // ATTScompany
  util = new ATTSutility();
 }

/**
 * destroy's method is to end the life-cycle of the servlet
 * @param none
 * @return none
 */
 public void destroy() 
 {
  dbFill.closeDatabase();  // ATTScompany
  dbFill2.closeDatabase(); // ATTSemployee
  util.close();
 }

/**
 * doGet's method is to process GET method from user
 * @param HttpServletRequest req
 *        HttpServletResponse res
 * @return none
 */
 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 }

/**
 * doPost's method is to process POST method from user
 * @param HttpServletRequest req contains user request
 *        HttpServletResponse res contains user respond
 * @return none
 */
 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS add company</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<BR></BR><H1></H1><HR></HR><BR></BR>"); 

  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException("<BR>Failure to make initial connection dbName</BR>");
   if(!dbFill2.makeInitialConnection(dbName2,out))
     throw new SQLException("<BR>Failure to make initial connection dbName2</BR>");
   createForm(req);
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
  }  
 } // end of doPost 

/**
 * createForm's method is to process the user request to add company to ATTScompany
 * It creates the form that will let user chooses the company executives.
 * @param  HttpServletRequest req contains user request
 * @return none
 */
 public void createForm(HttpServletRequest req) throws SQLException
 {
  String cName = req.getParameter("cName");
  String cFound= req.getParameter("cFound");  // date
  String cOwner= req.getParameter("cOwner");
  String cWeb  = req.getParameter("cWeb");    // may to be blank
  String cEmail= req.getParameter("cEmail");  // may to be blank
  String offBeg= req.getParameter("ohBegin"); // time
  String offEnd= req.getParameter("ohEnd");   // time
  String offDay= req.getParameter("oWork");
  String offFnd= req.getParameter("oFound");  // date
  String offStt= req.getParameter("oStatus");
  String offAdd= req.getParameter("oAddress");
  String offCty= req.getParameter("oCity");
  String offCnt= req.getParameter("oCountry");
  String offPhn= req.getParameter("ocPhone");
  String offFax= req.getParameter("ocFax");  // may to be blank
  String cWk   = req.getParameter("cWork");
  String cSk   = req.getParameter("cSick");
  String cVt   = req.getParameter("cVacation");
  String eID   = req.getParameter("eID");
  String ePK   = req.getParameter("ePK");
  String ePW   = req.getParameter("ePW");

  if((cName==null)||(cFound==null)||(cOwner==null)||(cWeb==null)||
     (cEmail==null)||(eID==null)||(ePK==null)||(ePW==null)||(offBeg==null)||
     (offEnd==null)||(offDay==null)||(offFnd==null)||(offStt==null)||
     (offAdd==null)||(offCty==null)||(offCnt==null)||(offPhn==null)||
     (offFax==null)||(cWk==null)||(cSk==null)||(cVt==null))
  {
   cls.error(1,null);
   return;  
  }
  boolean errorFlag = false; 
  if(cName.length()<1) 
  {
   cls.error(0,"Anda harus mengisi Company Name");
   errorFlag = true;
  }
  if(cFound.length()<1)
  {
   cls.error(0,"Anda harus mengisi Company Founded Date");
   errorFlag = true;
  }
  if((eID.length()<1)||(ePK.length()<1)||(ePW.length()<1))
  {
   cls.error(0,"Anda harus mengisi Employee ID/PassKey/Password anda");
   errorFlag = true;
  }
  if((cEmail.length()>1)&&(cEmail.indexOf('@')==-1))
  {
   cls.error(0,"Email anda harus mempunyai @ contoh: saya@boleh.com");
   errorFlag = true;
  }
  if((cWeb.length()>1)&&(!cWeb.startsWith("http://",0)))
  {
   cls.error(0,"Web Address harus dimulai dengan prefix: http://");
   errorFlag = true;
  }
  if((!util.checkDigit(ePK,true,out))||(!util.checkDigit(eID,true,out)))
  {
   cls.error(3,"Employee ID atau Employee PassKey");
   errorFlag = true; 
  }
  if((offBeg.length()<1)||(offEnd.length()<1))
  {
   cls.error(0,"Office Begin Hours/End Hours harus di-isi");
   errorFlag = true;
  }
  if(offDay.length()<1)
  {
   cls.error(0,"Office Work Days harus di-isi");
   errorFlag = true;
  }
  if(offFnd.length()<1)
  {
   cls.error(0,"Office Founded Date harus di-isi");
   errorFlag = true;
  }
  if(offStt.length()<1)
  {
   cls.error(0,"Office State harus di-isi");
   errorFlag = true;
  }
  if((offAdd.length()<1)||(offCty.length()<1)||(offCnt.length()<1))
  {
   cls.error(0,"Office Address/City/Country harus di-isi");
   errorFlag = true;
  }
  if((cWk.length()<1)||(cVt.length()<1)||(cSk.length()<1))
  {
   cls.error(0,"Company Work Days/Sick Days/Vacation Days harus di-isi");
   errorFlag = true;
  }
  if(errorFlag) 
    return; 

  DateUtil du = new DateUtil();
  if(!du.checkSQLDate(cFound,out)) 
  {
   cls.error(4,"Company Founded");
   errorFlag = true;
  }
  Vector employee = util.queryAllEmp(dbFill2,"WHERE employeeValid=\"Y\";",out);
  if(employee == null)
  {
   cls.error(0,"Tidak dapat menunjuk para executives perusahaan tanpa karyawan");
   return;
  }
  if(!util.verifyAuthorization(dbFill2,cls,out,Integer.parseInt(eID),ePK,ePW,2,null))
  {
   cls.error(7,null);
   errorFlag = true;
  }
  if((!util.checkTime(offBeg,out))||(!util.checkTime(offEnd,out)))
  {
   cls.error(3,"Office Begin Hour/End Hour");
   errorFlag = true; 
  }
  if(!util.checkDigit(offDay,true,out))
  {
   cls.error(3,"Office Work Days");
   errorFlag = true;
  }
  if(!du.checkSQLDate(offFnd,out))
  {
   cls.error(4,"Office Founded [date]");
   errorFlag = true;
  }
  else if(!du.checkSQLDateValidity(offFnd,4,out))
  {
   cls.error(4,"Office Founded [date]");
   errorFlag = true;
  }
  if(offStt.compareToIgnoreCase("NON")==0)
  {
   cls.error(0,"Office Status - anda harus memilih");
   errorFlag = true;
  }
  if((!util.checkDigit(cWk,true,out))||(!util.checkDigit(cSk,true,out))||
     (!util.checkDigit(cVt,true,out)))
  {
   cls.error(3,"Company Work Days/Sick Days/Vacation Days");
   errorFlag = true;
  }
  if(!util.checkDigit(offDay,true,out))
  {
   cls.error(3,"Office Work Days");
   errorFlag = true;
  }
  if(errorFlag)
    return; 
  Vector v = new Vector();
  DBEncapsulation dbI = new DBEncapsulation("company");  
  dbI.setColumnNameVal("companyName",cName);
  dbI.setColumnNameVal("companyFounded",cFound);
  if(cOwner.compareToIgnoreCase("PRIVATE")!=0)
    dbI.setColumnNameVal("companyOwnership",cOwner);
  if(cWeb.length()>1)
    dbI.setColumnNameVal("companyWeb",cWeb);
  if(cEmail.length()<1)
    dbI.setColumnNameVal("companyEmail",cEmail);
  dbI.setColumnNameVal("companyLastChange","CURRENT_DATE");
  dbI.setColumnNameVal("companyLastChangeBy",eID);
  dbI.setVariable("SET @companyID = LAST_INSERT_ID();");
  v.addElement(dbI.clone());
  dbI.clearAll();
  dbI.setDBTableName("companyOffice");
  dbI.setColumnNameVal("officeOfCompanyID","@companyID");
  dbI.setColumnNameVal("officeStatus",offStt);
  dbI.setColumnNameVal("officeRegHourOpen",offBeg);
  dbI.setColumnNameVal("officeRegHourClose",offEnd);
  dbI.setColumnNameVal("officeRegWorkDay",offDay);
  dbI.setColumnNameVal("officeFounded",offFnd);
  dbI.setColumnNameVal("officeAddr",offAdd);
  dbI.setColumnNameVal("officeCity",offCty);
  dbI.setColumnNameVal("officeCountry",offCnt);
  dbI.setVariable("SET @officeKey = LAST_INSERT_ID();");
  v.addElement(dbI.clone());
  dbI.clearAll();
  dbI.setDBTableName("companyOfficeContact");
  dbI.setColumnNameVal("officeContactPhone",offPhn);
  dbI.setColumnNameVal("officeContactFax",offFax);
  dbI.setColumnNameVal("officeContactOf","@officeKey");
  v.addElement(dbI.clone());
  dbI.clearAll();
  dbI.setDBTableName("companyRules");
  dbI.setColumnNameVal("rulesWorkHours",cWk);
  dbI.setColumnNameVal("rulesSickDays",cSk);
  dbI.setColumnNameVal("rulesVacationDays",cVt);
  dbI.setColumnNameVal("rulesOfCompanyID","@companyID");
  v.addElement(dbI.clone());
  dbI.destroyAll();
  dbFill.InsertDatabase(v,out);
  out.println("<BR><h1>Successfully add a new company to database</h1></BR>");
  out.println("<BR></BR>To add office please click the link below");
  v.removeAllElements();
 }

} // end of class 

