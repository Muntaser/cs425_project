/**
 * ATTSviewRoute class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSmanageCompany extends HttpServlet
{
 private DBFill dbFill = null;
 private DBFill dbFill2= null;
 private String dbName = "ATTScompany";
 private String dbName2= "ATTSemployee";
 private PrintWriter out = null;
 private Closure cls = null;
 private ATTSutility util = null;

/**
 * init's method is to initialize the servlet
 * @param none
 * @return none
 */
 public void init() throws ServletException
 {
  dbFill = new DBFill();
  dbFill2= new DBFill();
  util = new ATTSutility();
 }

/**
 * destroy's method is to end the life-cycle of the servlet
 * @param none
 * @return none
 */
 public void destroy() 
 {
  dbFill.closeDatabase();
  dbFill2.closeDatabase();
  util.close();
 }

/**
 * doGet's method is to process GET method from user
 * @param HttpServletRequest req
 *        HttpServletResponse res
 * @return none
 */
 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 }

/**
 * doPost's method is to process POST method from user
 * @param HttpServletRequest req contains user request
 *        HttpServletResponse res contains user respond
 * @return none
 */
 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS Manage Company</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<BR></BR><H1></H1><HR></HR><BR></BR>"); 
  String view = req.getParameter("view");
  if(view == null)
  {
   cls.error(1,"view");
   return;
  }

  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException("<BR>Failure to make initial connection : "+dbName+"</BR>");
   if(!dbFill2.makeInitialConnection(dbName2,out))
    throw new SQLException("<BR>Failure to make initial connection : "+dbName2+"</BR>");
   if(view.compareToIgnoreCase("LOG")==0)
    processLog(req);
   else if(view.compareToIgnoreCase("PRESENT")==0)
    processPresent(req);
   else if(view.compareToIgnoreCase("PROCESS")==0)
    process(req);
   else
    cls.error(6,null);
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
  }  
 } // end of doPost 

/**
 * processLog's method is to process  user login into the manageCompany modules.
 * It allows users with access level of 1/2 to manage the company.
 * @param HttpServletRequest req - contains user request to manage the company
 * @return none
 */
 public void processLog(HttpServletRequest req) throws SQLException
 {
  String empID = req.getParameter("eID");
  String empPK = req.getParameter("ePK");
  String empPW = req.getParameter("ePW");
  if((empID==null)||(empPK==null)||(empPW==null))
  {
   cls.error(1,"Employee ID/PassKey/Password");
   return;
  }
  if((empID.length()<1)||(empPK.length()<1)||(empPW.length()<1))
  {
   cls.error(2,null);
   return;
  }
  if(!util.checkDigit(empID,true,out))
  {
   cls.error(0,"Employee ID harus dalam bentuk angka/digit");
   return;
  }
  if(!util.checkDigit(empPK,true,out))
  {
   cls.error(0,"Employee PassKey harus dalam bentuk angka/digit");
   return;
  }
  if(!util.verifyAuthorization(dbFill2,cls,out,Integer.parseInt(empID),empPK,empPW,2,
     null))
  {
   cls.error(0,"Mungkin anda salah memasukan password/tak cukup right accessnya");
   return;
  } 
  createForm(Integer.parseInt(empID));
 }

/**
 * createForm's method is to create form for users to choose which company that
 * user would like to choose from. It queries to company's db if none available
 * it will display the error message and return. else it displays the info.
 * @param none
 * @return none
 */
 private void createForm(int eid) throws SQLException
 {
  Vector v = dbFill.QuerySpecific("company",null,out);
  if(v == null)
  {
   cls.error(0,"Database belum ada perusahaan");
   return;
  }
  int size = v.size();
  DBEncapsulation dbI = null;  
  int id = 0;
  util.createBegin("ATTSmanageCompany","view","PRESENT",true,out);
  out.println("<INPUT TYPE=HIDDEN NAME=\"eID\" VALUE=\""+eid+"\">");
  for(int i=0;i<size;i++)
  {
   dbI = (DBEncapsulation)((DBEncapsulation)v.get(i)).clone();
   out.println("<TR><TD><INPUT TYPE=RADIO NAME=\"CHOICE\" VALUE=\"");  
   id = dbI.getColumnIntegerValue(dbI.getColumnNamePosition("companyID"));
   out.println(id+"\"></TD><TD>Name</TD><TD>");
   out.println(dbI.getColumnStringValue(dbI.getColumnNamePosition("companyName")));
   out.println("</TD><TD>Founded</TD><TD>");
   out.println(dbI.getColumnStringValues(dbI.getColumnNamePosition("companyFounded")));
   out.println("</TD><TD>Validity</TD><TD>");
   out.println(dbI.getColumnStringValues(dbI.getColumnNamePosition("companyValid")));
   out.println("</TD></TR>");
   dbI.destroyAll();
  }
  util.createEnd(out);
  v.removeAllElements();
 }
 
/**
 * processPresent's method is to present user requested info that user might want
 * to modify or not OR activate the company
 * @param HttpServletRequest req - contains user request
 * @return none
 */
 public void processPresent(HttpServletRequest req) throws SQLException
 {
  int choice = Integer.parseInt(req.getParameter("CHOICE"));
  int eid = Integer.parseInt(req.getParameter("eID"));
  StringBuffer str = new StringBuffer();
  str.append("WHERE companyID=\""+choice+"\";");
  DBEncapsulation dbC = dbFill.QuerySingleSpecific("company",str.toString(),out);
  DBEncapsulation dbE = null;
  if(dbC == null)
  {
   cls.error(0,"Tidak ditemukan dalam database company yang anda pilih");
   return;
  }
  util.createBegin("ATTSmanageCompany","view","PROCESS",true,out);
  out.println("<INPUT TYPE=HIDDEN NAME=\"CHOICE\" VALUE=\""+choice+"\">");
  out.println("<INPUT TYPE=HIDDEN NAME=\"eID\" VALUE=\""+eid+"\">");
  out.println("<TR><TD>Nama Perusahaan</TD><TD>");
  out.println(dbC.getColumnStringValue(dbC.getColumnNamePosition("companyName")));
  out.println("</TD><TD><INPUT TYPE=TEXT SIZE=40 NAME=\"cName\" MAXLENGTH=64></TD></TR>");
  out.println("<TR><TD>Tanggal Pendirian</TD><TD>");
  out.println(dbC.getColumnStringValue(dbC.getColumnNamePosition("companyFounded")));
  out.println("</TD><TD><INPUT TYPE=TEXT SIZE=20 MAXLENGTH=10 NAME=\"cFound\"></TD></TR>");
  out.println("<TR><TD>Tanggal Penuntupan Perusahaan</TD><TD>");
  out.println(dbC.getColumnStringValue(dbC.getColumnNamePosition("companyEnd")));
  out.println("</TD><TD><INPUT TYPE=TEXT SIZE=20 MAXLENGTH=10 NAME=\"cEnd\"></TD></TR>");
  out.println("<TR><TD>Kepimilikan perusahaan</TD><TD>");
  out.println(dbC.getColumnStringValue(dbC.getColumnNamePosition("companyOwnership")));
  out.println("</TD><TD><SELECT NAME=\"cOwner\"><OPTION SELECTED VALUE=\"NON\">");
  out.println(" -- Rubah/Tetap -- </OPTION><OPTION VALUE=\"PRIVATE\">PRIVATE</OPTION>");
  out.println("<OPTION VALUE=\"PUBLIC\">PUBLIC</OPTION></SELECT></TD></TR>");
  out.println("<TR><TD>Website Perusahaan</TD><TD>");
  int p1 = dbC.getColumnNamePosition("companyWeb");
  if(dbC.getColumnStringValue(p1).length()<2)
   out.println("belum tersedia");
  else
   out.println(dbC.getColumnStringValue(p1));
  out.println("</TD><TD><INPUT TYPE=TEXT NAME=\"cWeb\" SIZE=40 MAXLENGTH=32></TD></TR>");
  out.println("<TR><TD>Email Perusahaan</TD><TD>");
  p1 = dbC.getColumnNamePosition("companyEmail");
  if(dbC.getColumnStringValue(p1).length()<2)
   out.println("belum tersedia");
  else
   out.println(dbC.getColumnStringValue(p1));
  out.println("</TD><TD><INPUT TYPE=TEXT NAME=\"cEmail\" SIZE=40 MAXLENGTH=24></TD></TR>");
  out.println("<TR><TD>Company Valid</TD><TD>");
  out.println(dbC.getColumnStringValues(dbC.getColumnNamePosition("companyValid"))+"</TD>");
  out.println("<TD><SELECT NAME=\"eValid\">");
  out.println("<OPTION SELECTED VALUE=\"NON\"> -- Rubah Validity --</OPTION>");
  out.println("<OPTION VALUE=\"Y\">YES</OPTION><OPTION VALUE=\"N\">NO</OPTION></SELECT>");
  out.println("</TD></TR>"); 
  // print out chief executive officer - CEO
  out.println("<TR><TD>Name Chief Of Executive Office</TD>");
  int id = dbC.getColumnIntegerValue(dbC.getColumnNamePosition("companyCEO"));
  if(id==0)
   out.println("<TD>belum dipilih</TD>");
  else
  { 
   str.append("WHERE employeeID=\""+id+"\";");
   dbE = dbFill2.QuerySingleSpecific("employee",str.toString(),out);
   if(dbE == null)
    out.println("<TD>tidak ditemukan employee dengan id "+id+"</TD>");
   else
   {
    p1 = dbE.getColumnNamePosition("employeeName");
    out.println("<TD>"+dbE.getColumnStringValue(p1)+"</TD>");
   }
   dbE.destroyAll();
  } 
  str.delete(0,str.length());
  str.append("WHERE employeeValid=\"Y\" AND employeeAccessLvl=\"1\";");
  Vector ve = dbFill2.QuerySpecific("employee",str.toString(),out);
  if(ve == null)
  {
   cls.error(0,"Employee Database belum ada pegawai yang active dengan accessLvl 1");
   return;
  } 
  out.println("<TD><SELECT NAME=\"eCEO\"><OPTION VALUE=\"-1\" SELECTED> -- Rubah CEO -- ");
  out.println("</OPTION>");
  printEmployee(ve,"OPTION");
  out.println("</SELECT></TD></TR>");

  // print out chief of finance officer - CFO
  out.println("<TR><TD>Name Chief of Financial Officer</TD>");
  id = dbC.getColumnIntegerValue(dbC.getColumnNamePosition("companyCFO"));
  if(id==0) 
   out.println("<TD>belum dipilih</TD>"); 
  else 
  {
   str.delete(0,str.length());
   str.append("WHERE employeeID=\""+id+"\";");
   dbE = dbFill2.QuerySingleSpecific("employee",str.toString(),out);
   if(dbE == null)
    out.println("<TD>tidak ditemukan employee dengan id " + id + "</TD>");
   else
   {
    p1 = dbE.getColumnNamePosition("employeeName");
    out.println("<TD>"+dbE.getColumnStringValues(p1)+"</TD>");
   }
   dbE.destroyAll();
  }
  out.println("<TD><SELECT NAME=\"eCFO\"><OPTION VALUE=\"-1\" SELECTED> -- Rubah CFO -- ");
  out.println("</OPTION>");
  printEmployee(ve,"OPTION");
  out.println("</SELECT></TD></TR>");
  // print out chief of information officer - CIO
  out.println("<TR><TD>Name Chief of Information Officer</TD>");
  id = dbC.getColumnIntegerValue(dbC.getColumnNamePosition("companyCIO"));
  if(id==0) 
   out.println("<TD>belum dipilih</TD>"); 
  else 
  {
   str.delete(0,str.length());
   str.append("WHERE employeeID=\""+id+"\";");
   dbE = dbFill2.QuerySingleSpecific("employee",str.toString(),out);
   if(dbE == null)
    out.println("<TD>tidak ditemukan employee dengan id " + id + "</TD>");
   else
   {
    p1 = dbE.getColumnNamePosition("employeeName");
    out.println("<TD>"+dbE.getColumnStringValues(p1)+"</TD>");
   }
   dbE.destroyAll();
  }
  out.println("<TD><SELECT NAME=\"eCIO\"><OPTION VALUE=\"-1\" SELECTED> -- Rubah CIO -- ");
  out.println("</OPTION>");
  printEmployee(ve,"OPTION");
  out.println("</SELECT></TD></TR>");
  // print out chief of operation officer - COO
  out.println("<TR><TD>Name Chief of Operation Officer</TD>");
  id = dbC.getColumnIntegerValue(dbC.getColumnNamePosition("companyCOO"));
  if(id==0) 
   out.println("<TD>belum dipilih</TD>"); 
  else 
  {
   str.delete(0,str.length());
   str.append("WHERE employeeID=\""+id+"\";");
   dbE = dbFill2.QuerySingleSpecific("employee",str.toString(),out);
   if(dbE == null)
    out.println("<TD>tidak ditemukan employee dengan id " + id + "</TD>");
   else
   {
    p1 = dbE.getColumnNamePosition("employeeName");
    out.println("<TD>"+dbE.getColumnStringValues(p1)+"</TD>");
   }
   dbE.destroyAll();
  }
  out.println("<TD><SELECT NAME=\"eCOO\"><OPTION VALUE=\"-1\" SELECTED> -- Rubah COO -- ");
  out.println("</OPTION>");
  printEmployee(ve,"OPTION");
  out.println("</SELECT></TD></TR>");

  str.delete(0,str.length());
  str.append("WHERE rulesOfCompanyID=\""+choice+"\";");
  dbC = dbFill.QuerySingleSpecific("companyRules",str.toString(),out);
  if(dbC == null)
  {
   cls.error(0,"Tidak ditemukan dalam database company rules");
   return;
  }
  out.println("</TABLE><BR></BR><FONT SIZE=+1>Peraturan Perusahaan</FONT><BR></BR>");
  out.println("<TABLE BORDER=0 WIDTH=\"75%\">");
  out.println("<TR><TD>Jam kerja dalam satu hari</TD><TD>");
  out.println(dbC.getColumnStringValues(dbC.getColumnNamePosition("rulesWorkHours")));
  out.println("</TD><TD><INPUT TYPE=TEXT NAME=\"cWH\" SIZE=20 MAXLENGTH=2></TD></TR>");
  out.println("<TR><TD>Jumlah hari tidak masuk karena sakit dalam 1 tahun</TD>");
  out.println("<TD>"+dbC.getColumnStringValues(dbC.getColumnNamePosition("rulesSickDays")));
  out.println("</TD><TD><INPUT TYPE=TEXT NAME=\"cSick\" SIZE=20 MAXLENGTH=2></TD></TR>");
  out.println("<TR><TD>Jumlah cuti dalam 1 tahun</TD><TD>");
  out.println(dbC.getColumnStringValues(dbC.getColumnNamePosition("rulesVactionDays")));
  out.println("</TD><TD><INPUT TYPE=TEXT NAME=\"cVac\" MAXLENGTH=2 SIZE=20></TD></TR>");
  out.println("<TR><TD>Nomor ID Kepagawaian anda</TD><TD>");
  out.println("<INPUT TYPE=TEXT NAME=\"eID\" SIZE=20 MAXLENGTH=10></TD><TD></TD></TR>");
  out.println("<TR><TD>PassKey Anda</TD><TD>");
  out.println("<INPUT TYPE=PASSWORD NAME=\"ePK\" SIZE=20 MAXLENGTH=6></TD><TD></TD></TR>");
  out.println("<TR><TD>Password Anda</TD><TD>");
  out.println("<INPUT TYPE=PASSWORD NAME=\"ePW\" SIZE=40 MAXLENGTH=32></TD><TD></TD></TR>");
  util.createEnd(out); 
  dbC.destroyAll();
 
 }

/**
 * printEmployee's method is to print out all the employees in the given Vector.
 * @param Vector v - contains employee
 * @return none
 */
 private void printEmployee(Vector v,String str1)
 {
  if((v == null)||(str1==null))
   return;
  int p1 = 0;
  int size = v.size();
  DBEncapsulation e = null;

  for(int i=0;i<size;i++)
  {
   e = (DBEncapsulation)((DBEncapsulation)v.get(i)).clone();
   if(str1.compareToIgnoreCase("OPTION")==0)
   {
    out.println("<OPTION VALUE=\"");
    p1 = e.getColumnNamePosition("employeeID");
    out.println(e.getColumnStringValues(p1)+"\">");
   }
   else
    out.println("<"+str1+">");
   p1 = e.getColumnNamePosition("employeeName");
   out.println(e.getColumnStringValues(p1));
   out.println("</"+str1+">");
   e.destroyAll();
  }
 }

/**
 * process' method is to process user submitted form from processPresent's 
 * It handles all the changes made by user regarding the company
 * @param HttpServletRequest req - contains user submitted request
 * @return none
 */
 public void process(HttpServletRequest req) throws SQLException
 {
  int choice = Integer.parseInt(req.getParameter("CHOICE"));
  int eid = Integer.parseInt(req.getParameter("eID"));
  String cName = req.getParameter("cName");
  String cFound= req.getParameter("cFounded");
  String cEnd  = req.getParameter("cEnd");
  String cOwn  = req.getParameter("cOwner");
  String cWeb  = req.getParameter("cWeb");
  String cEmail= req.getParameter("cEmail");
  String cCEO  = req.getParameter("eCEO");
  String cCFO  = req.getParameter("eCFO");
  String cCIO  = req.getParameter("eCIO");
  String cCOO  = req.getParameter("eCOO");
  String cValid= req.getParameter("eValid");
  String cSick = req.getParameter("cSick");
  String cVac  = req.getParameter("cVac");
  String cWH   = req.getParameter("cWH");
  String eID   = req.getParameter("eID");
  String ePK   = req.getParameter("ePK");
  String ePW   = req.getParameter("ePW");

  if((cName==null)||(cFound==null)||(cEnd==null)||(cOwn==null)||(cWeb==null)||
    (cEmail==null)||(cCEO==null)||(cCFO==null)||(cCIO==null)||(cCOO==null)||
    (eID==null)||(ePK==null)||(ePW==null))
  {
   cls.error(1,null);
   return;
  }
  if((cName.length()<1)&&(cFound.length()<1)&&(cEnd.length()<1)&&(cWeb.length()<1)&&
    (cEmail.length()<1)&&(cValid.compareToIgnoreCase("NON")==0)&&
    (cCEO.compareToIgnoreCase("-1")==0)&&(cCFO.compareToIgnoreCase("-1")==0)&&
    (cCIO.compareToIgnoreCase("-1")==0)&&(cCOO.compareToIgnoreCase("-1")==0)&&
    (cWH.length()<1)&&(cSick.length()<1)&&(cVac.length()<1))
  {
   cls.error(0,"Anda tidak melakukan perubahaan apapun");
   return;
  }

  boolean errorFlag = false;
  DateUtil du = new DateUtil();
  if((eID.length()<1)||(ePK.length()<1)||(ePW.length()<1))
  {
   cls.error(0,"Anda harus mengisi ID Kepegawaian anda/Passkey/Password dengan lengkap");
   errorFlag = true;
  }
  if(!util.checkDigit(eID,true,out))
  {
   cls.error(3,"ID Kepagawaian anda harus dalam bentuk angka/digit");
   errorFlag = true;
  }
  if(!util.checkDigit(ePK,true,out))
  {
   cls.error(3,"PassKey anda harus dalam bentuk angka/digit");
   errorFlag = true;
  }
  if(cFound.length()>0)
  {
   if(!du.checkSQLDate(cFound,out))
   {
    cls.error(4,"Tanggal Pendirian Perusahaan");
    errorFlag = true;
   }
   else if(!du.checkSQLDateValidity(cFound,4,out))
   {
    cls.error(4,"Tanggal Pendirian Perusahaan");
    errorFlag = true;
   }
  }
  if(cEnd.length()>0)
  {
   if(!du.checkSQLDate(cEnd,out))
   {
    cls.error(4,"Tanggal Penutupan Perusahaan");
    errorFlag = true;
   }
   else if(!du.checkSQLDateValidity(cEnd,4,out))
   {
    cls.error(4,"Tanggal Penutupan Perusahaan");
    errorFlag = true;
   }
  }
  if(cSick.length()>0)
  {
   if(!util.checkDigit(cSick,true,out))
   {
    cls.error(3,"Jumlah hari tidak masuk karena sakit harus dalam bentuk angka/digit");
    errorFlag = true;
   }
  }
  if(cVac.length()>0)
  {
   if(!util.checkDigit(cVac,true,out))
   {
    cls.error(3,"Jumlah hari cuti harus dalam bentuk angka/digit");
    errorFlag = true;
   }
  }
  if(cWH.length()>0)
  {
   if(!util.checkDigit(cWH,true,out))
   {
    cls.error(3,"Jumlah jam kerja harus dalam bentuk angka/digit");
    errorFlag = true;
   }
  } 
  if(errorFlag)
  {
   du.clearTime();
   return;
  }
  if(!util.verifyAuthorization(dbFill2,cls,out,eid,ePK,ePW,2,null))
  {
   cls.error(7,null);
   du.clearTime();
   return;
  }
  Vector v = new Vector();
  DBEncapsulation e = new DBEncapsulation();
  StringBuffer str = new StringBuffer();
  if(cName.length()>1)
  {
   e.setDBTableName("company");
   e.setColumnNameVal("companyName",cName);
  }
  if(cFound.length()>1)
  {
   if(e.isDBTableNameNull())
    e.setDBTableName("company");
   e.setColumnNameVal("companyFounded",cFound);
  }
  if(cEnd.length()>1)
  {
   if(e.isDBTableNameNull())
    e.setDBTableName("company");
   e.setColumnNameVal("companyEnd",cEnd);
  }
  if(cOwn.compareToIgnoreCase("NON")!=0)
  {
   if(e.isDBTableNameNull())
    e.setDBTableName("company");
   e.setColumnNameVal("companyOwnership",cOwn);
  }
  if(cWeb.length()>1)
  {
   if(e.isDBTableNameNull())
    e.setDBTableName("company");
   e.setColumnNameVal("companyWeb",cWeb);
  }
  if(cEmail.length()>1)
  {
   if(e.isDBTableNameNull())
    e.setDBTableName("company");
   e.setColumnNameVal("companyEmail",cEmail);
  }
  if(cCEO.compareToIgnoreCase("-1")!=0)
  {
   if(e.isDBTableNameNull())
    e.setDBTableName("company");
   e.setColumnNameVal("companyCEO",cCEO);
  }
  if(cCFO.compareToIgnoreCase("-1")!=0)
  {
   if(e.isDBTableNameNull())
    e.setDBTableName("company");
   e.setColumnNameVal("companyCFO",cCFO);
  }
  if(cCIO.compareToIgnoreCase("-1")!=0)
  {
   if(e.isDBTableNameNull())
    e.setDBTableName("company");
   e.setColumnNameVal("companyCIO",cCIO);
  }
  if(cCOO.compareToIgnoreCase("-1")!=0)
  {
   if(e.isDBTableNameNull())
    e.setDBTableName("company");
   e.setColumnNameVal("companyCOO",cCOO);
  }
  if(cValid.compareToIgnoreCase("NON")!=0)
  {
   if(e.isDBTableNameNull())
    e.setDBTableName("company");
   e.setColumnNameVal("companyValid",cValid);
  }
  e.setColumnNameVal("companyLastChange","CURRENT_DATE");
  e.setColumnNameVal("companyLastChangeBy",eid);
  if(!e.isDBTableNameNull())
  {
   str.append("WHERE companyID=\""+choice+"\";");
   e.setCondition(str.toString());
   str.delete(0,str.length());
  }
  v.addElement(e.clone());
  e.clearAll();

  if(cSick.length()>0)
  {
   e.setDBTableName("companyRules");
   e.setColumnNameVal("rulesSickDays",cSick);
  }
  if(cVac.length()>0)
  {
   if(e.isDBTableNameNull())
    e.setDBTableName("companyRules");
   e.setColumnNameVal("rulesVacationDays",cVac);
  }
  if(cWH.length()>0)
  {
   if(e.isDBTableNameNull())
    e.setDBTableName("companyRules"); 
   e.setColumnNameVal("rulesWorkHours",cWH);
  }
  if((cSick.length()>0)||(cVac.length()>0)||(cWH.length()>0))
  {
   str.append("WHERE rulesOfCompanyID=\""+choice+"\";");
   e.setCondition(str.toString());
   str.delete(0,str.length());
  }
  du.clearTime();
 }

} // end of ATTSmanageCompany

