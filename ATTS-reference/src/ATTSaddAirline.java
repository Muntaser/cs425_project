/**
 * ATTSaddAirline class
 * @author Edison Chindrawaly
 * @version 1.0 alpha
 */

package ATTS;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSaddAirline extends HttpServlet
{
 private DBFill dbFill = null;
 private String dbName = "ATTSairline";
 private ATTSutility util = null;
 private Closure cls = null;
 private PrintWriter out = null;

 public void init() throws ServletException
 {
  dbFill = new DBFill();
  util = new ATTSutility();
 }

 public void destroy()
 {
  dbFill.closeDatabase();
  util.close();
 }

/**
 * doGet method is to serve the GET method
 * @param HttpServletRequest req
 *	  HttpServletResponse res
 */
 public void doGet(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
 {
  doPost(req,res);
 }

/**
 * doPost method is to serve the POST method
 * @param HttpServletRequest req
 *	  HttpServletResponse res
 */
 public void doPost(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");

  try
  {
   process(req);
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
  }
 }

/**
 * process' method is to process user request and insert the user requests into
 * DBEncapsulation to be input into database.
 * @param HttpServletRequest req contains user request
 * @return none
 */
 private void process(HttpServletRequest req) throws SQLException
 {
  String airName = req.getParameter("airlineName");
  String airAbbr = req.getParameter("airlineAbbr");
  String airAdd = req.getParameter("airlineAdd");
  String airCity = req.getParameter("airlineCity");
  String airPhone = req.getParameter("airlinePhone");
  String airFax = req.getParameter("airlineFax");
  String airInfoNote = req.getParameter("airlineInfoNote");
  String airNote = req.getParameter("airlineNote");

  out.println("<HTML><HEAD><TITLE>ATTS add airline</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE VLINK=BLACK ALINK=RED>");
  out.println("<BR></BR>");
  out.println("<H1>ATTS Add Airline</H1>");
  out.println("<HR></HR><BR></BR>");
  
  if((airName==null)||(airAbbr==null)||(airAdd==null)||(airCity==null)||
     (airPhone==null)||(airFax==null)||(airInfoNote==null)||(airNote==null))
  {
   cls.error(1,null);
   return;
  }
  if((airName.length()<=1)||(airAbbr.length()<=1)||(airAdd.length()<=1)||
     (airCity.length()<=1)||(airPhone.length()<=1))
  {
   cls.error(2,null);
   return;
  }
  if((!util.checkDigit(airPhone,false,out))||(!util.checkDigit(airFax,false,out))) 
  {
   cls.error(3,"Phone/Fax");
   return;
  }

  if(!dbFill.makeInitialConnection(dbName,out))
   throw new SQLException("<BR>Failure to make initial connection</BR>");

  int p1 = 0;
  DBEncapsulation db = null;
  Vector dbVec = new Vector();
  StringBuffer cond = new StringBuffer();
  cond.append("WHERE cityName=\""+airCity+"\";");
  db = dbFill.QuerySingleSpecific("airlineCity",cond.toString(),out);
  if(db == null)
  {
   cls.error(0,"Airline City yg anda isi belum terdapat dalam database.");
   cls.error(0,"Harap di-isi terlebih dahulu data-data airline city");
   return; 
  } 
  p1 = db.getColumnNamePosition("cityKey");
  if(p1<0)
  {
   cls.error(0,"cityKey not found - perhaps wrong inquery to db");
   return;
  }
  int airKey = db.getColumnIntegerValue(p1);   
  db.clearAll();

  if(airNote.length()>1)
  { 
   db.setDBTableName("airlineBigNote");
   db.setColumnNameVal("bigNote",airNote);
   db.setVariable("SET @airlineBigNoteKey = LAST_INSERT_ID();"); 
   dbVec.addElement(db.clone());
   db.clearAll();
  }
  if(airInfoNote.length()>1)
  {
   db.setDBTableName("airlineSmallNote");
   db.setColumnNameVal("smallNote",airInfoNote);
   db.setVariable("SET @airInfoNote = LAST_INSERT_ID();");
   dbVec.addElement(db.clone());
   db.clearAll();
  }
  db.setDBTableName("airline");
  db.setColumnNameVal("airlineName",airName);
  db.setColumnNameVal("airlineAbbr",airAbbr);
  if(airInfoNote.length()>1)
    db.setColumnNameVal("airlineAgentInfoSNote","@airInfoNote");
  if(airNote.length()>1)
    db.setColumnNameVal("airlineBNote","@airlineBigNoteKey");
  db.setVariable("SET @airlineID = LAST_INSERT_ID();");
  dbVec.addElement(db.clone());
  db.clearAll();

  db.setDBTableName("airlineInfo");
  db.setColumnNameVal("airlineAdd",airAdd);
  db.setColumnNameVal("airlineAddCity",airKey);
  db.setColumnNameVal("airlineOfID","@airlineID");
  db.setVariable("SET @airlineInfoKey = LAST_INSERT_ID();");
  dbVec.addElement(db.clone());
  db.clearAll();
 
  db.setDBTableName("airlinePhone");
  db.setColumnNameVal("airlinePhoneNumber",airPhone);
  if(airFax.length()>1)
   db.setColumnNameVal("airlineFaxNumber",airFax);
  db.setColumnNameVal("airlineOfID","@airlineID");
  db.setColumnNameVal("airlineInfoAddKey","@airlineInfoKey");
  dbVec.addElement(db.clone());
  db.clearAll();
  dbFill.InsertDatabase(dbVec,out);
  out.println("<H2>Successfully Add Airline</H2>");
  
  dbVec.removeAllElements();
  dbVec = null;
  db.destroyAll();
  db = null;
  cond.delete(0,cond.length());
  cond = null;
 } 

}
