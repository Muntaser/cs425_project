/**
 * ATTSviewCurrency class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSviewCurrency extends HttpServlet
{
 private DBFill dbFill;
 private String dbName = "ATTSairline";
 private PrintWriter out = null;
 private ATTSutility util = null;
 private Closure cls = null;

 public void init() throws ServletException
 {
  String driver = "com.mysql.jdbc.Driver";
  String url = "jdbc:mysql://starcloud:3306/";
  String user= "edisonch";
  String pwd = "gblj21mysql03";
  dbFill = new DBFill(driver,url,user,pwd);
  util = new ATTSutility();
 }
 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
 }
 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS View Currency</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<HR></HR>");
  String view = req.getParameter("view");
  if((view==null)||(view.length()<1)||(view.compareToIgnoreCase("ALL")!=0))
  {
   out.println("doGet of ATTSviewCurrency is null or has incorrect parameter");
   cls.makeClosing();
   cls.closing();
   out.close();
   return;
  }   

  DBEncapsulation dbQ = new DBEncapsulation("airlineCurrency");
  dbQ.setColumnName("*");
  DBEncapsulation dbT = null;
  DBEncapsulation dbJ = null;
  Vector vec = null;

  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
   {
    out.println("<BR>makeInitialConnection failed");
    throw new SQLException();
   }
   vec = dbFill.QueryDatabase((DBEncapsulation)dbQ.clone(),out);
   dbQ.clearAll();
   if(vec == null)
   {
    cls.error(5,"currency belum di set");
    return;
   }
   int vecsize = vec.size();
   int pos1 = 0;
   int pos2 = 0;
   for(int a=0;a<vecsize;a++)
   {
    dbT = new DBEncapsulation((DBEncapsulation)((DBEncapsulation)vec.get(a)).clone()); 
    pos1 = dbT.getColumnNamePosition("currencyDescSNote");
    dbQ.setDBTableName("airlineSmallNote");
    dbQ.setColumnName("smallNote");
    dbQ.setCondition("WHERE airlineSmallNoteKey="+dbT.getColumnIntegerValue(pos1));
    dbJ = new DBEncapsulation(dbFill.QuerySingleResult(dbQ,out)); 
    if(dbJ == null)
      throw new SQLException("query to airlineSmallNote resulted in null");
    pos2 = dbJ.getColumnNamePosition("smallNote");
    if((pos2!=dbJ.NOT_FOUND)&&(pos2!=dbJ.SAME_SIZE)&&(pos2!=dbJ.OUT_RANGE))
      dbT.replaceValue(pos1,dbJ.getColumnStringValue(pos2)); 
    dbQ.clearAll();
    dbJ.destroyAll();
    vec.removeElementAt(a);
    vec.insertElementAt(dbT.clone(),a);
    dbT.destroyAll();
   }
   dbFill.viewContainer(vec,out);
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
   out = null;
   if((!vec.isEmpty())&&(vec!=null))
   {
     vec.removeAllElements(); 
     vec = null;
   }
  }
 } 

}

