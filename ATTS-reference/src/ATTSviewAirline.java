package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSviewAirline extends HttpServlet
{

 private DBFill dbFill = null;
 private String dbName = "ATTSairline";
 private PrintWriter out = null;
 private Closure cls = null;

 public void init() throws ServletException
 {
  String driver="com.mysql.jdbc.Driver";
  String url ="jdbc:mysql://starcloud:3306/";
  String user="edisonch";
  String pwd ="gblj21mysql03";
  dbFill = new DBFill(driver,url,user,pwd);
 }

 public void destroy()
 {
  dbFill.closeDatabase();
 }

 public void doGet(HttpServletRequest req,HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 } // end of doGet
 
 public void doPost(HttpServletRequest req,HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>View Airline Result</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  String view = req.getParameter("view");
  if(view == null)
  {
   cls.error(1,"Parameter view has null value");
   cls.makeClosing();
   cls.closing();
   out.close();
   return;
  }
  
  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException("<BR>><B>Error in making connection</B></BR>"); 
   if(view.compareToIgnoreCase("ALL")==0)
     viewForm();
   else if(view.compareToIgnoreCase("ADD")==0)
     addAddressForm();
   else if(view.compareToIgnoreCase("PROCESS_ALL")==0)
     processView(req);
   else if(view.compareToIgnoreCase("PROCESS_ADD")==0)
     processAdd(req);
   else
     cls.error(6,null);
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
  } 
  return;
 }

/**
 * queryAirline's method is to retrieve airline from db if available
 * @param none
 * @return none
 */
 private Vector queryAirline() throws SQLException
 {
  DBEncapsulation dbQ = new DBEncapsulation("airline");
  dbQ.setColumnName("airlineID");
  dbQ.setColumnName("airlineName");
  return dbFill.QueryDatabase(dbQ,out);
 }

/**
 * queryAirlineCity's method is to retrieve airline city from db if available
 * @param none
 * @return none
 */
 private Vector queryAirlineCity() throws SQLException
 {
  DBEncapsulation dbQ = new DBEncapsulation("airlineCity");
  dbQ.setColumnName("cityKey");
  dbQ.setColumnName("cityName");
  dbQ.setColumnName("cityCountry");
  return dbFill.QueryDatabase(dbQ,out);
 }

/**
 * viewForm's method is to present form for user to view
 * @param none
 * @return none
 */
 public void viewForm() throws SQLException
 {
  out.println("<hr></hr>");
  boolean success = true;
  Vector vAirline = queryAirline();
  if(vAirline == null)
  {
   cls.error(5,"airline");
   success=false;
  }
  Vector vCity = queryAirlineCity();
  if(vCity == null)
  {
   cls.error(5,"airline city");
   success=false;
  }
  if(!success)
    return;

  int pos1 = 0;
  int pos2 = 0;
  int pos3 = 0;
  DBEncapsulation dbR = null;
  StringBuffer str = new StringBuffer();

  out.println("<FORM METHOD=POST ACTION=\"ATTSviewAirline\">");
  out.println("<INPUT TYPE=HIDDEN NAME=\"view\" VALUE=\"PROCESS_ALL\">");
  out.println("<TABLE BORDER=0 WIDTH=75%>");
  out.println("<CAPTION><BR><B>Cari informasi airline atau kantor cabang");
  out.println("</B></BR></CAPTION>");
  out.println("<TR><TD>Dengan nama airline:</TD>");
  out.println("<TD><SELECT NAME=\"airlineID\">");
  out.println("<OPTION SELECTED VALUE=\"-1\">-- Pilih Airline --</OPTION>");
  out.println("<OPTION VALUE=\"0\">Semua airline yang ada</OPTION>");
  int vaSize = vAirline.size();
  for(int i=0;i<vaSize;i++)
  {
   dbR=new DBEncapsulation((DBEncapsulation)((DBEncapsulation)vAirline.get(i)).clone());
   pos1 = dbR.getColumnNamePosition("airlineID");
   pos2 = dbR.getColumnNamePosition("airlineName");
   str.append("<OPTION VALUE=\""+dbR.getColumnStringValues(pos1)+"\">");
   str.append(dbR.getColumnStringValues(pos2)+"</OPTION>");
   out.println(str.toString());
   dbR.destroyAll();
   str.delete(0,str.length());
  }
  out.println("</SELECT></TD></TR>"); 
  out.println("<TR><TD>Dengan nama kota</TD>");
  out.println("<TD><SELECT NAME=\"airCity\">");
  out.println("<OPTION SELECTED VALUE=\"-1\">-- Pilih Kota --</OPTION>");
  int vcSize = vCity.size();
  for(int i=0;i<vcSize;i++)
  {
   dbR=new DBEncapsulation((DBEncapsulation)((DBEncapsulation)vCity.get(i)).clone());
   pos1 = dbR.getColumnNamePosition("cityKey");
   pos2 = dbR.getColumnNamePosition("cityName");
   pos3 = dbR.getColumnNamePosition("cityCountry");
   str.append("<OPTION VALUE=\""+dbR.getColumnStringValues(pos1)+"\">");
   str.append(dbR.getColumnStringValues(pos2)+". Negara: "+dbR.getColumnStringValues(pos3));
   str.append("</OPTION>");
   out.println(str.toString());
   dbR.destroyAll();
   str.delete(0,str.length());
  }
  out.println("</SELECT></TD></TR>"); 
  out.println("<TR><TD>Dengan Nomor Telephone:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"airPhone\" SIZE=16 MAXLENGTH=14></TD></TR>");
  out.println("<TR><TD>Dengan Nomor Fax:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"airFax\" SIZE=16 MAXLENGTH=14></TD></TR>");
  out.println("</TABLE>");
  out.println("<INPUT TYPE=SUBMIT VALUE=\"SUBMIT\">&nbsp;");
  out.println("<INPUT TYPE=RESET VALUE=\"CLEAR\">");
  out.println("</FORM>");

  vAirline.removeAllElements();
  vCity.removeAllElements();
  dbR = null;
  str = null;
 }

/**
 * addAddressForm's method is present form for the user to add address to airline
 * @param none
 * @return none
 */
 public void addAddressForm() throws SQLException
 {
  out.println("<hr></hr>");
  boolean success = true;
  Vector vAirline = queryAirline();
  if(vAirline == null)
  {
   cls.error(5,"airline");
   success=false;
  }
  Vector vCity = queryAirlineCity();
  if(vCity == null)
  {
   cls.error(5,"airline city");
   success=false;
  }
  if(!success)
    return;

  int pos1 = 0;
  int pos2 = 0;
  int pos3 = 0;
  DBEncapsulation dbR = null;
  StringBuffer str = new StringBuffer();

  out.println("<FORM METHOD=POST ACTION=\"ATTSviewAirline\">");
  out.println("<INPUT TYPE=HIDDEN NAME=\"view\" VALUE=\"PROCESS_ADD\">");
  out.println("<TABLE BORDER=0 WIDTH=75%>");
  out.println("<CAPTION><BR><B>Menambah Informasi airline</B></BR></caption>");
  out.println("<TR><TD>Informasi untuk airline</TD>");
  out.println("<TD><SELECT NAME=\"airlineID\">");
  out.println("<OPTION VALUE=\"-1\">-- Pilih Airline --</OPTION>");
  int vaSize = vAirline.size();
  for(int i=0;i<vaSize;i++)
  {
   dbR=new DBEncapsulation((DBEncapsulation)((DBEncapsulation)vAirline.get(i)).clone());
   pos1=dbR.getColumnNamePosition("airlineID");
   pos2=dbR.getColumnNamePosition("airlineName");
   str.append("<OPTION VALUE=\""+dbR.getColumnStringValues(pos1)+"\">");
   str.append(dbR.getColumnStringValues(pos2)+"</OPTION>");
   out.println(str.toString());
   dbR.destroyAll();
   str.delete(0,str.length());
  }
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD>Untuk kota:</TD>");
  out.println("<TD><SELECT NAME=\"airCity\">");
  out.println("<OPTION VALUE=\"-1\">-- Pilih Kota --</OPTION>");
  int vcSize = vCity.size();
  for(int i=0;i<vcSize;i++)
  {
   dbR=new DBEncapsulation((DBEncapsulation)((DBEncapsulation)vCity.get(i)).clone());
   pos1=dbR.getColumnNamePosition("cityKey");
   pos2=dbR.getColumnNamePosition("cityName");
   pos3=dbR.getColumnNamePosition("cityCountry");
   str.append("<OPTION VALUE=\""+dbR.getColumnStringValues(pos1)+"\">");
   str.append(dbR.getColumnStringValues(pos2)+". Negara: "+dbR.getColumnStringValues(pos3));
   str.append("</OPTION>");
   out.println(str.toString());
   dbR.destroyAll();
   str.delete(0,str.length());
  } 
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD>Alamat Kantor Cabang di kota diatas</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"airAdd\" SIZE=32 MAXLENGTH=48></TD></TR>");
  out.println("<TR><TD>Nomor Telephone</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"airPhone\" SIZE=32 MAXLENGTH=14></TD></TR>");
  out.println("<TD><TD>Nomor Fax</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"airFax\" SIZE=32 MAXLENGTH=14></TD></TR>");
  out.println("</TABLE>");
  out.println("<INPUT TYPE=SUBMIT VALUE=\"SUBMIT\">&nbsp;");
  out.println("<INPUT TYPE=RESET VALUE=\"CLEAR\">");
  out.println("</FORM>");
 
  vAirline.removeAllElements();
  vCity.removeAllElements();
  dbR = null;
  str = null;
 }

/**
 * processView's method is to process the request to view the info in db
 * @param HttpServletRequest req - contains user's request
 * @return none
 * @exception SQLException
 */
 public void processView(HttpServletRequest req) throws SQLException
 {
  String airID = req.getParameter("airlineID");
  String airCity = req.getParameter("airCity");
  String airPh = req.getParameter("airPhone");
  String airFax = req.getParameter("airFax"); 

  int airI = Integer.parseInt(airID);
  int airC = Integer.parseInt(airCity);
  
  if((airI==-1)&&(airC==-1)&&(airPh.length()<1)&&(airFax.length()<1))     //0000
  {
   cls.error(2,null);
   return;
  }

  DateUtil du = new DateUtil();
  boolean errorFlag = false;

  if(airPh.length()>1)
    if(!du.checkDigit(airPh,out))
      errorFlag = true;
  if(airFax.length()>1)
    if(!du.checkDigit(airFax,out))
      errorFlag = true;
  if(errorFlag)
  {
   cls.error(3,null);
   return;
  }

  Vector vecQ = new Vector();
  DBEncapsulation dbQ = new DBEncapsulation();
  DBEncapsulation dbR = null;
  StringBuffer str = new StringBuffer();
  StringBuffer id = new StringBuffer(); 
  int pos1 = 0;
  int pos2 = 0;

  if((airI>-1)&&(airC>-1)&&(airPh.length()>1)&&(airFax.length()>1))       // 1111
  {
   //fetch airline from db
   dbQ.setDBTableName("airline");
   dbQ.setColumnName("*");
   if(airI!=0)
     dbQ.setCondition("WHERE airlineID="+airI);
   vecQ = dbFill.QueryDatabase(dbQ,out);
   if(vecQ == null)
   {
    cls.error(5,null);
    return;
   }  
   if(str.length()>0)
     str.delete(0,str.length());
   dbQ.destroyAll();
   vecQ = equipAirlineNote(vecQ); // replace the airline notes with the real content 

   //fetch airlineInfo
   str.append("WHERE airlineAddCity="+airC);
   if(airI!=0)
     str.append(" AND airlineID="+airI);
   //query the database
   dbR = dbFill.QuerySingleSpecific("airlineInfo",str.toString(),out);
   if(dbR == null)
   {
    cls.error(0,"info kantor cabang airline dikota ini belum ada");
    dbQ.destroyAll();
    if(!vecQ.isEmpty())
      vecQ.removeAllElements();
    str.delete(0,str.length());
    return;
   }
   pos1 = dbR.getColumnNamePosition("airlineID");
   if((pos1!=dbR.SAME_SIZE)||(pos1!=dbR.NOT_FOUND)||(pos1!=dbR.OUT_RANGE))
    vecQ = dbFill.merge(vecQ,dbR,"airlineID",dbR.getColumnStringValues(pos1));
   id.append(dbR.getColumnStringValues(pos1));

   str.delete(0,str.length());
   str.append("WHERE airlinePhoneNumber="+airPh+" AND airlineFaxNumber="+airFax);
   //query the database
   dbQ = dbFill.QuerySingleSpecific("airlinePhone",str.toString(),out); 
   if(dbQ == null)
    cls.error(0,"Untuk kantor airline di kota ini belum ada fax dan telephone");
   else
   {
    pos1 = dbQ.getColumnNamePosition("airlineInfoAddKey");
    pos2 = dbR.getColumnNamePosition("airlineInfoKey");
    int v1 = dbQ.getColumnIntegerValue(pos1);
    int v2 = dbQ.getColumnIntegerValue(pos2);
    if(v1 != v2)
    {
     dbQ.destroyAll();
     dbQ = null;
     cls.error(0,"Untuk kantor airline di kota ini belum ada fax dan telephone");
    }
    else
     vecQ=dbFill.merge(vecQ,dbQ,"airlineID",id.toString());
    id.delete(0,id.length());
   } // end of else-statement
   vecQ = equipCity(vecQ);
   dbFill.viewContainer(vecQ,out);
   dbR.destroyAll();
   dbQ.destroyAll();
   vecQ.removeAllElements();
  }
  else if((airI<0)&&(airC<0)&&(airPh.length()<1)&&(airFax.length()>1))    // 0001
  {
   dbQ.setDBTableName("airlinePhone");
   dbQ.setColumnName("*");
   dbQ.setCondition("WHERE airlineFaxNumber="+airFax);
   dbR = dbFill.QuerySingleResult(dbQ,out);
   if(dbR == null)
   {
    cls.error(0,"Tidak ditemukan airline info dengan Fax number ini: "+airFax);
    return; 
   }
   pos1 = dbR.getColumnNamePosition("airlineOfID");
   if((pos1!=dbR.NOT_FOUND)||(pos1!=dbR.OUT_RANGE)||(pos1!=dbR.SAME_SIZE))
   {
    cls.error(0,"airlineOfID tidak ditemukan");
    return;
   }  
   id.append(dbR.getColumnStringValues(pos1));
   vecQ = new Vector();
   vecQ.addElement((DBEncapsulation)dbR.clone());

   pos1 = dbR.getColumnNamePosition("airlineInfoAddKey");
   pos2 = dbR.getColumnIntegerValue(pos1);
   dbQ.destroyAll();
   dbR.destroyAll();
   dbQ.setDBTableName("airlineInfo");
   dbQ.setColumnName("*");
   dbQ.setCondition("WHERE airlineInfoKey="+pos2);
   dbR = dbFill.QuerySingleResult(dbQ,out);
   if(dbR == null)
   {
    cls.error(5,"Info untuk kantor airline dengan Fax number ini:"+airFax);
    return;
   }    

   vecQ = dbFill.merge(vecQ,(DBEncapsulation)dbR.clone(),"airlineOfID",id.toString());
   vecQ = equipCity(vecQ); // consolidate city name in the vector
   dbQ.clearAll();
   dbQ.setDBTableName("airline");
   dbQ.setColumnName("*");
   dbQ.setCondition("WHERE airlineID="+id.toString());
   dbR.destroyAll();
   dbR = dbFill.QuerySingleResult(dbQ,out);
   if(dbR == null)
   {
    cls.error(5,"Airline ini tidak ada ");
    return;
   }

   vecQ = dbFill.merge(vecQ,dbR,"airlineOfID",id.toString());
   dbFill.viewContainer((Vector)vecQ.clone(),out);
   vecQ.removeAllElements();
   dbR.destroyAll();
  }
  else if((airI<0)&&(airC<0)&&(airPh.length()>1)&&(airFax.length()<1))    // 0010
  {
   dbQ.setDBTableName("airlinePhone");
   dbQ.setColumnName("*");
   dbQ.setCondition("WHERE airlinePhoneNumber="+airPh);
   dbR = dbFill.QuerySingleResult(dbQ,out); 
   if(dbR == null)
   {
    cls.error(0,"Tidak ditemukan airline info dengan Telephone number ini: "+airPh);
    return;
   }
   pos1 = dbR.getColumnNamePosition("airlineOfID");
   if((pos1!=dbR.NOT_FOUND)||(pos1!=dbR.OUT_RANGE)||(pos1!=dbR.SAME_SIZE))
   { 
    cls.error(0,"airlineOfID tidak ditemukan");
    return;
   }
   id.append(dbR.getColumnStringValues(pos1));
   vecQ = new Vector();
   vecQ.addElement((DBEncapsulation)dbR.clone());
   pos1 = dbR.getColumnNamePosition("airlineInfoAddKey");
   pos2 = dbR.getColumnIntegerValue(pos1);
  
   dbQ.clearAll();
   dbR.destroyAll();
   dbQ.setDBTableName("airlineInfo");
   dbQ.setColumnName("*");
   dbQ.setCondition("WHERE airlineInfoKey="+pos2);
   dbR = dbFill.QuerySingleResult(dbQ,out);
   if(dbR == null)
   {
    cls.error(5,"Info untuk kantor airline dengan Telephone ini "+airPh);
    return;
   }
   vecQ = dbFill.merge(vecQ,(DBEncapsulation)dbR.clone(),"airlineOfID",id.toString());
   dbQ.clearAll();
   dbQ.setDBTableName("airline");
   dbQ.setColumnName("*");
   dbQ.setCondition("WHERE airlineID="+id.toString());
   dbR.destroyAll();
   dbR = dbFill.QuerySingleResult(dbQ,out);
   if(dbR == null)
   {
    cls.error(5,"Airline ini tidak ada ");
    return;
   }
   vecQ = dbFill.merge(vecQ,dbR,"airlineOfID",id.toString());
   dbFill.viewContainer((Vector)vecQ.clone(),out); 
   vecQ.removeAllElements();
   dbR.destroyAll();
   dbQ.destroyAll();
  }
  else if((airI<0)&&(airC>-1)&&(airPh.length()<1)&&(airFax.length()<1))   // 0100
  {
   dbQ.setDBTableName("airlineCity");
   dbQ.setColumnName("*");

   if(dbR == null)
    cls.error(5," ");

   if((pos1!=dbR.NOT_FOUND)||(pos1!=dbR.OUT_RANGE)||(pos1!=dbR.SAME_SIZE))
   {

   }

   dbFill.viewContainer((Vector)vecQ.clone(),out); 
   vecQ.removeAllElements();
   dbR.destroyAll();
   dbQ.destroyAll();
  }
  else if((airI>-1)&&(airC<0)&&(airPh.length()<1)&&(airFax.length()<1))   // 1000
  {
   dbQ.setColumnName("*");

   if((pos1!=dbR.NOT_FOUND)||(pos1!=dbR.OUT_RANGE)||(pos1!=dbR.SAME_SIZE))
   if(dbR == null)
    cls.error(5," ");
   dbFill.viewContainer((Vector)vecQ.clone(),out); 
   vecQ.removeAllElements();
   dbR.destroyAll();
   dbQ.destroyAll();
  }
  else if((airI<0)&&(airC<0)&&(airPh.length()>1)&&(airFax.length()>1))    // 0011
  {
   dbQ.setColumnName("*");

   if((pos1!=dbR.NOT_FOUND)||(pos1!=dbR.OUT_RANGE)||(pos1!=dbR.SAME_SIZE))
   if(dbR == null)
    cls.error(5," ");
   dbFill.viewContainer((Vector)vecQ.clone(),out); 
   vecQ.removeAllElements();
   dbR.destroyAll();
   dbQ.destroyAll();
  }
  else if((airI<0)&&(airC>-1)&&(airPh.length()<1)&&(airFax.length()>1))   // 0101
  {
   dbQ.setColumnName("*");

   if((pos1!=dbR.NOT_FOUND)||(pos1!=dbR.OUT_RANGE)||(pos1!=dbR.SAME_SIZE))
   if(dbR == null)
    cls.error(5," ");
   dbFill.viewContainer((Vector)vecQ.clone(),out); 
   vecQ.removeAllElements();
   dbR.destroyAll();
   dbQ.destroyAll();
  }
  else if((airI>-1)&&(airC<0)&&(airPh.length()<1)&&(airFax.length()>1))   // 1001
  {
   dbQ.setColumnName("*");

   if((pos1!=dbR.NOT_FOUND)||(pos1!=dbR.OUT_RANGE)||(pos1!=dbR.SAME_SIZE))
   if(dbR == null)
    cls.error(5," ");
   dbFill.viewContainer((Vector)vecQ.clone(),out); 
   vecQ.removeAllElements();
   dbR.destroyAll();
   dbQ.destroyAll();
  }
  else if((airI<0)&&(airC>-1)&&(airPh.length()>1)&&(airFax.length()>1))   // 0111
  {
   dbQ.setColumnName("*");

   if((pos1!=dbR.NOT_FOUND)||(pos1!=dbR.OUT_RANGE)||(pos1!=dbR.SAME_SIZE))
   if(dbR == null)
    cls.error(5," ");
   dbFill.viewContainer((Vector)vecQ.clone(),out); 
   vecQ.removeAllElements();
   dbR.destroyAll();
   dbQ.destroyAll();
  }
  else if((airI>-1)&&(airC<0)&&(airPh.length()>1)&&(airFax.length()>1))   // 1011
  {
   dbQ.setColumnName("*");

   if((pos1!=dbR.NOT_FOUND)||(pos1!=dbR.OUT_RANGE)||(pos1!=dbR.SAME_SIZE))
   if(dbR == null)
    cls.error(5," ");
   dbFill.viewContainer((Vector)vecQ.clone(),out); 
   vecQ.removeAllElements();
   dbR.destroyAll();
   dbQ.destroyAll();
  }
  else if((airI>-1)&&(airC>-1)&&(airPh.length()<1)&&(airFax.length()<1))  // 1100
  {
   dbQ.setColumnName("*");

   if((pos1!=dbR.NOT_FOUND)||(pos1!=dbR.OUT_RANGE)||(pos1!=dbR.SAME_SIZE))
   if(dbR == null)
    cls.error(5," ");
   dbFill.viewContainer((Vector)vecQ.clone(),out); 
   vecQ.removeAllElements();
   dbR.destroyAll();
   dbQ.destroyAll();
  }
  else if((airI>-1)&&(airC<0)&&(airPh.length()>1)&&(airFax.length()<1))   // 1010
  {
   dbQ.setColumnName("*");

   if((pos1!=dbR.NOT_FOUND)||(pos1!=dbR.OUT_RANGE)||(pos1!=dbR.SAME_SIZE))
   if(dbR == null)
    cls.error(5," ");
   dbFill.viewContainer((Vector)vecQ.clone(),out); 
   vecQ.removeAllElements();
   dbR.destroyAll();
   dbQ.destroyAll();
  }
  else if((airI>-1)&&(airC>-1)&&(airPh.length()>1)&&(airFax.length()<1))  // 1110
  {
   dbQ.setColumnName("*");

   if((pos1!=dbR.NOT_FOUND)||(pos1!=dbR.OUT_RANGE)||(pos1!=dbR.SAME_SIZE))
   if(dbR == null)
    cls.error(5," ");
   dbFill.viewContainer((Vector)vecQ.clone(),out); 
   vecQ.removeAllElements();
   dbR.destroyAll();
   dbQ.destroyAll();
  }
  else if((airI>-1)&&(airC>-1)&&(airPh.length()<1)&&(airFax.length()>1))  // 1101
  {
   dbQ.setColumnName("*");

   if((pos1!=dbR.NOT_FOUND)||(pos1!=dbR.OUT_RANGE)||(pos1!=dbR.SAME_SIZE))
   if(dbR == null)
    cls.error(5," ");
   dbFill.viewContainer((Vector)vecQ.clone(),out); 
   vecQ.removeAllElements();
   dbR.destroyAll();
   dbQ.destroyAll();
  } 
  else
  {
   cls.error(6,null);
   return;
  }

 } // end of processView's method

/**
 * processAdd's method is to process the request to add address of the airline
 * @param HttpServletRequest req
 * @return none
 * @exception SQLException
 */
 public void processAdd(HttpServletRequest req) throws SQLException
 {
  String airID = req.getParameter("airlineID");
  String airCity = req.getParameter("airCity");
  String airAddr = req.getParameter("airAdd");
  String airPh = req.getParameter("airPhone");
  String airFax = req.getParameter("airFax");

  int airI = Integer.parseInt(airID);
  int airC = Integer.parseInt(airCity);

  if((airI==-1)&&(airC==-1)&&(airAddr.length()<1)&&(airPh.length()<1)&&
     (airFax.length()<1))
  {
   cls.error(2,null);
   return;
  } 

 } // end of processAdd's method
 
/**
 * equipAirlineNote's method is to complete the airline note. it looks for
 * certain fields and replace that field with the actual data/note
 * @param Vector v - contains the data that will be replace the note with actual note
 * @return Vector - a complete vector
 */
 private Vector equipAirlineNote(Vector v) throws SQLException
 {
  if(v == null)
    return v;
  int pos1 = 0;
  int size = v.size();
  DBEncapsulation r = null;
  boolean change = false;
  StringBuffer str = new StringBuffer();
  for(int i=0;i<size;i++)
  {
   r=new DBEncapsulation((DBEncapsulation)((DBEncapsulation)v.get(i)).clone());
   pos1 = r.getColumnNamePosition("airlineAgentInfoSNote");
   if(r.getColumnIntegerValue(pos1)!=0)
   {
    str.append("WHERE airlineSmallNote="+r.getColumnStringValues(pos1));
    r=mergeNote(1,r,str.toString(),"airlineAgentInfoSNote");
    str.delete(0,str.length());
    change = true;
   }
   pos1 = r.getColumnNamePosition("airlineBNote");
   if(r.getColumnIntegerValue(pos1)!=0)
   {
    str.append("WHERE airlineBigNote="+r.getColumnStringValues(pos1));
    r=mergeNote(3,r,str.toString(),"airlineBNote");
    str.delete(0,str.length()); 
    change = true;
   }
   if(change)
   {
    v.removeElementAt(i);
    v.insertElementAt((DBEncapsulation)r.clone(),i);
   }
   r.destroyAll(); 
  } 
  return v;
 }

/**
 * equipCity's method is to complete the information of city from the db to v
 * @param Vector v contains the information to be completed
 * @return Vector that is completed with info from db
 */
 private Vector equipCity(Vector v) throws SQLException
 {
  if(v==null)
    return null;
  int pos1 = 0;
  int pos2 = 0;
  int size = v.size();
  DBEncapsulation r = null;
  DBEncapsulation c = null;
  DBEncapsulation q = new DBEncapsulation("airlineCity");
  q.setColumnName("*");

  StringBuffer str = new StringBuffer();
  str.append("WHERE cityKey=");
  int len = str.length();
  boolean change = false;

  for(int i=0;i<size;i++)
  {
   r=new DBEncapsulation((DBEncapsulation)((DBEncapsulation)v.get(i)).clone());
   pos1 = r.getColumnNamePosition("airlineAddCity");
   if(r.getColumnIntegerValue(pos1)!=0)
   {
    str.append(r.getColumnStringValues(pos1));
    q.setCondition(str.toString());
    c = dbFill.QuerySingleResult(q,out);
    if(c == null)
    {
     cls.error(0,"Cannot find airlineCity in database");
     break;   
    } 
    pos2 = c.getColumnNamePosition("cityName");
    if((pos2!=c.NOT_FOUND)||(pos2!=c.SAME_SIZE)||(pos2!=c.OUT_RANGE))
    {
     cls.error(0,"Cannot find cityName in database");
     break;
    }
    r.replaceValue(pos1,c.getColumnStringValues(pos2));
    v.removeElementAt(i);
    v.insertElementAt((DBEncapsulation)r.clone(),i);
    str.delete(len,str.length());
   } // end of big if-statement
   else
   {
    cls.error(0,"Cannot find airlineAddCity");
    break;
   } // end of big else-statement
   r.destroyAll();
  } // end of for-loop
  return v;
 }

/**
 * mergeNote's method is complete info of notes from db to dbencapsulation
 * @param int num - determine which note to retrieve from db
 *        DBEncapsulation con - contains the dbencapsulation to be completed
 *        String cond - contains condition that needs to be looked at db
 *        String replace - contains the replaced statement in DBEncapsulation
 * @return DBEncapsulation the completed info
 */
 private DBEncapsulation mergeNote(int num,DBEncapsulation con,String cond,String replace) 
	throws SQLException
 {
  int pos1 = 0;
  int pos2 = 0;
  if((con == null)||(cond == null)||(replace == null))
  {
   cls.error(1,"Container atau String condition atau String replace");
   return con;
  }
  DBEncapsulation q = new DBEncapsulation();
  DBEncapsulation r = null;
  switch(num)
  {
   case 1 : q.setDBTableName("airlineSmallNote");
            q.setColumnName("smallNote");
            break;
   case 2 : q.setDBTableName("airlineMediumNote");
            q.setColumnName("mediumNote");
            break;
   case 3 : q.setDBTableName("airlineBigNote");
            q.setColumnName("bigNote");
            break;
   default: cls.error(6,null); 
            return con;
  }
  q.setCondition(cond);
  r = dbFill.QuerySingleResult(q,out);
  if(r == null)
  {
   if(num == 1)
    cls.error(5,"airlineSmallNote dengan condition tertentu: "+cond);
   else if(num == 2)
    cls.error(5,"airlineMediumNote dengan condition tertentu: "+cond);
   else if(num == 3)
    cls.error(5,"airlineBigNote dengan condition tertentu: "+cond);
  }
  else
  {
   if(num == 1)
   {
    pos1 = r.getColumnNamePosition("smallNote");
    pos2 = con.getColumnNamePosition(replace);
    con.replaceValue(pos2,r.getColumnStringValues(pos1)); 
   }
   else if(num == 2)
   {
    pos1 = r.getColumnNamePosition("mediumNote");
    pos2 = con.getColumnNamePosition(replace);
    con.replaceValue(pos2,r.getColumnStringValues(pos1));
   }
   else if(num == 3) 
   {
    pos1 = r.getColumnNamePosition("bigNote");
    pos2 = con.getColumnNamePosition(replace);
    con.replaceValue(pos2,r.getColumnStringValues(pos1));
   }
  }
  return con;
 } // end of mergeNote

} // end of ATTSviewAirline

