package ATTS;

public class Replacer implements Cloneable
{
 private StringBuffer orig  = null;
 private StringBuffer replace = null;

 public Replacer()
 { 
  initialize(); 
 }
 public Replacer(String orig,String replace)
 {
  initialize();
  setOriginal(orig);
  setReplacer(replace);
 }
 public void setBoth(String orig,String replace)
 {
  setOriginal(orig);
  setReplacer(replace);
 }
 public void setOriginal(String orig)
 {
  this.orig.append(orig);
 }
 public void setReplacer(String replace)
 {
  this.replace.append(replace);
 }
 public String getOriginal()
 {
  return orig.toString();
 }
 public String getReplacer()
 {
  return replace.toString();
 }
 public int getLenReplacer()
 {
  return replace.length();
 }
 public int getLenOrig()
 {
  return orig.length();
 }
 public void reset()
 {
  orig.delete(0,orig.length());
  replace.delete(0,replace.length());
 }
 public void initialize()
 {
  orig = new StringBuffer();
  replace = new StringBuffer();
 }
 protected Object clone()
 {
  try
  {
   Replacer newRep = (Replacer)(super.clone());
   newRep.setBoth(this.orig.toString(),this.replace.toString());
   return newRep;
  }
  catch(CloneNotSupportedException e)
  {
   System.err.println(e);
   return this;
  }
 }
}
