package ATTS;

import java.io.PrintWriter;

public class Closure
{
 PrintWriter out = null;

 public Closure(PrintWriter out)
 {
  this.out = out;
 }

 public void makeClosing()
 {
  out.println("<BR></BR>");
  out.println("<FONT SIZE=-1>");
  out.print("|&nbsp;<A HREF=\"index.html\">home</A>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSaddAirline.html\">add airline</A>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSviewAirline?view=ALL\">view airline</A>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSviewAirline?view=ADD\">add address of airline</a>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSaddCity.html\">add city</A>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSviewCity?view=ALL\">view city</A>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSviewClass?view=SET\">set class</A>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSviewClass?view=ALL\">view class</A>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSviewBlockSeats?view=SET\">set block seats</a>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSviewBlockSeats?view=ALL\">view block seats</a>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSsetCurrency.html\">set currency</a>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSviewCurrency?view=ALL\">view currency</a>&nbsp;|");
  out.println("<BR>");
  out.print("|&nbsp;<A HREF=\"ATTSviewExchangeRate?view=SET\">set ExchangeRate</a>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSviewExchangeRate?view=ALL\">view ExchangeRate</a>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSviewStockTicket?view=SET\">set stockTicket</a>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSviewStockTicket?view=ALL\">view stockTicket</a>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSviewRoute?view=SET\">set route</a>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSviewRoute?view=ALL\">view route</a>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSviewAirlineRules?view=SET\">set airlineRules</A>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSviewAirlineRules?view=ALL\">view airlineRules</a>&nbsp;|");
  out.println("<BR>");
  out.print("|&nbsp;<A HREF=\"ATTSaddEmployee.html\">add employee</A>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSmanageEmployee.html\">manage employee</A>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSupdateEmployee.html\">update employee</A>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSempAbsence.html\">set absence</A>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSviewEmpAbsence.html\">view absence</A>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSsetEmpLocation.html\">set location</A>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSviewEmpLocAll.html\">view location</A>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSsetClock.html\">set clock</A>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSviewClock.html\">view clock</A>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSviewEmpSalary?view=SET\">set salary</A>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSviewEmpSalary?view=ALL\">view salary</A>&nbsp;");
  out.print("|&nbsp;<A HREF=\"&nbsp;");
  out.print("|&nbsp;&nbsp;");
  out.print("|&nbsp;&nbsp;");
  out.println("<BR>");
  out.print("|&nbsp;<A HREF=\"ATTSaddCompany.html\">add company</A>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSviewCompany.html\">view company</A>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSmanageCompany.html\">manage company</A>&nbsp;");
  out.print("|&nbsp;<A HREF=\"ATTSmanageOffice.html\">manage office</A>&nbsp;");
  out.print("|&nbsp;&nbsp;");
  out.println("<BR>");
  out.print("|&nbsp;&nbsp;");

  out.println("</FONT>");
  out.println("</BODY></HTML>");
 }

 public void error(int num,String msg)
 {
  out.println("<BR><FONT COLOR=RED><B>");

  if((msg!= null)&&(num==0))
   out.println(msg);
  else
  {
   switch(num)
   {
    case 1 : 
     if(msg == null)
       out.println("Ada input yang bernilai NULL"); 
     else
       out.println(msg+" bernilai NULL");
     break;
    case 2 : 
     out.println("Anda belum mengisi semua kolom yg tersedia dengan benar."); 
     out.println("<BR>Harap diperhatikan dengan seksama semua pilihan anda.");
     break;
    case 3 : 
     if(msg == null)
       out.println("Format input anda salah. Harap mengikuti aturan yg tersedia."); 
     else
       out.println(msg+" Anda belum mengikuti aturan yg tersedia");
     break;
    case 4 : 
     out.print("Format tanggal anda salah.");
     if(msg!=null)
       out.println(msg);
     out.print("<BR>Format tanggal panjangnya haruslah ");
     out.print("10 karakters.<BR>Format Tanggal: YYYY-MM-DD ");
     out.println("contoh: 2003-01-31");
     break;
    case 5 :
     if(msg!=null)
       out.println(msg+" datanya belum tersedia didalam database");
     else
       out.println("Data yang diminta belum tersedia didalam database");
     out.println("<BR>Harap dimasukan dahulu data yg diperlukan");
     break;
    case 6:
     out.println("Permintaan anda tidak dapat termasuk dalam pelayanan");
     break;
    case 7:
     out.println("Anda tidak mempunyai access level yg cukup untuk");
     out.println(" melakukan operasi ini");
     break;
    case 8:
     if(msg!=null)
       out.println(msg);
     out.println(" Time Format anda salah. Time Format yg betul: HH:MM:SS");
     out.println(" contoh: 21:01:58");
     break;
    default: 
     out.println("This should not happen!");
     break;
   }
  }
  out.println("</B></FONT></BR>");
  return; 
 }

 public void closing()
 {
  this.out.close();
  this.out = null;
 }
}
