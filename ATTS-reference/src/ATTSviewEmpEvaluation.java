/**
 * ATTSviewRoute class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSviewEmpEvaluation extends HttpServlet
{
 private DBFill dbFill = null;
 private String dbName = "ATTSutility";
 private PrintWriter out = null;
 private Closure cls = null;
 private ATTSutility util = null;

 public void init() throws ServletException
 {
  dbFill = new DBFill();
  util = new ATTSutility();
 } // end of init

 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
  dbFill = null;
 } // end of destroy

 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 } // end of doGet

 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS View Route</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=WHITE TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  String view = req.getParameter("view");
  if(view == null)
  {
   cls.error(1,"view");
   cls.makeClosing();
   cls.closing();
   out.close();
   return;
  }
  
  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException("<BR>Fail to make initial connection</BR>");
   if(view.compareToIgnoreCase("ALL")==0)  // to fill out the evaluation
     createView();
   else if(view.compareToIgnoreCase("SET")==0) // to find out which employee not eval
     createForm(req);
   else if(view.compareToIgnoreCase("PROCESS")==0)
     processAll(req); 
   else
    cls.error(6,null);
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out); 
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
   view = null;
   out = null;
   cls = null;
  }

 } // end of doPost 

 private void createView() throws SQLException
 {
  StringBuffer cond = new StringBuffer();
  cond.append("WHERE employeePosition <> \"EXECUTIVE\";");
  Vector v = dbFill.QuerySpecific("employee",cond.toString(),out);
  if(v == null)
  {
   cls.error(5,"Employee");
   return;
  }
  DBEncapsulation r = null;
  StringBuffer str = new StringBuffer();
  int size = v.size();
  int p1 = 0;
  int p2 = 0;  
  util.createBegin("ATTSsetEmpEvaluation",null,null,false,out);
  out.println("<TR><TD>Employee to be review</TD><TD><SELECT NAME=\"eRID\">");
  out.println("<OPTION VALUE=\"-1\" SELECTED>-- Pilih ingin di-review --</OPTION>");
  for(int i=0;i<size;i++)
  {
   r=new DBEncapsulation((DBEncapsulation)v.get(i));
   p1=r.getColumnNamePosition("employeeID");
   p2=r.getColumnNamePosition("employeeName");
   str.append("<OPTION VALUE=\""+r.getColumnStringValues(p1)+"\">");
   str.append(r.getColumnStringValues(p2)+"</OPTION>");
   r.destroyAll();
  }
  out.println(str.toString());
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD>Your Employee ID</TD><TD>SELECT NAME=\"eID\">");
  out.println("<OPTION VALUE=\"-1\" SELECTED>-- Pilih Employee ID anda --</OPTION>");
  out.println(str.toString());
  out.println("</SELECT></TD><TD></TD><TD></TD></TR>");
  out.println("<TR><TD>Your Employee PassKey</TD><TD>");
  out.println("<INPUT TYPE=PASSWORD NAME=\"empPK\" SIZE=20 MAXLENGTH=6></TD>");
  out.println("<TD></TD><TD></TD></TR>");
  out.println("<TR><TD>Your Employee Password</TD><TD>");
  out.println("<INPUT TYPE=PASSWORD NAME=\"empPW\" SIZE=20 MAXLENGTH=16></TD>");
  out.println("<TD></TD><TD></TD></TR>");
  createReview();
  util.createEnd(out);
 } // end of createView's method

/**
 * createForm's method is to serve user request from html and to let user view which
 * employee evaluation he/she has not fill out
 * @param  HttpServletRequest req
 * @return none
 */
 private void createForm(HttpServletRequest req) throws SQLException
 {
  String eID = req.getParameter("empID");
  String ePK = req.getParameter("empPK");
  String ePW = req.getParameter("empPW");
  String pBG = req.getParameter("perBeg");
  String pED = req.getParameter("perEnd");
  if((eID==null)||(ePK==null)||(ePW==null)||(pBG==null)||(pED==null))
  {
   cls.error(1,null);
   return;
  }
  if((eID.length()<1)||(ePK.length()<1)||(ePW.length()<1)||
     (pBG.length()<1)||(pED.length()<1))
  {
   cls.error(2,null);
   return;
  }
  if((!util.checkDigit(eID,true,out))||(!util.checkDigit(ePK,true,out)))
  {
   cls.error(3,"Employee ID/Employee Pass Key harus dalam bentuk digit/angka");
   return;
  }
  DateUtil du = new DateUtil();
  if((!du.checkSQLDate(pBG,out))||(!du.checkSQLDate(pED,out)))
  {
   cls.error(4,"Period Begin/End");
   return;
  }
  if((!du.checkSQLDateValidity(pBG,4,out))||(!du.checkSQLDateValidity(pED,4,out)))
  {
   cls.error(4,"Period Begin/End");
   return;
  }
  StringBuffer msg = new StringBuffer();
  msg.append("WHERE empEvalByID=\""+Integer.parseInt(eID)+"\" ");
  msg.append("AND empEvalDate>=\""+pBG+"\" AND empEvalDate<=\""+pED+"\";");
  Vector v = dbFill.QuerySpecific("employeeEvaluation",msg.toString(),out);
  if(v == null)
  {
   cls.error(5,"Employee Evaluation for period ");
   return;
  }
  int size = v.size();
  int p1 = 0;
  int p2 = 0; 
  DBEncapsulation r = null;
  Vector eval = new Vector(); // contain all the evaluatedID
  for(int i=0;i<size;i++)
  {
   r = new DBEncapsulation((DBEncapsulation)v.get(i)); // contain employeeEvalution
   p1 = r.getColumnNamePosition("empEvalOfID");
   eval.addElement(new Integer(r.getColumnIntegerValue(p1))); // store evaluatedID
   r.destroyAll();
  }
  msg.delete(0,msg.length());
  msg.append("WHERE employeeValid=\"Y\";");
  Vector poolEmployee = dbFill.QuerySpecific("employee",msg.toString(),out); 
  if(poolEmployee == null) // get all valid employee ID
  {
   cls.error(5,"Employee");
   return;
  }
  boolean found = false;
  size = eval.size(); // get the size of all the evaluatedID
  for(int i=0;i<poolEmployee.size();i++)
  {
   r = new DBEncapsulation((DBEncapsulation)poolEmployee.get(i));
   p1 = r.getColumnNamePosition("employeeID");
   do
   {
    for(int j=0;j<size;j++)
      if(((Integer)eval.get(j)).intValue()==r.getColumnIntegerValue(p1)) 
      {
       poolEmployee.removeElementAt(i);
       found = true;
       break;
      }
      else
       found = false;
   } while(found);
   r.removeNameVal("employeePosition");
   r.removeNameVal("employeePassKey");
   r.removeNameVal("employeePassword");
   r.removeNameVal("employeeValid");
   r.removeNameVal("employeeAccessLvl");
   poolEmployee.removeElementAt(i);
   poolEmployee.insertElementAt(r.clone(),i);
   r.destroyAll();
  } // end of for-loop 
  // poolEmployee will contain all the employees who hadnot been evaluated
  v.removeAllElements();
  eval.removeAllElements();
  out.println("<BR>Anda belum mengevaluasi employee berikut</BR>");
  dbFill.viewContainer(poolEmployee,out); 
  poolEmployee.removeAllElements();
 } // end of createForm's method

/**
 * processAll's method is to allow access level 1's user to view the result of evaluation
 * @param HttpServletRequest req 
 * @return none
 */
 private void processAll(HttpServletRequest req) throws SQLException
 {
  String eID = req.getParameter("empID");
  String pBG = req.getParameter("perBeg");
  String pED = req.getParameter("perEnd");
  String aID = req.getParameter("authID");
  String aPK = req.getParameter("authPK");
  String aPW = req.getParameter("authPW");
  if((eID==null)||(pBG==null)||(pED==null)||(aID==null)||(aPK==null)||(aPW==null))
  {
   cls.error(1,null);
   return;
  }
  if((eID.length()<1)||(pBG.length()<1)||(pED.length()<1)||
     (aID.length()<1)||(aPK.length()<1))
  {
   cls.error(2,null);
   return;
  }
  if((!util.checkDigit(eID,true,out))||(!util.checkDigit(aID,true,out))||
     (!util.checkDigit(aPK,true,out)))
  {
   cls.error(3,"Employee ID/Authorized Employee ID/Authorized Employee PassKey");
   return;
  }
  int eid = Integer.parseInt(eID);
  int aid = Integer.parseInt(aID);
  if((eid<0)||(aid<=0))
  {
   cls.error(3,"EmployeeID/Authorized EmployeeID");
   return;
  }
  DateUtil du = new DateUtil();
  if((!du.checkSQLDate(pBG,out))||(!du.checkSQLDate(pED,out)))
  {
   cls.error(4,"Period Begin/Period End");
   return;
  }
  else if((!du.checkSQLDateValidity(pBG,4,out))||(!du.checkSQLDateValidity(pED,4,out)))
  {
   cls.error(4,"Period Begin/Period End");
   return;
  }
  if(!util.verifyAuthorization(dbFill,cls,out,aid,aPK,aPW,1,null))
  {
   cls.error(7,null);
   return;
  }
  StringBuffer cond = new StringBuffer();
  cond.append("WHERE empEvalDate>=\""+pBG+"\" AND empEvalDate<=\""+pED+"\" ");
  if(eid > 0)
    cond.append("AND empEvalOfID=\""+eid+"\";");
  else
    cond.append(";");
  Vector v = dbFill.QuerySpecific("employeeEvaluation",cond.toString(),out);
  if(v == null)
  {
   cls.error(0,"Belum ada hasil evaluasi tersedia dalam database");
   return;
  }
  dbFill.viewContainer(v,out);
  if(!v.isEmpty())
    v.removeAllElements();
 } // end of processAll's method

 private void createReview()
 {
  out.println("<TR><TD>Selalu tepat waktu untuk masuk kerja</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"onTime\" VALUE=1>Selalu Terlambat</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"onTime\" VALUE=2>Kadang Terlambat</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"onTime\" VALUE=3>Tidak Terlambat</TD></TR>");
  out.println("<TR><TD>Selalu melaksakan tugas yang diembankan</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"capable\" VALUE=1>Tidak dengan sempurna</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"capable\" VALUE=2>Kadang dengan sempurna</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"capable\" VALUE=3>Selalu dengan sempura</TD><TR>");
  out.println("<TR><TD>Dapat bekerja sama dengan pegawai yang lain</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"team\" VALUE=1>Tidak dapat bekerja sama</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"team\" VALUE=2>Kadang dapat bekerja sama</td><td>");
  out.println("<INPUT TYPE=RADIO NAME=\"team\" VALUE=3>Selalu dapat bekerja sama</TD><TR>");
  out.println("<TR><TD>Hasil pekerjaan pegawai tersebut</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"work\" VALUE=1>Selalu buruk</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"work\" VALUE=2>Kadang buruk</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"work\" VALUE=3>Tidak buruk</TD></TR>");
  out.println("<TR><TD>Dapat bertanggung jawab atas pekerjaannya</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"resp\" VALUE=1>Tidak</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"resp\" VALUE=2>Kadang</TD><TD>"); 
  out.println("<INPUT TYPE=RADIO NAME=\"resp\" VALUE=3>Selalu</TD></TR>");
  out.println("<TR><TD>Pernah diketahui melanggar aturan perusahaan</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"viol\" VALUE=1>Sering</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"viol\" VALUE=2>Kadang</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"viol\" VALUE=3>Tidak</TD></TR>");
  out.println("<TR><TD>Sikap terhadap pembeli</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"attd\" VALUE=1>Tidak selalu ramah dan membantu");
  out.println("</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"attd\" VALUE=2>Kadang ramah dan membantu");
  out.println("</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"attd\" VALUE=3>Selalu ramah dan membantu");
  out.println("</TD></TR>");
  out.println("<TR><TD>Tanggap dalam melaksanakan tugasnya</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"quic\" VALUE=1>Tidak</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"quic\" VALUE=2>Kadang</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"quic\" VALUE=3>Selalu</TD></TR>");
  out.println("<TR><TD>Dalam melaksanakan tugasnya</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"spd\" VALUE=1>Lamban</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"spd\" VALUE=2>Sedang</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"spd\" VALUE=3>Cepat</TD></TR>");
  out.println("<TR><TD>Dedekasi terhadap tugasnya</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"dec\" VALUE=1>Rendah</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"dec\" VALUE=2>Sedang</TD><TD>");
  out.println("<INPUT TYPE=RADIO NAME=\"dec\" VALUE=3>Tinggi</TD></TR>");
  out.println("<TR><TD>Komentar terhadap pegawai tersebut [max 64 karacters]</TD><TD>");
  out.println("<INPUT TYPE=TEXT NAME=\"note\" MAXLENGTH=64 SIZE=40></TD><TD></TD>");
  out.println("<TD></TD></TR>");
 } // end of createReview's method

} // end of ATTSviewAirlineAccess' class

