/**
 * ATTSviewStockTicket class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSviewStockTicket extends HttpServlet
{
 private DBFill dbFill;
 private String dbName = "ATTSairline";
 private PrintWriter out = null;
 private Closure cls = null;
 private ATTSutility util = null;

 public void init() throws ServletException
 {
  dbFill = new DBFill();
  util = new ATTSutility();
 }
 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
 }
 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 }
 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  Closure cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS View Route</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<HR></HR>");
  String view = req.getParameter("view");
  if(view == null)
  {
   out.println("<BR>view is null");
   cls.makeClosing();
   cls.closing();
   out.close();
   return;
  }
  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException("<BR>Error in making connection</BR>");
   if(view.compareToIgnoreCase("ALL")==0)
     viewForm();
   else if(view.compareToIgnoreCase("SET")==0)
     createForm();
   else if(view.compareToIgnoreCase("PROCESS")==0)
     processForm(req);
   else
    out.println("<BR>Invalid parameter of view:"+view+". This should not take place");
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out); 
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
  } 
 } 

 public void viewForm() throws SQLException
 {
  Vector airTmp = util.queryAirline(dbFill,out);
  if((airTmp == null)||(airTmp.size()<1))
  {
   cls.error(0,"Belum ada airline didalam database");
   cls.error(0,"Harap dimasukan ke database data-data yang diperlukan");
   return;
  }
  int pos1 = 0;
  int pos2 = 0;
  DBEncapsulation dbR = null;
  out.println("<BR><H1>ATTS View Stock Ticket</H1></BR>");
  out.println("<HR></HR>");
  out.println("<FORM METHOD=POST ACTION=\"ATTSviewStockTicket\">");
  out.println("<INPUT TYPE=HIDDEN NAME=\"view\" VALUE=\"PROCESS\">");
  out.println("<TABLE BORDER=0 WIDTH=75%>");
  out.println("<CAPTION><H1><BR>View Stock Ticket</BR></H1></CAPTION>");
  out.println("<TR><TD>Airline:</TD>");
  out.println("<TD><SELECT NAME=\"airlineID\">");
  out.println("<OPTION SELECTED VALUE=\"-1\">-- Pilih --</OPTION>");
  out.println("<OPTION VALUE=\"0\">Semua Airline</OPTION>");
  while(airTmp.size()>0)
  {
   dbR = (DBEncapsulation)airTmp.remove(0);
   pos1= dbR.getColumnNamePosition("airlineID");
   pos2= dbR.getColumnNamePosition("airlineName");
   out.print("<OPTION VALUE=\""+dbR.getColumnStringValues(pos1)+"\">");
   out.println(dbR.getColumnStringValue(pos2)+"</OPTION>");
   dbR.destroyAll(); 
  }
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD>Tanggal:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"tktDate\" SIZE=16 MAXLENGTH=10></TD></TR>"); 
  out.println("<TR><TD>Di ambil oleh:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"tktTake\" SIZE=16 MAXLENGTH=9></TD></TR>");
  out.println("<TR><TD>Di check oleh:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"tktCheck\" SIZE=16 MAXLENGTH=9></TD></TR>");
  out.println("</TABLE>");
  out.println("<INPUT TYPE=SUBMIT VALUE=\"SUBMIT\">&nbsp;");
  out.println("<INPUT TYPE=RESET VALUE=\"RESET\">");
  out.println("</FORM>");
 } // end of viewForm's method 

 public void createForm() throws SQLException
 {
  Vector airTmp = util.queryAirline(dbFill,out);
  if((airTmp == null)||(airTmp.size()<1))
  {
   out.println("<BR><FONT COLOR=RED><H2>Belum ada airline didalam database");
   out.println("<BR>Harap dimasukan ke database data-data yang diperlukan</h2></font>");
   return; 
  }
  int pos1 = 0;
  int pos2 = 0;
  DBEncapsulation dbR = null;
  out.println("<BR><h1>ATTS Stock Ticket</h1></BR>");
  out.println("<HR></HR>");
  out.println("<FORM METHOD=POST ACTION=\"ATTSstockTicket\">");
  out.println("<TABLE BORDER=0 WIDTH=75%>");
  out.println("<CAPTION><H1><BR>SET STOCK TICKET</BR></H1></CAPTION>"); 
  out.println("<TR><TD>Airline:</TD>");
  out.println("<TD><SELECT NAME=\"airlineID\">");
  out.println("<OPTION SELECTED VALUE=\"-1\">-- Pilih --</OPTION>");
  while(airTmp.size()>0)
  {
   dbR = (DBEncapsulation)airTmp.remove(0);
   pos1= dbR.getColumnNamePosition("airlineID");
   pos2= dbR.getColumnNamePosition("airlineName");
   out.print("<OPTION VALUE=\""+dbR.getColumnStringValues(pos1)+"\">");
   out.println(dbR.getColumnStringValue(pos2)+"</OPTION>");  
   dbR.destroyAll();
  }
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD>Jumlah Ticket yang diambil untuk stock:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"stockAmount\" SIZE=10 MAXLENGTH=5></TD></TR>"); 
  out.println("<TR><TD>Nomor urut One-Way ticket awal:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"stockOWBeginNumber\" SIZE=28 MAXLENGTH=24>");
  out.println("</TD></TR>");
  out.println("<TR><TD>Nomor urut One-Way ticket akhir:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"stockOWEndNumber\" SIZE=28 MAXLENGTH=24>");
  out.println("</TD></TR>");
  out.println("<TR><TD>Nomor urut Route-Trip ticket awal:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"stockRTBeginNumber\" SIZE=28 MAXLENGTH=24>");
  out.println("</TD></TR>");
  out.println("<TR><TD>Nomor urut Route-Trip ticket akhir:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"stockRTEndNumber\" SIZE=28 MAXLENGTH=24>");
  out.println("</TD></TR>");
  out.println("<TR><TD>Ticket diambil oleh</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"stockTakeBy\" SIZE=10 MAXLENGTH=9></TD></TR>"); 
  out.println("<TR><TD>Ticket di-check oleh</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"stockCheckBy\" SIZE=10 MAXLENGTH=10></TD></TR>");
  out.println("<TR><TD>Catatan tambahan:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"stockTktBNote\" SIZE=28 MAXLENGTH=128>");
  out.println("</TD></TR>");
  out.println("</TABLE>");
  out.println("<INPUT TYPE=SUBMIT VALUE=\"SUBMIT\">&nbsp;");
  out.println("<INPUT TYPE=RESET VALUE=\"CLEAR\">&nbsp;");
  out.println("</FORM>");
 } // end of createForm's method

/**
 * processForm's method is to process the form submitted by viewForm's method.
 * NOTE: Empty Value in input type=text does not carry NULL value. You have to
 * check the length of the string. However NULL value can crash your inquiry of
 * the length of the string itself. It is best to guard against the null then
 * the empty value.
 * @param HttpServletRequest req
 * @return none
 */
 public void processForm(HttpServletRequest req) 
	throws ServletException, IOException, SQLException 
 {
  String view = req.getParameter("view");
  if((view==null)||(view.length()<1)||(view.compareToIgnoreCase("PROCESS")!=0))
  {
   out.println("<BR><FONT COLOR=RED><H2>view has wrong parameter or null</font></br>");
   return;
  }
  DBEncapsulation tmp1 = dbFill.QuerySingleSpecific("airline","WHERE airlineID=1;",out);
  if(tmp1 == null)
  {
   out.println("<BR><FONT COLOR=RED><H2>Belum ada airline di database");
   out.println("<BR>Harap dimasukan ke database data-data yang diperlukan</h2></font>");
   return;
  }
  tmp1.destroyAll();
  tmp1 = dbFill.QuerySingleSpecific("airlineStockTicket","stockTktKey=\"1\"",out);
  if(tmp1 == null) 
  {
   out.println("<BR><FONT COLOR=RED><H2>Belum ada stock tickets di database");
   out.println("<BR>Harap dimasukan ke database data-data yang diperlukan</h2></font>");
   if(tmp1!=null)
     tmp1.destroyAll();
   return;
  }
  if(tmp1!=null)
    tmp1.destroyAll();

  int airID= Integer.parseInt(req.getParameter("airlineID"));
  String tktDate   = req.getParameter("tktDate"); //YYYY-MM-DD
  String tktCheck  = req.getParameter("tktCheck");
  String tktTake   = req.getParameter("tktTake");

  if((tktDate==null)||(tktCheck==null)||(tktTake==null))
  {
   cls.error(1,null);
   return;
  }
  if((airID==-1)||(tktDate.length()<1)||(tktCheck.length()<1)||(tktTake.length()<1))
  {
   cls.error(2,null);
   return;
  }

  DateUtil du = new DateUtil();
  if(!du.checkSQLDate(tktDate,out))
  {
   cls.error(4,"Ticket Date"); 
   return;
  }
  else if(!du.checkSQLDateValidity(tktDate,4,out))
  {
   cls.error(4,"Ticket Date");
   return;
  }

  int tktDLen = tktDate.length();
  int tktCLen = tktCheck.length();
  int tktTLen = tktTake.length();

  StringBuffer tmp = new StringBuffer();
  
  if((airID>0)&&(tktDLen==10)&&(tktCLen>0)&&(tktTLen>0))  
   tmp.append("WHERE stockOfAirline="+airID+" AND stockTktDate="+tktDate+
              " AND stockTakeBy="+tktTake+" AND stockCheckBy="+tktCheck+";"); 
  else if((airID>0)&&(tktDLen==10)&&(tktCLen>0)&&(tktTLen<=0))
   tmp.append("WHERE stockOfAirline="+airID+" AND stockTktDate="+tktDate+ 
              " AND stockCheckBy="+tktCheck+";");
  else if((airID>0)&&(tktDLen==10)&&(tktCLen<=0)&&(tktTLen>0))
   tmp.append("WHERE stockOfAirline="+airID+" AND stockTktDate="+tktDate+
              " AND stockTakeBy="+tktTake+";");
  else if((airID>0)&&(tktDLen==10)&&(tktCLen<=0)&&(tktTLen<=0))
   tmp.append("WHERE stockOfAirline="+airID+" AND stockTktDate="+tktDate+";");
  else if((airID>0)&&(tktDLen!=10)&&(tktCLen>0)&&(tktTLen<=0))
   tmp.append("WHERE stockOfAirline="+airID+" AND stockCheckBy="+tktCheck+";");
  else if((airID>0)&&(tktDLen!=10)&&(tktCLen<=0)&&(tktTLen>0))
   tmp.append("WHERE stockOfAirline="+airID+" AND stockTakeBy="+tktTake+";");
  else if((airID>0)&&(tktDLen!=10)&&(tktCLen<=0)&&(tktTLen<=0))
   tmp.append("WHERE stockOfAirline="+airID+";");
  else if((airID<=0)&&(tktDLen==10)&&(tktCLen>0)&&(tktTLen>0))
   tmp.append("WHERE stockTktDate="+tktDate+" AND stockCheckBy="+tktCheck+
              " AND stockTakeBy="+tktTake+";");
  else if((airID<=0)&&(tktDLen==10)&&(tktCLen<=0)&&(tktTLen>0))
   tmp.append("WHERE stockTktDate="+tktDate+" AND stockTakeBy="+tktTake+";");
  else if((airID<=0)&&(tktDLen==10)&&(tktCLen>0)&&(tktTLen<=0))
   tmp.append("WHERE stockTktDate="+tktDate+" AND stockCheckBy="+tktCheck+";");
  else if((airID<=0)&&(tktDLen==10)&&(tktCLen<=0)&&(tktTLen<=0))
   tmp.append("WHERE stockTktDate="+tktDate+";");
  else if((airID<=0)&&(tktDLen!=10)&&(tktCLen>0)&&(tktTLen>0))
   tmp.append("WHERE stockCheckBy="+tktCheck+" AND stockTakeBy="+tktTake+";");
  else if((airID<=0)&&(tktDLen!=10)&&(tktCLen<=0)&&(tktTLen>0))
   tmp.append("WHERE stockTakeBy="+tktTake+";");
  else if((airID<=0)&&(tktDLen!=10)&&(tktCLen>0)&&(tktTLen<=0))
   tmp.append("WHERE stockCheckBy="+tktCheck+";");
  else if((airID==0)&&(tktDLen!=10)&&(tktCLen<=0)&&(tktTLen<=0))
  { }
  else if((airID<0)&&(tktDLen!=10)&&(tktCLen<=0)&&(tktTLen<=0)) 
  {
   out.println("<BR><FONT COLOR=RED>This should be filtered through at first</font>");
   return;
  }
  else
  {
   out.println("<BR><FONT COLOR=RED><B>This should not happen</B></FONT></BR>");
   return;
  } 
  
  Vector dbVec = dbFill.QuerySpecific("airlineStockTicket",tmp.toString(),out);
  if(dbVec == null)
  {
   out.println("<BR><B>Query tidak menghasilkan apa-apa</B></BR>");
   return;
  } 
  dbFill.viewContainer((Vector)dbVec.clone(),out);
  dbVec.removeAllElements(); 
  dbVec = null;
 } // end of processForm's method 

} // end of viewBlockSeats class

