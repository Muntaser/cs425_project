/**
 * ATTSaddAirSales class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSaddAirSales extends HttpServlet
{
 private DBFill dbFill = null; // ATTSsales
 private DBFill dbFill2= null; // ATTSemployee
 private DBFill dbFill3= null; // ATTSairline
 private DBFill dbFill4= null; // ATTScompany
 private String dbName = "ATTSsales";
 private String dbName2= "ATTSemployee";
 private String dbName3= "ATTSairline";
 private String dbName4= "ATTScompany";
 private PrintWriter out = null;
 private Closure cls = null;
 private ATTSutility util = null;
 private boolean blackList = false;

/**
 * init's method is to initialize the servlet
 * @param none
 * @return none
 */
 public void init() throws ServletException
 {
  dbFill = new DBFill();
  dbFill2 = new DBFill();
  dbFill3 = new DBFill();
  dbFill4 = new DBFill();
  util = new ATTSutility();
 }

/**
 * destroy's method is to end the life-cycle of the servlet
 * @param none
 * @return none
 */
 public void destroy() 
 {
  dbFill.closeDatabase();
  dbFill2.closeDatabase();
  dbFill3.closeDatabase();
  dbFill4.closeDatabase();
  util.close();
 }

/**
 * doGet's method is to process GET method from user
 * @param HttpServletRequest req
 *        HttpServletResponse res
 * @return none
 */
 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 }

/**
 * doPost's method is to process POST method from user
 * @param HttpServletRequest req contains user request
 *        HttpServletResponse res contains user respond
 * @return none
 */
 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS Add Air Sales</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<BR></BR><H1>ATTS Add Air Sales</H1><HR></HR><BR></BR>"); 
  String view = req.getParameter("view");
  try
  {
   if(view.compareToIgnoreCase("DISPLAY")==0)
   {
    databaseInit(1);
    databaseInit(3);
    createForm();
   }
   else if(view.compareToIgnoreCase("PROCESS")==0)
   {
    databaseInit(1);
    databaseInit(2);
    databaseInit(3);
    databaseInit(4);
    process(req);
   }
   else if(view.compareToIgnoreCase("FINAL")==0)
   { 
    databaseInit(1);
    processFinal(req); 
   } 
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
   view = null;
  }  
 } // end of doPost 

 private void databaseInit(int i) throws SQLException
 {
  if(i == 3)
   if(!dbFill3.makeInitialConnection(dbName3,out))
    throw new SQLException("<BR>Failure to make initial connection to "+dbName3+"</BR>");
  else if(i == 1)
   if(!dbFill.makeInitialConnection(dbName,out))
    throw new SQLException("<BR>Failure to make initial connection to "+dbName+"</BR>");
  else if(i == 2)
   if(!dbFill2.makeInitialConnection(dbName2,out))
    throw new SQLException("<BR>Failure to make initial connection to "+dbName2+"</BR>");
  else if(i == 4)
   if(!dbFill4.makeInitialConnection(dbName4,out))
    throw new SQLException("<BR>Failure to make initial connection to "+dbName4+"</BR>");
   else 
    throw new SQLException("the databaseInit does not recognize the sign");
 }

/**
 * createForm's method is to prepare the display screen for user to add or 
 * to place an order into airline sales. It shows prices and classes. Then
 * it asked for name of the passangers, the route of the chosen airline, and the
 * chosen time of the passangers. It requires the user to enter his/her employeeID
 * and the passkey to proceed. 
 * @param Vector v - contains completed airline routes
 * @return none
 */
 private void createForm() throws SQLException 
 {
  Vector v = util.prepareAirRoute(dbFill3,cls);
  if(v == null)
   return; 
  int p1 = 0;
  int rID = 0;
  StringBuffer str = new StringBuffer();
  DBEncapsulation e = null;
  util.createBegin("ATTSaddAirSales","view","PROCESS",true,out);
  for(int i=1;i<11;i++)
  {
   out.println("<TR><TD>Nama Penumpang "+i+"</TD><TD>");
   out.println("<INPUT TYPE=TEXT NAME=\"name"+i+"\" SIZE=40 MAXLENGTH=32>");
   out.println("</TD><TD>Umur dalam hitungan <B>bulan</B> [for Infant/Child]</TD><TD>");
   out.println("<INPUT TYPE=TEXT NAME=\"passAge"+i+"\" SIZE=4 MAXLENGTH=3></TD></TR>");
  }
  out.println("<TR><TD>Jumlah Penumpang</TD><TD><SELECT NAME=\"NumPass\">");
  out.println("<OPTION SELECTED VALUE=\"-1\">-- Pilih --</OPTION>");
  for(int i=1;i<11;i++)
   out.println("<OPTION VALUE=\""+i+"\">i</OPTION>");
  out.println("</SELECT></TD><TD></TD><TD></TD></TR>");
  
  out.println("<TR><TD>Airline dan Rute</TD><TD><SELECT NAME=\"rID\">");
  out.println("<OPTION SELECTED VALUE=\"-1\">-- Pilih --</OPTION>");
  while(v.size()!=0) 
  {
   e=(DBEncapsulation)v.remove(0);
   rID=e.getColumnIntegerValue(e.getColumnNamePosition("routeKey"));
   out.println("<OPTION VALUE=\""+rID+"\">");
   out.println(e.getColumnStringValue(e.getColumnNamePosition("airlineAbbr"))+" | ");
   p1=e.getColumnNamePosition("routeOrig");
   out.println(e.getColumnStringValue(p1)+"-");
   str.append(e.getColumnStringValue(p1)+"-");
   p1=e.getColumnNamePosition("routeViaSN");
   if(p1>0)
   {
    out.println(" ["+e.getColumnStringValue(p1)+"] -");
    str.append("["+e.getColumnStringValue(p1)+"]-");
   }
   p1=e.getColumnNamePosition("routeDest");
   str.append(e.getColumnStringValue(p1));
   out.println(e.getColumnStringValue(p1)+"</OPTION>");
   out.println("<INPUT TYPE=HIDDEN NAME=\"airlineID"+rID+"\" VALUE=\"");
   out.println(e.getColumnStringValues(e.getColumnNamePosition("airlineID"))+"\">");
   out.println("<INPUT TYPE=HIDDEN NAME=\"route"+rID+"\" VALUE=\""+str.toString()+"\">"); 
   e.destroyAll();
   str.delete(0,str.length());
  }
  out.println("</SELECT></TD><TD>Type of Flight</TD><TD>");
  out.println("<SELECT NAME=\"typeFlight\"><OPTION SELECTED VALUE=\"OW\">");
  out.println("One-Way</OPTION><OPTION VALUE=\"RT\">Round-Trip></OPTION></SELECT>");
  out.println("</TD></TR>");
  out.println("<TR><TD>Contact Person Untuk Order Organisasi</TD>");
  v = dbFill.QuerySpecific("salesCustContPerson",null,out);
  if(v!=null)
  {
   out.println("<TD><SELECT NAME=\"cpID\">");
   out.println("<OPTION SELECTED VALUE=\"-1\">-- Pilih --</OPTION>");
   while(v.size()!=0)
   {
    e = (DBEncapsulation)v.remove(0);
    out.println("<OPTION VALUE=\"");
    out.println(e.getColumnStringValues(e.getColumnNamePosition("salesCustContPersonKey")));
    out.println("\">");
    out.println(e.getColumnStringValue(e.getColumnNamePosition("salesCustContPerName")));
    out.println("</OPTION>");
    e.destroyAll();
   }
   out.println("</SELECT></TD>");
  }
  else
   out.println("<TD>Belum ada Contact Person di database</TD>"); 
  out.println("<TD>Kalau tidak ada, nama baru</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"cpName\" SIZE=40 MAXLENGTH=32></TD></TR>");

  out.println("<TR><TD>Contact Person Untuk Order Perorangan</TD>");
  out.println("<TD>Contact Penumpang Nomor <SELECT NAME=\"contCustID\">");
  out.println("<OPTION SELECTED VALUE=\"-1\">-- Pilih --</OPTION>");
  for(int i=1;i<11;i++)
   out.println("<OPTION VALUE=\""+i+"\">"+i+"</OPTION>");
  out.println("</SELECT></TD><TD>Atau Contact Person dengan nama</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"salesCust\" SIZE=40 MAXLENGTH=32></TD></TR>");
  
  out.println("<TR><TD>Alamat Contact Person/Penumpang</TD><TD>");
  out.println("<INPUT TYPE=TEXT NAME=\"custAddr\" SIZE=40 MAXLENGTH=32></TD><TD>");
  out.println("<B>boleh di-isi atau dikosongkan</B></TD><TD></TD></TR>");
  out.println("<TR><TD>Alamatnya berkedudukan dikota</TD><TD>");
  out.println("<INPUT TYPE=TEXT NAME=\"custAddrCity\" SIZE=20 MAXLENGTH=16></TD><TD>");
  out.println("<B>boleh di-isi atau dikosongkan</B></TD><TD></TD></TR>");
  out.println("<TR><TD>Alamatnya berkedudukan dinegara</TD><TD>");
  out.println("<INPUT TYPE=TEXT NAME=\"custAddrCountry\" SIZE=20 MAXLENGTH=16></TD><TD>");
  out.println("<B>boleh di-isi atau dikosongkan</B></TD><TD></TD></TR>");

  out.println("<TR><TD>Nomor HandPhone Contact Person/Penumpang</TD><TD>");
  out.println("<INPUT TYPE=TEXT NAME=\"cHP\" MAXLENGTH=14 SIZE=20></TD><TD>");
  out.println("</TD><TD></TD></TR>");
  out.println("<TR><TD>Nomor Telephone Contact Person/Penumpang</TD><TD>");
  out.println("<INPUT TYPE=TEXT NAME=\"cTP\" MAXLENGTH=14 SIZE=20></TD><TD>");
  out.println("</TD><TD></TD></TR>");
  out.println("<TR><TD>Nomor Fax Contact Person/Penumpang</TD><TD>");
  out.println("<INPUT TYPE=TEXT NAME=\"cFX\" MAXLENGTH=14 SIZE=20></TD><TD>");
  out.println("</TD><TD></TD></TR>");
  out.println("<TR><TD>Catatan Tambahan Untuk Menghubungi Contact Person/Penumpang</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"cNote\" SIZE=40 MAXLENGTH=32></TD><TD></TD>");
  out.println("<TD></TD></TR>"); 
  
  out.println("<TR><TD>Employee ID</TD><TD>");
  out.println("<INPUT TYPE=TEXT NAME=\"eID\" SIZE=20 MAXLENGTH=10></TD><TD></TD>");
  out.println("<TD></TD></TR>");
  out.println("<TR><TD>Employee PassKey</TD><TD>");
  out.println("<INPUT TYPE=PASSWORD NAME=\"ePK\" SIZE=20 MAXLENGTH=6></TD>");
  out.println("<TD></TD><TD></TD></TR>");
  util.createEnd(out);
 }

/**
 * process' method is to process the user submitted form. It will redisplay all
 * the customer order and it will ask for additional information before it finalizes
 * the order to the database. 
 * @param HttpServletRequest req
 * @return none
 */
 public void process(HttpServletRequest req) throws SQLException
 {
  int numPass = Integer.parseInt(req.getParameter("NumPass"));
  if((numPass<0)||(numPass>10))
  {
   cls.error(0,"Anda harus mengisi Jumlah Penumpang dengan benar");
   return;
  } 
  String empID = req.getParameter("eID");
  String empPK = req.getParameter("ePK");
  if((empID==null)||(empPK==null))
  {
   cls.error(1,"empID/empPK");
   return;
  }
  if((empID.length()<1)||(empPK.length()<1))
  {
   cls.error(0,"Anda harus mengisi Employee ID/Employee PassKey");
   return;
  }
  if((!util.checkDigit(empID,true,out))||(!util.checkDigit(empPK,true,out)))
  {
   cls.error(3,"Employee ID/Employee PassKey");
   return;
  }
  if(!util.verifyAuthorization(dbFill2,cls,out,Integer.parseInt(empID),empPK,null,3,null))
  {
   cls.error(0,"Employee ID/PassKey yang anda masukan salah");
   return;
  }
  boolean errorFlag = false;
  int routeID = Integer.parseInt(req.getParameter("rID"));
  int cpID = Integer.parseInt(req.getParameter("cpID"));
  int contCustID = Integer.parseInt(req.getParameter("contCustID"));
  String newCPID = req.getParameter("cpName");
  String salesCust = req.getParameter("salesCust");
  String custAddr = req.getParameter("custAddr");
  String custAddrCity = req.getParameter("custAddrCity");
  String custAddrCountry = req.getParameter("custAddrCountry");
  String custHP = req.getParameter("cHP");
  String custTP = req.getParameter("cTP");
  String custFX = req.getParameter("cFX");
  String custContNote = req.getParameter("cNote");
   
  if((newCPID==null)||(salesCust==null)||(custAddr==null)||(custAddrCity==null)||
    (custAddrCountry==null)||(custHP==null)||(custTP==null)||(custFX==null)||
    (custContNote==null))
  {
   cls.error(1,null);
   return;
  }

  if(routeID<=0)
  {
   errorFlag = true;
   cls.error(0,"Anda harus memilih Airline/Rute/Class/PAX-Type");
  }

  //verify all the customer contact person info
  if((contCustID<0)&&(cpID<0)&&(newCPID.length()<=1)&&(salesCust.length()<=1))
  {
   errorFlag = true;
   cls.error(0,"Anda harus mengisi Contact Person untuk Order Perorangan/Perusahaan");
  }
  else if((cpID>0)&&(newCPID.length()>1)&&(salesCust.length()>1)&&(contCustID>0))
  {
   errorFlag = true;
   cls.error(0,"Anda tidak boleh mengisi Contact Person Order Perorangan & Perusahaan dengan bersamaan!");
  }
  else if((cpID>0)&&(newCPID.length()>1))
  {
   errorFlag = true;
   cls.error(0,"Anda tidak boleh mengisi semua kolom Contact Person Order Perusahaan");
  }
  else if((cpID>0)&&(salesCust.length()>1))
  {
   errorFlag = true;
   cls.error(0,"Anda tidak boleh memilih Contact Person Order Perorangan & Perusahaan dengan bersamaan!");
  }
  else if((newCPID.length()>1)&&(salesCust.length()>1))
  {
   errorFlag = true;
   cls.error(0,"Anda tidak boleh memilih Contact Person Order Perorangan & Perusahaan dengan bersamaan!");
  }
  else if((contCustID>0)&&(salesCust.length()>1))
  {
   errorFlag = true;
   cls.error(0,"Anda tidak boleh mengisi semua kolom Contact Person Order Perorangan");
  }
  
  //verify all the customer contact info
  if(custHP.length()>1)
  {
   if((custHP.length()>=10)&&(custHP.length()<=14))
   {
    if(!util.checkDigit(custHP,false,out))
    {
     cls.error(3,"Customer HandPhone");
     errorFlag = true;
    }
   }
   else
   {
    errorFlag = true;
    cls.error(0,"Nomor Customer HandPhone harus panjangnya antara 10 - 14 digit");
   }
  }
  if(custTP.length()>1)
  {
   if((custTP.length()>=10)&&(custTP.length()<=14))
   {
    if(!util.checkDigit(custTP,false,out))
    {
     cls.error(3,"Customer TelePhone");
     errorFlag = true;
    }
   } 
   else
   {
    errorFlag = true;
    cls.error(0,"Nomor Customer TelePhone harus panjangnya antara 10 - 14 digit");
   }
  }
  if(custFX.length()>1)
  {
   if((custFX.length()>=10)&&(custFX.length()<=14))
   {
    if(!util.checkDigit(custFX,false,out))
    {
     cls.error(3,"Customer Fax");
     errorFlag = true;
    }
   }
   else
   {
    errorFlag = true;
    cls.error(0,"Nomor Customer Fax harus panjangnya antara 10 - 14 digit");
   }
  }
  Vector pass = new Vector();
  StringBuffer str = new StringBuffer();
  StringBuffer str2= new StringBuffer();
  Replacer rep = new Replacer(); 
  for(int i=1;i<numPass+1;i++)
  {
   str.append("name"+i);
   str2.append(req.getParameter(str.toString()));
   if(str2.length()>1)
   {
    rep.setOriginal(str2.toString());
    str.delete(0,str.length());
    str2.delete(0,str2.length());
    str.append("passAge"+i);
    str2.append(req.getParameter(str.toString()));
    if(str2.length()>0)
     if(util.checkDigit(str2.toString(),true,out))
      rep.setReplacer(str2.toString()); 
    pass.addElement(rep.clone());
    rep.reset();
   }
   str.delete(0,str.length());
   str2.delete(0,str2.length());
  }
  if(pass.size()==0)
  {
   cls.error(0,"Anda harus mengisi nama-nama penumpang");
   errorFlag = true;
  }

  if(errorFlag)
  {
   if(!pass.isEmpty())
    pass.removeAllElements();
   return;
  }  
  str.append("airlineID"+routeID);
  int aID = Integer.parseInt(req.getParameter(str.toString()));
  String typeFlight = req.getParameter("typeFlight");

  util.createBegin("ATTSaddAirSales","view","FINAL",true,out);
  out.println("<INPUT TYPE=HIDDEN NAME=\"eID\" VALUE=\""+empID+"\">");
  out.println("<INPUT TYPE=HIDDEN NAME=\"rID\" VALUE=\""+routeID+"\">");
  out.println("<INPUT TYPE=HIDDEN NAME=\"numPass\" VALUE=\""+numPass+"\">");

  writePassanger(pass,rep,aID);
  writeCPInfo(cpID,contCustID,newCPID,salesCust);

  writeCOrder(pass,rep,routeID,aID,typeFlight);
  writeCTicket(numPass,pass,rep);
  writeCPayment();
  util.createEnd(out);
 }

/**
 * writePassanger's method is write out all the name of passangers and their ages
 * if available (for infant and child only). It will also write out the input hidden
 * field so the final process does not need to re-fetch info from database.
 * @param Vector pass contains list of passangers with their ages.
 *        Replacer rep contains the empty name-value pair of passanger's name and age.
 *        int aID contains airline ID needed to fetch the specific airline Rules regarding
 *        the passangers age for Infant or Child
 * @return none
 */
 private void writePassanger(Vector pass,Replacer rep,int aID) throws SQLException
 {
  int size = pass.size();
  out.println("<TR><TD><B>PASSANGER</B></TD><TD></TD><TD></TD><TD></TD></TR>");
  int inc = 0;
  int p1 = 0;
  int p2 = 0;
  DBEncapsulation e = null;
  StringBuffer str = new StringBuffer();
  for(int i=0;i<size;i++) // printing out passanger's names
  {
   inc = i+1;
   str.append("name"+inc);
   out.println("<TR><TD>NAMA "+inc+"</TD><TD>"); 
   rep = (Replacer)((Replacer)pass.get(i)).clone();
   out.println(rep.getOriginal()+"</TD>");
   out.println("<INPUT TYPE=HIDDEN NAME=\"name"+inc+"\" VALUE=\"");
   out.println(rep.getOriginal()+"\">");
   str.delete(0,str.length());
   str.append("passAge"+inc);
   if(rep.getLenReplacer()>=1)
   {
    str.append("WHERE airlineRulesOf=\""+aID+"\";");
    e = dbFill3.QuerySingleSpecific("airlineRules",str.toString(),out);
    p1 = e.getColumnIntegerValue(e.getColumnNamePosition("airlineINFMaxAge"));
    p2 = e.getColumnIntegerValue(e.getColumnNamePosition("airlineCHDMaxAge"));
    if(p1>=Integer.parseInt(rep.getReplacer()))
    {
     out.println("<TD>Infant [umur: "+rep.getOriginal()+" bulan]</TD>"); 
     out.println("<INPUT TYPE=HIDDEN NAME=\"age"+inc+"\" VALUE=\"");
     out.println(rep.getOriginal()+"\">");
     out.println("<INPUT TYPE=HIDDEN NAME=\"codePAX"+inc+"\" VALUE=\"I\">");
    }
    else if(p2>=Integer.parseInt(rep.getReplacer())) 
    {
     out.println("<TD>Child [umur: "+rep.getOriginal()+" bulan]</TD>");
     out.println("<INPUT TYPE=HIDDEN NAME=\"age"+inc+"\" VALUE=\"");
     out.println(rep.getOriginal()+"\">");
     out.println("<INPUT TYPE=HIDDEN NAME=\"codePAX"+inc+"\" VALUE=\"C\">");
    }
    else
    {
     out.println("<TD>Adult [melewati batas umur Child "+p2+" bulan]</TD>");
     out.println("<INPUT TYPE=HIDDEN NAME=\"age"+inc+"\" VALUE=\"-1\">");
     out.println("<INPUT TYPE=HIDDEN NAME=\"codePAX"+inc+"\" VALUE=\"A\">");
    }
    e.destroyAll();
   }
   else
   {
    out.println("<TD>Adult</TD>");
    out.println("<INPUT TYPE=HIDDEN NAME=\"age"+inc+"\" VALUE=\"-1\">");
    out.println("<INPUT TYPE=HIDDEN NAME=\"codePAX"+inc+"\" VALUE=\"A\">");
   }
   str.delete(0,str.length());
   rep.reset();
   out.println("<TD></TD></TR>");
  } // end of printing out passanger's names
  out.println("</TABLE><BR></BR>");
 }

/**
 * writeCPInfo's method is to write the contact person info including into the hidden
 * type field form thus to prevent the re-fetching of the information from the database
 * in the final phrase. 
 * @param int cpID - contains the existing contact person ID [if available]
 *        int custContID - contains the existing customer contact ID [if available]
 *        String newCPID - contains the new name of Contact Person(CP) [if available]
 *        String salesCust - contains the sales Customer name [if available]
 * @return none
 */
 public void writeCPInfo(int cpID,int custContID,String newCPID,String salesCust) 
  throws SQLException
 {
  // Customer's Contact Person and Info
  out.println("<TABLE BORDER=0 WIDTH=\"75%\">");
  out.println("<TR><TD>Contact Person</TD><TD></TD><TD></TD><TD></TD></TR>");
  if(cpID>0) // untuk Contact Person yang sudah terdaftar
  {
   printExistedCP(cpID);
  }
  if(newCPID.length()>1) // untuk Contact Person yang baru for organization
  {
   printNewCP(newCPID);
  }
  out.println("</TABLE><BR></BR>");
 }

/**
 * printExistedCP's method is to print the existed Contact Person Info
 * @param int cpID - contains contact person id
 * @return none
 */
 private void printExistedCP(int cpID) throws SQLException
 {
  int p1 = 0;
  out.println("<TR><TD>");
  StringBuffer str = new StringBuffer();
  str.append("LEFT JOIN salesCustomer ON salesCustomer.salesCustKey=");
  str.append("salesCustContPerson.salesCustContPersonKey ");
  str.append("LEFT JOIN salesCustAddr ON salesCustContPerson.salesCustContAddKey=");
  str.append("salesCustAddr.salesCustAddr ");
  str.append("LEFT JOIN salesCustContact ON salesCustContPerson.salesCustContPersonKey=");
  str.append("salesCustCPKey ");
  str.append("WHERE salesCustContPersonKey=\""+cpID+"\";");
  DBEncapsulation e = dbFill.QuerySingleSpecific("salesCustContPerson",str.toString(),out);
  if(e != null)
  {
   str.delete(0,str.length());
   out.println("Name</TD><INPUT TYPE=HIDDEN NAME=\"nCP\" VALUE=\"N\"><TD>");
   p1 = e.getColumnNamePosition("salesCustContPerName");
   out.println(e.getColumnStringValues(p1)+"</TD>");
   out.println("<INPUT TYPE=HIDDEN NAME=\"salesCustContPerName\" VALUE=\"");
   out.println(e.getColumnStringValue(p1)+"\"><TD></TD><TD></TD></TR>");
   out.println("<TR><TD>Nama Organisasi </TD><TD>");
   p1 = e.getColumnNamePosition("salesCustName");
   out.println(e.getColumnStringValues(p1)+"</TD><INPUT TYPE=HIDDEN NAME=\"");
   out.println("salesCustName\" VALUE=\""+e.getColumnStringValue(p1)+"\">");
   out.println("<TD>Tipe Organisasi</TD><TD>");
   str.append(e.getColumnStringValues(e.getColumnNamePosition("salesCustType")));
   if(str.toString().compareToIgnoreCase("CORP")==0) 
    out.println("PERUSAHAAN</TD></TR>");
   else if(str.toString().compareToIgnoreCase("TR_AGENT")==0) 
    out.println("TRAVEL AGENT</TD></TR>");
   else if(str.toString().compareToIgnoreCase("GOVT")==0) 
    out.println("BADAN PEMERINTAH</TD></TR>");
   else if(str.toString().compareToIgnoreCase("REG")==0) 
    out.println("LANGGANAN TETAP</TD></TR>");
   else if(str.toString().compareToIgnoreCase("ONE_TIME")==0) 
    out.println("BELUM LANGGANAN TETAP</TD></TR>");
   out.println("<INPUT TYPE=HIDDEN NAME=\"custType\" VALUE=\""+str.toString()+"\">");
   out.println("<TR><TD>Alamat Organisasi</TD><TD>");
   str.delete(0,str.length());
   p1 = e.getColumnNamePosition("salesCustAddr");
   if(e.getColumnStringValue(p1).length()<3)
   {
    out.println("Belum ada alamat di database");
    out.println("<INPUT TYPE=HIDDEN NAME=\"salesCustAddr\" VALUE=\"NON\">");
   }
   else
   {
    out.println(e.getColumnStringValue(p1));
    out.println("<INPUT TYPE=HIDDEN NAME=\"salesCustAddr\" VALUE=\"");
    out.println(e.getColumnStringValue(p1)+"\">");
   }
   out.println("</TD><TD>");
   p1 = e.getColumnNamePosition("salesCustAddrCity");
   out.println(e.getColumnStringValue(p1));
   out.println("<INPUT TYPE=HIDDEN NAME=\"salesCustAddrCity\" VALUE=\"");
   out.println(e.getColumnStringValue(p1)+"\">");
   out.println("</TD><TD>");
   p1 = e.getColumnNamePosition("salesCustAddrCountry");
   out.println(e.getColumnStringValue(p1));
   out.println("<INPUT TYPE=HIDDEN NAME=\"salesCustAddrCountry\" VALUE=\"");
   out.println(e.getColumnStringValue(p1)+"\">");
   out.println("</TD></TR>");
   out.println("<TR><TD>Contact Person HandPhone</TD><TD>");
   p1 = e.getColumnNamePosition("salesCustHandPhone");
   if(e.getColumnStringValue(p1).length()<3)
   {
    out.println("Belum ada nomor HandPhone di database");
    out.println("<INPUT TYPE=HIDDEN NAME=\"salesCustHP\" VALUE=\"NON\">");
   }
   else
   {
    out.println(e.getColumnStringValue(p1));
    out.println("<INPUT TYPE=HIDDEN NAME=\"salesCustHP\" VALUE=\"");
    out.println(e.getColumnStringValue(p1)+"\">");
   }
   out.println("</TD><TD>Contact Person Phone</TD><TD>");
   p1 = e.getColumnNamePosition("salesCustPhone");
   if(e.getColumnStringValue(p1).length()<3)
   {
    out.println("Belum ada nomor Telephone di database");
    out.println("<INPUT TYPE=HIDDEN NAME=\"salesCustTP\" VALUE=\"NON\">");
   }
   else
   {
    out.println(e.getColumnStringValue(p1));
    out.println("<INPUT TYPE=HIDDEN NAME=\"salesCustTP\" VALUE=\"");
    out.println(e.getColumnStringValue(p1)+"\">");
   }
   p1 = e.getColumnNamePosition("salesCustFax");
   if(e.getColumnStringValue(p1).length()<3)
   {
    out.println("Belum ada nomor Fax di database");
    out.println("<INPUT TYPE=HIDDEN NAME=\"salesCustFX\" VALUE=\"NON\">");
   }
   else 
   {
    out.println(e.getColumnStringValue(p1));
    out.println("<INPUT TYPE=HIDDEN NAME=\"salesCustFX\" VALUE=\"");
    out.println(e.getColumnStringValue(p1)+"\">");
   } 
   out.println("</TD></TR>");
   p1 = e.getColumnNamePosition("salesCustContNote");
   if(e.getColumnStringValue(p1).length()>2)
   {
    out.println("<TR><TD>Catatan Khusus untuk Contact Person di-atas</TD><TD>");
    out.println(e.getColumnStringValue(p1)+"</TD><INPUT TYPE=HIDDEN NAME=\"newCCPNote\" ");
    out.println("VALUE=\""+e.getColumnStringValue(p1)+"\">");
    out.println("<TD></TD><TD></TD><TD></TD></TR>");
   }
   p1 = e.getColumnNamePosition("salesCustBlackList");
   out.println("<TR><TD><FONT COLOR=RED><B>Black List</B></FONT></TD><TD><B>");
   if(e.getColumnStringValues(p1).compareToIgnoreCase("Y")==0)
   {
    blackList = true;
    out.println("<FONT COLOR=RED>YES</FONT></B></TD><TD></TD><TD></TD><TD></TD></TR>");
   }
   else
    out.println("NO</B></TD><TD></TD><TD></TD><TD></TD><TD></TD></TR>");
   e.destroyAll();
  }
  else // Query does not produce any result
  {
   out.println("Tidak tersedia data</TD><TD></TD><TD></TD><TD></TD></TR>");
   out.println("<INPUT TYPE=HIDDEN NAME=\"nCP\" VALUE=\"NE\">");
  }
  str.delete(0,str.length());
 }


/**
 * printNewCP's method is to print out the new Contact Person 
 * @param
 * @return
 */ 
 private void printNewCP(String newCPID) throws SQLException
 {
  out.println("<TR><TD>Nama Contact Person yang baru</TD>");
  out.println("<INPUT TYPE=HIDDEN NAME=\"nCP\" VALUE=\"Y\">");
  out.println("<TD>"+newCPID+"</TD>"); 
  out.println("<INPUT TYPE=HIDDEN NAME=\"newCP\" VALUE=\""+newCPID+"\">");
  Vector v = dbFill.QuerySpecific("salesCustomer","WHERE salesCustType<>\"ONE_TIME\";",out);
  if(v == null)
  {
   out.println("Nama Customer yang baru <INPUT TYPE=TEXT NAME=\"newSalesCust\"");
   out.println(" SIZE=40 MAXLENGTH=32></TD><TD>Customer Type<SELECT NAME=\"custType\">");
   out.println("<OPTION SELECTED VALUE=\"NON\">-- Pilih --</OPTION>");
   out.println("<OPTION VALUE=\"CORP\">PERUSAHAAN</OPTION>");
   out.println("<OPTION VALUE=\"TR_AGENT\">TRAVEL AGENT</OPTION>");
   out.println("<OPTION VALUE=\"GOVT\">BADAN PEMERINTAH</OPTION>");
   out.println("<OPTION VALUE=\"HOTEL\">HOTEL</OPTION>");
   out.println("</SELECT></TD></TR>");
  }
  else
  {
   DBEncapsulation e = null;
   out.println("Untuk Organisasi atau Langganan Tetap");
   out.println("<SELECT NAME=\"orgCP\">");
   out.println("<OPTION SELECTED VALUE=\"-1\">-- Pilih --</OPTION>");
   while(v.size()!=0)
   {
    e = (DBEncapsulation)v.remove(0);
    out.println("<OPTION VALUE=\"");
    out.println(e.getColumnIntegerValue(e.getColumnNamePosition("salesCustKey")));
    out.println("\">");
    out.println(e.getColumnStringValue(e.getColumnNamePosition("salesCustName")));
    out.println("</OPTION>");
    e.destroyAll();
   }
   out.println("</SELECT></TD><TD></TD></TR>");
  } // end of else's statement  
  out.println("<TR><TD>Nomor HandPhone</TD><TD><INPUT TYPE=TEXT NAME=\"newCPHP\" ");
  out.println("SIZE=20 MAXLENGTH=14></TD><TD></TD><TD></TD><TD></TD></TR>");
  out.println("<TR><TD>Nomor Telephone</TD><TD><INPUT TYPE=TEXT NAME=\"newCPTP\" ");
  out.println("SIZE=20 MAXLENGTH=14></TD><TD></TD><TD></TD><TD></TD></TR>");
  out.println("<TR><TD>Nomor Fax</TD><TD><INPUT TYPE=TEXT NAME=\"newCPFX\" ");
  out.println("SIZE=20 MAXLENGTH=14></TD><TD></TD><TD></TD><TD></TD></TR>");
  out.println("<TR><TD>Catatan Khusus untuk Contact Person</TD><TD>");
  out.println("<INPUT TYPE=TEXT NAME=\"newCCPNote\" SIZE=40 MAXLENGTH=32></TD>");
  out.println("<TD></TD><TD></TD><TD></TD></TR>");
  out.println("<TR><TD>Catatan Khusus untuk Organisasi/Langganan</TD><TD>");
  out.println("<INPUT TYPE=TEXT NAME=\"newCNote\" SIZE=40 MAXLENGTH=32></TD>");
  out.println("<TD></TD><TD></TD><TD></TD></TR>");
 }
    
/**
 * writeCOrder's method 
 * @param
 * @return
 */
 private void writeCOrder(Vector p,Replacer rep,int rID,int aID,String typeFlight) 
  throws SQLException
 {
  if((p==null)||(p.size()<1)) 
   throw new SQLException("Empty Passanger Vector"); 
  if((rID<=0)||(aID<=0))
   throw new SQLException("RouteID/ClassKeyID/AirlineID has invalid value"); 
  if(blackList)
    return; 
  // Customer's Order Info
  StringBuffer str = new StringBuffer();
  str.append("LEFT JOIN airlineRouteClass ON airlineRoute.routeKey=");
  str.append("airlineRouteClass.classRouteKey LEFT JOIN airlineExtraInfo ON ");
  str.append("airlineExtraInfo.extraInfoRouteClassKey=airlineRouteClass.classKey ");
  str.append("LEFT JOIN airlineRouteDT ON airlineRouteDT.routeDTKey=");
  str.append("airlineRoute.airRouteDTKey WHERE routeKey=\""+rID+"\";");
  Vector v = dbFill3.QuerySpecific("airlineRoute",str.toString(),out);
  if(v == null)
   throw new SQLException("The airlineRoute is not found");
  DBEncapsulation e = null;
  out.println("<TABLE BORDER=0 WIDTH=\"75%\">");
  out.println("<TR><TD><B>ORDER</B></TD><TD></TD><TD></TD><TD></TD><TD></TD></TR>");
  out.println("<TR><TD>Flight Number</TD><TD>");
  out.println(e.getColumnStringValues(e.getColumnNamePosition("routeFlightNum"))+"</TD>");
  out.println("<TR><TD> ");
  out.println("</TABLE><BR></BR>");
 }

/**
 * writeCTicket's method
 * @param 
 * @return
 */
 private void writeCTicket(int numPass,Vector pass,Replacer rep) throws SQLException
 {
  if(numPass!=pass.size())
   throw new SQLException("The Number of Passanger with the supplied number is unequal"); 
  if(blackList)
   return; 
  // Customer's Ticket Info
  out.println("<TABLE BORDER=0 WIDTH=\"75%\">");
  out.println("<TR><TD><B>TICKET</B></TD><TD></TD><TD></TD><TD></TD></TR>");
  int inc = 0;
  StringBuffer str = new StringBuffer();
  for(int i=0;i<numPass;i++)
  {
   inc+=1;
   out.println("<TR><TD>Name "+inc+"</TD><TD>");
   rep = (Replacer)((Replacer)pass.get(i)).clone();
   str.append("name"+inc);
   out.println(rep.getOriginal()+"</TD><TD>Nomor Ticket</TD><TD>");
   out.println("<INPUT TYPE=TEXT NAME=\"ticketNo\" SIZE=28 MAXLENGTH=24></TD></TR>");
   str.delete(0,str.length());
   rep.reset();
  }
  out.println("<TR><TD>DI-SUPPLIED</TD><TD><SELECT NAME=\"supplied\">");
  out.println("<OPTION SELECTED VALUE=\"OWN\">SENDIRI</OPTION>");
  out.println("<OPTION VALUE=\"OTHER\">OLEH TRAVEL AGENT</OPTION></SELECT></TD>");
  str.append("WHERE salesCustType=\"TR_AGENT\";"); 
  Vector v = dbFill.QuerySpecific("salesCustomer",str.toString(),out); 
  if(v == null)
   out.println("<TD>Belum ada Travel Agent yang lain di database</TD><TD></TD></TR>");
  else
  {
   out.println("<TD><SELECT NAME=\"supplier\"><OPTION SELECTED VALUE=\"-1\">");
   out.println("-- Pilih Ticket Supplier -- </OPTION>");
   DBEncapsulation e = null;
   while(v.size()!=0)
   {
    e = (DBEncapsulation)v.remove(0);
    inc = e.getColumnNamePosition("salesCustKey");
    out.println("<OPTION VALUE=\""+e.getColumnStringValues(inc)+"\">");
    out.println(e.getColumnStringValues(e.getColumnNamePosition("salesCustName")));
    out.println("</OPTION>");
    e.destroyAll(); 
   } 
   out.println("</SELECT></TD><TD>Bila tidak ada, ");
   out.println("masukan nama travel agent itu ");
   out.println("<INPUT TYPE=TEXT NAME=\"newSupplier\" SIZE=40 MAXLENGTH=32>");
   out.println("</TD></TR>");
  } 
  out.println("<TR><TD>Methode Pengambilan</TD><TD><SELECT NAME=\"ticketDeliver\">");
  out.println("<OPTION SELECTED VALUE=\"PICKUP\">Diambil sendiri</OPTION>");
  out.println("<OPTION VALUE=\"DELIVER\">Diantar</OPTION>");
  out.println("</SELECT></TD><TD></TD><TD></TD></TR>");
  out.println("</TABLE><BR></BR>");
 }

/**
 * writeCPayment's method 
 * @param
 * @return
 */
 private void writeCPayment() throws SQLException
 {
  if(blackList)
   return; 
  // Customer's Payment Info
  out.println("<TABLE BORDER=0 WIDTH=\"75%\">");
  out.println("<TR><TH><B>PEMBAYARAN</B></TH><TH></TH><TH></TH><TH></TH></TR>");
  out.println("<TR><TD>METHODE PEMBAYARAN</TD><TD><SELECT NAME=\"method\">");
  out.println("<OPTION SELECTED VALUE=\"CASH\">CASH</OPTION>");
  out.println("<OPTION VALUE=\"CHEQUE\">CHEQUE</OPTION>");
  out.println("<OPTION VALUE=\"GIRO\">GIRO</OPTION>");
  out.println("<OPTION VALUE=\"DC\">DEBIT CARD</OPTION>");
  out.println("<OPTION VALUE=\"CC\">CREDIT CARD</OPTION>");
  out.println("</TD><TD></TD><TD></TD><TD></TD></TR>");
  out.println("<TR><TD></TD><TD>CHEQUE/GIRO NUMBER</TD><TD><INPUT TYPE=TEXT NAME=\"");
  out.println("CH_GR_NUM\" SIZE=40 MAXLENGTH=32></TD><TD>TANGGAL CAIR</TD><TD>");
  out.println("<INPUT TYPE=TEXT NAME=\"MATURITY\" SIZE=40 MAXLENGTH=32></TD></TR>");
  out.println("<TR><TD></TD><TD>CREDIT CARD NOMOR</TD><TD><INPUT TYPE=TEXT NAME=\"");
  out.println("CC_DB_NUM\" SIZE=40 MAXLENGTH=32></TD><TD>EXPIRE DATE [TANGGAL");
  out.println(" KADARLUASA]</TD><TD><INPUT TYPE=TEXT NAME=\"expDate\" SIZE=20 ");
  out.println("MAXLENGTH=10></TD></TR>");
  out.println("<TR><TD></TD><TD>NAMA BANK</TD><TD><INPUT TYPE=TEXT NAME=\"bank\"");
  out.println(" SIZE=40 MAXLENGTH=32></TD><TD></TD><TD></TD><TD></TD></TR>");
  out.println("<TR><TD></TD><TD>NAMA YANG TERTERA DI CHEQUE/GIRO/DEBIT/CREDIT CARD</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"NAME_ON_DOC\" SIZE=40 MAXLENGTH=32></TD>");
  out.println("<TD></TD><TD></TD></TR>");
  out.println("<TR><TD>JUMLAH PEMBAYARAN</TD><TD><SELECT NAME=\"CURR\">");
  out.println("<OPTION SELECTED VALUE=\"NON\"> -- Pilih Currency -- </OPTION>");
  Vector v = util.queryCurrency(dbFill3,out);
  if(v == null)
   throw new SQLException("Need to fill the currency table in airline database");
  DBEncapsulation e = null;
  int p1 = 0;
  while(v.size()>0)
  {
   e = (DBEncapsulation)v.remove(0);
   p1 = e.getColumnNamePosition("currencyAbbr");
   out.println("<OPTION VALUE=\""+e.getColumnStringValue(p1)+"\">");
   out.println(e.getColumnStringValue(p1)+"</OPTION>"); 
   e.destroyAll();
  }
  out.println("</SELECT></TD><TD><INPUT TYPE=TEXT NAME=\"amount\" SIZE=20 MAXLENGTH=12 ");
  out.println("VALUE=\"0.00\"></TD><TD></TD><TD></TD></TR>"); 
  out.println("</TABLE><BR></BR>");
 }

/**
 * processFinal's method
 * @param
 * @return
 */
 public void processFinal(HttpServletRequest req) throws SQLException
 {

 }

} // end of ATTSaddAirSales

