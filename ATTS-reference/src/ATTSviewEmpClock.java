/**
 * ATTSviewRoute class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSviewEmpClock extends HttpServlet
{
 private DBFill dbFill = null;
 private String dbName = "ATTSemployee";
 private PrintWriter out = null;
 private Closure cls = null;
 private ATTSutility util = null;

 public void init() throws ServletException
 {
  String driver = "com.mysql.jdbc.Driver";
  String url = "jdbc:mysql://starcloud:3306/";
  String user= "edisonch";
  String pwd = "gblj21mysql03";
  dbFill = new DBFill(driver,url,user,pwd);
  util = new ATTSutility();
 } // end of init

 public void destroy() 
 {
  dbFill.closeDatabase();
  dbFill = null;
 } // end of destroy

 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 } // end of doGet

 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS View Employee Clock</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<HR></HR>");
  String view = req.getParameter("view");
  if(view == null)
  {
   cls.error(1,"view");
   cls.closing();
   out.close();
   return;
  }
  
  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException("<BR>Fail to make initial connection</BR>");
   if(view.compareToIgnoreCase("ALL")==0)
     processView(req);
   else if(view.compareToIgnoreCase("SET")==0)
     createForm();
   else if(view.compareToIgnoreCase("PROCESS")==0)
     processAll(req); 
   else
    cls.error(6,null);
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out); 
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
   view = null;
   out = null;
   cls = null;
  }

 } // end of doPost 

 public void processView(HttpServletRequest req) throws SQLException
 {
  String empID = req.getParameter("empID");
  String empPK = req.getParameter("empPK");
  String perBg = req.getParameter("perBeg");
  String perEn = req.getParameter("perEnd");
  if((empID==null)||(empPK==null)||(perBg==null)||(perEn==null))
  {
   cls.error(1,null);
   return;
  }  
  if((empID.length()<1)||(empPK.length()<1)||(perBg.length()<1)||(perEn.length()<1))
  {
   cls.error(2,null);
   return;
  }
  DateUtil du = new DateUtil();
  if((!du.checkDigit(empID,out))||(!du.checkDigit(empPK,out)))
  {
   cls.error(3,"Employee ID/Employee PassKey");
   return;
  } 
  if((!du.checkSQLDate(perBg,out))||(!du.checkSQLDate(perEn,out)))
  {
   cls.error(3,"Period Begin atau/dan Period End");
   return;
  }
  if((!du.checkSQLDateValidity(perBg,4,out))||(!du.checkSQLDateValidity(perEn,4,out)))
  {
   cls.error(4,"Period Begin atau/dan Period End");
   return;
  }

  int eID = Integer.parseInt(empID);

  if(!util.verifyAuthorization(dbFill,cls,out,eID,empPK,null,3,null))
  {
   cls.error(0,"Employee ID dan EmployeePassKey tidak sesuai");
   return;
  }

  StringBuffer msg = new StringBuffer();
  msg.append("WHERE empClockDate>=\""+perBg+"\" AND empClockDate<=\""+perEn+"\";");
  Vector v = dbFill.QuerySpecific("employeeViolatedClock",msg.toString(),out);
  if(v == null)
  {
   cls.error(0,"Data Clock-In/Clock-Out untuk periode yg diminta belum tersedia");
   return;
  }
  dbFill.viewContainer(v,out);
  msg.delete(0,msg.length());
 } // end of createView's method

 public void createForm() throws SQLException
 {
  StringBuffer msg = new StringBuffer();
  msg.append("WHERE empClockOut=\"00:00:00\" ORDER BY empClockDate, empClockOfID;");
  Vector v = dbFill.QuerySpecific("employeeViolatedClock",msg.toString(),out);
  if(v == null)
  {
   cls.error(0,"All employee already clock-out");
   return;
  }
  DBEncapsulation r = null;
  int size = v.size();
  int pos1 = 0;
  int pos2 = 0;
  int pos3 = 0;
  int pos4 = 0;
  msg.delete(0,msg.length());
  util.createBegin("ATTSviewEmpClock","view","PROCESS",true,out);
  out.println("<CAPTION><BR><B>Pilih Clock yang ingin di-ubah</B></BR></CAPTION>");
  for(int i=0;i<size;i++)
  {
   out.println("<TR><TD>");
   r=(DBEncapsulation)((DBEncapsulation)v.get(i)).clone();
   pos1=r.getColumnNamePosition("empClockKey");
   pos2=r.getColumnNamePosition("empClockOfID");
   pos3=r.getColumnNamePosition("empClockDate");
   pos4=r.getColumnNamePosition("empClockIn");
   msg.append("<INPUT TYPE=RADIO NAME=id VALUE=\""+r.getColumnStringValues(pos1)+"\">");
   msg.append("</TD><TD>");
   msg.append("Employee ID: "+r.getColumnStringValues(pos2)+" Date: ");
   msg.append(r.getColumnStringValues(pos3)+" Clock-In: "+r.getColumnStringValues(pos4));
   out.println(msg.toString()+"</TD></TR>");
  }
  out.println("</TABLE><BR></BR>");
  out.println("<TABLE BORDER=0 WIDTH=75%>");
  out.println("<TR><TD>Clock-Out Jam:</TD>");
  out.println("<TD><INPUT TYPE=TEXT SIZE=20 MAXLENGTH=8 NAME=out></TD></TR>");
  util.createEnd(out);
 } // end of createForm's method

 public void processAll(HttpServletRequest req) throws SQLException
 {
  int clockID = Integer.parseInt(req.getParameter("id"));
  String clockOut = req.getParameter("out");
  if(clockOut==null)
  {
   cls.error(1,"clock out");
   return;
  }
  if(!util.checkTime(clockOut,out))
  {
   cls.error(3,"employee clock out");
   return;
  }
  Vector v = new Vector();
  DBEncapsulation dbI = new DBEncapsulation("employeeViolatedClock");
  dbI.setColumnNameVal("empClockOut",clockOut);
  dbI.setCondition("WHERE empClockKey="+clockID);
  v.addElement((DBEncapsulation)dbI.clone());
  dbFill.UpdateDatabase(v,out);
  dbI.destroyAll();
  if(!v.isEmpty())
    v.removeAllElements(); 
 } // end of processAll's method

} // end of ATTSviewAirlineAccess' class

