/**
 * ATTSaddCity class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSstockTicket extends HttpServlet
{
 private DBFill dbFill = null;
 private String dbName = "ATTSairline";
 private PrintWriter out = null;
 private ATTSutility util = null;
 private Closure cls = null;

 public void init() throws ServletException
 {
  String driver = "com.mysql.jdbc.Driver";
  String url = "jdbc:mysql://starcloud:3306/";
  String user= "edisonch";
  String pwd = "gblj21mysql03";
  dbFill = new DBFill(driver,url,user,pwd);
  util = new ATTSutility();
 }
 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
 }
 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 }
 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS Stock Ticket </TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<HR></HR>");

  int air = Integer.parseInt(req.getParameter("airlineID"));
  String stockAmount = req.getParameter("stockAmount");
  String stockOWBeginNumber = req.getParameter("stockOWBeginNumber");
  String stockOWEndNumber = req.getParameter("stockOWEndNumber");
  String stockRTBeginNumber = req.getParameter("stockRTBeginNumber");
  String stockRTEndNumber = req.getParameter("stockRTEndNumber");
  String stockTakeBy = req.getParameter("stockTakeBy");
  String stockCheckBy = req.getParameter("stockCheckBy");
  String stockTktBNote= req.getParameter("stockTktBNote");
  
  if((stockAmount==null)||(stockOWBeginNumber==null)||(stockOWEndNumber==null)||
     (stockRTBeginNumber==null)||(stockRTEndNumber==null)||(stockTakeBy==null)||
     (stockCheckBy==null))
  {
   cls.error(1,null);
   return;
  }
  if((stockAmount.length()<1)||(stockOWBeginNumber.length()<1)||
     (stockOWEndNumber.length()<1)||(stockRTBeginNumber.length()<1)||
     (stockRTEndNumber.length()<1)||(stockTakeBy.length()<1)||(stockCheckBy.length()<1))
  {
   cls.error(2,null);
   return;
  }

  if((!util.checkDigit(stockAmount,true,out))||
     (!util.checkDigit(stockOWBeginNumber,true,out))||
     (!util.checkDigit(stockOWEndNumber,true,out))||
     (!util.checkDigit(stockRTBeginNumber,true,out))||
     (!util.checkDigit(stockRTEndNumber,true,out))||
     (!util.checkDigit(stockTakeBy,true,out))||
     (!util.checkDigit(stockCheckBy,true,out)))
  {
   cls.error(0,"Format Input salah. Harus berupa digit/angka");
   return; 
  }
 
  Vector db = new Vector();
  DBEncapsulation dbT = new DBEncapsulation();
  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException();  

   if(stockTktBNote.length()>1)
   {
    dbT.setDBTableName("airlineBigNote");
    dbT.setColumnNameVal("bigNote",stockTktBNote);
    dbT.setVariable("SET @airlineBigNoteKey = LAST_INSERT_ID();");
    db.addElement((DBEncapsulation)dbT.clone());
    dbT.clearAll();
   }
   dbT.setDBTableName("airlineStockTicket");
   dbT.setColumnNameVal("stockOfAirline",air);
   dbT.setColumnNameVal("stockAmount",stockAmount);
   dbT.setColumnNameVal("stockOWBeginNumber",stockOWBeginNumber); 
   dbT.setColumnNameVal("stockOWEndNumber",stockOWEndNumber);
   dbT.setColumnNameVal("stockRTBeginNumber",stockRTBeginNumber);
   dbT.setColumnNameVal("stockRTEndNumber",stockRTEndNumber);
   dbT.setColumnNameVal("stockTakeBy",stockTakeBy);
   dbT.setColumnNameVal("stockCheckBy",stockCheckBy);
   dbT.setColumnNameVal("stockTktDate","CURRENT_DATE");
   dbT.setColumnNameVal("stockTktTime","CURRENT_TIME");
   if((stockTktBNote!=null)&&(stockTktBNote.length()>0))
     dbT.setColumnNameVal("stockTktBNote","@airlineBigNoteKey");
   db.addElement((DBEncapsulation)dbT.clone());
   dbFill.InsertDatabase(db,out); 
   out.println("<BR>Successfully Insert stock ticket");
   dbT.destroyAll();
  } // end of try
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
   if(!db.isEmpty())
     db.removeAllElements();
   db = null;
  } 
 }  


}

