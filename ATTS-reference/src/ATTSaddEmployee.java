/**
 * ATTSaddEmployee class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSaddEmployee extends HttpServlet
{
 private Closure cls = null;
 private DBFill dbFill = null;
 private PrintWriter out = null;
 private String dbName = "ATTSemployee";
 private ATTSutility util = null;

/**
 * init's method is to initialize the servlet
 * @param none
 * @return none
 */
 public void init() throws ServletException
 {
  dbFill = new DBFill();
  util = new ATTSutility();
 }

/**
 * destroy's method is to end the life-cycle of the servlet
 * @param none
 * @return none
 */
 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
  dbFill = null;
 }

/**
 * doGet's method is to process GET method from user
 * @param HttpServletRequest req
 *        HttpServletResponse res
 * @return none
 */
 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 }

/**
 * doPost's method is to process POST method from user
 * @param HttpServletRequest req contains user request
 *        HttpServletResponse res contains user respond
 * @return none
 */
 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS add Employee </TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<BR></BR><H1>ATTS Add Employee</H1>");
  out.println("<HR></HR><BR></BR>");
  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException("<BR>Failure to make initial connection to "+dbName+"</BR>");
   processEmployee(req);
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
   cls = null;
   out = null; 
  }  
 } // end of doPost 

 public void processEmployee(HttpServletRequest req) throws ServletException,SQLException
 {
  String eName = req.getParameter("empName");
  String eAddr = req.getParameter("empAddr");
  String eCity = req.getParameter("empCity");
  String eZip  = req.getParameter("empZip");      // number
  String ePh   = req.getParameter("empPh");       // number
  String eHp   = req.getParameter("empHp");       // number
  String eIDC  = req.getParameter("empIDCard");
  String eICE  = req.getParameter("empExIDCard"); // DATE
  String ePBeg = req.getParameter("empBegDate");  // DATE
  String eBDay = req.getParameter("empBDate");    // DATE
  String eHome = req.getParameter("empHome");
  String eCPer = req.getParameter("empContactPerson");
  String eCPC  = req.getParameter("empContactNumber"); // number
  String eCPN  = req.getParameter("empNote");     // boleh tidak di-isi
  String ePK1  = req.getParameter("empPassKey");  // number
  String ePK2  = req.getParameter("empPassKey2"); // number
  String ePWD1 = req.getParameter("empPassword");
  String ePWD2 = req.getParameter("empPassword2");

  if((eName==null)||(eAddr==null)||(eCity==null)||(eZip==null)||(ePh==null)||
     (eHp==null)||(eIDC==null)||(eICE==null)||(ePBeg==null)||
     (eBDay==null)||(eHome==null)||(eCPer==null)||(eCPC==null)||(eCPN==null)||
     (ePK1==null)||(ePK2==null)||(ePWD1==null)||(ePWD2==null))
  {
   cls.error(1,null);
   return;
  }
  if((eName.length()<1)||(eAddr.length()<1)||(eCity.length()<1)||(eZip.length()<1)||
     (ePh.length()<1)||(eHp.length()<1)||(eIDC.length()<1)||(eICE.length()<1)||
     (ePBeg.length()<1)||(eBDay.length()<1)||(eHome.length()<1)||
     (eCPer.length()<1)||(eCPC.length()<1)||(ePK1.length()<1)||
     (ePK2.length()<1)||(ePWD1.length()<1)||(ePWD2.length()<1))
  {
   cls.error(2,null);
   return;
  }
  if((!util.checkDigit(ePK1,true,out))||(!util.checkDigit(ePK2,true,out))||
     (!util.checkDigit(eZip,true,out))||(!util.checkDigit(ePh,false,out))||
     (!util.checkDigit(eHp,false,out))||(!util.checkDigit(eCPC,false,out))||
     (!util.checkDigit(eIDC,false,out)))
  {
   cls.error(3,null);
   return;
  }
  if(ePK1.compareToIgnoreCase(ePK2)!=0)
  {
   cls.error(0,"PassKey yg anda masukan pertama tidak sama dengan yg kedua");
   return;
  }
  if(ePWD1.compareToIgnoreCase(ePWD2)!=0)
  {
   cls.error(0,"Password yg anda masukan pertama tidak sama dengan yg kedua");
   return;
  }
  DateUtil du = new DateUtil();
  if((!du.checkSQLDate(eICE,out))||(!du.checkSQLDate(ePBeg,out))||
     (!du.checkSQLDate(eBDay,out)))
  {
   cls.error(4,null);
   return;
  }  
  if(!du.checkSQLDateValidity(eICE,3,out))
  {
   cls.error(4,null);
   return;
  }
  if(!du.checkSQLDateValidity(eBDay,1,out))
  {
   cls.error(4,"Anda tidak mungkin lahir pada tanggal yg anda isi");
   return;
  }
  if(!du.checkSQLDateValidity(ePBeg,4,out))
  {
   cls.error(4,null);
   return;
  }

  DBEncapsulation dbI = new DBEncapsulation();
  Vector v = new Vector();
  StringBuffer tmp = new StringBuffer();

  if(eCPN.length()>1)
  {
   dbI.setDBTableName("employeeNotes");
   dbI.setColumnNameVal("emNotes",eCPN);
   dbI.setVariable("SET @empNotesKey = LAST_INSERT_ID();");
   v.addElement(dbI.clone());
   dbI.clearAll();
  }
  tmp.append("MD5(\""+ePK1+"\")");
  
  dbI.setDBTableName("employee");
  dbI.setColumnNameVal("employeeName",eName);
  dbI.setColumnNameVal("employeePassKey",tmp.toString()); 
  tmp.delete(0,tmp.length());
  tmp.append("MD5(\""+ePWD1+"\")");
  dbI.setColumnNameVal("employeePassword",tmp.toString());
  dbI.setVariable("SET @empID = LAST_INSERT_ID();");
  tmp.delete(0,tmp.length());
  v.addElement(dbI.clone());
  dbI.clearAll();
 
  dbI.setDBTableName("employeeInfo");
  dbI.setColumnNameVal("empInfoIDCard",eIDC);
  dbI.setColumnNameVal("empInfoIDExpire",eICE);
  dbI.setColumnNameVal("empInfoBirthDate",eBDay);
  dbI.setColumnNameVal("empInfoHometown",eHome);
  dbI.setColumnNameVal("empInfoOfID","@empID");
  v.addElement(dbI.clone());
  dbI.clearAll();
 
  dbI.setDBTableName("employeeAddress");
  dbI.setColumnNameVal("empHomeAdd",eAddr);
  dbI.setColumnNameVal("empHomeAddCity",eCity);
  dbI.setColumnNameVal("empHomeAddZip",eZip);
  dbI.setColumnNameVal("empAddOfID","@empID");
  v.addElement(dbI.clone());
  dbI.clearAll();

  dbI.setDBTableName("employeeContact");
  dbI.setColumnNameVal("empContactHomePhone",ePh);
  dbI.setColumnNameVal("empContactHandPhone",eHp);
  dbI.setColumnNameVal("empContactPersonName",eCPC);
  dbI.setColumnNameVal("empContactPersonNumber",eCPN);
  if(eCPN.length()>1)
    dbI.setColumnNameVal("empContactPersonNotes","@empNotesKey");
  v.addElement(dbI.clone());
  dbI.clearAll();
  dbFill.InsertDatabase(v,out);
  out.println("<BR>Successfully insert new employee info into database</BR>"); 
  if(!v.isEmpty())
    v.removeAllElements();

 } // end of processEmployee's method

} // end of ATTS add Employee class

