/**
 * ATTSsetExchangeRate class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSsetExchangeRate extends HttpServlet
{
 private DBFill dbFill = null;
 private String dbName = "ATTSairline";
 private PrintWriter out = null;
 private Closure cls = null;
 private ATTSutility util = null;

 public void init() throws ServletException
 {
  String driver = "com.mysql.jdbc.Driver";
  String url = "jdbc:mysql://starcloud:3306/";
  String user= "edisonch";
  String pwd = "gblj21mysql03";
  dbFill = new DBFill(driver,url,user,pwd);
  util = new ATTSutility();
 }
 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
 }
 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 }
 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS set Exchange Rate</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<HR></HR>");

  String airCurrFrom= req.getParameter("currencyFrom");
  String airCurrTo  = req.getParameter("currencyTo");
  String airRate    = req.getParameter("rate");
  String airInput   = req.getParameter("airInputby");
  String airNote    = req.getParameter("airNote");

  if((airCurrFrom==null)||(airCurrTo==null)||(airRate==null)||(airInput==null)||
     (airNote==null))
  {
   cls.error(1,null);
   cls.makeClosing();
   cls.closing();
   out.close();
   return;
  }

  if((airCurrFrom.length()<1)||(airRate.length()<1)||(airInput.length()<1) 
     ||(airCurrTo.length()<1))
  {
    cls.error(2,null);
    cls.makeClosing();
    cls.closing();
    out.close();
    return;
  }

  Float fl1 = new Float(airRate);
  Vector dbVec = new Vector();  
  DBEncapsulation dbEncap = new DBEncapsulation();

  dbEncap.setDBTableName("airlineMediumNote"); 
  dbEncap.setColumnNameVal("mediumNote",airNote);
  dbEncap.setVariable("SET @airlineMNote = LAST_INSERT_ID();");
  dbVec.addElement((DBEncapsulation)dbEncap.clone());
  dbEncap.clearAll();

  dbEncap.setDBTableName("airlineExchangeRate");
  dbEncap.setColumnNameVal("exchangeFrom",Integer.parseInt(airCurrFrom));
  dbEncap.setColumnNameVal("exchangeTo",Integer.parseInt(airCurrTo));
  dbEncap.setColumnNameVal("exchangeRate",fl1.floatValue()); 
  dbEncap.setColumnNameVal("exchangeInputBy",Integer.parseInt(airInput));
  dbEncap.setColumnNameVal("exchangeChangeDate","CURRENT_DATE");
  dbEncap.setColumnNameVal("exchangeChangeTime","CURRENT_TIME");
  if(airNote.length()>1)
    dbEncap.setColumnNameVal("exchangeMNote","@airlineMNote");
  dbVec.addElement(dbEncap);

  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException();  
   dbFill.InsertDatabase(dbVec,out);
   out.println("Successfully insert the new exchange rate to the database");
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   dbEncap.destroyAll();
   dbVec.removeAllElements();
   out.close();
   dbEncap= null;
   dbVec = null;
  } 
 } 

}

