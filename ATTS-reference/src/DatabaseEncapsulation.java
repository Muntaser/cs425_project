package ATTS;

import java.util.Enumeration;
import java.sql.SQLException;

/**
 * Condition is for the condition requirement for search,delete,insert
 * Variable is for setting memory in db
 */
public interface DatabaseEncapsulation 
{

 final public static int SP_VAR = 99;
 final public static int SAME_SIZE = -17;
 final public static int NOT_FOUND = -19;
 final public static int OUT_RANGE = -23;

 public boolean isDBTableNameNull();
 public void setDBTableName(String tableName);
 public void setColumnName(String colName);
 public void setColumnValue(String colValue);
 public void setColumnValue(int colValue);
 public void setColumnValue(float colValue);
 public void setColumnValue(double colValue);
 public void setColumnNameVal(String colName,String colVal);
 public void setColumnNameVal(String colName,int colVal);
 public void setColumnNameVal(String colName,float colVal);
 public void setColumnNameVal(String colName,double colVal);
 public void setSizeColumnName();
 public void setSizeColumnValue();
 public void setSizeColumnType();
 public void setCondition(String cond) throws SQLException;
 public void setVariable(String var) throws SQLException;
 public void setRepeatFlag(int number);

 public Enumeration getAllColumnNames();
 public Enumeration getAllColumnValues(); 
 public String getColumnNames(int position) throws SQLException;
 public String getColumnStringValues(int position) throws SQLException;
 public String getColumnStringValue(int position) throws SQLException;
 public String getDBTableName();
 public String getCondition();
 public String getVariable();
 public int getColumnType(int position);
 public int getColumnIntegerValue(int position) throws SQLException;
 public float getColumnFloatValue(int position) throws SQLException;
 public double getColumnDoubleValue(int position) throws SQLException;
 public int getSizeColumnName();
 public int getSizeColumnValue();
 public int getSizeColumnType();
 public int getConditionFlag();
 public int getVariableFlag();
 public int getRepeatFlag(); 
 public void destroyAll();
 public void clearAll();

 public int getColumnNamePosition(String colName) throws SQLException;
 public void replaceName(int posName, String name) throws SQLException;
 public void replaceValue(int posValue, String val) throws SQLException;
 public void replaceValue(int posValue, int val) throws SQLException;
 public void replaceValue(int posValue, float val) throws SQLException; 
 public void replaceValue(int posValue, double val) throws SQLException;
 public void removeNameVal(String name) throws SQLException;
 public void removeNameVal(int pos); 
 public void mergeEncap(DBEncapsulation a) throws SQLException;
  
}
