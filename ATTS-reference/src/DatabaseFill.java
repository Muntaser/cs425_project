package ATTS;

import java.sql.*;
import java.io.PrintWriter;
import java.util.Vector;

public interface DatabaseFill  
{
 public void closeDatabase();
 public boolean makeInitialConnection(String DBName, PrintWriter out);
 public boolean UpdateDatabase(Vector vec, PrintWriter out) throws SQLException;
 public void InsertDatabase(Vector vec, PrintWriter out) throws SQLException;
 public void DeleteDatabase(Vector vec, PrintWriter out) throws SQLException;
 public DBEncapsulation QuerySingleResult(DBEncapsulation dbEncap,PrintWriter out)
	throws SQLException;
 public Vector QueryDatabase(DBEncapsulation dbEncap,PrintWriter out) throws SQLException; 
 public void viewContainer(Vector vec, PrintWriter out) throws SQLException;
 public void viewEncap(DBEncapsulation encap, PrintWriter out) throws SQLException;
 public Vector QuerySpecific(String tableName,String cond,PrintWriter out) 
	throws SQLException;
 public DBEncapsulation QuerySingleSpecific(String tableName,String cond,PrintWriter out)
	throws SQLException;
 public Vector merge(Vector v,DBEncapsulation dbE,String name,String val) throws SQLException;
}
