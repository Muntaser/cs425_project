/**
 * ATTSviewBlockSeats class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSviewBlockSeats extends HttpServlet
{
 private DBFill dbFill;
 private String dbName    = "ATTSairline";
 private PrintWriter out  = null;
 private Closure cls      = null;
 private ATTSutility util = null;
 
 public void init() throws ServletException
 {
  dbFill = new DBFill();
  util = new ATTSutility();
 }
 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
 }
 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 }
 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS view block seats</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<HR></HR>");
  String view = req.getParameter("view");
  if(view == null)
  {
   cls.error(1,"view");
   cls.makeClosing();
   cls.closing();
   out.close();
   return;
  } 

  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException();  
   if(view.compareToIgnoreCase("ALL")==0)
     viewForm();
   else if(view.compareToIgnoreCase("PROCESS")==0)
     processForm(req); 
   else if(view.compareToIgnoreCase("SET")==0) 
     createForm();
   else
    out.println("<BR><FONT COLOR=RED><B>view has wrong parameter:"+view+"</FONT></BR>");
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
   out = null;
   view= null;
  } 
 } 

 private Vector queryAirline() throws SQLException
 {
  DBEncapsulation dbQ = new DBEncapsulation("airline");
  dbQ.setColumnName("airlineID");
  dbQ.setColumnName("airlineName");
  Vector tmp = dbFill.QueryDatabase((DBEncapsulation)dbQ.clone(),out);
  dbQ.destroyAll();
  return tmp;
 }

 private Vector queryRoute() throws SQLException
 {
  DBEncapsulation dbQ = new DBEncapsulation("airlineRoute");
  dbQ.setColumnName("routeKey");
  dbQ.setColumnName("routeOrig");
  dbQ.setColumnName("routeDest");
  dbQ.setColumnName("routeViaSN");
  dbQ.setColumnName("routeAirlineID");
  Vector vec = dbFill.QueryDatabase((DBEncapsulation)dbQ.clone(),out);  
  if(vec == null)
    return null;
  return remakeResult(vec);
 }

 public Vector remakeResult(Vector vec) throws SQLException
 {
  int pos1 = 0;
  int pos2 = 0;
  DBEncapsulation dbT = null;
  DBEncapsulation dbR = null;
  Vector result = new Vector();
  int size = vec.size();
  for(int a=0;a<size;a++)
  {
   dbT = new DBEncapsulation((DBEncapsulation)((DBEncapsulation)vec.get(a)).clone());
   //find & replace route original
   pos1= dbT.getColumnNamePosition("routeOrig");
   dbR=dbFill.QuerySingleSpecific("airlineCity","cityKey="+dbT.getColumnStringValues(pos1),out);
   if(dbR == null)
     throw new SQLException("failed querySingleSpecific cityKey - routeOrig");
   pos2= dbR.getColumnNamePosition("cityName");
   if((pos2==dbR.SAME_SIZE)||(pos2==dbR.NOT_FOUND)||(pos2==dbR.OUT_RANGE))
     throw new SQLException("failed to find cityName"); 
   dbT.replaceValue(pos1,dbR.getColumnStringValue(pos2));
   //find & replace route destination
   pos1= dbT.getColumnNamePosition("routeDest");
   dbR.destroyAll();
   dbR=dbFill.QuerySingleSpecific("airlineCity","cityKey="+dbT.getColumnStringValues(pos1),out);
   if(dbR == null)
     throw new SQLException("failed querySingleSpecific cityKey - routeDest");
   pos2= dbR.getColumnNamePosition("cityName");
   if((pos2==dbR.SAME_SIZE)||(pos2==dbR.NOT_FOUND)||(pos2==dbR.OUT_RANGE))
     throw new SQLException("failed to find cityName");     
   dbT.replaceValue(pos1,dbR.getColumnStringValue(pos2));
   //find & replace route via if exist
   pos1= dbT.getColumnNamePosition("routeViaSN");
   if(dbT.getColumnIntegerValue(pos1)>0)
   {
    dbR.destroyAll();
    dbR=dbFill.QuerySingleSpecific("airlineCity","cityKey="+dbT.getColumnStringValues(pos1),out);
    if(dbR != null)
    {
     pos2= dbR.getColumnNamePosition("cityName");
     if((pos2!=dbR.SAME_SIZE)&&(pos2!=dbR.NOT_FOUND)&&(pos2!=dbR.OUT_RANGE))
       dbT.replaceValue(pos1,dbR.getColumnStringValue(pos2));
    }
   }
   //find & replace airline name
   dbR.destroyAll();
   pos1= dbT.getColumnNamePosition("routeAirlineID"); 
   dbT=dbFill.QuerySingleSpecific("airline","airlineID="+dbT.getColumnStringValues(pos1),out);   
   pos2= dbR.getColumnNamePosition("airlineName");
   dbT.replaceValue(pos1,dbR.getColumnStringValue(pos2));
   //putting the result into vector
   result.addElement((DBEncapsulation)dbT.clone());
   dbR.destroyAll(); 
   dbT.destroyAll();
  } // end of for-loop
//  dbFill.viewContainer(result,out);
  return result; 
 } // remakeResult's method

 public void createForm() throws SQLException
 {
  Vector vecRoute = queryRoute();
  if(vecRoute == null)
  {
   out.println("<BR><FONT COLOR=RED><B>Belum ada rute penerbangan di dalam database");
   out.println("<BR>Harap dimasukan dahulu data-data rute penerbangan</BR></FONT>");
   return;
  }
  int routeSize = vecRoute.size();
  int size = vecRoute.size();
  int pos1 = 0;
  int pos2 = 0;
  int pos3 = 0;
  int pos4 = 0;
  int pos5 = 0;
  DBEncapsulation dbR = null;
  out.println("<BR></BR>");
  out.println("<FORM METHOD=POST ACTION=\"ATTSblockSeats\">");
  out.println("<TABLE BORDER=0 WIDTH=75%>");
  out.println("<CAPTION><BR><H1>Set Block Seats</H1></BR></CAPTION>");
  out.println("<TR><TD>Rute untuk block seat</TD>");
  out.println("<TD><SELECT NAME=\"routeKey\">");
  out.println("<OPTION SELECTED VALUE=\"-1\">-- Pilih --</OPTION>"); 
  for(int b=0;b<routeSize;b++)
  {
   dbR=new DBEncapsulation((DBEncapsulation)((DBEncapsulation)vecRoute.get(b)).clone());
   pos1=dbR.getColumnNamePosition("routeKey");
   pos2=dbR.getColumnNamePosition("routeOrig");
   pos3=dbR.getColumnNamePosition("routeDest");
   pos4=dbR.getColumnNamePosition("routeViaSN");
   pos5=dbR.getColumnNamePosition("routeAirlineID");
   out.print("<OPTION VALUE=\""+dbR.getColumnStringValues(pos1)+"\">");
   out.print(dbR.getColumnStringValues(pos2)+"-"+dbR.getColumnStringValues(pos3));
   if(dbR.getColumnStringValues(pos4).compareToIgnoreCase("0")!=0)
     out.print(" via "+dbR.getColumnStringValues(pos4));
   out.println(" "+dbR.getColumnStringValues(pos5)+"</OPTION>");
   dbR.destroyAll();
  } 
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD>Block dari tanggal:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"BeginDate\" SIZE=16 MAXLENGTH=10></TD></TR>");
  out.println("<TR><TD>Block ke tanggal:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"EndDate\" SIZE=16 MAXLENGTH=10></TD></TR>");
  out.println("<TR><TD>Jumlah untuk block seat:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"Total\" SIZE=16 MAXLENGTH=10></TD></TR>");
  out.println("<TR><TD>Diminta oleh:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"RequestBy\" SIZE=16 MAXLENGTH=10></TD></TR>"); 
  out.println("</TABLE>");
  out.println("<INPUT TYPE=SUBMIT VALUE=\"Submit\">&nbsp;");
  out.println("<INPUT TYPE=RESET VALUE=\"Clear\">"); 
  out.println("</FORM>");
 }

 public void viewForm() throws SQLException
 {
  Vector vec = queryAirline();
  if(vec == null)
  {
   out.println("<BR><B><FONT COLOR=RED>Belum ada airline di dalam database");
   out.println("<BR>Harap dimasukan dahulu data-data airline kedalam database</FONT></B>");
   return;
  }
  DBEncapsulation dbR=dbFill.QuerySingleSpecific("airlineBlockSeats","WHERE blockSeatsKey=1",out);
  if(dbR == null)
  {
   out.println("<BR><B><FONT COLOR=RED>Belum ada block seats di dalam database");
   out.println("<BR>Harap dimasukan dahulu data-data block seats </FONT></B>");
   return; 
  }
  Vector v = queryRoute();
  if(v == null)
  {
   out.println("<BR><B><FONT COLOR=RED>Belum ada rute penerbangan di dalam database");
   out.println("<BR>Harap dimasukan dahulu data-data rute penerbangan sebelum ");
   return;
  }
  out.println("<FORM METHOD=POST ACTION=\"ATTSviewBlockSeats\">");
  out.println("<INPUT TYPE=HIDDEN NAME=\"view\" VALUE=\"PROCESS\">");
  out.println("<TABLE BORDER=0 WIDTH=75%>");
  out.println("<CAPTION><BR><H1>View Block Seats</H1></BR></CAPTION>");
  out.println("<TR><TD>Block Seats untuk airline:</TD>");
  out.println("<TD><SELECT NAME=\"airlineID\">");
  out.println("<OPTION SELECTED VALUE=\"-1\">-- Pilih --</OPTION>");
  out.println("<OPTION VALUE=\"0\">TIDAK PILIH</OPTION>");
  int size = vec.size();
  int pos1 = 0;
  int pos2 = 0;
  int pos3 = 0;
  dbR.destroyAll();
  for(int a=0;a<size;a++)
  {
   dbR=new DBEncapsulation((DBEncapsulation)((DBEncapsulation)vec.get(a)).clone());
   pos1=dbR.getColumnNamePosition("airlineID");
   pos2=dbR.getColumnNamePosition("airlineName");
   out.print("<OPTION VALUE=\""+dbR.getColumnStringValues(pos1)+"\">");
   out.println(dbR.getColumnStringValue(pos2)+"</OPTION>");
   dbR.destroyAll();
  }
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD>Jumlah block seats</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"Total\" SIZE=8 MAXLENGTH=3></TD></TR>");
  out.println("<TR><TD>Block Seats mulai tanggal:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"beginDate\" SIZE=16 MAXLENGTH=10></TD></TR>");
  out.println("<TR><TD>Block Seats akhir tanggal:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"endDate\" SIZE=16 MAXLENGTH=10></TD></TR>");
  out.println("<TR><TD>Input oleh [Employee ID]:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"requestBy\" SIZE=16 MAXLENGTH=10></TD></TR>");
  out.println("</TABLE>");
  out.println("<INPUT TYPE=SUBMIT VALUE=\"Submit\">&nbsp;");
  out.println("<INPUT TYPE=RESET VALUE=\"Clear\">"); 
  out.println("</FORM>");
 }

 public void processForm(HttpServletRequest req) 
	throws ServletException,SQLException,IOException
 {
  String airID = req.getParameter("airlineID"); //1 -> 1 character length
  String airTot= req.getParameter("Total");     //1 -> 1 character length
  String airBd = req.getParameter("beginDate"); //1980-12-31 -> 10 char length
  String airEd = req.getParameter("endDate");   //1980-12-31 -> 10 char length
  String airRb = req.getParameter("requestBy"); //1 -> 1 character length
  if((airID==null)||(airTot==null)||(airBd==null)||(airEd==null)||(airRb==null))
  {
   cls.error(1,null);
   return;
  }
  if(airTot.length()>0)
  {
   if(!util.checkDigit(airTot,true,out))
   {
    cls.error(0,"Jumlah Block Seat harus dalam bentuk angka/digit");
    return;
   }
  }
  if(airRb.length()>0)
  {
   if(!util.checkDigit(airRb,true,out))
   {
    cls.error(0,"Input By [Employee ID] harus dalam bentuk angka/digit");
    return; 
   }
  }
  DateUtil du = new DateUtil();
  if(airBd.length()>1)
  {
   if(!du.checkSQLDate(airBd,out))
   {
    cls.error(4,"Begin Date");
    return;
   }
   else if(!du.checkSQLDateValidity(airBd,4,out))
   {
    cls.error(4,"Begin Date");
    return;
   } 
  }
  if(airEd.length()>1)
  {
   if(!du.checkSQLDate(airEd,out))
   {
    cls.error(4,"End Date");
    return;
   }
   else if(!du.checkSQLDateValidity(airBd,4,out))
   {
    cls.error(4,"End Date");
    return;
   }
  } 
  int air     = Integer.parseInt(airID);
  int airTLen = airTot.length();
  int airBLen = airBd.length();
  int airELen = airEd.length();
  int airRLen = airRb.length();
 
  if((air<1)&&(airTLen<1)&&(airBLen!=10)&&(airELen!=10)&&(airRLen<1)) //1          00000
  {
   out.print("<BR><FONT COLOR=RED>Anda harus meng-isi salah satu parameter");
   out.println(" di atas secara benar</FONT>");
   return;
  }
  DBEncapsulation dbQ = new DBEncapsulation("airlineBlockSeats");
  dbQ.setColumnName("*");
   
  if((air<1)&&(airTLen<1)&&(airBLen!=10)&&(airELen!=10)&&(airRLen>=1)) //2         00001
  {
   dbQ.setCondition("WHERE blockSeatsRequestBy="+airRb);
  }
  else if((air<1)&&(airTLen<1)&&(airBLen!=10)&&(airELen==10)&&(airRLen<1)) //3     00010 
  {
   dbQ.setCondition("WHERE blockSeatsEndDate="+airEd);
  }
  else if((air<1)&&(airTLen<1)&&(airBLen==10)&&(airELen!=10)&&(airRLen<1)) //4     00100
  {
   dbQ.setCondition("WHERE blockSeatsBeginDate="+airBd);
  }
  else if((air<1)&&(airTLen>=1)&&(airBLen!=10)&&(airELen!=10)&&(airRLen<1)) //5    01000
  {
   dbQ.setCondition("WHERE blockSeatsTotal="+airTot);
  }
  else if((air>=1)&&(airTLen<1)&&(airBLen!=10)&&(airELen!=10)&&(airRLen<1)) //6    10000 
  {
   dbQ.setCondition("WHERE blockSeatsOfAirline="+air);
  }
  else if((air>=1)&&(airTLen>=1)&&(airBLen!=10)&&(airELen!=10)&&(airRLen<1)) //7   11000
  {
   dbQ.setCondition("WHERE blockSeatsOfAirline="+air+" AND blockSeatsTotal="+airTot);
  }
  else if((air>=1)&&(airTLen<1)&&(airBLen==10)&&(airELen!=10)&&(airRLen<1)) //8    10100
  {
   dbQ.setCondition("WHERE blockSeatsOfAirline="+air+" AND blockSeatsBeginDate="+airBd);
  }
  else if((air>=1)&&(airTLen<1)&&(airBLen!=10)&&(airELen==10)&&(airRLen<1)) //9    10010
  {
   dbQ.setCondition("WHERE blockSeatsOfAirline="+air+" AND blockSeatsEndDate="+airEd);
  }
  else if((air>=1)&&(airTLen<1)&&(airBLen!=10)&&(airELen!=10)&&(airRLen>=1)) //10  10001
  {
   dbQ.setCondition("WHERE blockSeatsOfAirline="+air+" AND blockSeatsRequestBy="+airRb);
  }
  else if((air>=1)&&(airTLen>=1)&&(airBLen==10)&&(airELen!=10)&&(airRLen<1)) //11  11100
  {
   dbQ.setCondition("WHERE blockSeatsOfAirline="+air+" AND blockSeatsBeginDate="+airBd);
  }
  else if((air>=1)&&(airTLen>=1)&&(airBLen!=10)&&(airELen==10)&&(airRLen<1)) //12  11010
  {
   dbQ.setCondition("WHERE blockSeatsOfAirline="+air+" AND blockSeatsTotal="+airTot+
                    " AND blockSeatsEndDate="+airEd);
  }
  else if((air>=1)&&(airTLen>=1)&&(airBLen!=10)&&(airELen!=10)&&(airRLen>=1)) //13 11001
  {
   dbQ.setCondition("WHERE blockSeatsOfAirline="+air+" AND blockSeatsTotal="+airTot+
		    " AND blockSeatsRequestBy="+airRb);
  }
  else if((air>=1)&&(airTLen>=1)&&(airBLen==10)&&(airELen==10)&&(airRLen<1)) //14  11110
  {
   dbQ.setCondition("WHERE blockSeatsOfAirline="+air+" AND blockSeatsTotal="+airTot+
		    " AND blockSeatsBeginDate="+airBd+" AND blockSeatsEndDate="+airEd);
  }
  else if((air>=1)&&(airTLen>=1)&&(airBLen==10)&&(airELen!=10)&&(airRLen>=1)) //15 11101
  {
   dbQ.setCondition("WHERE blockSeatsOfAirline="+air+" AND blockSeatsTotal="+airTot+
		    " AND blockSeatsBeginDate="+airBd+" AND blockSeatsRequestBy="+airRb);
  }
  else if((air>=1)&&(airTLen>=1)&&(airBLen==10)&&(airELen==10)&&(airRLen>=1)) //16 11111
  {
   dbQ.setCondition("WHERE blockSeatsOfAirline="+air+" AND blockSeatsTotal="+airTot+
		    " AND blockSeatsBeginDate="+airBd+" AND blockSeatsEndDate="+airEd+
 		    " AND blockSeatsRequestBy="+airRb);
  }
  else if((air<1)&&(airTLen>=1)&&(airBLen==10)&&(airELen==10)&&(airRLen>=1)) //17  01111
  {
   dbQ.setCondition("WHERE blockSeatsTotal="+airTot+" AND blockSeatsBeginDate="+airBd+
		    " AND blockSeatsEndDate="+airEd+" AND blockSeatsRequestBy="+airRb);
  }
  else if((air<1)&&(airTLen<1)&&(airBLen==10)&&(airELen==10)&&(airRLen>=1)) //18   00111
  {
   dbQ.setCondition("WHERE blockSeatsBeginDate="+airBd+" AND blockSeatsEndDate="+airEd+
                    " AND blockSeatsRequestBy="+airRb);
  }
  else if((air<1)&&(airTLen<1)&&(airBLen!=10)&&(airELen==10)&&(airRLen>=1)) //19   00011
  {
   dbQ.setCondition("WHERE blockSeatsEndDate="+airEd+" AND blockSeatsRequestBy="+airRb);
  }
  else if((air<1)&&(airTLen<1)&&(airBLen==10)&&(airELen!=10)&&(airRLen>=1)) //20   00101
  {
   dbQ.setCondition("WHERE blockSeatsBeginDate="+airBd+" AND blockSeatsRequestBy="+airRb);
  }
  else if((air<1)&&(airTLen>=1)&&(airBLen!=10)&&(airELen==10)&&(airRLen>=1)) //21  01011
  {
   dbQ.setCondition("WHERE blockSeatsTotal="+airTot+" AND blockSeatsEndDate="+airEd+
                    " AND blockSeatsRequestBy="+airRb);
  }
  else if((air>=1)&&(airTLen<1)&&(airBLen!=10)&&(airELen==10)&&(airRLen>=1))  //22 10011
  {
   dbQ.setCondition("WHERE blockSeatsOfAirline="+air+" AND blockSeatsEndDate="+airEd+
		    " AND blockSeatsRequestBy="+airRb);
  }
  else if((air>=1)&&(airTLen<1)&&(airBLen==10)&&(airELen==10)&&(airRLen>=1))  //23 10111
  {
   dbQ.setCondition("WHERE blockSeatsOfAirline="+air+" AND blockSeatsBeginDate="+airBd+
		    " AND blockSeatsEndDate="+airEd+" AND blockSeatsRequestBy="+airRb);
  }
  else if((air<1)&&(airTLen>=1)&&(airBLen!=10)&&(airELen!=10)&&(airRLen>=1))  //24 01001 
  {
   dbQ.setCondition("WHERE blockSeatsTotal="+airTot+" AND blockSeatsRequestBy="+airRb);
  }
  else if((air<1)&&(airTLen>=1)&&(airBLen!=10)&&(airELen==10)&&(airRLen<1))   //25 01010
  {
   dbQ.setCondition("WHERE blockSeatsTotal="+airTot+" AND blockSeatsEndDate="+airEd);
  }
  else if((air<1)&&(airTLen>=1)&&(airBLen==10)&&(airELen!=10)&&(airRLen<1))   //26 01100
  {
   dbQ.setCondition("WHERE blockSeatsTotal="+airTot+" AND blockSeatsBeginDate="+airBd);
  }
  else if((air>=1)&&(airTLen<1)&&(airBLen==10)&&(airELen!=10)&&(airRLen>=1))  //27 10101
  {
   dbQ.setCondition("WHERE blockSeatsOfAirline="+air+" AND blockSeatsBeginDate="+airBd+
		    " AND blockSeatsRequestBy="+airRb);
  }
  else if((air<1)&&(airTLen<1)&&(airBLen==10)&&(airELen==10)&&(airRLen<1))    //28 00110
  {
   dbQ.setCondition("WHERE blockSeatsBeginDate="+airBd+" AND blockSeatsEndDate="+airEd);
  }
  else if((air<1)&&(airTLen>=1)&&(airBLen==10)&&(airELen==10)&&(airRLen<1))   //29 01110
  {
   dbQ.setCondition("WHERE blockSeatsTotal="+airTot+" AND blockSeatsBeginDate="+airBd+
		    " AND blockSeatsEndDate="+airEd);
  }
  else if((air>=1)&&(airTLen<1)&&(airBLen==10)&&(airELen==10)&&(airRLen<1))   //30 10110
  {
   dbQ.setCondition("WHERE blockSeatsOfAirline="+air+" AND blockSeatsBeginDate="+airBd+
		    " AND blockSeatsEndDate="+airEd);
  }
  else if((air<1)&&(airTLen>=1)&&(airBLen==10)&&(airELen!=10)&&(airRLen>=1))  //31 01101
  {
   dbQ.setCondition("WHERE blockSeatsTotal="+airTot+" AND blockSeatsBeginDate="+airBd+
		    " AND blockSeatsRequestBy="+airRb);
  }
  else // must be an error
  {
   dbQ.destroyAll();
   out.println("<BR><FONT COLOR=RED>The possiblity of this happening is low</FONT>");
   return;
  }
  Vector vecR = dbFill.QueryDatabase(dbQ,out); 
  if(vecR == null)
  {
   out.println("<BR><FONT COLOR=RED>The query does not produce any result.</font>");
   return;
  }
  dbFill.viewContainer(vecR,out);
 }
} // end of ATTSviewBlockSeats' class

