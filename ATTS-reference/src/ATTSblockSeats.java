/**
 * ATTSblockSeats class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSblockSeats extends HttpServlet
{
 private DBFill dbFill;
 private String dbName   = "ATTSairline";
 private PrintWriter out = null;
 private ATTSutility util = null;
 private Closure cls = null;
 
 public void init() throws ServletException
 {
  String driver = "com.mysql.jdbc.Driver";
  String url = "jdbc:mysql://starcloud:3306/";
  String user= "edisonch";
  String pwd = "gblj21mysql03";
  dbFill = new DBFill(driver,url,user,pwd);
  util = new ATTSutility();
 }
 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
 }
 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 }
 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  try
  {
   process(req);
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
  }
 }


 private void process(HttpServletRequest req) throws SQLException
 {
  out.println("<HTML><HEAD><TITLE>ATTS set block seats</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<hr></hr>");

  String blockSeatRKey = req.getParameter("routeKey"); 
  String blockSeatBd   = req.getParameter("BeginDate"); //1900-12-31 = 10
  String blockSeatEd   = req.getParameter("EndDate");   //1900-12-31 = 10
  String blockSeatTot  = req.getParameter("Total");
  String blockSeatRby  = req.getParameter("RequestBy");

  if((blockSeatRKey==null)||(blockSeatBd==null)||(blockSeatEd==null)||
     (blockSeatTot ==null)||(blockSeatRby==null))
  {
   cls.error(1,null); 
   return;
  }
 
  int bsRouteKey = Integer.parseInt(blockSeatRKey);
  if(((blockSeatBd.length()!=10)||(blockSeatEd.length()!=10))||
     ((blockSeatTot.length()<1)||(blockSeatTot.length()>10))||
     ((blockSeatRby.length()<1)||(blockSeatRby.length()>10))||
     (bsRouteKey<1))     
  {
   cls.error(2,null);
   return;
  }
  int bsTotal = Integer.parseInt(blockSeatTot);
  int bsRequest  = Integer.parseInt(blockSeatRby);
  DateUtil du = new DateUtil();
  if((!du.checkSQLDate(blockSeatBd,out))||(!du.checkSQLDate(blockSeatEd,out)))
  {
   cls.error(4,null);
   return;
  }
  else if((!du.checkSQLDateValidity(blockSeatBd,4,out))||
          (!du.checkSQLDateValidity(blockSeatEd,4,out)))
  {
   cls.error(4,null);
   return;
  }

  DBEncapsulation dbR = null;
  DBEncapsulation dbA = null;
  Vector dbVec     = null;
  Vector doneVec   = null;
  StringBuffer tmp = null;

  if(!dbFill.makeInitialConnection(dbName,out))
    throw new SQLException();  
  dbVec = new Vector();

  // Verify that block seats is not set previously
  tmp = new StringBuffer();
  tmp.append("blockSeatsRouteKey="+blockSeatRKey);
  tmp.append(" AND blockSeatsBeginDate="+blockSeatBd);
  tmp.append(" AND blockSeatsEndDate="+blockSeatEd);
  tmp.append(" AND blockSeatsTotal="+blockSeatTot);
  doneVec = dbFill.QuerySpecific("airlineBlockSeats",tmp.toString(),out); 
  if(doneVec!=null)
  {
   out.println("<BR><FONT COLOR=RED><B>Block Seats ini sudah di-set</b></font>");
   out.println("<BR>");
   dbFill.viewContainer(doneVec,out);
   tmp.delete(0,tmp.length());
   if(!doneVec.isEmpty())
     doneVec.removeAllElements();
   return;
  }
  tmp.delete(0,tmp.length());
  tmp.append("routeKey="+bsRouteKey);
  dbR = dbFill.QuerySingleSpecific("airlineRoute",tmp.toString(),out);
  if(dbR == null)
  {
   out.print("<BR><FONT COLOR=RED><B>Airline Route("+bsRouteKey+") tidak terdapat");
   out.println(" didalam database</B></FONT>");
   return;
  }
  int airID = dbR.getColumnNamePosition("routeAirlineID");
  dbA = new DBEncapsulation("airlineBlockSeats");
  dbA.setColumnNameVal("blockSeatsOfAirline",airID);
  dbA.setColumnNameVal("blockSeatsBeginDate",blockSeatBd);
  dbA.setColumnNameVal("blockSeatsEndDate",blockSeatEd);
  dbA.setColumnNameVal("blockSeatsTotal",bsTotal);
  dbA.setColumnNameVal("blockSeatsRouteKey",bsRouteKey);
  dbA.setColumnNameVal("blockSeatsRequestBy",bsRequest);
  dbVec.addElement(dbA);
  dbFill.InsertDatabase(dbVec,out);
  out.println("<BR>Successfully add block seat");

  dbA.destroyAll();
  dbR.destroyAll();
  tmp.delete(0,tmp.length());
  if(!dbVec.isEmpty())
    dbVec.removeAllElements();
  du.clearTime();
 } 

}

