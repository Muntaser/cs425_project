/**
 * ATTSviewRoute class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSsetEmpSalary extends HttpServlet
{
 private DBFill dbFill = null;
 private String dbName = "ATTSemployee";
 private PrintWriter out = null;
 private Closure cls = null;
 private ATTSutility util = null;

/**
 * init's method is to initialize the servlet
 * @param none
 * @return none
 */
 public void init() throws ServletException
 {
  dbFill = new DBFill();
  util = new ATTSutility();
 }

/**
 * destroy's method is to end the life-cycle of the servlet
 * @param none
 * @return none
 */
 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
  dbFill = null;
 }

/**
 * doGet's method is to process GET method from user
 * @param HttpServletRequest req
 *        HttpServletResponse res
 * @return none
 */
 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 }

/**
 * doPost's method is to process POST method from user
 * @param HttpServletRequest req contains user request
 *        HttpServletResponse res contains user respond
 * @return none
 */
 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS Set Employee Salary</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<BR></BR><H1>ATTS Set Employee Salary</H1><HR></HR><BR></BR>"); 
  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException("<BR>Failure to make initial connection</BR>");
   process(req);
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
  }  
 } // end of doPost 

 public void process(HttpServletRequest req) throws SQLException
 {
  int eID = Integer.parseInt(req.getParameter("empID"));
  int aID = Integer.parseInt(req.getParameter("authID"));   
  String eSal = req.getParameter("empSalary");
  String aPK = req.getParameter("ePK");
  String aPW = req.getParameter("ePW");
  if((eSal==null)||(aPK==null)||(aPW==null))
  {
   cls.error(1,null);
   return;
  }
  if((eSal.length()<1)||(aPK.length()<1)||(aPW.length()<1))
  {
   cls.error(0,"Employee Salary/Authorize PassKey/Password harus di-isi");
   return;
  } 
  if((eID<0)||(aID<0))
  {
   cls.error(0,"Employee ID/Authorize ID harus di-isi");
   return;
  }
  if((!util.checkDigit(eSal,false,out))||(!util.checkDigit(aPK,true,out)))
  {
   cls.error(3,"Employee Salary/Authorize PassKey");
   return; 
  }
  if(!util.verifyAuthorization(dbFill,cls,out,aID,aPK,aPW,1,null))
  {
   cls.error(7,null);
   return;
  }
  DBEncapsulation dbI = new DBEncapsulation("employeeSalary");
  dbI.setColumnNameVal("empSalary",Float.parseFloat(eSal));
  dbI.setColumnNameVal("empSalaryDate","CURRENT_DATE");
  dbI.setColumnNameVal("empSalaryAuthByID",aID);
  dbI.setColumnNameVal("empSalaryOfID",eID);
  Vector v = new Vector();
  v.addElement(dbI.clone());
  dbFill.InsertDatabase(v,out);
  out.println("<BR>Successfully insert new salary into database</BR>"); 
  v.removeAllElements();
 } // end of process' method

}// end of ATTSsetEmpSalary

