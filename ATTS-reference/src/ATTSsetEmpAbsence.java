/**
 * ATTSsetEmpAbsence class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSsetEmpAbsence extends HttpServlet
{
 private DBFill dbFill = null;
 private String dbName = "ATTSairline";
 private PrintWriter out = null;
 private Closure cls = null;
 private ATTSutility util = null;

/**
 * init's method is to initialize the servlet
 * @param none
 * @return none
 */
 public void init() throws ServletException
 {
  String driver = "com.mysql.jdbc.Driver";
  String url = "jdbc:mysql://starcloud:3306/";
  String user= "edisonch";
  String pwd = "gblj21mysql03";
  dbFill = new DBFill(driver,url,user,pwd);
  util = new ATTSutility();
 }

/**
 * destroy's method is to end the life-cycle of the servlet
 * @param none
 * @return none
 */
 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
  dbFill = null;
 }

/**
 * doGet's method is to process GET method from user
 * @param HttpServletRequest req
 *        HttpServletResponse res
 * @return none
 */
 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 }

/**
 * doPost's method is to process POST method from user
 * @param HttpServletRequest req contains user request
 *        HttpServletResponse res contains user respond
 * @return none
 */
 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS Set Employee Absence</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<HR></HR>");
 
  String empID = req.getParameter("empID");
  String perBg = req.getParameter("perBeg");
  String perEd = req.getParameter("perEnd");
  int aType = Integer.parseInt(req.getParameter("aType"));
  String reason = req.getParameter("reason");
  String authID = req.getParameter("authID");
  String authPK = req.getParameter("authPK");
 
  if((empID==null)||(perBg==null)||(perEd==null)||(reason==null)||(authID==null)||
     (authPK==null))
  {
   cls.error(1,null);
   return;
  }
  if((empID.length()<1)||(perBg.length()<1)||(perEd.length()<1)||
     (authID.length()<1)||(authPK.length()<1))
  {
   cls.error(2,null);
   return;
  }


  DateUtil du = new DateUtil();
  if((!du.checkSQLDate(perBg,out))||(!du.checkSQLDate(perEd,out)))
  {
   cls.error(3,"Period Begin/Period End");
   return;
  }
  if((!du.checkSQLDateValidity(perBg,4,out))||(!du.checkSQLDateValidity(perEd,4,out)))
  {
   cls.error(4,"Period Begin/Period End");
   return;
  }

  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException("<BR>Failure to make initial connection</BR>");
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out);
  }
  finally
  {

  }  
 } // end of doPost 

}

