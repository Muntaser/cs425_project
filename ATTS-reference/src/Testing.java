/**
 * Testing ATTS
 * @author: Edison Chindrawaly
 */

import java.io.PrintWriter;
import java.io.IOException;
import java.util.Enumeration;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Testing extends HttpServlet
{
 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws IOException, ServletException
 {
  res.setContentType("text/html");
  PrintWriter out = res.getWriter();
  String msg = new String("doGet");
  writeIt(req,out,msg); 
  out.close();
 }

 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws IOException, ServletException
 {
  res.setContentType("text/html");
  PrintWriter out = res.getWriter();
  String msg = new String("doPost");
  writeIt(req,out,msg); 
  out.close();
 }

 public void writeIt(HttpServletRequest req, PrintWriter out, String msg)
 {
  out.println("<HTML><HEAD><TITLE>Enter the " + msg + "</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=BLACK TEXT=WHITE LINK=BLUE VLINK=BLACK ALINK=RED>");
  out.println("<H1><CENTER>" + msg + "</CENTER></H1>");
  out.println("<BR></BR>");
  out.println("The content of the request: ");
  out.println("<TABLE BORDER=\"0\" WIDTH=\"75%\">");
  Enumeration names = req.getHeaderNames();
  while(names.hasMoreElements())
  {
   String name = (String)names.nextElement();
   out.println("<TR>");
   out.println("<TH ALIGN=\"RIGHT\">" + name + ": </TH>");
   out.println("<TD>" + req.getHeader(name) + "</TD>");
   out.println("</TR>"); 
  }
  out.println("<TR><TH ALIGN=\"RIGHT\"> TestField :</TH>");
  out.println("<TD>" + req.getParameter("TestField") + "</TD></TR>");
  out.println("</TABLE>");
  out.println("</BODY></HTML>");
 }
} 
