/**
 * ATTSmanageOffice class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSmanageOffice extends HttpServlet
{
 private DBFill dbFill = null;
 private DBFill dbFill2= null;
 private String dbName = "ATTScompany";
 private String dbName2= "ATTSemployee";
 private PrintWriter out = null;
 private Closure cls = null;
 private ATTSutility util = null;

/**
 * init's method is to initialize the servlet
 * @param none
 * @return none
 */
 public void init() throws ServletException
 {
  dbFill = new DBFill();
  util = new ATTSutility();
 }

/**
 * destroy's method is to end the life-cycle of the servlet
 * @param none
 * @return none
 */
 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
 }

/**
 * doGet's method is to process GET method from user
 * @param HttpServletRequest req
 *        HttpServletResponse res
 * @return none
 */
 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 }

/**
 * doPost's method is to process POST method from user
 * @param HttpServletRequest req contains user request
 *        HttpServletResponse res contains user respond
 * @return none
 */
 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS Manage Office</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<BR></BR><H1>ATTS Manage Office</H1><HR></HR><BR></BR>"); 
  String view = req.getParameter("view");
  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
    throw new SQLException("<BR>Failure to make initial connection to :"+dbName+"</BR>");
   if(!dbFill2.makeInitialConnection(dbName2,out))
    throw new SQLException("<BR>Failure to make initial connection to :"+dbName2+"</BR>");
   if(view.compareToIgnoreCase("LOG")==0)
    processLog(req);
   if(view.compareToIgnoreCase("PRESENT")==0)
    processPresent(req);
   if(view.compareToIgnoreCase("PROCESS")==0)
    process(req);
   else
    cls.error(6,null);
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
  }  
 } // end of doPost 

 public void processLog(HttpServletRequest req) throws SQLException
 {
  String eID = req.getParameter("eID");
  String ePK = req.getParameter("ePK");
  String ePW = req.getParameter("ePW");
  if((eID==null)||(ePK==null)||(ePW==null))
  {
   cls.error(1,null);
   return;
  }
  if((eID.length()<1)||(ePK.length()<1)||(ePW.length()<1))
  {
   cls.error(2,null);
   return;
  }
  boolean errorFlag = false;
  if(!util.checkDigit(eID,true,out))
  {
   cls.error(3,"ID Pegawai");
   errorFlag = true;
  }
  if(!util.checkDigit(ePK,true,out))
  {
   cls.error(3,"PassKey Pegawai");
   errorFlag = true;
  }
  if(errorFlag)
   return;
  if(!util.verifyAuthorization(dbFill2,cls,out,Integer.parseInt(eID),ePK,ePW,2,null))
  {
   cls.error(0,"Hak Access anda tidak cukup atau passkey/password anda tidak benar");
   return;
  }
  createForm(Integer.parseInt(eID));
 }

 private void createForm(int eid) throws SQLException
 {
  Vector v = dbFill.QuerySpecific("companyOffice",null,out);
  if(v==null)
  {
   cls.error(0,"Informasi Office belum ada didalam database");
   return;
  }
  DBEncapsulation o = null; 
  DBEncapsulation c = null;
  int p1   = 0;
  int size = v.size();
  util.createBegin("ATTSmanageOffice","view","PRESENT",true,out);
  out.println("<INPUT TYPE=HIDDEN NAME=\"eID\" VALUE=\""+eid+"\">");
  StringBuffer str = new StringBuffer();
  for(int i=0;i<size;i++)
  {
   o = (DBEncapsulation)((DBEncapsulation)v.get(i)).clone();
   p1= o.getColumnNamePosition("officeID");
   if(p1<0)
    throw new SQLException("<BR>Office ID not found in the container</BR>");
   out.println("<TR><TD><INPUT TYPE=RADIO NAME=\"OID\" VALUE=\"");
   out.println(o.getColumnStringValues(p1)+"\"></TD><TD>");
   p1 = o.getColumnNamePosition("officeOfCompanyID");
   str.append("WHERE companyID=\""+o.getColumnStringValues(p1)+"\";");
   c = dbFill.QuerySingleSpecific("company",str.toString(),out);
   if(c == null)
    throw new SQLException("<BR>Company ID in the Office Query not found!</BR>");
   p1 = c.getColumnNamePosition("companyName");
   out.println(c.getColumnStringValue(p1)+"</TD>");
   out.println("<TD>Perusahaan masih berdiri</TD><TD>");
   p1 = c.getColumnNamePosition("companyValid");
   out.println(c.getColumnStringValues(p1)+"</TD>");
   out.println("<TD>Alamat</TD><TD>");
   out.println(o.getColumnStringValue(o.getColumnNamePosition("officeAddr"))+"</TD>");
   out.println("<TD>Kota</TD><TD>");
   out.println(o.getColumnStringValue(o.getColumnNamePosition("officeCity"))+"</TD>");
   out.println("<TD>Negara</TD><TD>");
   out.println(o.getColumnStringValue(o.getColumnNamePosition("officeCountry"))+"</TD>");
   out.println("<TD>Status</TD><TD>");
   out.println(o.getColumnStringValue(o.getColumnNamePosition("officeStatus"))+"</TD>");
   out.println("<TD>Active</TD><TD>");
   out.println(o.getColumnStringValue(o.getColumnNamePosition("officeActive"))+"</TD>"); 
   out.println("</TR>"); 
   str.delete(0,str.length());
   o.destroyAll();
   c.destroyAll();
  }
  util.createEnd(out);
 }

 public void processPresent(HttpServletRequest req) throws SQLException
 {
  int eid = Integer.parseInt(req.getParameter("eID"));
  int oid = Integer.parseInt(req.getParameter("OID"));
  StringBuffer str = new StringBuffer();
  str.append("WHERE officeID=\""+oid+"\";");
  DBEncapsulation o = dbFill.QuerySingleSpecific("companyOffice",str.toString(),out);
  int p1 = o.getColumnNamePosition("officeOfCompanyID");
  str.delete(0,str.length());
  str.append("WHERE companyID=\""+o.getColumnStringValues(p1)+"\";");
  DBEncapsulation c = dbFill.QuerySingleSpecific("companyOffice",str.toString(),out);

  util.createBegin("ATTSmanageOffice","view","PROCESS",true,out);
  out.println("<INPUT TYPE=HIDDEN NAME=\"eID\" VALUE=\""+eid+"\">");
  out.println("<INPUT TYPE=HIDDEN NAME=\"oID\" VALUE=\""+oid+"\">");
  out.println("<TR><TD>Nama Perusahaan</TD><TD>");
  out.println(c.getColumnStringValue(c.getColumnNamePosition("companyName"))+"</TD>");
  out.println("<TD></TD></TR>");
  out.println("<TR><TD>Status Perusahaan</TD><TD>");
  out.println(c.getColumnStringValues(c.getColumnNamePosition("companyValid"))+"</TD>");
  out.println("<TD></TD></TR>");

  out.println("<TR><TD>Office ID</TD><TD>"+oid+"</TD><TD></TD></TR>");
  out.println("<TR><TD>Alamat Office</TD><TD>");
  out.println(o.getColumnStringValue(o.getColumnNamePosition("officeAddr"))+"</TD><TD>");
  out.println("<INPUT TYPE=TEXT NAME=\"oAddr\" SIZE=40 MAXLENGTH=32></TD></TR>");
  out.println("<TR><TD>Kota</TD><TD>");
  out.println(o.getColumnStringValue(o.getColumnNamePosition("officeCity"))+"</TD><TD>");
  out.println("<INPUT TYPE=TEXT NAME=\"oCity\" SIZE=40 MAXLENGTH=16></TD></TR>");
  out.println("<TR><TD>Negara</TD><TD>");
  out.println(o.getColumnStringValue(o.getColumnNamePosition("officeCountry"))+"</TD><TD>");
  out.println("<INPUT TYPE=TEXT NAME=\"oCountry\" SIZE=40 MAXLENGTH=16></TD></TR>");
  out.println("<TR><TD>Office Status</TD><TD>");
  out.println(o.getColumnStringValues(o.getColumnNamePosition("officeStatus"))+"</TD><TD>");
  out.println("<SELECT NAME=\"oStatus\"><OPTION SELECTED VALUE=\"NON\"> -- Rubah/Tetap ");
  out.println("-- </OPTION><OPTION VALUE=\"HEADQUARTER\">HEADQUARTER</OPTION>");
  out.println("<OPTION VALUE=\"BRANCH\">BRANCH</OPTION></SELECT></TD></TR>");
  out.println("<TR><TD>Office Active</TD><TD>");
  out.println(o.getColumnStringValues(o.getColumnNamePosition("officeActive"))+"</TD><TD>");
  out.println("<SELECT NAME=\"oActive\"><OPTION SELECTED VALUE=\"NON\"> -- Rubah/Tetap ");
  out.println("--</OPTION><OPTION VALUE=\"Y\">YES</OPTION><OPTION VALUE=\"N\">NO");
  out.println("</OPTION></SELECT></TD></TR>");
  out.println("<TR><TD>Jam Buka Office</TD><TD>");
  out.println(o.getColumnStringValues(o.getColumnNamePosition("officeRegHourOpen")));
  out.println("</TD><TD><INPUT TYPE=TEXT NAME=\"oHO\" SIZE=20 MAXLENGTH=8></TD></TR>");
  out.println("<TR><TD>Jam Tutup Office</TD><TD>");
  out.println(o.getColumnStringValues(o.getColumnNamePosition("officeRegHourClose")));
  out.println("</TD><TD><INPUT TYPE=TEXT NAME=\"oHC\" SIZE=20 MAXLENGTH=8></TD></TR>");
  out.println("<TR><TD>Hari Kerja Office</TD><TD>");
  out.println(o.getColumnStringValues(o.getColumnNamePosition("officeRegWorkDay")));
  out.println("</TD><TD><INPUT TYPE=TEXT NAME=\"oWD\" SIZE=20 MAXLENGTH=7></TD></TR>");
  out.println("<TR><TD>Office Diresmikan</TD><TD>");
  out.println(o.getColumnStringValues(o.getColumnNamePosition("officeFounded"))+"</TD>");
  out.println("<TD></TD></TR>");
  
  o.destroyAll();
  c.destroyAll();
  str.delete(0,str.length());
  str.append("WHERE officeContactOf=\""+oid+"\";");
  Vector v = dbFill.QuerySpecific("companyOfficeContact",str.toString(),out);
  if(v != null)
  {
   int size = v.size();
   out.println("<INPUT TYPE=HIDDEN NAME=\"cSize\" VALUE=\""+size+"\">");
   for(int i=0;i<size;i++)
   {
    o = (DBEncapsulation)((DBEncapsulation)v.get(i)).clone();
    out.println("<INPUT TYPE=HIDDEN NAME=\"contactID\" VALUE=\"");
    out.println(o.getColumnStringValues(o.getColumnNamePosition("officeContactID")));
    out.println("\">");
    out.println("<TR><TD>Office Phone</TD><TD>");
    o.getColumnStringValue(o.getColumnNamePosition("officeContactPhone")); 
    out.println("</TD><TD><INPUT TYPE=TEXT NAME=\"oPhone"+i+"\" SIZE=20 MAXLENGTH=14>");
    out.println("</TD></TR>");
    out.println("<TR><TD>Office Fax</TD><TD>");
    o.getColumnStringValue(o.getColumnNamePosition("officeContactPhone")); 
    out.println("</TD><TD><INPUT TYPE=TEXT NAME=\"oFax"+i+"\" SIZE=20 MAXLENGTH=14>");
    out.println("</TD></TR>");
    o.destroyAll();
   }
  }
  else
  {
   out.println("<TR><TD>Office Phone</TD><TD>Belum ada</TD><TD></TD></TR>");
   out.println("<TR><TD>Office Fax</TD><TD>Belum ada</TD><TD></TD></TR>");
  }

  out.println("<TR><TD>Employee PassKey</TD><TD>");
  out.println("<INPUT TYPE=PASSWORD NAME=\"ePK\" SIZE=20 MAXLENGTH=6></TD><TD></TD></TR>");
  out.println("<TR><TD>Employee Password</TD><TD>");
  out.println("<INPUT TYPE=PASSWORD NAME=\"ePW\" SIZE=20 MAXLENGTH=32></TD><TD></TD></TR>");
  util.createEnd(out);
 }

 public void process(HttpServletRequest req) throws SQLException
 {
  int eid = Integer.parseInt(req.getParameter("eID"));
  int oid = Integer.parseInt(req.getParameter("oID"));
  int cSize = Integer.parseInt(req.getParameter("cSize"));
  String ePK = req.getParameter("ePK");
  String ePW = req.getParameter("ePW");
  String oAddr = req.getParameter("oAddr");
  String oCity = req.getParameter("oCity");
  String oCountry = req.getParameter("oCompany");
  String oStatus  = req.getParameter("oStatus");
  String oActive  = req.getParameter("oActive");
  String oHO = req.getParameter("oHO");
  String oHC = req.getParameter("oHC");
  String oWD = req.getParameter("oWD");
  Vector c = new Vector();
  Vector c2= new Vector();
  StringBuffer str = new StringBuffer();
  StringBuffer str2= new StringBuffer();
  boolean errorFlag = false;

  if((ePK==null)||(ePW==null)||(oAddr==null)||(oCity==null)||(oCountry==null)||
    (oStatus==null)||(oActive==null)||(oHO==null)||(oHC==null)||(oWD==null))
  {
   cls.error(1,null);
   return;
  }
  // to get Office Contact
  for(int i=0;i<cSize;i++)
  {
   str.append("oPhone"+i);
   str2.append(req.getParameter(str.toString()));
   if((str2.length()>=11)&&(str2.length()<=13))
   {
    if(util.checkDigit(str2.toString(),false,out))
      c.addElement(new Replacer(req.getParameter("contactID"),str2.toString()));
    else
    {
     errorFlag = true;
     cls.error(3,"Nomor telephone harus paling tidak 11 sampai 13 digit panjangnya");
    }
   }
   else
   {
    if(str2.length()>1)
    {
     errorFlag = true;
     cls.error(3,"Nomor telephone harus paling tidak 11 sampai 13 digit panjangnya");  
    }
   }
   str.delete(0,str.length());
   str2.delete(0,str2.length());

   str.append("oFax"+i);
   str2.append(req.getParameter(str.toString()));
   if((str2.length()>=11)&&(str2.length()<=13))
   {
    if(util.checkDigit(str2.toString(),false,out))
      c2.addElement(new Replacer(req.getParameter("contactID"),str2.toString()));
    else
    {
     errorFlag = true;
     cls.error(3,"Nomor Fax harus paling tidak 11 sampai 13 digit panjangnya");
    }
   }
   else
   {
    if(str2.length()>1)
    {
     errorFlag = true;
     cls.error(3,"Nomor Fax harus paling tidak 11 sampai 13 digit panjangnya");
    }
   }
   str.delete(0,str.length());
   str2.delete(0,str2.length());
  } // end of for-loop


  if((ePK.length()<2)&&(ePW.length()<2)&&(oAddr.length()<2)&&(oCity.length()<2)&&
    (oCountry.length()<2)&&(oStatus.compareToIgnoreCase("NON")==0)&&
    (oActive.compareToIgnoreCase("NON")==0)&&(oHO.length()<2)&&(oHC.length()<2)&&
    (oWD.length()<2)&&(c.size()==0)&&(c2.size()==0))
  {
   cls.error(2,null);
   return;
  }
  if(!util.checkInteger(oWD,1,7))
  {
   cls.error(0,"Hari Kerja hanya mulai dari hari ke 1 sampai hari ke 7");
   errorFlag = false;
  }
  if((ePK.length()<2)||(ePW.length()<2))
  {
   errorFlag = true;
   cls.error(0,"Passkey dan password harus di-isi");
  }
  if(!util.checkDigit(ePK,true,out))
  {
   errorFlag = true;
   cls.error(3,"PassKey harus dalam bentuk angka/digit");
  }
  if((!util.checkDigit(oHO,true,out))||(!util.checkDigit(oHC,true,out)))
  {
   errorFlag = true;
   cls.error(0,"Jam Buka dan Jam Tutup musti dalam bentuk angka/digit");
  }
  if((!util.checkTime(oHO,out))||(!util.checkTime(oHC,out)))
  {
   errorFlag = true;
   cls.error(3,"Jam Buka/Jam Tutup");
  }
  
  if(errorFlag)
   return;
  if(!util.verifyAuthorization(dbFill,cls,out,eid,ePK,ePW,2,null))
  {
   cls.error(0,"Passkey/Password anda salah atau access level anda tidak cukup");
   return;
  }
  Vector v = new Vector();
  DBEncapsulation dbE = new DBEncapsulation("companyOffice");
  str.append("WHERE officeID=\""+oid+"\";");
  dbE.setCondition(str.toString());
  if(oAddr.length()>0)
   dbE.setColumnNameVal("officeAddr",oAddr);
  if(oCity.length()>0)
   dbE.setColumnNameVal("officeCity",oCity);
  if(oCountry.length()>0)
   dbE.setColumnNameVal("officeCountry",oCountry);
  if(oActive.compareToIgnoreCase("NON")!=0)
   dbE.setColumnNameVal("officeActive",oActive);
  if(oStatus.compareToIgnoreCase("NON")!=0)
   dbE.setColumnNameVal("officeStatus",oStatus);
  if(oWD.length()>0)
   dbE.setColumnNameVal("officeRegWorkDay",oWD);
  if(oHO.length()>0)
   dbE.setColumnNameVal("officeRegHourOpen",oHO);
  if(oHC.length()>0)
   dbE.setColumnNameVal("officeRegHourClose",oHC);
  dbE.setColumnNameVal("officeLastChange","CURRENT_DATE");
  dbE.setColumnNameVal("officeLastChangeBy",eid);
  v.addElement(dbE.clone());
  dbE.clearAll();
  
  if(c.size()!=0)
  {
   Replacer r = null;
   while(c.size()>0)
   {
    r = (Replacer)c.remove(0);
    dbE.setDBTableName("companyOfficeContact");
    dbE.setColumnNameVal("officeContactPhone",r.getReplacer());
    str.append("WHERE officeID=\""+r.getOriginal()+"\";");
    dbE.setCondition(str.toString());
    v.addElement(dbE.clone());
    r.reset();
    dbE.clearAll();
    str.delete(0,str.length());
   }
  }
  if(c2.size()!=0)
  {
   Replacer r = null;
   while(c2.size()>0)
   {
    r = (Replacer)c.remove(0);
    dbE.setDBTableName("companyOfficeContact");
    dbE.setColumnNameVal("officeContactFax",r.getReplacer());
    str.append("WHERE officeID=\""+r.getOriginal()+"\";");
    dbE.setCondition(str.toString());
    v.addElement(dbE.clone());
    r.reset();
    dbE.clearAll();
    str.delete(0,str.length());
   }
  }
  dbFill.UpdateDatabase(v,out);
  if(!v.isEmpty())
   v.removeAllElements();
  out.println("<BR></BR><H2>Success Managing Office</H2><BR></BR>");
 } // end of process' method


} // end of ATTSmanageOffice's class

