/**
 * ATTSviewRoute class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSviewEmpLocation extends HttpServlet
{
 private DBFill dbFill = null;
 private String dbName = "ATTSairline";
 private PrintWriter out = null;
 private Closure cls = null;
 private ATTSutility util = null;

 public void init() throws ServletException
 {
  dbFill = new DBFill();
  util = new ATTSutility();
 } // end of init

 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
  dbFill = null;
 } // end of destroy

 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 } // end of doGet

 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS View Employee Location</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  String view = req.getParameter("view");
  if(view == null)
  {
   cls.error(1,"view");
   cls.closing();
   out.close();
   return;
  }
  
  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException("<BR>Fail to make initial connection</BR>");
   if(view.compareToIgnoreCase("ALL")==0)
     createView();
   else if(view.compareToIgnoreCase("PROCESS_VIEW")==0)
     processForm(req);
   else if(view.compareToIgnoreCase("PROCESS")==0)
     processAll(req); 
   else
    cls.error(6,null);
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out); 
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
   view = null;
   out = null;
   cls = null;
  }

 } // end of doPost 

/**
 * createView's method is to create form where user can see the current location of
 * employee and let the user change the status - check-in back to the office.
 * @param none
 * @return none
 */
 private void createView() throws SQLException
 {
  StringBuffer msg = new StringBuffer();
  msg.append("WHERE empLocBack=\"00:00:00\" ORDER BY empLocGo;");
  Vector v = dbFill.QuerySpecific("employeeLocation",msg.toString(),out); 
  if(v == null)
  {
   cls.error(0,"Semua Messenger ada di kantor");
   return; 
  }
  DBEncapsulation r = null;
  int size=v.size();
  int p1 = 0;
  int p2 = 0;
  msg.delete(0,msg.length());
  for(int i=0;i<size;i++)
  {
   r  = new DBEncapsulation((DBEncapsulation)v.get(i));
   p1 = r.getColumnNamePosition("employeeLocationKey");
   p2 = r.getColumnNamePosition("empLocOfID");
   msg.append("<TR><TD>");
   msg.append("<INPUT TYPE=RADIO NAME=\"eLocKey\" ");
   msg.append("VALUE=\""+r.getColumnStringValues(p1)+"\">");
   msg.append("</TD><TD>Employee ID: "+r.getColumnStringValues(p2)+"</OPTION></TD><TD>");
   p1 = r.getColumnNamePosition("empDeliverOrder");
   p2 = r.getColumnNamePosition("empLocationAdd");
   if(r.getColumnIntegerValue(p1)==0)
   {
    msg.append("Deliver Order:</TD><TD>"+r.getColumnIntegerValue(p1)+"</TD>");   
    if(r.getColumnStringValue(p2).length()<1)
      msg.append("<TD></TD><TD></TD>");
    else
      msg.append("<TD>At Location :</TD><TD>"+r.getColumnStringValue(p2)+"</TD>");
   }
   else
   {
    msg.append("At Location:</TD><TD>"+r.getColumnStringValues(p2)+"</TD>");
    p1 = r.getColumnNamePosition("empLocNote");
    if(p1<1)
      msg.append("<TD></TD><TD></TD>");
    else
      msg.append("<TD>For :</TD><TD>"+getEmpLocNote(p1)+"</TD>");
   }
   msg.append("</TR>");
   r.destroyAll();
  } // end of for-loop
  out.println("<BR><B><FONT SIZE=+1>Current Employee Location:</FONT></B></BR>");
  util.createBegin("ATTSviewEmpLocation","view","PROCESS",true,out);
  out.println(msg.toString());
  out.println("<TR><TD>Employee PassKey</TD><TD><INPUT TYPE=PASSWORD NAME=\"ePK\"");
  out.println(" SIZE=20 MAXLENGTH=6></TD><TD></TD><TD></TD><TD></TD><TD></TD>");
  util.createEnd(out); 
 } // end of createView's method

/**
 * getEmpLocNote's method is to retrieve the employee note given the position of note.
 * @param int position
 * @return String of the note if found else return String value [Kosong]
 */
 private String getEmpLocNote(int pos) throws SQLException
 {
  if(pos<1)
    return new String("Kosong");
  StringBuffer tmp = new StringBuffer("WHERE employeeNotesKey=\""+pos+"\";");
  DBEncapsulation r2 = dbFill.QuerySingleSpecific("employeeNotes",tmp.toString(),out);
  if(r2 == null)
    return new String("Kosong"); 
  int p1 = r2.getColumnNamePosition("empNotes");
  if(p1<0)
    return new String("Kosong");
  return r2.getColumnStringValue(p1); 
 }

/**
 * processForm's method is to process user request to view all the employee location
 * within the certain period of time [date].
 * @param HttpServletRequest req
 * @return none
 */
 private void processForm(HttpServletRequest req) throws SQLException
 {
  String id = req.getParameter("empID");
  String pb = req.getParameter("perBeg");
  String pe = req.getParameter("perEnd");
  if((id==null)||(pb==null)||(pe==null))
  {
   cls.error(1,null);
   return;
  }
  if((id.length()<1)||(pb.length()<1)||(pe.length()<1))
  {
   cls.error(2,null);
   return;
  }
  if(!util.checkDigit(id,true,out))
  {
   cls.error(3,"Employee ID");
   return;
  }
  DateUtil du = new DateUtil();
  if((!du.checkSQLDate(pb,out))||(!du.checkSQLDate(pe,out)))
  {
   cls.error(4,null);
   return;
  }
  if((!du.checkSQLDateValidity(pb,4,out))||(!du.checkSQLDateValidity(pe,4,out)))
  {
   cls.error(4,null);
   return;
  }
  if(!du.checkEarly(pb,pe))
  {
   cls.error(0,"Period Begin is earlier than Period End");
   return;
  } 
  int eid = Integer.parseInt(id);
  if(eid<0)
  {
   cls.error(3,"Employee ID");
   return;
  }
  StringBuffer cond = new StringBuffer("WHERE empLocDate>=\""+pb+"\" AND ");
  cond.append("empLocDate<=\""+pe+"\"");
  if(eid == 0)
    cond.append(";");
  else
    cond.append(" empLocOfID=\""+eid+"\";");
  Vector v = dbFill.QuerySpecific("employeeLocation",cond.toString(),out);
  if(v == null)
  {
   cls.error(5,"Employee Location");
   return;
  }
  dbFill.viewContainer(v,out);
  v.removeAllElements();
  cond.delete(0,cond.length());
 } // end of processForm's method

/**
 * processAll's method is to process user request from createView's method above.
 * It clocks employee back into the system to update the employee location.
 * @param  none
 * @return none
 */
 private void processAll(HttpServletRequest req) throws SQLException
 {
  int eLocKey = Integer.parseInt(req.getParameter("eLocKey")); 
  String ePK = req.getParameter("ePK");
  if(ePK == null)
  {
   cls.error(1,null);
   return;
  }
  if(ePK.length()<1)
  {
   cls.error(2,null);
   return;
  }
  if(!util.checkDigit(ePK,true,out))
  {
   cls.error(0,"Employee PassKey harus dalam bentuk digit/angka");
   return;
  }
  StringBuffer cond = new StringBuffer("WHERE empLocationKey=\""+eLocKey+"\";");
  DBEncapsulation dbI = dbFill.QuerySingleSpecific("employeeLocation",cond.toString(),out); 
  if(dbI == null)
  {
   cls.error(0,"Data of Employee Location Key "+eLocKey+" not found");
   return;
  }
  int p1 = dbI.getColumnNamePosition("empLocOfID");
  if(p1<0)
  {
   cls.error(0,"Employee ID not found");
   return;
  }
  int eid = dbI.getColumnIntegerValue(p1);
  if(!util.verifyAuthorization(dbFill,cls,out,eid,ePK,null,3,null))
  {
   cls.error(0,"Employee PassKey salah");
   return;
  }
  dbI.clearAll();
  dbI.setDBTableName("employeeLocation");
  dbI.setColumnNameVal("empLocBack","CURRENT_TIME");
  dbI.setCondition(cond.toString());
  Vector v = new Vector();
  v.addElement(dbI.clone());
  dbI.destroyAll();
  dbFill.UpdateDatabase(v,out);
  v.removeAllElements();
  out.println("<BR>You have successfully check back into the system.</BR>");
 } // end of processAll's method

} // end of ATTSviewAirlineAccess' class

