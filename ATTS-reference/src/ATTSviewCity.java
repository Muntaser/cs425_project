/**
 * ATTSviewCity class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSviewCity extends HttpServlet
{
 private DBFill dbFill;
 private String dbName = "ATTSairline";
 private PrintWriter out = null;
 private Closure cls = null;
 private ATTSutility util = null;

 public void init() throws ServletException
 {
  String driver = "com.mysql.jdbc.Driver";
  String url = "jdbc:mysql://starcloud:3306/";
  String user= "edisonch";
  String pwd = "gblj21mysql03";
  dbFill = new DBFill(driver,url,user,pwd);
  util = new ATTSutility();
 }
 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
 }
 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS View City</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  String view = req.getParameter("view");
  if((view == null)||(view.length()<1)||(view.compareToIgnoreCase("ALL")!=0))
  {
   out.println("doGet parameter is either null or donot have the correct parameters");
   cls.makeClosing();
   cls.closing();
   out.close();
   out = null;
   return;
  }
  Vector vecResult = null;
  DBEncapsulation dbE = new DBEncapsulation("airlineCity");
  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
   {
    out.println("failure in making connection to db");
    cls.makeClosing();
    cls.closing();
    out.close();
    out = null;
    return;
   }
   if(view.compareToIgnoreCase("ALL")==0)
     dbE.setColumnName("*");
   vecResult=dbFill.QueryDatabase(dbE,out);
   if((vecResult == null)||(vecResult.size()<1)) 
   {
    out.println("airlineCity table is still empty");
    cls.makeClosing();
    cls.closing();
    out.close();
    out = null;
    return;
   }
   dbFill.viewContainer(vecResult,out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
   out = null;
   dbE.destroyAll();
   dbE = null;
   if((!vecResult.isEmpty())&&(vecResult!=null))
   {
     vecResult.removeAllElements(); 
     vecResult = null;
   }
  } 
 } 

}

