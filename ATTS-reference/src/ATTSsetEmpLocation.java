/**
 * ATTSsetEmpLocation class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSsetEmpLocation extends HttpServlet
{
 private DBFill dbFill = null;
 private String dbName = "ATTSemployee";
 private PrintWriter out = null;
 private Closure cls = null;
 private ATTSutility util = null;

/**
 * init's method is to initialize the servlet
 * @param none
 * @return none
 */
 public void init() throws ServletException
 {
  dbFill = new DBFill();
  util = new ATTSutility();
 }

/**
 * destroy's method is to end the life-cycle of the servlet
 * @param none
 * @return none
 */
 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
  dbFill = null;
 }

/**
 * doGet's method is to process GET method from user
 * @param HttpServletRequest req
 *        HttpServletResponse res
 * @return none
 */
 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 }

/**
 * doPost's method is to process POST method from user
 * @param HttpServletRequest req contains user request
 *        HttpServletResponse res contains user respond
 * @return none
 */
 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS Set Employee Location</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<BR></BR><H1>ATTS Set Employee Location</H1><HR></HR><BR></BR>"); 

  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException("<BR>Failure to make initial connection</BR>");
   process(req);
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
  }  
 } // end of doPost 

 private void process(HttpServletRequest req) throws SQLException
 {
  String eID = req.getParameter("eID");
  String ePK = req.getParameter("ePK");
  String eDO = req.getParameter("eDO");
  String eDL = req.getParameter("eDL");
  String eNT = req.getParameter("eNT");
  if((eID==null)||(ePK==null)||(eDO==null)||(eDL==null)||(eNT==null))
  {
   cls.error(1,null);
   return;
  }
  if((eID.length()<1)||(ePK.length()<1))
  {
   cls.error(1,null);
   return;
  } 
  StringBuffer msg = new StringBuffer();
  if((eDO.length()<1)&&(eDL.length()<1))
  {
   msg.append("Employee Deliver Order dan Employee Deliver Location tidak boleh");
   msg.append(" kosong dua-duanya. Harus di-isi dua-duanya atau isi salah satu");
   cls.error(0,msg.toString());
   return;
  }
  else if((eDO.length()<1)&&(eDL.length()>1)&&(eNT.length()<1))
  {
   msg.append("Kalau Employee Deliver Location yang hanya di-isi, Employee");
   msg.append(" Location Note harus di-isi juga");
   cls.error(0,msg.toString());
   return;
  }
  if((!util.checkDigit(eID,true,out))||(!util.checkDigit(ePK,true,out))||
     (!util.checkDigit(eDO,true,out)))
  {
   cls.error(3,"Employee ID/Employee Deliver Order Number/Employee Pass Key");
   return;
  }
  if(!util.verifyAuthorization(dbFill,cls,out,Integer.parseInt(eID),ePK,null,3,null))
  {
   cls.error(0,"Employee ID dan Employee PassKey tidak cocok");
   return;
  }
   
  DBEncapsulation dbI = new DBEncapsulation();
  Vector v = new Vector();
  if(eNT.length()>1)
  {
   dbI.setDBTableName("employeeNotes");
   dbI.setColumnNameVal("empNotes",eNT);
   v.addElement(dbI.clone());
   dbI.clearAll();
  }
  dbI.setDBTableName("employeeLocation");
  dbI.setColumnNameVal("empLocGo","CURRENT_TIME");
  dbI.setColumnNameVal("empLocDate","CURRENT_DATE");
  dbI.setColumnNameVal("empLocOfID",Integer.parseInt(eID));
  if(eDO.length()>0)
    dbI.setColumnNameVal("empDeliverOrder",eDO);
  if(eDL.length()>0)
    dbI.setColumnNameVal("empLocationAdd",eDL);
  if(eNT.length()>0)
    dbI.setColumnNameVal("empLocNote","@empNoteKey");
  v.addElement(dbI.clone());
  dbI.destroyAll();
  dbFill.InsertDatabase(v,out);
  v.removeAllElements();
  out.println("<BR>Successfully insert new employee location to database</BR>");
 }

} // end of ATTSsetEmpLocation

