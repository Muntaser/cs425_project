package ATTS;

import java.io.PrintWriter;

public interface DateUtility
{
 final static int SUNDAY = 1;
 final static int MONDAY = 2;
 final static int TUESDAY= 3;
 final static int WEDNESDAY= 4;
 final static int THURSDAY= 5;
 final static int FRIDAY= 6; 
 final static int SATURDAY= 7;
 final static int[] thirtyOneDays= {1,3,5,7,8,10,12};
 final static int[] thirtyDays= {4,6,9,11};

 public int getMonth();
 public int getYear();
 public int getDate();
 public int getDay();
 public int getAMPM();
 public int getHour();
 public int getMinute();
 public int getSecond();
 public int getWOM();
 public int getWOY();
 public int getDOY();
 public int getMaxDate();
 public int getDiffYear();
 public int getDiffMonth();
 public int getDiffDOY();
 public int getFlagDiffMonth();
 public int getMondayDOW();
 public int calculateAnyDOW(int qDay);
// private int checkLess(int y1,int m1,int d1);
// private int checkLessStrict(int y1,int m1,int d1);
// private int checkMore(int y1,int m1,int d1);
 public boolean checkSQLDateValidity(String dt,int strict,PrintWriter out);
 public String getCurrentTime();
 public String formTime();
 public String formSQLDate();
 public String formIntlDate();
 public String formUSDate();
 public String formDay();
 public String formMonth();
 public String convertDay(int indication);
 public String convertMonth(int indication);
 public String convertAM_PM(int indication);
 public boolean setTime(int option, int number); 
 public boolean rollTime(int option, int number);
 public boolean addTime(int option, int number);
 public boolean part31Days(int mon);
 public boolean part30Days(int mon);
 public boolean checkDigit(String dt,PrintWriter out);
 public boolean checkSQLDate(String dt,PrintWriter out);
 public void resetAll();
 public void clearTime();
}
