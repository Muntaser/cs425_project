/**
 * ATTSviewEmployeeAbsence class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSviewEmpAbsence extends HttpServlet
{
 private DBFill dbFill = null;
 private String dbName = "ATTSairline";
 private PrintWriter out = null;
 private Closure cls = null;
 private ATTSutility util = null;

 public void init() throws ServletException
 {
  String driver = "com.mysql.jdbc.Driver";
  String url = "jdbc:mysql://starcloud:3306/";
  String user= "edisonch";
  String pwd = "gblj21mysql03";
  dbFill = new DBFill(driver,url,user,pwd);
  util = new ATTSutility();
 } // end of init

 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
  dbFill = null;
 } // end of destroy

 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 } // end of doGet

 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS View Route</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  String view = req.getParameter("view");
  if(view == null)
  {
   cls.error(1,"view");
   cls.closing();
   out.close();
   return;
  }
  
  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException("<BR>Fail to make initial connection</BR>");
   if(view.compareToIgnoreCase("ALL")==0)
     createView();
   else if(view.compareToIgnoreCase("SET")==0)
     createForm(req);
   else if(view.compareToIgnoreCase("PROCESS")==0)
     processAll(req); 
   else
    cls.error(6,null);
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out); 
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
   view = null;
   out = null;
   cls = null;
  }

 } // end of doPost 

 public void createView() throws SQLException
 {

 } // end of createView's method

 public void createForm(HttpServletRequest req) throws SQLException
 {
  String perBeg = req.getParameter("perBeg");
  String perEnd = req.getParameter("perEnd");
  if((perBeg==null)||(perEnd==null))
  {
   cls.error(1,"Period Begin/Period End");
   return;
  }
  DateUtil du = new DateUtil();
  if((!du.checkSQLDate(perBeg,out))||(!du.checkSQLDate(perEnd,out)))
  {
   cls.error(4,"Period Begin/Period End");
   return;
  }
  if((!du.checkSQLDateValidity(perBeg,4,out))||(!du.checkSQLDateValidity(perEnd,4,out)))
  {
   cls.error(4,"Period Begin/Period End");
   return;
  }
  StringBuffer msg = new StringBuffer();
  msg.append("WHERE empAbsenceBegin>="+perBeg+" AND empAbsenceEnd<="+perEnd);
  msg.append(" ORDER BY empAbsenceOfID");
  Vector v = dbFill.QuerySpecific("employeeAbsence",msg.toString(),out);
  if(v == null)
  {
   cls.error(0,"Belum tersedia data absence untuk period yg diminta");
   return;
  }
  DBEncapsulation e = null;
  int p1 = 0;
  int p2 = 0;
  int p3 = 0;
  int p4 = 0;
  int size = v.size();
  util.createBegin("ATTSviewEmpAbsence","view","PROCESS",true,out);
  msg.delete(0,msg.length());
  for(int i=0;i<size;i++)
  {
   e=(DBEncapsulation)((DBEncapsulation)v.get(i)).clone();
   p1=e.getColumnNamePosition("empAbsenceKey");
   p2=e.getColumnNamePosition("empAbsenceOfID");
   p3=e.getColumnNamePosition("empAbsenceBegin");
   p4=e.getColumnNamePosition("empAbsenceEnd");
   msg.append("<TR><TD>");
   msg.append("<INPUT TYPE=RADIO NAME=id VALUE=\""+e.getColumnStringValues(p1));
   msg.append("\"></TD><TD>employee ID: "+e.getColumnStringValues(p2)+"</TD><TD>");
   msg.append("Employee Absence Period:"+e.getColumnStringValues(p3)+" - ");
   msg.append(e.getColumnStringValues(p4)+"</TD><TD>");
   p4=e.getColumnNamePosition("empAbsenceReq");
   p1=e.getColumnNamePosition("empAbsenceType");
   p2=e.getColumnNamePosition("empAbsencePermitted");
   p3=e.getColumnNamePosition("empPermittedByID");
   msg.append("Employee Request Date:"+e.getColumnStringValues(p4)+"</TD><TD>");
   msg.append("Type:"+e.getColumnStringValues(p1)+"</TD><TD>Admitted:</TD>");
   msg.append("<TD>"+e.getColumnStringValues(p2)+"</TD><TD>Admitted By:</TD>");
   msg.append("<TD>"+e.getColumnStringValues(p3)+"</TD></TR>"); 
   out.println(msg.toString());
   e.destroyAll();
   msg.delete(0,msg.length()); 
  }
  out.println("</TABLE><BR></BR>");
  out.println("<TABLE BORDER=0 WIDTH=75%>");
  out.println("<CAPTION><BR><B>Yang Ingin Dirubah?</B></BR></CAPTION>");
  out.println("<TR><TD>Period Absence Begin:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=perBeg SIZE=20 MAXLENGTH=10></TD></TR>");
  out.println("<TR><TD>Period Absence End:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=perEnd SIZE=20 MAXLENGTH=10></TD></TR>");
  out.println("<TR><TD>Absence Type</TD>");
  out.println("<TD><SELECT NAME=aType>");
  out.println("<OPTION VALUE=-1 SELECTED>-- Tidak Berubah --</OPTION>");
  out.println("<OPTION VALUE=1>ILL</OPTION>");
  out.println("<OPTION VALUE=2>DAYS_OFF</OPTION>");
  out.println("<OPTION VALUE=3>UNKNOWN</OPTION>");
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD>Permitted</TD>");
  out.println("<TD><SELECT NAME=permit>");
  out.println("<OPTION VALUE=-1 SELECTED>-- Tidak Berubah --</OPTION>");
  out.println("<OPTION VALUE=1>YES</OPTION>");
  out.println("<OPTION VALUE=2>NO</OPTION>");
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD>Authorized Employee ID</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=authID SIZE=20 MAXLENGTH=10></td></TR>");
  out.println("<TR><TD>Authorized Employee PassKey</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=authPK SIZE=20 MAXLENGTH=6></TD></TR>");
  util.createEnd(out);
  if(!v.isEmpty())
    v.removeAllElements();
 } // end of createForm's method

 public void processAll(HttpServletRequest req) throws SQLException
 {
  int choice = Integer.parseInt(req.getParameter("id"));
  int atype = Integer.parseInt(req.getParameter("aType"));
  int permit= Integer.parseInt(req.getParameter("permit"));
  String perBeg = req.getParameter("perBeg");
  String perEnd = req.getParameter("perEnd");
  String authID = req.getParameter("authID");
  String authPK = req.getParameter("authPK");
  String authPW = req.getParameter("authPW");
 
  if((perBeg==null)||(perEnd==null)||(authID==null)||(authPK==null)||(authPW==null))
  {
   cls.error(1,"Period Begin/Period End");
   return;
  }
  if((authID.length()<1)||(authPK.length()<1)||(authPW.length()<1)||
     (perBeg.length()<1)||(perEnd.length()<1))
  {
   cls.error(2,null);
   return;
  }

  if((!util.checkDigit(authID,true,out))||(!util.checkDigit(authPK,true,out)))
  {
   cls.error(3,"Authorized ID/Authorized Employee PassKey");
   return;
  }
  DateUtil du = new DateUtil();
  if(perBeg.length()>1)
  {
   if(!du.checkSQLDate(perBeg,out))
   {
    cls.error(4,null);
    return;
   }
   else if(!du.checkSQLDateValidity(perBeg,4,out))
   {
    cls.error(4,null);
    return;
   }
  }
  if(perEnd.length()>1)
  {
   if(!du.checkSQLDate(perEnd,out))
   {
    cls.error(4,null);
    return;
   }
   else if(!du.checkSQLDateValidity(perEnd,4,out))
   {
    cls.error(4,null);
    return;
   }
  }
  int aID = Integer.parseInt(authID);
  if(!util.verifyAuthorization(dbFill,cls,out,aID,authPK,authPW,2,null))
  {
   cls.error(7,null);  
   return;
  }
  
  DBEncapsulation dbI = new DBEncapsulation("employeeAbsence");
  dbI.setCondition("WHERE empAbsenceKey="+choice); 
  if(perEnd.length()>1)
    dbI.setColumnNameVal("empAbsenceEnd",perEnd);
  if(perBeg.length()>1)
    dbI.setColumnNameVal("empAbsenceBegin",perBeg);
  if(atype!=-1)
    dbI.setColumnNameVal("empAbsenceType",atype);
  if(permit!=-1)
    dbI.setColumnNameVal("empAbsencePermitted",permit);
  dbI.setColumnNameVal("empPermittedByID",aID);
  Vector v = new Vector();
  v.addElement((DBEncapsulation)dbI.clone());
  dbI.destroyAll();
  dbFill.UpdateDatabase(v,out);
  if(!v.isEmpty())
    v.removeAllElements();
  out.println("Successfully Update Employee Absence");
 } // end of processAll's method

} // end of ATTSviewEmpAbsence' class

