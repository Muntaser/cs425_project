/**
 * ATTSviewExchangeRate class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSviewExchangeRate extends HttpServlet
{
 private PrintWriter out = null;
 private DBFill dbFill;
 private String dbName = "ATTSairline";
 private Closure cls = null;
 private ATTSutility util = null;

 public void init() throws ServletException
 {
  dbFill = new DBFill();
  util = new ATTSutility();
 }
 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
 }
 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS View Exchange Rate</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  out.println("<HR></HR>");
  String view = req.getParameter("view");
  cls = new Closure(out);
  if((view==null)||(view.length()<1))
  {
   out.println("doGet of ATTSviewExchangeRate is null or has incorrect parameter");
   cls.makeClosing();
   cls.closing();
   out.close();
   out = null;
  }
  DBEncapsulation dbE = new DBEncapsulation("airlineExchangeRate");
  dbE.setColumnName("*");
  DBEncapsulation dbT = null;
  DBEncapsulation dbJ = null;
  Vector vecR = new Vector(); // final container
  Vector vecResult = null; // main container
  int pos1 = 0;
  int pos2 = 0;
  int pos3 = 0;
  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
   {
    out.println("makeInitialConnection failed");
    throw new SQLException();
   }
   if(view.compareToIgnoreCase("SET")==0)
     createForm(out);
   else
   {
   vecResult = dbFill.QueryDatabase((DBEncapsulation)dbE.clone(),out);
   // if vecResult = null, airlineExchangeRate is still empty
   // then transfer the control to createForm
   if(vecResult == null)
   {
    createForm(out);
    cls.makeClosing();
    cls.closing();
    out.close();
    dbE.destroyAll();
    if((!vecR.isEmpty())&&(vecR!=null))
    {
      vecR.removeAllElements();
      vecR = null;
    } 
    return; // return to controlling class
   }
   int vecsize = vecResult.size();
   dbE.clearAll();
  
   //search & replace from the main container
   for(int a=0;a<vecsize;a++)
   {
     dbT=(DBEncapsulation)((DBEncapsulation)vecResult.get(a)).clone(); 
     pos1 = dbT.getColumnNamePosition("exchangeMNote");
     dbE.setDBTableName("airlineMediumNote");
     dbE.setColumnName("mediumNote");
     dbE.setCondition("WHERE airlineMediumNoteKey="+dbT.getColumnIntegerValue(pos1));
     dbE.clearAll();
     dbJ = new DBEncapsulation(dbFill.QuerySingleResult(dbE,out)); 
     pos2 = dbJ.getColumnNamePosition("mediumNote");
     //replace value at the temp master container
     if((pos2!=dbJ.NOT_FOUND)&&(pos2!=dbJ.SAME_SIZE)&&(pos2!=dbJ.OUT_RANGE))
       dbT.replaceValue(pos1,dbJ.getColumnStringValue(pos2));
     dbJ.destroyAll();
     //prepare query for exchangeFrom and exchangeTo
     pos2 = dbT.getColumnNamePosition("exchangeFrom");
     dbE.setDBTableName("airlineCurrency");
     dbE.setColumnName("*");
     dbE.setCondition("WHERE currencyKey="+dbT.getColumnIntegerValue(pos2));
     dbJ = new DBEncapsulation(dbFill.QuerySingleResult(dbE,out));
     pos3 = dbJ.getColumnNamePosition("currencyAbbr");
     if((pos3!=dbJ.NOT_FOUND)&&(pos3!=dbJ.SAME_SIZE)&&(pos3!=dbJ.OUT_RANGE))
       dbT.replaceValue(pos2,dbJ.getColumnStringValue(pos3));
     dbE.clearAll();
     dbJ.destroyAll();
 
     pos2 = dbT.getColumnNamePosition("exchangeTo");
     dbE.setDBTableName("airlineCurrency");
     dbE.setColumnName("*");
     dbE.setCondition("WHERE currencyKey="+dbT.getColumnIntegerValue(pos2));
     dbJ = new DBEncapsulation(dbFill.QuerySingleResult(dbE,out));      
     pos3= dbJ.getColumnNamePosition("currencyAbbr");
     if((pos3!=dbJ.NOT_FOUND)&&(pos3!=dbJ.SAME_SIZE)&&(pos3!=dbJ.OUT_RANGE))
       dbT.replaceValue(pos2,dbJ.getColumnStringValue(pos3));
     dbE.clearAll();
     dbJ.destroyAll();
     vecR.addElement(dbT.clone());
     dbT.destroyAll();
   } 
   dbFill.viewContainer(vecR,out);
   }
   
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out);
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
   if((!vecResult.isEmpty())&&(vecResult!=null))
   {
    vecResult.removeAllElements();
    vecResult = null;
   }
   if((!vecR.isEmpty())&&(vecR!=null))
   {
    vecR.removeAllElements();
    vecR = null;
   }
   dbT = null;
   dbJ = null;
   dbE = null;
  }
 } 

 public void createForm(PrintWriter out) throws SQLException
 {
  DBEncapsulation dbT = null;
  DBEncapsulation dbQ = new DBEncapsulation("airlineCurrency");
  dbQ.setColumnName("*");
  out.println("<BR></BR>");
  out.println("<FORM METHOD=POST ACTION=\"ATTSsetExchangeRate\">");
  out.println("<TABLE BORDER=0 WIDTH=75%>");
  Vector tmpVec = dbFill.QueryDatabase(dbQ,out);
  if(tmpVec == null)
  {
   cls.error(0,"Data Currency belum tersedia");
   cls.error(0,"Harap di-isi data currency dahulu");
   return;
  }
  int size = tmpVec.size();
  int pos1 = 0;
  int pos2 = 0;
  int pos3 = 0;
  StringBuffer strBuf = new StringBuffer();
  out.println("<TR><TD>Currency Asal</TD>");
  out.println("<TD><SELECT NAME=\"currencyFrom\">"); 
  out.println("<OPTION SELECTED VALUE=\"-1\">-- Pilih --</OPTION>");
  for(int a=0;a<size;a++)
  {
   dbT = (DBEncapsulation)((DBEncapsulation)tmpVec.get(a)).clone(); 
   pos1 = dbT.getColumnNamePosition("currencyKey");
   pos2 = dbT.getColumnNamePosition("currencyAbbr"); 
   strBuf.append("<OPTION VALUE=\""+dbT.getColumnIntegerValue(pos1)+"\">");
   strBuf.append(dbT.getColumnStringValue(pos2)+"</OPTION>");
   dbT.destroyAll();
  }
  out.println(strBuf.toString());
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD>Currency Tujuan</TD>");
  out.println("<TD><SELECT NAME=\"currencyTo\">");
  out.println("<OPTION SELECTED VALUE=\"-1\">-- Pilih --</OPTION>");
  out.println(strBuf.toString());
  out.println("</SELECT></TD></TR>");
  out.println("<TR><TD>Exchange Rate:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"rate\" MAXLENGTH=10 SIZE=16></TD></TR>");
  out.println("<TR><TD>Input by:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"airInputby\" MAXLENGTH=10 SIZE=16></TD></TR>");
  out.println("<TR><TD>Note:</TD>");
  out.println("<TD><INPUT TYPE=TEXT NAME=\"airNote\" MAXLENGTH=64 SIZE=16></TD></TR>");
  out.println("</TABLE>");
  out.println("<INPUT TYPE=SUBMIT VALUE=\"SUBMIT\">&nbsp;");
  out.println("<INPUT TYPE=RESET VALUE=\"CLEAR\">");
  out.println("<BR></BR>");
  out.println("</FORM>");
  strBuf.delete(0,strBuf.length());
 }
}

