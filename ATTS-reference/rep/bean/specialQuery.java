package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Enumeration;
import java.util.Vector;
import javax.servlet.http.*;
import javax.servlet.*;

public class specialQuery
{
 private String driver  = "com.mysql.jdbc.Driver";
 private String url     = "jdbc:mysql://starcloud:3306/";
 private String user    = "edisonch";
 private String pwd     = "gblj21mysql03";
 private String dbName  = "ATTSairline";
 private PrintWriter out  = null;
 private DBFill dbFill    = null;
 private Vector vecQuery  = null;
 private Vector vecResult = null;
 private boolean startFlag= false;
 private DBEncapsulation dbResult = null;

/**
 * start's method is to start the bean service by getting
 * the HTTPServletResponse from the calling class then make
 * the initialized connection to the database. It also used the 
 * response to getWriter() so the class can output the error directly
 * to the calling class.
 * @param HttpServletResponse res
 * @return true upon successful creation of the connection to database
 */
 public boolean start(HttpServletResponse res)
 {
  try
  {
   out = res.getWriter();
   res.setContentType("text/html");
   dbFill = new DBFill(driver,url,user,pwd);
   initialize();
  }
  catch(Exception ie)
  {
    System.out.println("Exception in start " + ie);
    resetStart();
    return false;
  } 
  return dbFill.makeInitialConnection(dbName,out);
 }

/**
 * prepareQuery's method is to consolidate the queries into db.
 * Requiring the tableName(String) and the name of columns(Vector of String)
 * then enter them into DBEncapsulation for later query.
 * Considering that could be more than 1 table query, we consolidate all
 * the DBEncapsulation query into vecQuery(Vector of DBEncapsulation)
 * @param String tableName, Vector colName (contain Strings of columnName)
 * @return none
 */
 public void prepareQuery(String tableName,Vector colName,String cond)
 {
  if((tableName == null)||(tableName.length()<1))
    throw new DefaultValueException("tableName is null");
  DBEncapsulation dbQ = new DBEncapsulation(tableName);
  int size = colName.size();
  for(int cn=0;cn<size;cn++)
    dbQ.setColumnName((String)colName.get(cn));
  if(cond != null)
    dbQ.setCondition(cond);
  vecQuery.addElement((DBEncapsulation)dbQ.clone());
  dbQ.destroyAll();
  dbQ = null;
 }

/**
 * query's method is the workhorse of the class. It created the statement
 * for query that may return a single result or multiple result. this method
 * consolidate the various result into vector.
 * @param none
 * @return none
 */
 public boolean query()
 {
  DBEncapsulation dbR = null; 
  DBEncapsulation dbT = null;
  Vector tmpVec = null;
  String str = null;
  int size = vecQuery.size();
  int tmpSize = 0;
  int tmpSize2= 0;
  Enumeration enum1 = null;
  boolean success=true;
  try
  {
   for(int i=0;i<size;i++)
   {
    //query individual table
    dbT = new DBEncapsulation((DBEncapsulation)vecQuery.get(i));
    enum1 = dbT.getAllColumnNames(); // get all the query name
    tmpVec = dbFill.QueryDatabase(dbT,out); // query the database
    if((tmpVec == null) || (tmpVec.size()<1))
      throw new Exception("QueryDatabase failed to produce any result");
    //search for the query column name & add it into vecResult
    tmpSize = tmpVec.size();
    for(int b=0;b<tmpSize;b++)
    { // get the result from the container
     dbR = new DBEncapsulation((DBEncapsulation)tmpVec.get(b)); 
     tmpSize2 = dbR.getSizeColumnName();   
     while(enum1.hasMoreElements()) //compare the colName result with query
     {
      str = new String((String)enum1.nextElement());
      for(int c=0;c<tmpSize2;c++)
       if(str.compareToIgnoreCase(dbR.getColumnNames(c))==0) 
       {
	dbResult.setColumnNameVal(dbR.getColumnNames(c),dbR.getColumnStringValues(c));
       }
      str = null;
     } // end of while-loop
     dbR.destroyAll();
    } // end of middle for-loop
    dbT.destroyAll();
   } // end of outer for-loop 
   //consolidate result from vecResult into dbResult
   //makeResult();
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
   success=false;
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out);
   success=false;
  }
  finally
  {
   dbR.destroyAll();
   if((!tmpVec.isEmpty())&&(tmpVec!=null))
   {
    tmpVec.removeAllElements();
    tmpVec = null;
   }
  } // end of try-catch-finally statement
  return success;
 }

/**
 * makeResult's method is to collect all the result from vector and put it
 * into dbEncapsulation's object to make it easier to view. It used the
 * dbQuery which contains query of strings to create the consolidation of
 * the various result into a single result in dbResult
 * @param none
 * @return none
 */
/**
 public void makeResult()
 {

 }
*/
/** 
 * viewVector's method is to provide service that can view the content of
 * the vector which hold the dbEncapsulation's object
 * @param none
 * @return none
 */
 public void viewVector()
 {
  if(!vecResult.isEmpty())
    dbFill.viewContainer(vecResult,out);
 }
/**
 * viewEncap's method is to provide service to view the dbEncapsulation's object
 * @param none
 * @return none
 */
 public void viewEncap()
 {
  if(dbResult != null)
    dbFill.viewEncap(dbResult,out);
 }
/**
 * getColVal's method is to return the column value of the result
 * @param none
 * @return none
 */
 public Enumeration getColVal()
 {
  if(dbResult==null)
    return null;
  return dbResult.getAllColumnValues();
 }
/**
 * getColName's method is to return the column name of the result 
 * @param none
 * @return none
 */
 public Enumeration getColName()
 {
  if(dbResult==null)
    return null;
  return dbResult.getAllColumnNames();
 }
/**
 * initialize's method is to initialize the bean services
 * @param none
 * @return none
 */
 private void initialize()
 { 
   vecResult = new Vector();
   vecQuery  = new Vector();
   dbResult = new DBEncapsulation();
   setStart();
 }
/**
 * stop's method is to stop the bean service and terminate the 
 * connection to the database
 * @param none
 * @return none
 */
 public void stop()
 {
  if(getStart()) resetStart(); 
  else return;
  clear();
  dbFill.closeDatabase();
  dbFill = null;
  dbResult = null;
  out = null;
 }
/**
 * clear's method is to clear all the global variables in the bean
 * @param none
 * @return none
 */
 public void clear()
 {
  if((!vecResult.isEmpty()) && (vecResult!=null))
  {
   vecResult.removeAllElements();
   vecResult = null;
  }
  if((!vecQuery.isEmpty()) && (vecQuery!=null))
  {
   vecQuery.removeAllElements();
   vecQuery = null;
  }
  dbResult.destroyAll();
 }
/**
 * setStart's method is to set the start condition of the bean
 * @param none
 * @return none
 */
 private void setStart()
 { if(!startFlag) startFlag = true; }
/**
 * resetStart's method is to reset the startFlag to false
 * @param none
 * @return none
 */
 private void resetStart()
 { if(startFlag) setOff(); }
/**
 * setOff's method is to set the start condition to false 
 * @param none
 * @return none
 */
 private void setOff()
 { startFlag = false; }
/**
 * getStart method is to get the start condition 
 * @param none
 * @return none
 */
 private boolean getStart()
 { return startFlag; }
}
