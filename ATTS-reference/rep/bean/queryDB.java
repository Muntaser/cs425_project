package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Enumeration;
import java.util.Vector;
import javax.servlet.http.*;
import javax.servlet.*;

public class queryDB
{
 private String driver = "com.mysql.jdbc.Driver";
 private String url    = "jdbc:mysql://starcloud:3306/";
 private String user   = "edisonch";
 private String pwd    = "gblj21mysql03";
 private String dbName = "ATTSairline";
 private String tableName = null;
 private DBFill dbFill = null;
 private PrintWriter out = null;
 private DBEncapsulation dbEncap = null; // store the result
 private Vector vec = null; // store the multiple db query result
 private boolean startFlag = false;

 public boolean start(HttpServletResponse res, String tableName)
 {
  setStart();
  try
  {
    out = res.getWriter();
    res.setContentType("text/html");
  }
  catch(IOException ie)
  {
   System.out.println("IOException in start: " + ie);
   return false;
  }
  dbFill  = new DBFill(driver,url,user,pwd);
  initialize(tableName);
  return dbFill.makeInitialConnection(dbName,out);
 }

 public void initialize(String tableName)
 {
  dbEncap = new DBEncapsulation();
  this.tableName = tableName;
 }

 public void viewIt()
 {
  if(!vec.isEmpty())
    dbFill.viewContainer(vec,out);

  dbFill.viewEncap(dbEncap,out);
 }

 public boolean query(String val, String name)
 {
  boolean flag = false;
  //setup query dbEncapsulation
  DBEncapsulation dbQuery = new DBEncapsulation(tableName);
  dbQuery.setColumnName("*");
  
  //create temp DBEncapsulation
  DBEncapsulation dbTemp = new DBEncapsulation();
  int vecSize = 0;
  int pos1 = 0;
  int pos2 = 0;

  try
  {
   vec = dbFill.QueryDatabase(dbQuery,out);
   vecSize = vec.size();
   if(vecSize<1)
     throw new Exception("QueryDatabase does not produce any result");
   dbEncap.setDBTableName(tableName);
   for(int ac=0;ac<vecSize;ac++)
   {
    dbTemp = (DBEncapsulation)((DBEncapsulation)vec.get(ac)).clone();
    pos1 = dbTemp.getColumnNamePosition(name); // for column name
    pos2 = dbTemp.getColumnNamePosition(val); // for column value
    if((pos1<0)||(pos2<0))
      throw new Exception("Cannot find "+val+" or "+name+" in dbEncap");
    dbEncap.setColumnNameVal(dbTemp.getColumnStringValues(pos2),
			     dbTemp.getColumnStringValue(pos1));
    dbTemp.destroyAll();
   }
   flag = true;
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out);
  }
  finally
  {
   dbQuery.destroyAll();
   dbTemp = null;
   dbQuery= null;
  }
  return flag;
 }

 public Enumeration getColVal()
 {  
  return dbEncap.getAllColumnValues(); 
 }

 public Enumeration getColName()
 {
  return dbEncap.getAllColumnNames();
 }

 public void setStart()
 {
  startFlag = true;
 }

 public void offStart()
 {
  startFlag = false;
 }

 public boolean getStart()
 {
  return startFlag;
 }

 public void stop()
 {
  if(getStart())
    offStart();
  else
    return;
  clear();
  dbFill.closeDatabase(); 
  dbFill = null;
  dbEncap= null;
  vec = null;
  out = null;
 }

 public void clear()
 {
  tableName = null;
  if((vec != null)&&(!vec.isEmpty()))
      vec.removeAllElements();
  if(dbEncap != null)
    dbEncap.destroyAll(); 
 }
}
