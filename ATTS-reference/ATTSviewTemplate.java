/**
 * ATTSviewRoute class
 * @author Edison Chindrawaly
 */
package ATTS;

import java.io.*;
import java.sql.*;
import java.util.Vector;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ATTSviewAirlineAccess extends HttpServlet
{
 private DBFill dbFill = null;
 private String dbName = "ATTSairline";
 private PrintWriter out = null;
 private Closure cls = null;
 private ATTSutility util = null;

 public void init() throws ServletException
 {
  dbFill = new DBFill();
  util = new ATTSutility();
 } // end of init

 public void destroy() 
 {
  dbFill.closeDatabase();
  util.close();
  dbFill = null;
 } // end of destroy

 public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  doPost(req,res);
 } // end of doGet

 public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException
 {
  out = res.getWriter();
  cls = new Closure(out);
  res.setContentType("text/html");
  out.println("<HTML><HEAD><TITLE>ATTS View Route</TITLE></HEAD>");
  out.println("<BODY BGCOLOR=IVORY TEXT=BLACK LINK=BLUE ALINK=RED VLINK=BLACK>");
  String view = req.getParameter("view");
  if(view == null)
  {
   cls.error(1,"view");
   cls.closing();
   out.close();
   return;
  }
  
  try
  {
   if(!dbFill.makeInitialConnection(dbName,out))
     throw new SQLException("<BR>Fail to make initial connection</BR>");
   if(view.compareToIgnoreCase("ALL")==0)
     createView();
   else if(view.compareToIgnoreCase("SET")==0)
     createForm();
   else if(view.compareToIgnoreCase("PROCESS")==0)
     processAll(req); 
   else
    cls.error(6,null);
  }
  catch(SQLException sqle)
  {
   sqle.printStackTrace(out);
  }
  catch(Exception ex)
  {
   ex.printStackTrace(out); 
  }
  finally
  {
   cls.makeClosing();
   cls.closing();
   out.close();
   view = null;
   out = null;
   cls = null;
  }

 } // end of doPost 

 public void createView() throws SQLException
 {

 } // end of createView's method

 public void createForm() throws SQLException
 {

 } // end of createForm's method

 public void processAll(HttpServletRequest req) throws SQLException
 {

 } // end of processAll's method

} // end of ATTSviewAirlineAccess' class

