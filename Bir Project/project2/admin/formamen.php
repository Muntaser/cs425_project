<?php require_once('../Connections/config.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_config = new KT_connection($config, $database_config);

// Start trigger
$formValidation = new tNG_FormValidation();
$tNGs->prepareValidation($formValidation);
// End trigger

// Make an insert transaction instance
$ins_amenities = new tNG_multipleInsert($conn_config);
$tNGs->addTransaction($ins_amenities);
// Register triggers
$ins_amenities->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_amenities->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_amenities->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$ins_amenities->setTable("amenities");
$ins_amenities->addColumn("Amenity_name", "STRING_TYPE", "POST", "Amenity_name");
$ins_amenities->setPrimaryKey("Amen_id", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_amenities = new tNG_multipleUpdate($conn_config);
$tNGs->addTransaction($upd_amenities);
// Register triggers
$upd_amenities->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_amenities->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_amenities->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$upd_amenities->setTable("amenities");
$upd_amenities->addColumn("Amenity_name", "STRING_TYPE", "POST", "Amenity_name");
$upd_amenities->setPrimaryKey("Amen_id", "NUMERIC_TYPE", "GET", "Amen_id");

// Make an instance of the transaction object
$del_amenities = new tNG_multipleDelete($conn_config);
$tNGs->addTransaction($del_amenities);
// Register triggers
$del_amenities->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Delete1");
$del_amenities->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$del_amenities->setTable("amenities");
$del_amenities->setPrimaryKey("Amen_id", "NUMERIC_TYPE", "GET", "Amen_id");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsamenities = $tNGs->getRecordset("amenities");
$row_rsamenities = mysql_fetch_assoc($rsamenities);
$totalRows_rsamenities = mysql_num_rows($rsamenities);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?>
<script src="../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: true,
  show_as_grid: true,
  merge_down_value: true
}
</script>
</head>

<body>
<?php
	echo $tNGs->getErrorMsg();
?>
<div class="KT_tng">
  <h1>
    <?php 
// Show IF Conditional region1 
if (@$_GET['Amen_id'] == "") {
?>
      <?php echo NXT_getResource("Insert_FH"); ?>
      <?php 
// else Conditional region1
} else { ?>
      <?php echo NXT_getResource("Update_FH"); ?>
      <?php } 
// endif Conditional region1
?>
    Amenities </h1>
  <div class="KT_tngform">
    <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
      <?php $cnt1 = 0; ?>
      <?php do { ?>
        <?php $cnt1++; ?>
        <?php 
// Show IF Conditional region1 
if (@$totalRows_rsamenities > 1) {
?>
          <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
          <?php } 
// endif Conditional region1
?>
        <table cellpadding="2" cellspacing="0" class="KT_tngtable">
          <tr>
            <td class="KT_th"><label for="Amenity_name_<?php echo $cnt1; ?>">Amenity_name:</label></td>
            <td><input type="text" name="Amenity_name_<?php echo $cnt1; ?>" id="Amenity_name_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsamenities['Amenity_name']); ?>" size="32" maxlength="40" />
                <?php echo $tNGs->displayFieldHint("Amenity_name");?> <?php echo $tNGs->displayFieldError("amenities", "Amenity_name", $cnt1); ?> </td>
          </tr>
        </table>
        <input type="hidden" name="kt_pk_amenities_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($row_rsamenities['kt_pk_amenities']); ?>" />
        <?php } while ($row_rsamenities = mysql_fetch_assoc($rsamenities)); ?>
      <div class="KT_bottombuttons">
        <div>
          <?php 
      // Show IF Conditional region1
      if (@$_GET['Amen_id'] == "") {
      ?>
            <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Insert_FB"); ?>" />
            <?php 
      // else Conditional region1
      } else { ?>
            <div class="KT_operations">
              <input type="submit" name="KT_Insert1" value="<?php echo NXT_getResource("Insert as new_FB"); ?>" onclick="nxt_form_insertasnew(this, 'Amen_id')" />
            </div>
            <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
            <input type="submit" name="KT_Delete1" value="<?php echo NXT_getResource("Delete_FB"); ?>" onclick="return confirm('<?php echo NXT_getResource("Are you sure?"); ?>');" />
            <?php }
      // endif Conditional region1
      ?>
          <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '../includes/nxt/back.php')" />
        </div>
      </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
</body>
</html>
