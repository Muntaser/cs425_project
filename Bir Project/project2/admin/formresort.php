<?php require_once('../Connections/config.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_config = new KT_connection($config, $database_config);

// Start trigger
$formValidation = new tNG_FormValidation();
$formValidation->addField("Name", true, "text", "", "", "", "");
$formValidation->addField("city_ID", true, "numeric", "", "", "", "");
$formValidation->addField("AddressID", true, "text", "", "", "", "");
$formValidation->addField("phone_number", true, "numeric", "", "", "", "");
$tNGs->prepareValidation($formValidation);
// End trigger

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_config, $config);
$query_Recordset1 = "SELECT City_id, city_name FROM city ORDER BY City_id";
$Recordset1 = mysql_query($query_Recordset1, $config) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);

mysql_select_db($database_config, $config);
$query_citymenu = "SELECT * FROM city";
$citymenu = mysql_query($query_citymenu, $config) or die(mysql_error());
$row_citymenu = mysql_fetch_assoc($citymenu);
$totalRows_citymenu = mysql_num_rows($citymenu);

mysql_select_db($database_config, $config);
$query_addmenu = "SELECT * FROM resortadd";
$addmenu = mysql_query($query_addmenu, $config) or die(mysql_error());
$row_addmenu = mysql_fetch_assoc($addmenu);
$totalRows_addmenu = mysql_num_rows($addmenu);

// Make an insert transaction instance
$ins_resort = new tNG_multipleInsert($conn_config);
$tNGs->addTransaction($ins_resort);
// Register triggers
$ins_resort->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_resort->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_resort->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$ins_resort->setTable("resort");
$ins_resort->addColumn("Name", "STRING_TYPE", "POST", "Name");
$ins_resort->addColumn("city_ID", "STRING_TYPE", "POST", "city_ID", "{citymenu.City_id}");
$ins_resort->addColumn("AddressID", "STRING_TYPE", "POST", "AddressID", "{addmenu.AddressID}");
$ins_resort->addColumn("phone_number", "NUMERIC_TYPE", "POST", "phone_number");
$ins_resort->setPrimaryKey("Resort_id", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_resort = new tNG_multipleUpdate($conn_config);
$tNGs->addTransaction($upd_resort);
// Register triggers
$upd_resort->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_resort->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_resort->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$upd_resort->setTable("resort");
$upd_resort->addColumn("Name", "STRING_TYPE", "POST", "Name");
$upd_resort->addColumn("city_ID", "STRING_TYPE", "POST", "city_ID");
$upd_resort->addColumn("AddressID", "STRING_TYPE", "POST", "AddressID");
$upd_resort->addColumn("phone_number", "NUMERIC_TYPE", "POST", "phone_number");
$upd_resort->setPrimaryKey("Resort_id", "NUMERIC_TYPE", "GET", "Resort_id");

// Make an instance of the transaction object
$del_resort = new tNG_multipleDelete($conn_config);
$tNGs->addTransaction($del_resort);
// Register triggers
$del_resort->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Delete1");
$del_resort->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$del_resort->setTable("resort");
$del_resort->setPrimaryKey("Resort_id", "NUMERIC_TYPE", "GET", "Resort_id");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsresort = $tNGs->getRecordset("resort");
$row_rsresort = mysql_fetch_assoc($rsresort);
$totalRows_rsresort = mysql_num_rows($rsresort);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?>
<script src="../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: true,
  show_as_grid: true,
  merge_down_value: true
}
</script>
</head>

<body>
<?php
	echo $tNGs->getErrorMsg();
?>
<div class="KT_tng">
  <h1>
    <?php 
// Show IF Conditional region1 
if (@$_GET['Resort_id'] == "") {
?>
      <?php echo NXT_getResource("Insert_FH"); ?>
      <?php 
// else Conditional region1
} else { ?>
      <?php echo NXT_getResource("Update_FH"); ?>
      <?php } 
// endif Conditional region1
?>
    Resort </h1>
  <div class="KT_tngform">
    <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
      <?php $cnt1 = 0; ?>
      <?php do { ?>
        <?php $cnt1++; ?>
        <?php 
// Show IF Conditional region1 
if (@$totalRows_rsresort > 1) {
?>
          <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
          <?php } 
// endif Conditional region1
?>
        <table cellpadding="2" cellspacing="0" class="KT_tngtable">
          <tr>
            <td class="KT_th"><label for="Name_<?php echo $cnt1; ?>">Name:</label></td>
            <td><input type="text" name="Name_<?php echo $cnt1; ?>" id="Name_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsresort['Name']); ?>" size="32" maxlength="40" />
                <?php echo $tNGs->displayFieldHint("Name");?> <?php echo $tNGs->displayFieldError("resort", "Name", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="city_ID_<?php echo $cnt1; ?>">city_ID:</label></td>
            <td><select name="city_ID_<?php echo $cnt1; ?>" id="city_ID_<?php echo $cnt1; ?>">
              <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
              <?php 
do {  
?>
              <option value="<?php echo $row_Recordset1['City_id']?>"<?php if (!(strcmp($row_Recordset1['City_id'], $row_rsresort['city_ID']))) {echo "SELECTED";} ?>><?php echo $row_Recordset1['city_name']?></option>
              <?php
} while ($row_Recordset1 = mysql_fetch_assoc($Recordset1));
  $rows = mysql_num_rows($Recordset1);
  if($rows > 0) {
      mysql_data_seek($Recordset1, 0);
	  $row_Recordset1 = mysql_fetch_assoc($Recordset1);
  }
?>
            </select>
                <?php echo $tNGs->displayFieldError("resort", "city_ID", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="AddressID_<?php echo $cnt1; ?>">AddressID:</label></td>
            <td><select name="AddressID_<?php echo $cnt1; ?>" id="AddressID_<?php echo $cnt1; ?>">
              <option value=""><?php echo NXT_getResource("Select one..."); ?></option>
              <?php 
do {  
?>
              <option value="<?php echo $row_addmenu['AddressID']?>"<?php if (!(strcmp($row_addmenu['AddressID'], $row_rsresort['AddressID']))) {echo "SELECTED";} ?>><?php echo $row_addmenu['street']?></option>
              <?php
} while ($row_addmenu = mysql_fetch_assoc($addmenu));
  $rows = mysql_num_rows($addmenu);
  if($rows > 0) {
      mysql_data_seek($addmenu, 0);
	  $row_addmenu = mysql_fetch_assoc($addmenu);
  }
?>
            </select>
                <?php echo $tNGs->displayFieldError("resort", "AddressID", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="phone_number_<?php echo $cnt1; ?>">Phone_number:</label></td>
            <td><input type="text" name="phone_number_<?php echo $cnt1; ?>" id="phone_number_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsresort['phone_number']); ?>" size="7" />
                <?php echo $tNGs->displayFieldHint("phone_number");?> <?php echo $tNGs->displayFieldError("resort", "phone_number", $cnt1); ?> </td>
          </tr>
        </table>
        <input type="hidden" name="kt_pk_resort_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($row_rsresort['kt_pk_resort']); ?>" />
        <?php } while ($row_rsresort = mysql_fetch_assoc($rsresort)); ?>
      <div class="KT_bottombuttons">
        <div>
          <?php 
      // Show IF Conditional region1
      if (@$_GET['Resort_id'] == "") {
      ?>
            <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Insert_FB"); ?>" />
            <?php 
      // else Conditional region1
      } else { ?>
            <div class="KT_operations">
              <input type="submit" name="KT_Insert1" value="<?php echo NXT_getResource("Insert as new_FB"); ?>" onclick="nxt_form_insertasnew(this, 'Resort_id')" />
            </div>
            <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
            <input type="submit" name="KT_Delete1" value="<?php echo NXT_getResource("Delete_FB"); ?>" onclick="return confirm('<?php echo NXT_getResource("Are you sure?"); ?>');" />
            <?php }
      // endif Conditional region1
      ?>
          <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '../includes/nxt/back.php')" />
        </div>
      </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($Recordset1);

mysql_free_result($citymenu);

mysql_free_result($addmenu);
?>
