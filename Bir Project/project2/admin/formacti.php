<?php require_once('../Connections/config.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the tNG classes
require_once('../includes/tng/tNG.inc.php');

// Load the KT_back class
require_once('../includes/nxt/KT_back.php');

// Make a transaction dispatcher instance
$tNGs = new tNG_dispatcher("../");

// Make unified connection variable
$conn_config = new KT_connection($config, $database_config);

// Start trigger
$formValidation = new tNG_FormValidation();
$tNGs->prepareValidation($formValidation);
// End trigger

// Make an insert transaction instance
$ins_activities = new tNG_multipleInsert($conn_config);
$tNGs->addTransaction($ins_activities);
// Register triggers
$ins_activities->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Insert1");
$ins_activities->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$ins_activities->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$ins_activities->setTable("activities");
$ins_activities->addColumn("Activity_name", "STRING_TYPE", "POST", "Activity_name");
$ins_activities->addColumn("Description", "STRING_TYPE", "POST", "Description");
$ins_activities->setPrimaryKey("Activity_id", "NUMERIC_TYPE");

// Make an update transaction instance
$upd_activities = new tNG_multipleUpdate($conn_config);
$tNGs->addTransaction($upd_activities);
// Register triggers
$upd_activities->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Update1");
$upd_activities->registerTrigger("BEFORE", "Trigger_Default_FormValidation", 10, $formValidation);
$upd_activities->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$upd_activities->setTable("activities");
$upd_activities->addColumn("Activity_name", "STRING_TYPE", "POST", "Activity_name");
$upd_activities->addColumn("Description", "STRING_TYPE", "POST", "Description");
$upd_activities->setPrimaryKey("Activity_id", "NUMERIC_TYPE", "GET", "Activity_id");

// Make an instance of the transaction object
$del_activities = new tNG_multipleDelete($conn_config);
$tNGs->addTransaction($del_activities);
// Register triggers
$del_activities->registerTrigger("STARTER", "Trigger_Default_Starter", 1, "POST", "KT_Delete1");
$del_activities->registerTrigger("END", "Trigger_Default_Redirect", 99, "../includes/nxt/back.php");
// Add columns
$del_activities->setTable("activities");
$del_activities->setPrimaryKey("Activity_id", "NUMERIC_TYPE", "GET", "Activity_id");

// Execute all the registered transactions
$tNGs->executeTransactions();

// Get the transaction recordset
$rsactivities = $tNGs->getRecordset("activities");
$row_rsactivities = mysql_fetch_assoc($rsactivities);
$totalRows_rsactivities = mysql_num_rows($rsactivities);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<?php echo $tNGs->displayValidationRules();?>
<script src="../includes/nxt/scripts/form.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/form.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_FORM_SETTINGS = {
  duplicate_buttons: true,
  show_as_grid: true,
  merge_down_value: true
}
</script>
</head>

<body>
<?php
	echo $tNGs->getErrorMsg();
?>
<div class="KT_tng">
  <h1>
    <?php 
// Show IF Conditional region1 
if (@$_GET['Activity_id'] == "") {
?>
      <?php echo NXT_getResource("Insert_FH"); ?>
      <?php 
// else Conditional region1
} else { ?>
      <?php echo NXT_getResource("Update_FH"); ?>
      <?php } 
// endif Conditional region1
?>
    Activities </h1>
  <div class="KT_tngform">
    <form method="post" id="form1" action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>">
      <?php $cnt1 = 0; ?>
      <?php do { ?>
        <?php $cnt1++; ?>
        <?php 
// Show IF Conditional region1 
if (@$totalRows_rsactivities > 1) {
?>
          <h2><?php echo NXT_getResource("Record_FH"); ?> <?php echo $cnt1; ?></h2>
          <?php } 
// endif Conditional region1
?>
        <table cellpadding="2" cellspacing="0" class="KT_tngtable">
          <tr>
            <td class="KT_th"><label for="Activity_name_<?php echo $cnt1; ?>">Activity_name:</label></td>
            <td><input type="text" name="Activity_name_<?php echo $cnt1; ?>" id="Activity_name_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsactivities['Activity_name']); ?>" size="32" maxlength="40" />
                <?php echo $tNGs->displayFieldHint("Activity_name");?> <?php echo $tNGs->displayFieldError("activities", "Activity_name", $cnt1); ?> </td>
          </tr>
          <tr>
            <td class="KT_th"><label for="Description_<?php echo $cnt1; ?>">Description:</label></td>
            <td><input type="text" name="Description_<?php echo $cnt1; ?>" id="Description_<?php echo $cnt1; ?>" value="<?php echo KT_escapeAttribute($row_rsactivities['Description']); ?>" size="32" maxlength="200" />
                <?php echo $tNGs->displayFieldHint("Description");?> <?php echo $tNGs->displayFieldError("activities", "Description", $cnt1); ?> </td>
          </tr>
        </table>
        <input type="hidden" name="kt_pk_activities_<?php echo $cnt1; ?>" class="id_field" value="<?php echo KT_escapeAttribute($row_rsactivities['kt_pk_activities']); ?>" />
        <?php } while ($row_rsactivities = mysql_fetch_assoc($rsactivities)); ?>
      <div class="KT_bottombuttons">
        <div>
          <?php 
      // Show IF Conditional region1
      if (@$_GET['Activity_id'] == "") {
      ?>
            <input type="submit" name="KT_Insert1" id="KT_Insert1" value="<?php echo NXT_getResource("Insert_FB"); ?>" />
            <?php 
      // else Conditional region1
      } else { ?>
            <div class="KT_operations">
              <input type="submit" name="KT_Insert1" value="<?php echo NXT_getResource("Insert as new_FB"); ?>" onclick="nxt_form_insertasnew(this, 'Activity_id')" />
            </div>
            <input type="submit" name="KT_Update1" value="<?php echo NXT_getResource("Update_FB"); ?>" />
            <input type="submit" name="KT_Delete1" value="<?php echo NXT_getResource("Delete_FB"); ?>" onclick="return confirm('<?php echo NXT_getResource("Are you sure?"); ?>');" />
            <?php }
      // endif Conditional region1
      ?>
          <input type="button" name="KT_Cancel1" value="<?php echo NXT_getResource("Cancel_FB"); ?>" onclick="return UNI_navigateCancel(event, '../includes/nxt/back.php')" />
        </div>
      </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
</body>
</html>
