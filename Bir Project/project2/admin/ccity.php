<?php require_once('../Connections/config.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the required classes
require_once('../includes/tfi/TFI.php');
require_once('../includes/tso/TSO.php');
require_once('../includes/nav/NAV.php');

// Make unified connection variable
$conn_config = new KT_connection($config, $database_config);

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

// Filter
$tfi_listcity1 = new TFI_TableFilter($conn_config, "tfi_listcity1");
$tfi_listcity1->addColumn("city.city_name", "STRING_TYPE", "city_name", "%");
$tfi_listcity1->addColumn("city.country", "STRING_TYPE", "country", "%");
$tfi_listcity1->Execute();

// Sorter
$tso_listcity1 = new TSO_TableSorter("rscity1", "tso_listcity1");
$tso_listcity1->addColumn("city.city_name");
$tso_listcity1->addColumn("city.country");
$tso_listcity1->setDefault("city.city_name");
$tso_listcity1->Execute();

// Navigation
$nav_listcity1 = new NAV_Regular("nav_listcity1", "rscity1", "../", $_SERVER['PHP_SELF'], 10);

//NeXTenesio3 Special List Recordset
$maxRows_rscity1 = $_SESSION['max_rows_nav_listcity1'];
$pageNum_rscity1 = 0;
if (isset($_GET['pageNum_rscity1'])) {
  $pageNum_rscity1 = $_GET['pageNum_rscity1'];
}
$startRow_rscity1 = $pageNum_rscity1 * $maxRows_rscity1;

// Defining List Recordset variable
$NXTFilter_rscity1 = "1=1";
if (isset($_SESSION['filter_tfi_listcity1'])) {
  $NXTFilter_rscity1 = $_SESSION['filter_tfi_listcity1'];
}
// Defining List Recordset variable
$NXTSort_rscity1 = "city.city_name";
if (isset($_SESSION['sorter_tso_listcity1'])) {
  $NXTSort_rscity1 = $_SESSION['sorter_tso_listcity1'];
}
mysql_select_db($database_config, $config);

$query_rscity1 = "SELECT city.city_name, city.country, city.City_id FROM city WHERE {$NXTFilter_rscity1} ORDER BY {$NXTSort_rscity1}";
$query_limit_rscity1 = sprintf("%s LIMIT %d, %d", $query_rscity1, $startRow_rscity1, $maxRows_rscity1);
$rscity1 = mysql_query($query_limit_rscity1, $config) or die(mysql_error());
$row_rscity1 = mysql_fetch_assoc($rscity1);

if (isset($_GET['totalRows_rscity1'])) {
  $totalRows_rscity1 = $_GET['totalRows_rscity1'];
} else {
  $all_rscity1 = mysql_query($query_rscity1);
  $totalRows_rscity1 = mysql_num_rows($all_rscity1);
}
$totalPages_rscity1 = ceil($totalRows_rscity1/$maxRows_rscity1)-1;
//End NeXTenesio3 Special List Recordset

$nav_listcity1->checkBoundries();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/list.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/list.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_LIST_SETTINGS = {
  duplicate_buttons: true,
  duplicate_navigation: true,
  row_effects: true,
  show_as_buttons: true,
  record_counter: true
}
</script>
<style type="text/css">
  /* Dynamic List row settings */
  .KT_col_city_name {width:140px; overflow:hidden;}
  .KT_col_country {width:140px; overflow:hidden;}
</style>
</head>

<body>
<div class="KT_tng" id="listcity1">
  <h1> City
    <?php
  $nav_listcity1->Prepare();
  require("../includes/nav/NAV_Text_Statistics.inc.php");
?>
  </h1>
  <div class="KT_tnglist">
    <form action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" method="post" id="form1">
      <div class="KT_options"> <a href="<?php echo $nav_listcity1->getShowAllLink(); ?>"><?php echo NXT_getResource("Show"); ?>
        <?php 
  // Show IF Conditional region1
  if (@$_GET['show_all_nav_listcity1'] == 1) {
?>
          <?php echo $_SESSION['default_max_rows_nav_listcity1']; ?>
          <?php 
  // else Conditional region1
  } else { ?>
          <?php echo NXT_getResource("all"); ?>
          <?php } 
  // endif Conditional region1
?>
            <?php echo NXT_getResource("records"); ?></a> &nbsp;
        &nbsp;
                <?php 
  // Show IF Conditional region2
  if (@$_SESSION['has_filter_tfi_listcity1'] == 1) {
?>
                  <a href="<?php echo $tfi_listcity1->getResetFilterLink(); ?>"><?php echo NXT_getResource("Reset filter"); ?></a>
                  <?php 
  // else Conditional region2
  } else { ?>
                  <a href="<?php echo $tfi_listcity1->getShowFilterLink(); ?>"><?php echo NXT_getResource("Show filter"); ?></a>
                  <?php } 
  // endif Conditional region2
?>
      </div>
      <table cellpadding="2" cellspacing="0" class="KT_tngtable">
        <thead>
          <tr class="KT_row_order">
            <th> <input type="checkbox" name="KT_selAll" id="KT_selAll"/>
            </th>
            <th id="city_name" class="KT_sorter KT_col_city_name <?php echo $tso_listcity1->getSortIcon('city.city_name'); ?>"> <a href="<?php echo $tso_listcity1->getSortLink('city.city_name'); ?>">City Name</a> </th>
            <th id="country" class="KT_sorter KT_col_country <?php echo $tso_listcity1->getSortIcon('city.country'); ?>"> <a href="<?php echo $tso_listcity1->getSortLink('city.country'); ?>">Country</a> </th>
            <th>&nbsp;</th>
          </tr>
          <?php 
  // Show IF Conditional region3
  if (@$_SESSION['has_filter_tfi_listcity1'] == 1) {
?>
            <tr class="KT_row_filter">
              <td>&nbsp;</td>
              <td><input type="text" name="tfi_listcity1_city_name" id="tfi_listcity1_city_name" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listcity1_city_name']); ?>" size="20" maxlength="20" /></td>
              <td><input type="text" name="tfi_listcity1_country" id="tfi_listcity1_country" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listcity1_country']); ?>" size="20" maxlength="20" /></td>
              <td><input type="submit" name="tfi_listcity1" value="<?php echo NXT_getResource("Filter"); ?>" /></td>
            </tr>
            <?php } 
  // endif Conditional region3
?>
        </thead>
        <tbody>
          <?php if ($totalRows_rscity1 == 0) { // Show if recordset empty ?>
            <tr>
              <td colspan="4"><?php echo NXT_getResource("The table is empty or the filter you've selected is too restrictive."); ?></td>
            </tr>
            <?php } // Show if recordset empty ?>
          <?php if ($totalRows_rscity1 > 0) { // Show if recordset not empty ?>
            <?php do { ?>
              <tr class="<?php echo @$cnt1++%2==0 ? "" : "KT_even"; ?>">
                <td><input type="checkbox" name="kt_pk_city" class="id_checkbox" value="<?php echo $row_rscity1['City_id']; ?>" />
                    <input type="hidden" name="City_id" class="id_field" value="<?php echo $row_rscity1['City_id']; ?>" />
                </td>
                <td><div class="KT_col_city_name"><?php echo KT_FormatForList($row_rscity1['city_name'], 20); ?></div></td>
                <td><div class="KT_col_country"><?php echo KT_FormatForList($row_rscity1['country'], 20); ?></div></td>
                <td><a class="KT_edit_link" href="formcity.php?City_id=<?php echo $row_rscity1['City_id']; ?>&amp;KT_back=1"><?php echo NXT_getResource("edit_one"); ?></a> <a class="KT_delete_link" href="#delete"><?php echo NXT_getResource("delete_one"); ?></a> </td>
              </tr>
              <?php } while ($row_rscity1 = mysql_fetch_assoc($rscity1)); ?>
            <?php } // Show if recordset not empty ?>
        </tbody>
      </table>
      <div class="KT_bottomnav">
        <div>
          <?php
            $nav_listcity1->Prepare();
            require("../includes/nav/NAV_Text_Navigation.inc.php");
          ?>
        </div>
      </div>
      <div class="KT_bottombuttons">
        <div class="KT_operations"> <a class="KT_edit_op_link" href="#" onclick="nxt_list_edit_link_form(this); return false;"><?php echo NXT_getResource("edit_all"); ?></a> <a class="KT_delete_op_link" href="#" onclick="nxt_list_delete_link_form(this); return false;"><?php echo NXT_getResource("delete_all"); ?></a> </div>
<span>&nbsp;</span>
        <select name="no_new" id="no_new">
          <option value="1">1</option>
          <option value="3">3</option>
          <option value="6">6</option>
        </select>
        <a class="KT_additem_op_link" href="formcity.php?KT_back=1" onclick="return nxt_list_additem(this)"><?php echo NXT_getResource("add new"); ?></a> </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($rscity1);
?>
