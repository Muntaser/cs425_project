<?php require_once('../Connections/config.php'); ?>
<?php
// Load the common classes
require_once('../includes/common/KT_common.php');

// Load the required classes
require_once('../includes/tfi/TFI.php');
require_once('../includes/tso/TSO.php');
require_once('../includes/nav/NAV.php');

// Make unified connection variable
$conn_config = new KT_connection($config, $database_config);

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

// Filter
$tfi_listactivities2 = new TFI_TableFilter($conn_config, "tfi_listactivities2");
$tfi_listactivities2->addColumn("activities.Activity_name", "STRING_TYPE", "Activity_name", "%");
$tfi_listactivities2->addColumn("activities.Description", "STRING_TYPE", "Description", "%");
$tfi_listactivities2->Execute();

// Sorter
$tso_listactivities2 = new TSO_TableSorter("rsactivities1", "tso_listactivities2");
$tso_listactivities2->addColumn("activities.Activity_name");
$tso_listactivities2->addColumn("activities.Description");
$tso_listactivities2->setDefault("activities.Activity_name");
$tso_listactivities2->Execute();

// Navigation
$nav_listactivities2 = new NAV_Regular("nav_listactivities2", "rsactivities1", "../", $_SERVER['PHP_SELF'], 10);

//NeXTenesio3 Special List Recordset
$maxRows_rsactivities1 = $_SESSION['max_rows_nav_listactivities2'];
$pageNum_rsactivities1 = 0;
if (isset($_GET['pageNum_rsactivities1'])) {
  $pageNum_rsactivities1 = $_GET['pageNum_rsactivities1'];
}
$startRow_rsactivities1 = $pageNum_rsactivities1 * $maxRows_rsactivities1;

// Defining List Recordset variable
$NXTFilter_rsactivities1 = "1=1";
if (isset($_SESSION['filter_tfi_listactivities2'])) {
  $NXTFilter_rsactivities1 = $_SESSION['filter_tfi_listactivities2'];
}
// Defining List Recordset variable
$NXTSort_rsactivities1 = "activities.Activity_name";
if (isset($_SESSION['sorter_tso_listactivities2'])) {
  $NXTSort_rsactivities1 = $_SESSION['sorter_tso_listactivities2'];
}
mysql_select_db($database_config, $config);

$query_rsactivities1 = "SELECT activities.Activity_name, activities.Description, activities.Activity_id FROM activities WHERE {$NXTFilter_rsactivities1} ORDER BY {$NXTSort_rsactivities1}";
$query_limit_rsactivities1 = sprintf("%s LIMIT %d, %d", $query_rsactivities1, $startRow_rsactivities1, $maxRows_rsactivities1);
$rsactivities1 = mysql_query($query_limit_rsactivities1, $config) or die(mysql_error());
$row_rsactivities1 = mysql_fetch_assoc($rsactivities1);

if (isset($_GET['totalRows_rsactivities1'])) {
  $totalRows_rsactivities1 = $_GET['totalRows_rsactivities1'];
} else {
  $all_rsactivities1 = mysql_query($query_rsactivities1);
  $totalRows_rsactivities1 = mysql_num_rows($all_rsactivities1);
}
$totalPages_rsactivities1 = ceil($totalRows_rsactivities1/$maxRows_rsactivities1)-1;
//End NeXTenesio3 Special List Recordset

$nav_listactivities2->checkBoundries();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="../includes/skins/mxkollection3.css" rel="stylesheet" type="text/css" media="all" />
<script src="../includes/common/js/base.js" type="text/javascript"></script>
<script src="../includes/common/js/utility.js" type="text/javascript"></script>
<script src="../includes/skins/style.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/list.js" type="text/javascript"></script>
<script src="../includes/nxt/scripts/list.js.php" type="text/javascript"></script>
<script type="text/javascript">
$NXT_LIST_SETTINGS = {
  duplicate_buttons: true,
  duplicate_navigation: true,
  row_effects: true,
  show_as_buttons: true,
  record_counter: true
}
</script>
<style type="text/css">
  /* Dynamic List row settings */
  .KT_col_Activity_name {width:140px; overflow:hidden;}
  .KT_col_Description {width:140px; overflow:hidden;}
</style>
</head>

<body>
<div class="KT_tng" id="listactivities2">
  <h1> Activities
    <?php
  $nav_listactivities2->Prepare();
  require("../includes/nav/NAV_Text_Statistics.inc.php");
?>
  </h1>
  <div class="KT_tnglist">
    <form action="<?php echo KT_escapeAttribute(KT_getFullUri()); ?>" method="post" id="form1">
      <div class="KT_options"> <a href="<?php echo $nav_listactivities2->getShowAllLink(); ?>"><?php echo NXT_getResource("Show"); ?>
        <?php 
  // Show IF Conditional region1
  if (@$_GET['show_all_nav_listactivities2'] == 1) {
?>
          <?php echo $_SESSION['default_max_rows_nav_listactivities2']; ?>
          <?php 
  // else Conditional region1
  } else { ?>
          <?php echo NXT_getResource("all"); ?>
          <?php } 
  // endif Conditional region1
?>
            <?php echo NXT_getResource("records"); ?></a> &nbsp;
        &nbsp;
                <?php 
  // Show IF Conditional region2
  if (@$_SESSION['has_filter_tfi_listactivities2'] == 1) {
?>
                  <a href="<?php echo $tfi_listactivities2->getResetFilterLink(); ?>"><?php echo NXT_getResource("Reset filter"); ?></a>
                  <?php 
  // else Conditional region2
  } else { ?>
                  <a href="<?php echo $tfi_listactivities2->getShowFilterLink(); ?>"><?php echo NXT_getResource("Show filter"); ?></a>
                  <?php } 
  // endif Conditional region2
?>
      </div>
      <table cellpadding="2" cellspacing="0" class="KT_tngtable">
        <thead>
          <tr class="KT_row_order">
            <th> <input type="checkbox" name="KT_selAll" id="KT_selAll"/>
            </th>
            <th id="Activity_name" class="KT_sorter KT_col_Activity_name <?php echo $tso_listactivities2->getSortIcon('activities.Activity_name'); ?>"> <a href="<?php echo $tso_listactivities2->getSortLink('activities.Activity_name'); ?>">Activity_name</a> </th>
            <th id="Description" class="KT_sorter KT_col_Description <?php echo $tso_listactivities2->getSortIcon('activities.Description'); ?>"> <a href="<?php echo $tso_listactivities2->getSortLink('activities.Description'); ?>">Description</a> </th>
            <th>&nbsp;</th>
          </tr>
          <?php 
  // Show IF Conditional region3
  if (@$_SESSION['has_filter_tfi_listactivities2'] == 1) {
?>
            <tr class="KT_row_filter">
              <td>&nbsp;</td>
              <td><input type="text" name="tfi_listactivities2_Activity_name" id="tfi_listactivities2_Activity_name" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listactivities2_Activity_name']); ?>" size="20" maxlength="40" /></td>
              <td><input type="text" name="tfi_listactivities2_Description" id="tfi_listactivities2_Description" value="<?php echo KT_escapeAttribute(@$_SESSION['tfi_listactivities2_Description']); ?>" size="20" maxlength="200" /></td>
              <td><input type="submit" name="tfi_listactivities2" value="<?php echo NXT_getResource("Filter"); ?>" /></td>
            </tr>
            <?php } 
  // endif Conditional region3
?>
        </thead>
        <tbody>
          <?php if ($totalRows_rsactivities1 == 0) { // Show if recordset empty ?>
            <tr>
              <td colspan="4"><?php echo NXT_getResource("The table is empty or the filter you've selected is too restrictive."); ?></td>
            </tr>
            <?php } // Show if recordset empty ?>
          <?php if ($totalRows_rsactivities1 > 0) { // Show if recordset not empty ?>
            <?php do { ?>
              <tr class="<?php echo @$cnt1++%2==0 ? "" : "KT_even"; ?>">
                <td><input type="checkbox" name="kt_pk_activities" class="id_checkbox" value="<?php echo $row_rsactivities1['Activity_id']; ?>" />
                    <input type="hidden" name="Activity_id" class="id_field" value="<?php echo $row_rsactivities1['Activity_id']; ?>" />
                </td>
                <td><div class="KT_col_Activity_name"><?php echo KT_FormatForList($row_rsactivities1['Activity_name'], 20); ?></div></td>
                <td><div class="KT_col_Description"><?php echo KT_FormatForList($row_rsactivities1['Description'], 20); ?></div></td>
                <td><a class="KT_edit_link" href="formacti.php?Activity_id=<?php echo $row_rsactivities1['Activity_id']; ?>&amp;KT_back=1"><?php echo NXT_getResource("edit_one"); ?></a> <a class="KT_delete_link" href="#delete"><?php echo NXT_getResource("delete_one"); ?></a> </td>
              </tr>
              <?php } while ($row_rsactivities1 = mysql_fetch_assoc($rsactivities1)); ?>
            <?php } // Show if recordset not empty ?>
        </tbody>
      </table>
      <div class="KT_bottomnav">
        <div>
          <?php
            $nav_listactivities2->Prepare();
            require("../includes/nav/NAV_Text_Navigation.inc.php");
          ?>
        </div>
      </div>
      <div class="KT_bottombuttons">
        <div class="KT_operations"> <a class="KT_edit_op_link" href="#" onclick="nxt_list_edit_link_form(this); return false;"><?php echo NXT_getResource("edit_all"); ?></a> <a class="KT_delete_op_link" href="#" onclick="nxt_list_delete_link_form(this); return false;"><?php echo NXT_getResource("delete_all"); ?></a> </div>
<span>&nbsp;</span>
        <select name="no_new" id="no_new">
          <option value="1">1</option>
          <option value="3">3</option>
          <option value="6">6</option>
        </select>
        <a class="KT_additem_op_link" href="formacti.php?KT_back=1" onclick="return nxt_list_additem(this)"><?php echo NXT_getResource("add new"); ?></a> </div>
    </form>
  </div>
  <br class="clearfixplain" />
</div>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($rsactivities1);
?>
