-- This inserts into the Agent table

INSERT INTO Agent VALUES(1, '7735980474', 'johndoe@gmail.com', 'John', 'Doe', DATE '1990-07-14','60616','male');
INSERT INTO Agent VALUES(2, '7736783232', 'sjobs@gmail.com', 'Steve', 'Jobs', DATE '1955-02-24','60616','male');
INSERT INTO Agent VALUES(3, '7735525521', 'bgates@gmail.com', 'Bill', 'Gates', DATE '1955-10-28','60616','male');
INSERT INTO Agent VALUES(4, '8005422425', 'lpage@gmail.com', 'Larry', 'Page', DATE '1973-03-26','60616','male');
INSERT INTO Agent VALUES(5, '2034452235', 'rbranson@gmail.com', 'Richard', 'Branson', DATE '1950-07-18','60616','male');
INSERT INTO Agent VALUES(6, '7734014144', 'mzuckerherg@gmail.com', 'Mark', 'Zuckerberg', DATE '1984-05-14','60616','male');
INSERT INTO Agent VALUES(7, '7736032452', 'tcook@gmail.com', 'Tim', 'Cook', DATE '1960-11-01','60616','male');
INSERT INTO Agent VALUES(8, '2423623334', 'lellison@gmail.com', 'Larry', 'Ellison', DATE '1944-08-17','60616','male');
INSERT INTO Agent VALUES(9, '7735223005', 'sballmer@gmail.com', 'Steve', 'Balmer', DATE '1956-03-24','60616','male');
INSERT INTO Agent VALUES(10, '7752235234', 'mmayer@gmail.com', 'Marissa', 'Mayer', DATE '1975-05-30','60616','female');


